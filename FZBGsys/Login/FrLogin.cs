﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using System.Linq;


namespace FZBGsys.NsLogin
{
    public partial class FrLoginForm : Form
    {


        /// <summary>
        /// Key for the crypto provider
        /// </summary>
        private static readonly byte[] _key = { 0xA1, 0xF1, 0xA6, 0xBB, 0xA2, 0x5A, 0x37, 0x6F, 0x81, 0x2E, 0x17, 0x41, 0x72, 0x2C, 0x43, 0x27 };
        /// <summary>
        /// Initialization vector for the crypto provider
        /// </summary>
        private static readonly byte[] _initVector = { 0xE1, 0xF1, 0xA6, 0xBB, 0xA9, 0x5B, 0x31, 0x2F, 0x81, 0x2E, 0x17, 0x4C, 0xA2, 0x81, 0x53, 0x61 };

        public FrMain MainForm { get; set; }
        public FrLoginForm()
        {
            InitializeComponent();
        }

        public FrLoginForm(FrMain main)
        {
            InitializeComponent();
            this.MainForm = main;
            //   db = new ModelEntities();
        }


#if (DEBUG) //Only compile this method for local debugging.
        /// <summary>
        /// Decrypt a string
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        private static string Decrypt(string Value)
        {

            SymmetricAlgorithm mCSP;
            ICryptoTransform ct = null;
            MemoryStream ms = null;
            CryptoStream cs = null;
            byte[] byt;
            byte[] _result;

            mCSP = new RijndaelManaged();

            try
            {
                mCSP.Key = _key;
                mCSP.IV = _initVector;
                ct = mCSP.CreateDecryptor(mCSP.Key, mCSP.IV);


                byt = Convert.FromBase64String(Value);

                ms = new MemoryStream();
                cs = new CryptoStream(ms, ct, CryptoStreamMode.Write);
                cs.Write(byt, 0, byt.Length);
                cs.FlushFinalBlock();

                cs.Close();
                _result = ms.ToArray();
            }
            catch
            {
                _result = null;
            }
            finally
            {
                if (ct != null)
                    ct.Dispose();
                if (ms != null)
                    ms.Dispose();
                if (cs != null)
                    cs.Dispose();
            }

            //   return ASCIIEncoding.UTF8.GetString(_result);

            return Value;
        }
#endif

        /// <summary>
        /// Encrypt a string
        /// </summary>
        /// <param name="Password"></param>
        /// <returns></returns>
        private static string Encrypt(string Password)
        {
            if (string.IsNullOrEmpty(Password))
                return string.Empty;

            byte[] Value = Encoding.UTF8.GetBytes(Password);
            SymmetricAlgorithm mCSP = new RijndaelManaged();
            mCSP.Key = _key;
            mCSP.IV = _initVector;
            using (ICryptoTransform ct = mCSP.CreateEncryptor(mCSP.Key, mCSP.IV))
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, ct, CryptoStreamMode.Write))
                    {
                        cs.Write(Value, 0, Value.Length);
                        cs.FlushFinalBlock();
                        cs.Close();
                        // return Convert.ToBase64String(ms.ToArray());
                        return Password;
                    }
                }
            }
        }

        /// <summary>
        /// Looks up the users password crypto string in the database
        /// </summary>
        /// <param name="Username"></param>
        /// <returns></returns>


        /// <summary>
        /// Obviously the .Focus() code doesn't apply to ASP.NET
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonLogin_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtUsername.Text))
            {
                //Focus box before showing a message
                txtUsername.Focus();
                this.ShowInformation("Entrez le Nom d'utilisateur!");
                //  MessageBox.Show("Entrez le Nom d'utilisateur!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //Focus again afterwards, sometimes people double click message boxes and select another control accidentally
                txtUsername.Focus();
                return;
            }
            else if (!chInt.Checked && string.IsNullOrEmpty(txtPassword.Text))
            {
                txtPassword.Focus();
                this.ShowInformation("Entrez le Mot de passe!");
                // MessageBox.Show("Entrez le Mot de passe!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtPassword.Focus();
                return;
            }

            //OK they enter a user and pass, lets see if they can authenticate
            ApplicationUtilisateur user = null;
            using (var db = new ModelEntities())
            {


                if (!chInt.Checked)
                {
                    user = db.ApplicationUtilisateurs.SingleOrDefault(u => u.EmployerMatricule == txtUsername.Text.Trim());
                }
                else
                {
                    user = db.ApplicationUtilisateurs.SingleOrDefault(u => u.WindowsUserName == txtUsername.Text.Trim());

                }
            }
            if (user == null)
            {
                txtUsername.Focus();
                this.ShowError("Utilisateur Invalide!");
                // MessageBox.Show("Utilisateur Invalide.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtUsername.Focus();
                return;
            }
            else
            {
                //Always compare the resulting crypto string or hash value, never the decrypted value
                //By doing that you never make a call to Decrypt() and the application is harder to
                //reverse engineer. I included the Decrypt() method here for informational purposes
                //only. I do not recommend shipping an assembly with Decrypt() methods.

                string dbPassword = Convert.ToString(user.Password);
                string appPassword = Encrypt(txtPassword.Text); //we store the password as encrypted in the DB
                if (chInt.Checked || string.Compare(dbPassword, appPassword) == 0)
                {

                    if (!user.Enabled)
                    {
                        this.ShowInformation("Ce Compte est désactivé,\n Veillez contacter un Administrateur.");
                        return;
                    }
                    else
                    {

                        MainForm.UserID = user.ID;
                        this.Dispose();
                    }
                }
                else
                {
                    //You may want to use the same error message so they can't tell which field they got wrong
                    txtPassword.Focus();
                    this.ShowError("Mot de Passe Invalide!");
                    //  MessageBox.Show("Mot de Passe Invalide!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    txtPassword.Focus();
                    return;
                }
            }

        }



        private void LoginForm_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void LoginForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //   db.Dispose();
        }

        private void chInt_CheckedChanged(object sender, EventArgs e)
        {
            if (chInt.Checked)
            {

                txtUsername.Text = Environment.UserName;
                txtPassword.Text = "";

                txtPassword.Visible = false;
                txtUsername.ReadOnly = true;
                label1.Text = "Utilisateur:";
            }
            else
            {
                txtPassword.Visible = true;
                txtUsername.ReadOnly = false;
                txtUsername.Text = "";
                label1.Text = "Matricule:";
            }
        }
    }
}