﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsStock
{
    public partial class FrListArrivage : Form
    {
        ModelEntities db = new ModelEntities();
        public FrListArrivage()
        {
            InitializeComponent();
            InitializeControls();
        }

        private void InitializeControls()
        {
            //----------------------- Fournisseur --------------------------------------------------
            cbFournisseur.Items.Add(new Fournisseur { ID = 0, Nom = "[Tout]" });
            cbFournisseur.Items.AddRange(db.Fournisseurs.Where(p => p.ID > 0).ToArray());
            cbFournisseur.DisplayMember = "Nom";
            cbFournisseur.ValueMember = "ID";
            cbFournisseur.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFournisseur.SelectedIndex = 0;


            //---------------------- Categorie (type Ressource) -------------------------------------
            cbCategorie.Items.Add(new TypeRessource { ID = 0, Nom = "[Tout]" });
            cbCategorie.Items.AddRange(db.TypeRessources.Where(p => p.ID > 0).ToArray());
            cbCategorie.DisplayMember = "Nom";
            cbCategorie.ValueMember = "ID";
            cbCategorie.DropDownStyle = ComboBoxStyle.DropDownList;
            cbCategorie.SelectedIndex = 0;


            //----------------------------------------------------------------------------------------
        }



        private void FrListArrivage_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        private void btRecherche_Click(object sender, EventArgs e)
        {
            string filterCat = "", filterFour = "", filterDate = "";
            ListFound = null;
            IQueryable<Arrivage> result = db.Arrivages;

            if (chDate.Checked)
            {
                result = result.Where(p => p.DateArrivage <= dtAu.Value.Date && p.DateArrivage >= dtDu.Value.Date);
                filterDate = (dtDu.Value.Date == dtAu.Value.Date) ?
                    "date du " + dtDu.Value.ToShortDateString() : "période du " + dtDu.Value.Date.ToShortDateString() + " au " + dtAu.Value.ToShortDateString();
            }

            if (cbCategorie.SelectedIndex != 0)
            {
                //var idCat = int.Parse(cbCategorie.SelectedValue.ToString());
                var selected = (TypeRessource)cbCategorie.SelectedItem;
                result = result.Where(p => p.Ressource.TypeRessourceID == selected.ID);
                filterCat = selected.Nom;

            }


            if (cbFournisseur.SelectedIndex != 0)
            {
                //var idCat = int.Parse(cbCategorie.SelectedValue.ToString());
                var selected = (Fournisseur)cbFournisseur.SelectedItem;
                filterFour = "fournisseur " + selected.Nom;
                result = result.Where(p => p.Ressource.FournisseurID == selected.ID);

            }

            this.ListFound = result.ToList();

            RefreshGrid();
            labFilterTxt.Text = filterDate + " " + filterCat + " " + filterFour;

        }

        private void RefreshGrid()
        {
            dgvPET.DataSource = ListFound.Select(p => new
            {
                ID = p.ID,
                Date = p.DateArrivage.Value.ToShortDateString(),
                Fournisseur = p.Ressource.Fournisseur.Nom,
                Descption = p.Ressource.ToDescription(),
                Nombre = p.Piece + " " + p.Ressource.TypeRessource.UniteDePiece + ((p.Piece > 1) ? "s" : ""),
                Quantite = p.Quantite.ToAffInt() + " " + p.Ressource.TypeRessource.UniteDeMesure
            }
              ).ToList();

            dgvPET.Columns["ID"].Visible = false;
        }

        public List<Arrivage> ListFound { get; set; }

        private void btDelete_Click(object sender, EventArgs e)
        {
            var id = dgvPET.GetSelectedID();

            var toDel = db.Arrivages.SingleOrDefault(p => p.ID == id);
            if (toDel != null)
            {
                if (this.ConfirmWarning("Confirmer suppression élement?"))
                {
                    try
                    {
                        FZBGsys.NsStock.FrStockMatiere.RemoveFromStockMT(toDel, db);
                        //   db.DeleteObject(toDel.Ressource);
                        db.DeleteObject(toDel);
                        db.SaveChanges();

                    }
                    catch (Exception exp)
                    {

                        this.ShowError(exp.AllMessages("Impossible de supprimer un element!"));
                    }
                }
            }
            else
            {
                this.ShowError("element introuvable!");
            }



            var index = dgvPET.SelectedRows[0].Index;
            ListFound.RemoveAt(index);
            RefreshGrid();

        }

        private void btPrint_Click(object sender, EventArgs e)
        {
            if (ListFound == null || ListFound.Count == 0)
            {
                this.ShowInformation("Il n'y a rien à imprimer!");
                return;
            }
            Tools.ClearPrintTemp(); // ------------------------------------------- must clear printTemp before

            foreach (var arrivage in ListFound)
            {
                db.AddToPrintTemps(new PrintTemp()
                {
                    Date = arrivage.DateArrivage,
                    Description = arrivage.Ressource.ToDescription(),
                    Nom = arrivage.Ressource.Fournisseur.Nom,
                    Piece = arrivage.Piece,
                    Quantite = arrivage.Quantite,
                    UID = Tools.CurrentUserID,
                    val1 = arrivage.Ressource.TypeRessource.UniteDeMesure,
                    val2 = arrivage.Ressource.TypeRessource.UniteDePiece

                });
            }

            db.SaveChanges();

            FZBGsys.NsRapports.FrViewer.PrintListArrivage(labFilterTxt.Text);
        }

        private void chDate_CheckedChanged(object sender, EventArgs e)
        {
            panDate.Visible = chDate.Checked;
        }

        private void btFermer_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
