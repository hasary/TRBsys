﻿namespace FZBGsys.NsStock
{
    partial class FrListTransfertMatiere
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvPET = new System.Windows.Forms.DataGridView();
            this.btPrint = new System.Windows.Forms.Button();
            this.btFermer = new System.Windows.Forms.Button();
            this.btDelete = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chAutre = new System.Windows.Forms.CheckBox();
            this.chCanette = new System.Windows.Forms.CheckBox();
            this.chPET = new System.Windows.Forms.CheckBox();
            this.panDate = new System.Windows.Forms.Panel();
            this.dtAu = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.dtDu = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.btRecherche = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cbCategorie = new System.Windows.Forms.ComboBox();
            this.chDate = new System.Windows.Forms.CheckBox();
            this.labFilterTxt = new System.Windows.Forms.Label();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tpPET = new System.Windows.Forms.TabPage();
            this.tpCannette = new System.Windows.Forms.TabPage();
            this.dgvCanette = new System.Windows.Forms.DataGridView();
            this.tpAutre = new System.Windows.Forms.TabPage();
            this.dgvAutre = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.labTotal = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPET)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panDate.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tpPET.SuspendLayout();
            this.tpCannette.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCanette)).BeginInit();
            this.tpAutre.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAutre)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvPET
            // 
            this.dgvPET.AllowUserToAddRows = false;
            this.dgvPET.AllowUserToDeleteRows = false;
            this.dgvPET.AllowUserToResizeColumns = false;
            this.dgvPET.AllowUserToResizeRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvPET.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvPET.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvPET.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvPET.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvPET.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPET.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPET.Location = new System.Drawing.Point(3, 2);
            this.dgvPET.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvPET.MultiSelect = false;
            this.dgvPET.Name = "dgvPET";
            this.dgvPET.ReadOnly = true;
            this.dgvPET.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvPET.RowHeadersVisible = false;
            this.dgvPET.RowTemplate.Height = 16;
            this.dgvPET.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPET.Size = new System.Drawing.Size(781, 411);
            this.dgvPET.TabIndex = 7;
            // 
            // btPrint
            // 
            this.btPrint.Location = new System.Drawing.Point(816, 495);
            this.btPrint.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btPrint.Name = "btPrint";
            this.btPrint.Size = new System.Drawing.Size(152, 34);
            this.btPrint.TabIndex = 8;
            this.btPrint.Text = "Imprimer La liste";
            this.btPrint.UseVisualStyleBackColor = true;
            this.btPrint.Click += new System.EventHandler(this.btPrint_Click);
            // 
            // btFermer
            // 
            this.btFermer.Location = new System.Drawing.Point(973, 495);
            this.btFermer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btFermer.Name = "btFermer";
            this.btFermer.Size = new System.Drawing.Size(112, 34);
            this.btFermer.TabIndex = 9;
            this.btFermer.Text = "Fermer";
            this.btFermer.UseVisualStyleBackColor = true;
            this.btFermer.Click += new System.EventHandler(this.btFermer_Click);
            // 
            // btDelete
            // 
            this.btDelete.Location = new System.Drawing.Point(291, 495);
            this.btDelete.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btDelete.Name = "btDelete";
            this.btDelete.Size = new System.Drawing.Size(173, 34);
            this.btDelete.TabIndex = 8;
            this.btDelete.Text = "Supprimer Selection";
            this.btDelete.UseVisualStyleBackColor = true;
            this.btDelete.Click += new System.EventHandler(this.btDelete_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.panDate);
            this.groupBox1.Controls.Add(this.btRecherche);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cbCategorie);
            this.groupBox1.Controls.Add(this.chDate);
            this.groupBox1.Location = new System.Drawing.Point(8, 10);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(272, 519);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Recherche";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chAutre);
            this.groupBox2.Controls.Add(this.chCanette);
            this.groupBox2.Controls.Add(this.chPET);
            this.groupBox2.Location = new System.Drawing.Point(16, 265);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(239, 118);
            this.groupBox2.TabIndex = 22;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Destinations";
            // 
            // chAutre
            // 
            this.chAutre.AutoSize = true;
            this.chAutre.Checked = true;
            this.chAutre.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chAutre.Location = new System.Drawing.Point(96, 80);
            this.chAutre.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chAutre.Name = "chAutre";
            this.chAutre.Size = new System.Drawing.Size(95, 26);
            this.chAutre.TabIndex = 0;
            this.chAutre.Text = "Autres";
            this.chAutre.UseVisualStyleBackColor = true;
            // 
            // chCanette
            // 
            this.chCanette.AutoSize = true;
            this.chCanette.Checked = true;
            this.chCanette.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chCanette.Location = new System.Drawing.Point(96, 53);
            this.chCanette.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chCanette.Name = "chCanette";
            this.chCanette.Size = new System.Drawing.Size(155, 26);
            this.chCanette.TabIndex = 0;
            this.chCanette.Text = "Unité Canette";
            this.chCanette.UseVisualStyleBackColor = true;
            // 
            // chPET
            // 
            this.chPET.AutoSize = true;
            this.chPET.Checked = true;
            this.chPET.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chPET.Location = new System.Drawing.Point(96, 26);
            this.chPET.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chPET.Name = "chPET";
            this.chPET.Size = new System.Drawing.Size(125, 26);
            this.chPET.TabIndex = 0;
            this.chPET.Text = "Unité PET";
            this.chPET.UseVisualStyleBackColor = true;
            // 
            // panDate
            // 
            this.panDate.Controls.Add(this.dtAu);
            this.panDate.Controls.Add(this.label5);
            this.panDate.Controls.Add(this.dtDu);
            this.panDate.Controls.Add(this.label4);
            this.panDate.Location = new System.Drawing.Point(28, 79);
            this.panDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panDate.Name = "panDate";
            this.panDate.Size = new System.Drawing.Size(213, 70);
            this.panDate.TabIndex = 21;
            this.panDate.Visible = false;
            // 
            // dtAu
            // 
            this.dtAu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtAu.Location = new System.Drawing.Point(63, 38);
            this.dtAu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtAu.Name = "dtAu";
            this.dtAu.Size = new System.Drawing.Size(139, 22);
            this.dtAu.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 17);
            this.label5.TabIndex = 20;
            this.label5.Text = "Au:";
            // 
            // dtDu
            // 
            this.dtDu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDu.Location = new System.Drawing.Point(63, 10);
            this.dtDu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDu.Name = "dtDu";
            this.dtDu.Size = new System.Drawing.Size(139, 22);
            this.dtDu.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 17);
            this.label4.TabIndex = 18;
            this.label4.Text = "Du:";
            // 
            // btRecherche
            // 
            this.btRecherche.Location = new System.Drawing.Point(0, 479);
            this.btRecherche.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btRecherche.Name = "btRecherche";
            this.btRecherche.Size = new System.Drawing.Size(147, 34);
            this.btRecherche.TabIndex = 8;
            this.btRecherche.Text = "Recherche";
            this.btRecherche.UseVisualStyleBackColor = true;
            this.btRecherche.Click += new System.EventHandler(this.btRecherche_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 183);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 17);
            this.label2.TabIndex = 15;
            this.label2.Text = "Categorie: ";
            // 
            // cbCategorie
            // 
            this.cbCategorie.FormattingEnabled = true;
            this.cbCategorie.Location = new System.Drawing.Point(28, 203);
            this.cbCategorie.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbCategorie.Name = "cbCategorie";
            this.cbCategorie.Size = new System.Drawing.Size(215, 24);
            this.cbCategorie.TabIndex = 14;
            // 
            // chDate
            // 
            this.chDate.AutoSize = true;
            this.chDate.Location = new System.Drawing.Point(16, 52);
            this.chDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chDate.Name = "chDate";
            this.chDate.Size = new System.Drawing.Size(126, 21);
            this.chDate.TabIndex = 0;
            this.chDate.Text = "Date Transfert:";
            this.chDate.UseVisualStyleBackColor = true;
            this.chDate.CheckedChanged += new System.EventHandler(this.chDate_CheckedChanged);
            // 
            // labFilterTxt
            // 
            this.labFilterTxt.AutoSize = true;
            this.labFilterTxt.Location = new System.Drawing.Point(288, 22);
            this.labFilterTxt.Name = "labFilterTxt";
            this.labFilterTxt.Size = new System.Drawing.Size(0, 17);
            this.labFilterTxt.TabIndex = 11;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tpPET);
            this.tabControl.Controls.Add(this.tpCannette);
            this.tabControl.Controls.Add(this.tpAutre);
            this.tabControl.Location = new System.Drawing.Point(291, 46);
            this.tabControl.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(795, 444);
            this.tabControl.TabIndex = 12;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // tpPET
            // 
            this.tpPET.Controls.Add(this.dgvPET);
            this.tpPET.Location = new System.Drawing.Point(4, 25);
            this.tpPET.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tpPET.Name = "tpPET";
            this.tpPET.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tpPET.Size = new System.Drawing.Size(787, 415);
            this.tpPET.TabIndex = 0;
            this.tpPET.Text = "Vers Unité PET";
            this.tpPET.UseVisualStyleBackColor = true;
            // 
            // tpCannette
            // 
            this.tpCannette.Controls.Add(this.dgvCanette);
            this.tpCannette.Location = new System.Drawing.Point(4, 25);
            this.tpCannette.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tpCannette.Name = "tpCannette";
            this.tpCannette.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tpCannette.Size = new System.Drawing.Size(787, 415);
            this.tpCannette.TabIndex = 1;
            this.tpCannette.Text = "Vers Unité Canette";
            this.tpCannette.UseVisualStyleBackColor = true;
            // 
            // dgvCanette
            // 
            this.dgvCanette.AllowUserToAddRows = false;
            this.dgvCanette.AllowUserToDeleteRows = false;
            this.dgvCanette.AllowUserToResizeColumns = false;
            this.dgvCanette.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvCanette.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvCanette.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvCanette.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvCanette.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvCanette.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCanette.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCanette.Location = new System.Drawing.Point(3, 2);
            this.dgvCanette.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvCanette.MultiSelect = false;
            this.dgvCanette.Name = "dgvCanette";
            this.dgvCanette.ReadOnly = true;
            this.dgvCanette.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvCanette.RowHeadersVisible = false;
            this.dgvCanette.RowTemplate.Height = 16;
            this.dgvCanette.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCanette.Size = new System.Drawing.Size(781, 409);
            this.dgvCanette.TabIndex = 8;
            // 
            // tpAutre
            // 
            this.tpAutre.Controls.Add(this.dgvAutre);
            this.tpAutre.Location = new System.Drawing.Point(4, 25);
            this.tpAutre.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tpAutre.Name = "tpAutre";
            this.tpAutre.Size = new System.Drawing.Size(787, 415);
            this.tpAutre.TabIndex = 2;
            this.tpAutre.Text = "Autres Destinations";
            this.tpAutre.UseVisualStyleBackColor = true;
            // 
            // dgvAutre
            // 
            this.dgvAutre.AllowUserToAddRows = false;
            this.dgvAutre.AllowUserToDeleteRows = false;
            this.dgvAutre.AllowUserToResizeColumns = false;
            this.dgvAutre.AllowUserToResizeRows = false;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvAutre.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvAutre.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvAutre.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvAutre.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvAutre.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAutre.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAutre.Location = new System.Drawing.Point(0, 0);
            this.dgvAutre.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvAutre.MultiSelect = false;
            this.dgvAutre.Name = "dgvAutre";
            this.dgvAutre.ReadOnly = true;
            this.dgvAutre.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvAutre.RowHeadersVisible = false;
            this.dgvAutre.RowTemplate.Height = 16;
            this.dgvAutre.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAutre.Size = new System.Drawing.Size(787, 415);
            this.dgvAutre.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(495, 505);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 17);
            this.label1.TabIndex = 13;
            this.label1.Text = "Total:";
            // 
            // labTotal
            // 
            this.labTotal.AutoSize = true;
            this.labTotal.Location = new System.Drawing.Point(551, 505);
            this.labTotal.Name = "labTotal";
            this.labTotal.Size = new System.Drawing.Size(28, 17);
            this.labTotal.TabIndex = 14;
            this.labTotal.Text = "     ";
            // 
            // FrListTransfertMatiere
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1099, 542);
            this.Controls.Add(this.labTotal);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.labFilterTxt);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btFermer);
            this.Controls.Add(this.btDelete);
            this.Controls.Add(this.btPrint);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrListTransfertMatiere";
            this.Text = "Historique Transfert Matiere Premiere";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrListArrivage_FormClosing);
            this.Load += new System.EventHandler(this.FrListTransfertMatiere_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPET)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panDate.ResumeLayout(false);
            this.panDate.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.tpPET.ResumeLayout(false);
            this.tpCannette.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCanette)).EndInit();
            this.tpAutre.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAutre)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvPET;
        private System.Windows.Forms.Button btPrint;
        private System.Windows.Forms.Button btFermer;
        private System.Windows.Forms.Button btDelete;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chDate;
        private System.Windows.Forms.Panel panDate;
        private System.Windows.Forms.DateTimePicker dtAu;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtDu;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbCategorie;
        private System.Windows.Forms.Button btRecherche;
        private System.Windows.Forms.Label labFilterTxt;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tpPET;
        private System.Windows.Forms.TabPage tpCannette;
        private System.Windows.Forms.DataGridView dgvCanette;
        private System.Windows.Forms.TabPage tpAutre;
        private System.Windows.Forms.DataGridView dgvAutre;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chAutre;
        private System.Windows.Forms.CheckBox chCanette;
        private System.Windows.Forms.CheckBox chPET;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labTotal;
    }
}