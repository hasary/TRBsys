﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FZBGsys;
namespace FZBGsys.NsStock
{
    public partial class FrTransfertMatiere : Form
    {
        ModelEntities db = new ModelEntities();
        public FrTransfertMatiere(EnSection Destination)
        {
            InitializeComponent();
            InitialiseControls();
            this.Destination = Destination;
            ListToSave = new List<TransfertMatiere>();
        }

        private void InitialiseControls()
        {
            //---------------------- Fournisseur
            cbFournisseur.Items.Add(new Fournisseur { ID = 0, Nom = "" });
            // cbFournisseur.Items.AddRange(db.Fournisseurs.Where(p => p.ID > 0 ).ToArray());
            cbFournisseur.DisplayMember = "Nom";
            cbFournisseur.ValueMember = "ID";
            cbFournisseur.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFournisseur.SelectedIndex = 0;

            panObs.Visible = Destination == EnSection.Autre;

            UnLoadMatiere();
        }


        private void btOuvrirStock_Click(object sender, EventArgs e)
        {
            var id = FZBGsys.NsStock.FrStockMatiere.SelectIDStockMTDialog(ListToSave, Destination, db);
            if (id != null)
            {
                SelectedStockMT = db.StockMatieres.SingleOrDefault(p => p.ID == id.Value);
                if (SelectedStockMT == null)
                {
                    this.ShowError("Impossible de trouver matiere selectionnée!");
                    UnLoadMatiere();
                    return;
                }
                /*else if (ListToSave.Select(p=>p.StockMatiereID).ToList().Contains(SelectedStockMT.ID))
                {
                    var qttEnstock = ListToSave.Where(p => p.StockMatiereID == SelectedStockMT.ID).Sum(p=>p.QunatiteUM);

                    if (qttEnstock   >= SelectedStockMT.Quantite   )
                    {
                        this.ShowError("Quantie en stock insuffisante (déja saisie)");
                        UnLoadMatiere();
                        return;
                    }
                }*/
                LoadMatiere();
            }



        }

        private void LoadMatiere()
        {
            UnLoadMatiere();
            gbMatiere.Enabled = true;
            var ressource = SelectedStockMT.Ressource;

            txtMatiere.Text = ressource.ToDescription();
            cbFournisseur.Items.Clear();
            cbFournisseur.Items.Add(new Fournisseur { ID = 0, Nom = "" });
            cbFournisseur.Items.AddRange(db.Fournisseurs.ToList().Where(p => p.TypeRessources.Contains(ressource.TypeRessource)).ToArray());

            cbFournisseur.SelectedItem = ressource.Fournisseur;

            txtQttEnStock.Text = SelectedStockMT.Quantite.ToAffInt();
            txtPiceEnStock.Text = SelectedStockMT.Piece.ToString();

            labUniteDeMesure.Text = labUniteDeMesure2.Text = ressource.TypeRessource.UniteDeMesure;
            labUniteDePiece.Text = labUniteDePiece2.Text = ressource.TypeRessource.UniteDePiece;


          //  txtPiece.Visible = ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece;
            txtPiceEnStock.Enabled = ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece;
        }

        private void UnLoadMatiere()
        {
            txtMatiere.Text = "";
            cbFournisseur.Items.Clear();
            cbFournisseur.Items.Add(new Fournisseur { ID = 0, Nom = "" });


            txtQttEnStock.Text = "";
            txtPiceEnStock.Text = "";

            labUniteDeMesure.Text = labUniteDeMesure2.Text = "";
            labUniteDePiece.Text = labUniteDePiece2.Text = "";
            gbMatiere.Enabled = false;
        }

        internal EnSection Destination { get; set; }

        private void FrTransfert_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        public StockMatiere SelectedStockMT { get; set; }

        private void chDateArrivage_CheckedChanged(object sender, EventArgs e)
        {
            dtDateArrivage.Visible = chDateArrivage.Checked;
        }

        private void chDateFabrication_CheckedChanged(object sender, EventArgs e)
        {
            dtDateFabrication.Visible = chDateFabrication.Checked;
        }

        private void btAjouter_Click(object sender, EventArgs e)
        {
            if (!isValidAll())
            {
                return;
            }


            var newTransfert = new TransfertMatiere();
            if (panObs.Visible)
            {
                newTransfert.Observation = txtObservation.Text.Trim();
            }

            newTransfert.DestinationSectionID = (int)Destination;
            newTransfert.Date = dtDate.Value.Date;
            if (chDateArrivage.Checked) newTransfert.DateArrivage = dtDateArrivage.Value.Date;
            if (chDateFabrication.Checked) newTransfert.DateFabrication = dtDateFabrication.Value.Date;
            newTransfert.Ressource = SelectedStockMT.Ressource;
            newTransfert.Fournisseur = (Fournisseur)cbFournisseur.SelectedItem;
            newTransfert.QunatiteUM = txtQuantite.Text.ParseToDec();
            if (SelectedStockMT.Ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece) newTransfert.QuantitePiece = txtPiece.Text.ParseToInt();

            //newTransfert.StockMatiere = SelectedStockMT;

            FZBGsys.NsStock.FrStockMatiere.RemoveFromStockMT(newTransfert, db);
            ListToSave.Add(newTransfert);
            db.AddToTransfertMatieres(newTransfert);

            RefreshGrid();
            UpdateControls();

        }

        private void UpdateControls()
        {
            UnLoadMatiere();
            txtQuantite.Text = "";
            txtPiece.Text = "";
        }

        private void RefreshGrid()
        {
            dgv.DataSource = ListToSave.Select(p => new
            {
                ID = p.ID,
                Date = p.Date.Value.ToShortDateString(),
                Matiere = p.Ressource.ToDescription(),
                Fournisseur = p.Fournisseur.Nom,
                Fabrication = (p.DateFabrication != null) ? p.DateFabrication.Value.ToShortDateString() : "",
                Quantie = (p.QuantitePiece != null) ? p.QuantitePiece + " " + p.Ressource.TypeRessource.UniteDePiece : "/",
                Total = p.QunatiteUM + " " + p.Ressource.TypeRessource.UniteDeMesure,


            }).ToList();

            dgv.HideIDColumn();
        }
        public List<TransfertMatiere> ListToSave { get; set; }
        private bool isValidAll()
        {
            var message = "";

            if (SelectedStockMT == null)
            {
                message = "Selectionner matiere première avec boutton \"Ouvrir le stock\"";

            }
            else
            {
                if (txtQuantite.Text.ParseToDec() == null)
                {
                    message += "\nQuantite invalide!";
                }
                else if (txtQuantite.Text.ParseToDec() > txtQttEnStock.Text.ParseToDec())
                {
                    message += "\nQuantite Insufissante !!";
                }

                if (SelectedStockMT.Ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece)
                {

                    if (txtPiece.Text.ParseToInt() == null)
                    {
                        message += "\nQuantite invalide!";
                    }
                    else if (txtPiece.Text.ParseToInt() > txtPiceEnStock.Text.ParseToInt())
                    {
                        message += "\nQuantite Insufissante !!";
                    }
                }
            }

            if (message == "")
            {
                return true;
            }


            this.ShowWarning(message);
            return false;
        }

        private void btAnnuler_Click(object sender, EventArgs e)
        {
            if (ListToSave == null || ListToSave.Count == 0) Dispose();
            else
                if (this.ConfirmWarning("Annuler tout et fermer ?\n(ATTENTION: rien ne sera enregistré!)"))
                {
                    Dispose();
                }
        }

        private void btEnregistrer_Click(object sender, EventArgs e)
        {
            if (ListToSave == null || ListToSave.Count == 0)
            {
                this.ShowInformation("La liste est vide, remplire d'abord avec \"Ajouter\" ");
                return;
            }


            //      FZBGsys.NsStock.FrStockMatiere.RemoveRangeFromStockMT(ListToSave, db);

            try
            {
                db.SaveChanges();
                Dispose();
            }
            catch (Exception exp)
            {

                this.ShowError(exp.AllMessages("Impossible d'enregistrer:"));
            }

        }

        private void btEnlever_Click(object sender, EventArgs e)
        {
            var index = dgv.SelectedRows[0].Index;
            var toDel = ListToSave.ElementAt(index);
            ListToSave.Remove(toDel);

            var toDelMatiere = toDel.StockMatiere;
            FZBGsys.NsStock.FrStockMatiere.AddToStockMT(toDelMatiere, db);
            db.DeleteObject(toDelMatiere);

            db.DeleteObject(toDel);
            RefreshGrid();

        }

        private void txtPiece_TextChanged(object sender, EventArgs e)
        {
            if (txtPiece.Text.ParseToInt() != null && SelectedStockMT.Ressource.TypeRessource.QuantiteParPieceFix == true)
            {
                txtQuantite.Text = (txtPiece.Text.ParseToInt() * SelectedStockMT.Ressource.TypeRessource.QuantiteParPiece).ToString();
            }
        }

        private void txtObservation_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLower(e.KeyChar))
            {
                e.KeyChar = char.ToUpper(e.KeyChar);
            }
        }
    }
}
