﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsStock
{
    public partial class FrListTransfertMatiere : Form
    {
        ModelEntities db = new ModelEntities();
        public FrListTransfertMatiere()
        {
            InitializeComponent();
            InitializeControls();
        }

        private void InitializeControls()
        {
            //----------------------- Fournisseur --------------------------------------------------
            /*   cbFournisseur.Items.Add(new Fournisseur { ID = 0, Nom = "[Tout]" });
               cbFournisseur.Items.AddRange(db.Fournisseurs.Where(p => p.ID > 0).ToArray());
               cbFournisseur.DisplayMember = "Nom";
               cbFournisseur.ValueMember = "ID";
               cbFournisseur.DropDownStyle = ComboBoxStyle.DropDownList;
               cbFournisseur.SelectedIndex = 0;
               */

            //---------------------- Categorie (type Ressource) -------------------------------------
            cbCategorie.Items.Add(new TypeRessource { ID = 0, Nom = "[Tout]" });
            cbCategorie.Items.AddRange(db.TypeRessources.Where(p => p.ID > 0).ToArray());
            cbCategorie.DisplayMember = "Nom";
            cbCategorie.ValueMember = "ID";
            cbCategorie.DropDownStyle = ComboBoxStyle.DropDownList;
            cbCategorie.SelectedIndex = 0;


            //----------------------------------------------------------------------------------------
        }



        private void FrListArrivage_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        private void btRecherche_Click(object sender, EventArgs e)
        {
            Recherche();

        }

        private void Recherche()
        {

            string filterCat = "", filterFour = "", filterDate = "";
            ListFound = null;
            IQueryable<TransfertMatiere> result = db.TransfertMatieres;

            if (chDate.Checked)
            {
                result = result.Where(p => p.Date <= dtAu.Value.Date && p.Date >= dtDu.Value.Date);
                filterDate = (dtDu.Value.Date == dtAu.Value.Date) ?
                    "journée du " + dtDu.Value.ToShortDateString() : "période du " + dtDu.Value.Date.ToShortDateString() + " au " + dtAu.Value.ToShortDateString();
            }

            if (cbCategorie.SelectedIndex != 0)
            {
                //var idCat = int.Parse(cbCategorie.SelectedValue.ToString());
                var selected = (TypeRessource)cbCategorie.SelectedItem;
                result = result.Where(p => p.Ressource.TypeRessourceID == selected.ID);
                filterCat = selected.Nom;

            }

            if (!chPET.Checked)
            {
                result = result.Where(p => p.DestinationSectionID != (int)EnSection.Unité_PET);
            }

            if (!chCanette.Checked)
            {
                result = result.Where(p => p.DestinationSectionID != (int)EnSection.Unité_Canette);
            }

            if (!chAutre.Checked)
            {
                result = result.Where(p => p.DestinationSectionID != (int)EnSection.Autre);
            }

            /* if (cbFournisseur.SelectedIndex != 0)
             {
                 //var idCat = int.Parse(cbCategorie.SelectedValue.ToString());
                 var selected = (Fournisseur)cbFournisseur.SelectedItem;
                 filterFour = "Fournisseur " + selected.Nom;
                 result = result.Where(p => p.Ressource.FournisseurID == selected.ID );

             }*/

            this.ListFound = result.ToList();

            RefreshGrid();
            UpdateControls();
            UpdateTotal();
            labFilterTxt.Text = filterDate + " " + filterCat + " " + filterFour;

        }

        private void UpdateTotal()
        {
            if (cbCategorie.SelectedIndex == 0)
            {
                labTotal.Text = "";
                return;
            }
            IEnumerable<TransfertMatiere> ListTemp = null;
            if (tabControl.SelectedTab == tpPET)
            {
                ListTemp = ListFound.Where(p => p.DestinationSectionID == (int)EnSection.Unité_PET);
            }
            else if (tabControl.SelectedTab == tpCannette)
            {
                ListTemp = ListFound.Where(p => p.DestinationSectionID == (int)EnSection.Unité_Canette);
            }
            else if (tabControl.SelectedTab == tpCannette)
            {
                ListTemp = ListFound.Where(p => p.DestinationSectionID == (int)EnSection.Autre);
            }
            else
            {
                labTotal.Text = "";
                return;
            }


            var ressource = (TypeRessource)cbCategorie.SelectedItem;

            labTotal.Text += ListTemp.Sum(p => p.QunatiteUM) + " " + ressource.UniteDeMesure;

            if (ressource.DefaultUnite == (int)EnUniteParDefault.Piece)
            {
                labTotal.Text = ListTemp.Sum(p => p.QuantitePiece) + " " + ressource.UniteDePiece + " " + labTotal.Text;
            }

        }

        private void UpdateControls()
        {
            return; //TODO: ne marche pas
            tabControl.TabPages.Clear();
            if (dgvPET.Rows.Count != 0) tabControl.TabPages.Add(tpPET);
            if (dgvCanette.Rows.Count != 0) tabControl.TabPages.Add(tpCannette);
            if (dgvAutre.Rows.Count != 0) tabControl.TabPages.Add(tpAutre);
        }

        private void RefreshGrid()
        {
            var listPET = ListFound.Where(p => p.DestinationSectionID == (int)EnSection.Unité_PET);
            var listCan = ListFound.Where(p => p.DestinationSectionID == (int)EnSection.Unité_Canette);
            var listAut = ListFound.Where(p => p.DestinationSectionID == (int)EnSection.Autre);

            dgvPET.DataSource = listPET.Select(p => new
            {
                ID = p.ID,
                Date = p.Date.Value.ToShortDateString(),
                Fournisseur = (p.Fournisseur != null) ? p.Fournisseur.Nom : "/",
                Descption = p.Ressource.ToDescription(),
                Nombre = (p.QuantitePiece != null) ? p.QuantitePiece + " " + p.Ressource.TypeRessource.UniteDePiece : "/",
                Quantite = p.QunatiteUM.ToAffInt() + " " + p.Ressource.TypeRessource.UniteDeMesure,
                Observation = p.Observation,
            }
              ).ToList();
            dgvPET.Refresh();
            dgvPET.Columns["ID"].Visible = false;

            //----------------------------------------------------
            dgvCanette.DataSource = listCan.Select(p => new
            {
                ID = p.ID,
                Date = p.Date.Value.ToShortDateString(),
                Fournisseur = (p.Fournisseur != null) ? p.Fournisseur.Nom : "/",
                Descption = p.Ressource.ToDescription(),
                Nombre = (p.QuantitePiece != null) ? p.QuantitePiece + " " + p.Ressource.TypeRessource.UniteDePiece : "/",
                Quantite = p.QunatiteUM.ToAffInt() + " " + p.Ressource.TypeRessource.UniteDeMesure,
                Observation = p.Observation,
            }
              ).ToList();

            dgvCanette.Columns["ID"].Visible = false;

            //---------------------------------------------------------------

            dgvAutre.DataSource = listAut.ToList().Select(p => new
            {
                ID = p.ID,
                Date = p.Date.Value.ToShortDateString(),
                Fournisseur = (p.Fournisseur != null) ? p.Fournisseur.Nom : "/",
                Descption = p.Ressource.ToDescription(),
                Nombre = (p.QuantitePiece != null) ? p.QuantitePiece + " " + p.Ressource.TypeRessource.UniteDePiece : "/",
                Quantite = p.QunatiteUM.ToAffInt() + " " + p.Ressource.TypeRessource.UniteDeMesure,
                Observation = p.Observation
            }
              ).ToList();

            dgvAutre.Columns["ID"].Visible = false;


            //---------------------


        }

        public List<TransfertMatiere> ListFound { get; set; }

        private void btDelete_Click(object sender, EventArgs e)
        {
            var id = dgvPET.GetSelectedID();

            var toDel = db.TransfertMatieres.SingleOrDefault(p => p.ID == id);
            if (
                toDel.ProductionSoufflages.Count != 0 ||
                toDel.ProductionBouchons.Count != 0 ||
                toDel.ProductionEtiquettes.Count != 0 ||
                toDel.ProductionFilmThermoes.Count != 0 ||
                toDel.ProductionSiroperies.Count != 0
                )
            {
                this.ShowWarning("impossible de supprimer transfert automatique lié à la consomation\n.");
                return;
            }



            if (toDel != null)
            {
                if (this.ConfirmWarning("Confirmer suppression élement?"))
                {
                    try
                    {
                        var toDelMatiere = toDel.StockMatiere;
                        FZBGsys.NsStock.FrStockMatiere.AddToStockMT(toDelMatiere, db);
                        db.DeleteObject(toDelMatiere);

                        db.DeleteObject(toDel);
                        db.SaveChanges();
                    }
                    catch (Exception exp)
                    {

                        this.ShowError(exp.AllMessages("Impossible de supprimer un element!"));
                    }
                }
            }
            else
            {
                this.ShowError("element introuvable!");
            }



            var index = dgvPET.SelectedRows[0].Index;
            ListFound.RemoveAt(index);
            RefreshGrid();

        }

        private void btPrint_Click(object sender, EventArgs e)
        {
            IEnumerable<TransfertMatiere> ListTemp = null;
            EnSection DestinationTxt;
            if (tabControl.SelectedTab == tpPET)
            {
                ListTemp = ListFound.Where(p => p.DestinationSectionID == (int)EnSection.Unité_PET);
                DestinationTxt = EnSection.Unité_PET;
            }
            else if (tabControl.SelectedTab == tpCannette)
            {
                ListTemp = ListFound.Where(p => p.DestinationSectionID == (int)EnSection.Unité_Canette);
                DestinationTxt = EnSection.Unité_Canette;
            }
            else if (tabControl.SelectedTab == tpCannette)
            {
                ListTemp = ListFound.Where(p => p.DestinationSectionID == (int)EnSection.Autre);
                DestinationTxt = EnSection.Autre;
            }
            else
            {
                labTotal.Text = "";
                return;
            }


            if (ListTemp == null || ListTemp.Count() == 0)
            {
                this.ShowInformation("Il n'y a rien à imprimer!");
                return;
            }
            Tools.ClearPrintTemp(); // ------------------------------------------- must clear printTemp before

            foreach (var transfert in ListTemp)
            {
                db.AddToPrintTemps(new PrintTemp()
                {
                    Date = transfert.DateArrivage,
                    Description = transfert.Ressource.ToDescription(),
                    Nom = transfert.Ressource.Fournisseur.Nom,
                    Piece = transfert.QuantitePiece,
                    Quantite = transfert.QunatiteUM,
                    UID = Tools.CurrentUserID,
                    val1 = transfert.Ressource.TypeRessource.UniteDeMesure,
                    val2 = transfert.Ressource.TypeRessource.UniteDePiece

                });
            }

            db.SaveChanges();

          //  FZBGsys.NsRapports.FrViewer.PrintListTransfert(labFilterTxt.Text, DestinationTxt);
        }

        private void chDate_CheckedChanged(object sender, EventArgs e)
        {
            panDate.Visible = chDate.Checked;
        }

        private void btFermer_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateTotal();
        }

        private void FrListTransfertMatiere_Load(object sender, EventArgs e)
        {

        }
    }
}
