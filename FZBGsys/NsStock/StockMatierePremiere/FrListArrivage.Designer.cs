﻿namespace FZBGsys.NsStock
{
    partial class FrListArrivage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btPrint = new System.Windows.Forms.Button();
            this.btFermer = new System.Windows.Forms.Button();
            this.btDelete = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panDate = new System.Windows.Forms.Panel();
            this.dtAu = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.dtDu = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.cbFournisseur = new System.Windows.Forms.ComboBox();
            this.btRecherche = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbCategorie = new System.Windows.Forms.ComboBox();
            this.chDate = new System.Windows.Forms.CheckBox();
            this.labFilterTxt = new System.Windows.Forms.Label();
            this.dgvPET = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            this.panDate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPET)).BeginInit();
            this.SuspendLayout();
            // 
            // btPrint
            // 
            this.btPrint.Location = new System.Drawing.Point(612, 342);
            this.btPrint.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btPrint.Name = "btPrint";
            this.btPrint.Size = new System.Drawing.Size(114, 28);
            this.btPrint.TabIndex = 8;
            this.btPrint.Text = "Imprimer La liste";
            this.btPrint.UseVisualStyleBackColor = true;
            this.btPrint.Click += new System.EventHandler(this.btPrint_Click);
            // 
            // btFermer
            // 
            this.btFermer.Location = new System.Drawing.Point(730, 342);
            this.btFermer.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btFermer.Name = "btFermer";
            this.btFermer.Size = new System.Drawing.Size(84, 28);
            this.btFermer.TabIndex = 9;
            this.btFermer.Text = "Fermer";
            this.btFermer.UseVisualStyleBackColor = true;
            this.btFermer.Click += new System.EventHandler(this.btFermer_Click);
            // 
            // btDelete
            // 
            this.btDelete.Location = new System.Drawing.Point(214, 342);
            this.btDelete.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btDelete.Name = "btDelete";
            this.btDelete.Size = new System.Drawing.Size(130, 28);
            this.btDelete.TabIndex = 8;
            this.btDelete.Text = "Supprimer Selection";
            this.btDelete.UseVisualStyleBackColor = true;
            this.btDelete.Click += new System.EventHandler(this.btDelete_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panDate);
            this.groupBox1.Controls.Add(this.cbFournisseur);
            this.groupBox1.Controls.Add(this.btRecherche);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cbCategorie);
            this.groupBox1.Controls.Add(this.chDate);
            this.groupBox1.Location = new System.Drawing.Point(6, 8);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(204, 362);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Recherche";
            // 
            // panDate
            // 
            this.panDate.Controls.Add(this.dtAu);
            this.panDate.Controls.Add(this.label5);
            this.panDate.Controls.Add(this.dtDu);
            this.panDate.Controls.Add(this.label4);
            this.panDate.Location = new System.Drawing.Point(21, 50);
            this.panDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panDate.Name = "panDate";
            this.panDate.Size = new System.Drawing.Size(160, 57);
            this.panDate.TabIndex = 21;
            this.panDate.Visible = false;
            // 
            // dtAu
            // 
            this.dtAu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtAu.Location = new System.Drawing.Point(47, 31);
            this.dtAu.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtAu.Name = "dtAu";
            this.dtAu.Size = new System.Drawing.Size(105, 20);
            this.dtAu.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 31);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Au:";
            // 
            // dtDu
            // 
            this.dtDu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDu.Location = new System.Drawing.Point(47, 8);
            this.dtDu.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtDu.Name = "dtDu";
            this.dtDu.Size = new System.Drawing.Size(105, 20);
            this.dtDu.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 8);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Du:";
            // 
            // cbFournisseur
            // 
            this.cbFournisseur.FormattingEnabled = true;
            this.cbFournisseur.Location = new System.Drawing.Point(21, 148);
            this.cbFournisseur.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbFournisseur.Name = "cbFournisseur";
            this.cbFournisseur.Size = new System.Drawing.Size(162, 21);
            this.cbFournisseur.TabIndex = 17;
            // 
            // btRecherche
            // 
            this.btRecherche.Location = new System.Drawing.Point(3, 329);
            this.btRecherche.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btRecherche.Name = "btRecherche";
            this.btRecherche.Size = new System.Drawing.Size(110, 28);
            this.btRecherche.TabIndex = 8;
            this.btRecherche.Text = "Recherche";
            this.btRecherche.UseVisualStyleBackColor = true;
            this.btRecherche.Click += new System.EventHandler(this.btRecherche_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 132);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Fournisseur:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 199);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Categorie: ";
            // 
            // cbCategorie
            // 
            this.cbCategorie.FormattingEnabled = true;
            this.cbCategorie.Location = new System.Drawing.Point(21, 215);
            this.cbCategorie.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbCategorie.Name = "cbCategorie";
            this.cbCategorie.Size = new System.Drawing.Size(162, 21);
            this.cbCategorie.TabIndex = 14;
            // 
            // chDate
            // 
            this.chDate.AutoSize = true;
            this.chDate.Location = new System.Drawing.Point(12, 28);
            this.chDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chDate.Name = "chDate";
            this.chDate.Size = new System.Drawing.Size(93, 17);
            this.chDate.TabIndex = 0;
            this.chDate.Text = "Date arrivage:";
            this.chDate.UseVisualStyleBackColor = true;
            this.chDate.CheckedChanged += new System.EventHandler(this.chDate_CheckedChanged);
            // 
            // labFilterTxt
            // 
            this.labFilterTxt.AutoSize = true;
            this.labFilterTxt.Location = new System.Drawing.Point(216, 18);
            this.labFilterTxt.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labFilterTxt.Name = "labFilterTxt";
            this.labFilterTxt.Size = new System.Drawing.Size(0, 13);
            this.labFilterTxt.TabIndex = 11;
            // 
            // dgvPET
            // 
            this.dgvPET.AllowUserToAddRows = false;
            this.dgvPET.AllowUserToDeleteRows = false;
            this.dgvPET.AllowUserToResizeColumns = false;
            this.dgvPET.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvPET.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPET.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvPET.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvPET.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvPET.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPET.Location = new System.Drawing.Point(220, 37);
            this.dgvPET.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dgvPET.MultiSelect = false;
            this.dgvPET.Name = "dgvPET";
            this.dgvPET.ReadOnly = true;
            this.dgvPET.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvPET.RowHeadersVisible = false;
            this.dgvPET.RowTemplate.Height = 16;
            this.dgvPET.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPET.Size = new System.Drawing.Size(586, 292);
            this.dgvPET.TabIndex = 12;
            // 
            // FrListArrivage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(824, 382);
            this.Controls.Add(this.dgvPET);
            this.Controls.Add(this.labFilterTxt);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btFermer);
            this.Controls.Add(this.btDelete);
            this.Controls.Add(this.btPrint);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FrListArrivage";
            this.Text = "Historique Arrivage Matiere Premiere";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrListArrivage_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panDate.ResumeLayout(false);
            this.panDate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPET)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

       // private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Button btPrint;
        private System.Windows.Forms.Button btFermer;
        private System.Windows.Forms.Button btDelete;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chDate;
        private System.Windows.Forms.Panel panDate;
        private System.Windows.Forms.DateTimePicker dtAu;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtDu;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbFournisseur;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbCategorie;
        private System.Windows.Forms.Button btRecherche;
        private System.Windows.Forms.Label labFilterTxt;
        private System.Windows.Forms.DataGridView dgvPET;
    }
}