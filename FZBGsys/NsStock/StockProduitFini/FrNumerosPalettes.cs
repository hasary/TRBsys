﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsStock
{
    public partial class FrNumerosPalettes : Form
    {
        public List<int> PalettesDispo { get; set; }
        public List<int> PalettesSelected { get; set; }


        public FrNumerosPalettes(DateTime dateProduction, GoutProduit goutProduit, string DispoNumeros, string SelectedNumeros = null)
        {
            InitializeComponent();
            this.DateProduction = dateProduction;
            this.GoutProduit = goutProduit;
            InitialiseContorls();


            if (!string.IsNullOrEmpty(SelectedNumeros))
            {
                PalettesSelected = SelectedNumeros.ToListInt();
                PalettesDispo = DispoNumeros.RemoveNumeros(SelectedNumeros).ToListInt();
            }
            else
            {
                PalettesDispo = DispoNumeros.ToListInt();
                PalettesSelected = new List<int>();
            }
            BindListes();
        }

        private void InitialiseContorls()
        {
            txtDate.Text = DateProduction.ToShortDateString();
            txtGoutProduit.Text = GoutProduit.Nom;


        }
        public static string ReturnNumeros { get; set; }
        public static string SelectPalettesDialog(DateTime date, GoutProduit typeProduit, string NumerosDispo, string selectedNumeros = null)
        {
            var dialog = new FrNumerosPalettes(date, typeProduit, NumerosDispo, selectedNumeros);
            dialog.ShowDialog();
            return ReturnNumeros;
        }



        #region Numeros Functions

        public static string ListInt2Numeros(List<int> list)
        {
            if (list == null)
            {
                return null;
            }
            else if (list.Count == 0)
            {
                return "";
            }

            list = list.OrderBy(p => p).ToList();
            string output = string.Empty;
            foreach (var value in list)
            {

                if (list.Contains(value + 1))
                {
                    if (output.EndsWith(",") || output == string.Empty)
                        output += value + "_";
                }
                else
                {
                    output += value + ",";
                }

            }

            return output.Trim().TrimEnd(',');

        }
        public static string ListNumeros2GroupedNumeros(List<string> NumeorsList)
        {

            var numerosAll = "";

            foreach (var numeros in NumeorsList)
            {
                numerosAll += "," + numeros;

            }

            return Numeros2OrderedNumeros(numerosAll.Substring(1));

        }
        public static string Numeros2OrderedNumeros(string numerosAll)
        {
            var listInt = Numeros2ListInt(numerosAll);
            //--- ListInt2Numeros will order automatiquely
            return ListInt2Numeros(listInt);
        }
        public static int Numeros2Nombre(string Numeros)
        {
            int result = 0;
            if (Numeros == String.Empty)
            {
                return result;
            }


            try
            {
                foreach (var filter in Numeros.Split(','))
                { //- 2,3,4,5,10,11,12
                    // 3_5,10_20,21
                    if (filter.Contains("-") || filter.Contains("_") || filter.Contains(".."))
                    { //TODA: tappe "_45" in palques bug
                        var min = int.Parse(filter.Trim().Replace("-", "|").Replace("_", "|").Replace("..", "|").Split('|')[0]);
                        var max = int.Parse(filter.Trim().Replace("-", "|").Replace("_", "|").Replace("..", "|").Split('|')[1]);
                        if (min >= max) return 0; // erreur seq (ex. 26_5 )
                        for (int i = min; i <= max; i++)
                        {
                            // if (!NumerosPlaquesDispo.Contains(i)) return 0; // error
                        }

                        result += max - min + 1;
                    }
                    else
                    {
                        var Numero = int.Parse(filter.Trim());
                        //   if (!NumerosPlaquesDispo.Contains(Numero)) return 0;
                        result++;
                    }
                }
            }
            catch (Exception)
            {

                return 0;
            }

            return result;
        }
        public static List<int> Numeros2ListInt(string Numeros)
        {
            List<int> result = null;
            if (Numeros == String.Empty)
            {
                return result;
            }

            result = new List<int>();

            foreach (var filter in Numeros.Split(','))
            { //- 2,3,4,5,10,11,12
                // 3_5,10_20,21
                if (filter.Contains("-") || filter.Contains("_") || filter.Contains(".."))
                { //TODA: tappe "_45" in palques bug
                    var min = int.Parse(filter.Trim().Replace("-", "|").Replace("_", "|").Replace("..", "|").Split('|')[0]);
                    var max = int.Parse(filter.Trim().Replace("-", "|").Replace("_", "|").Replace("..", "|").Split('|')[1]);
                    if (min >= max) return null; // erreur seq (ex. 26_5 )
                    for (int i = min; i <= max; i++)
                    {
                        // ne verif neededif (!NumerosPlaquesDispo.Contains(i)) return 0; // error
                        if (!result.Contains(i)) result.Add(i);
                    }


                }
                else
                {
                    var Numero = int.Parse(filter.Trim());
                    if (!result.Contains(Numero)) result.Add(Numero);

                }
            }

            return result;
        }
        public static string CommonNumeros(string NumerosTypedGrand, string NumerosDispoPetit)
        {
            if (string.IsNullOrEmpty(NumerosTypedGrand) || string.IsNullOrEmpty(NumerosDispoPetit)) return null;
            var listTyped = Numeros2ListInt(NumerosTypedGrand);
            var listDispo = Numeros2ListInt(NumerosDispoPetit);

            var listCommon = listDispo.Intersect(listTyped).ToList();
            if (listCommon.Count == 0) return null;
            return ListInt2Numeros(listCommon);
        }
        internal static string MinusNumeros(string NumerosBig, string NumerosSmall)
        {
            if (string.IsNullOrEmpty(NumerosSmall)) return NumerosBig;
            var listBig = NumerosBig.ToListInt();
            var listSmall = NumerosSmall.ToListInt();

            var listMinuis = listBig.Where(p => !listSmall.Contains(p)).ToList();

            return listMinuis.ToNumerosPalettes();

        }

        public static string AddNumeros(string Numeros, string NumerosToAdd)
        {
            List<string> list = new List<string>();
            list.Add(Numeros);
            list.Add(NumerosToAdd);
            return ListNumeros2GroupedNumeros(list);
        }



        #endregion

        public DateTime DateProduction { get; set; }
        public GoutProduit GoutProduit { get; set; }

        private void btAnnuler_Click(object sender, EventArgs e)
        {
            ReturnNumeros = null;
            Dispose();
        }
        private void btOk_Click(object sender, EventArgs e)
        {
            ReturnNumeros = txtNumeros.Text;
            Dispose();
        }
        private void BindListes()
        {
            lbDispo.DataSource = null;
            lbSelected.DataSource = null;
            if (PalettesDispo.Count != 0)
            {
                lbDispo.DataSource = new BindingSource(PalettesDispo.Select(p => new { ID = p, Text = "Palette N°  " + p.ToString().PadLeft(2, '0') }), null);
                lbDispo.DisplayMember = "Text";
                lbDispo.ValueMember = "ID";
            }
            if (PalettesSelected.Count != 0)
            {
                lbSelected.DataSource = new BindingSource(PalettesSelected.Select(p => new { ID = p, Text = "Palette N°  " + p.ToString().PadLeft(2, '0') }), null);
                lbSelected.DisplayMember = "Text";
                lbSelected.ValueMember = "ID";
            }

            if (PalettesSelected.Count != 0)
            {
                txtNumeros.Text = ListInt2Numeros(PalettesSelected);
            }
        }



        private void btAjouter_Click(object sender, EventArgs e)
        {
            if (lbDispo.SelectedIndices.Count == 0)
            {
                this.ShowInformation("Selectionner d'abord des palettes dans la liste 'Disponibles'.");
                return;
            }

            foreach (var item in lbDispo.SelectedItems)
            {
                var text = lbDispo.GetItemText(item).Replace("  ", " ");
                var numero = int.Parse(text.Split(' ')[text.Split(' ').Length - 1].ToString().Trim());

                var Index = PalettesDispo.IndexOf(numero);

                PalettesDispo.RemoveAt(Index);
                PalettesSelected.Add((int)numero);

            }

            BindListes();
        }

        private void btEnlever_Click(object sender, EventArgs e)
        {
            if (lbSelected.SelectedIndices.Count == 0)
            {
                this.ShowInformation("Selectionner d'abord des palettes dans la liste Selectionnées.");
                return;
            }

            foreach (var item in lbSelected.SelectedItems)
            {
                var text = lbSelected.GetItemText(item).Replace("  ", " ");
                var numero = int.Parse(text.Split(' ')[text.Split(' ').Length - 1].ToString().Trim());
                var index = PalettesSelected.IndexOf(numero);

                PalettesSelected.RemoveAt(index);
                PalettesDispo.Add(numero);


            }

            BindListes();
        }
    }
}
