﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsStock
{
    public partial class FrListTransfertProduitFini : Form
    {
        ModelEntities db = new ModelEntities();
        public FrListTransfertProduitFini()
        {
            InitializeComponent();
            InitialiseControls();
        }

        private void InitialiseControls()
        {
            //--------------------------- type produit
            cbTypeProduit.Items.Add(new GoutProduit { ID = 0, Nom = "[Tout]" });
            cbTypeProduit.Items.AddRange(db.GoutProduits.Where(p => p.ID > 0).ToArray());
            cbTypeProduit.SelectedIndex = 0;
            cbTypeProduit.ValueMember = "ID";
            cbTypeProduit.DisplayMember = "Nom";
            cbTypeProduit.DropDownStyle = ComboBoxStyle.DropDownList;
            //---------------------- Format (Bouteilles)
            cbFormat.Items.Add(new Format { ID = 0, Volume = "[Tout]" });
            cbFormat.Items.AddRange(db.Formats.Where(p => p.ID > 0).ToArray());
            cbFormat.DisplayMember = "Volume";
            cbFormat.ValueMember = "ID";
            cbFormat.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFormat.SelectedItem = null;

            //---------------------- Lot
            cbLot.Items.Add("[Tout]");
            cbLot.Items.Add("A");
            cbLot.Items.Add("B");
            cbLot.Items.Add("C");
            cbLot.Items.Add("D");
            cbLot.Items.Add("E");




        }

        private void FrListTransfertProduitFini_FormClosed(object sender, FormClosedEventArgs e)
        {
            db.Dispose();
        }

        private void FrListTransfertProduitFini_Load(object sender, EventArgs e)
        {

        }

        private void cbRechercheAvance_CheckedChanged(object sender, EventArgs e)
        {
            panRecherche.Visible = chRechercheAvance.Checked;
        }

        private void btRecherche_Click(object sender, EventArgs e)
        {
            Recherche();
        }

        private void Recherche()
        {
            IQueryable<TransfertPalette> resultPalette = db.TransfertPalettes;
            IQueryable<TransfertFardeau> resultFardeau = db.TransfertFardeaux;

            if (!chRechercheAvance.Checked)
            {
                var date = dtDate.Value.Date;
                resultPalette = resultPalette.Where(p => p.DateTransfert == date);
                resultFardeau = resultFardeau.Where(p => p.DateTransfert == date);
            }
            else
            {
                var dateDu = dtDateDu.Value.Date;
                var dateAu = dtDateAu.Value.Date;
                var dateProdDu = dtDateProductionDu.Value.Date;
                var dateProdAu = dtDateProductionAu.Value.Date;

                resultPalette = resultPalette.Where(p =>
                    p.DateTransfert >= dateDu &&
                    p.DateTransfert <= dateAu &&
                    p.StockPETPalette.DateProduction <= dateProdDu &&
                    p.StockPETPalette.DateProduction >= dateProdAu);

                resultFardeau = resultFardeau.Where(p =>
                    p.DateTransfert >= dateDu &&
                    p.DateTransfert <= dateAu &&
                    p.StockPETFardeau.DateProduction <= dateProdDu &&
                    p.StockPETFardeau.DateProduction >= dateProdAu);



                if (cbTypeProduit.SelectedIndex > 0)
                {
                    var typeProduitID = ((GoutProduit)cbTypeProduit.SelectedItem).ID;
                    resultPalette = resultPalette.Where(p => p.StockPETPalette.SessionProduction.GoutProduitID == typeProduitID);
                    resultFardeau = resultFardeau.Where(p => p.StockPETFardeau.SessionProduction.GoutProduitID == typeProduitID);

                }

                if (cbFormat.SelectedIndex > 0)
                {
                    var formatID = ((Format)cbFormat.SelectedItem).ID;
                    resultPalette = resultPalette.Where(p => p.StockPETPalette.SessionProduction.Production.BouteileFormatID == formatID);
                    resultFardeau = resultFardeau.Where(p => p.StockPETFardeau.SessionProduction.Production.BouteileFormatID == formatID);
                }

                if (cbLot.SelectedIndex > 0)
                {
                    var lot = cbLot.SelectedItem.ToString();
                    resultPalette = resultPalette.Where(p => p.StockPETPalette.Lot == lot);
                    resultFardeau = resultFardeau.Where(p => p.StockPETFardeau.Lot == lot);
                }

            }


            this.ListPaletteFound = resultPalette.ToList();
            this.ListFardeauFound = resultFardeau.ToList();

            RefreshGrid();
            UpdateControls();

        }

        private void UpdateControls()
        {
            tcDataView.TabPages.Clear();
            if (ListPaletteFound.Count != 0)
            {
                tcDataView.TabPages.Add(tpPalette);

            }
            if (ListFardeauFound.Count != 0)
            {
                tcDataView.TabPages.Add(tpFardeaux);

            }
            tcDataView.Refresh();
        }





        private void RefreshGrid()
        {

            dgvPalettes.DataSource = ListPaletteFound.Select(p => new
            {
                Date = p.DateTransfert,
                Produit = p.StockPETPalette.SessionProduction.GoutProduit.Nom,
                Format = p.StockPETPalette.SessionProduction.Production.Format.Volume,
                Palettes = p.StockPETPalette.Nombre.ToString(),
                Numéros = p.StockPETPalette.Numeros,
                Total_Bouteilles = p.StockPETPalette.TotalBouteilles,

            }).ToList();

            dgvFardeaux.DataSource = ListFardeauFound.Select(p => new
            {
                Date = p.DateTransfert,
                Produit = p.StockPETFardeau.SessionProduction.GoutProduit.Nom,
                Format = p.StockPETFardeau.SessionProduction.Production.Format.Volume,
                Fardeaux = p.StockPETFardeau.Nombre,
                Total_Bouteilles = p.StockPETFardeau.TotalBouteilles,

            }).ToList();


        }



        public List<TransfertPalette> ListPaletteFound { get; set; }
        public List<TransfertFardeau> ListFardeauFound { get; set; }

        private void btSupprimer_Click(object sender, EventArgs e)
        {
            if (tcDataView.SelectedTab == tpPalette && dgvPalettes.SelectedRows.Count != 0)
            {
                var selectedIndex = dgvPalettes.GetSelectedIndex();
                var transfert = this.ListPaletteFound.ElementAt(selectedIndex.Value);

                if (this.ConfirmWarning("Etes-vous sure de vouloir supprimer Transfert?"))
                {
                    FrTransfertProduitFini.RemovePalette(transfert, db);
                }
            }

            if (tcDataView.SelectedTab == tpFardeaux && dgvFardeaux.SelectedRows.Count != 0)
            {
                var selectedIndex = dgvFardeaux.GetSelectedIndex();
                var transfert = this.ListFardeauFound.ElementAt(selectedIndex.Value);

                if (this.ConfirmWarning("Etes-vous sure de vouloir supprimer Transfert?"))
                {
                    FrTransfertProduitFini.RemoveFardeau(transfert, db);
                }
            }
            try
            {
                db.SaveChanges();
            }
            catch (Exception exp)
            {
                this.ShowError(exp.AllMessages("Impossible d'enregistrer les changements"));
            }
            Recherche();
        }

        private void btFermer_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btImprimer_Click(object sender, EventArgs e)
        {

        }
    }
}
