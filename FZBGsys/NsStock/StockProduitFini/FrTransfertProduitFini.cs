﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;


namespace FZBGsys.NsStock
{
    public partial class FrTransfertProduitFini : Form
    {
        ModelEntities db = new ModelEntities();

        public List<TransfertPalette> ListPalettesToSave { get; set; }

        public FrTransfertProduitFini()
        {
            InitializeComponent();
            InitialiseControls();
            ListPalettesToSave = new List<TransfertPalette>();
            ListFardeauToSave = new List<TransfertFardeau>();

        }

        private void InitialiseControls()
        {


            //--------------------------- type produit
            cbTypeProduitFardeaux.Items.Add(new GoutProduit { ID = 0, Nom = "" });
            cbTypeProduitFardeaux.Items.AddRange(db.GoutProduits.Where(p => p.ID > 0).ToArray());
            cbTypeProduitFardeaux.SelectedIndex = 0;
            cbTypeProduitFardeaux.ValueMember = "ID";
            cbTypeProduitFardeaux.DisplayMember = "Nom";
            cbTypeProduitFardeaux.DropDownStyle = ComboBoxStyle.DropDownList;

            //--------------------------- type Produit
            cbTypeProduitPalette.Items.Add(new GoutProduit { ID = 0, Nom = "" });
            cbTypeProduitPalette.Items.AddRange(db.GoutProduits.Where(p => p.ID > 0).ToArray());
            cbTypeProduitPalette.SelectedIndex = 0;
            cbTypeProduitPalette.ValueMember = "ID";
            cbTypeProduitPalette.DisplayMember = "Nom";
            cbTypeProduitPalette.DropDownStyle = ComboBoxStyle.DropDownList;

            //----------------------------------------------------------------------------------

            dtDateProductionPalette.Value = DateTime.Now.Date;
            dtDateProductionFardeaux.Value = DateTime.Now.Date;
            dtDate.Value = DateTime.Now.Date;
        }

        private void dtDateProductionPalette_ValueChanged(object sender, EventArgs e)
        {
            this.SelectedDatePalette = dtDateProductionPalette.Value.Date;
            this.SelectedTypeProduitPalette = null;
            UpdatePaletteDispo();
            cbTypeProduitPalette.Items.Clear();
            if (StockPalettesDispo.Count == 0)
            {
                return;
            }

            UpdateControlTypeProduitPalette();


            this.Format = StockPalettesDispo.First().SessionProduction.Production.Format;
            txtFormatPalette.Text = Format.Volume;

            //------------------------------------------------------------------------------------
        }

        private void UpdateControlTypeProduitPalette()
        {
            var GoutsDispos = StockPalettesDispo.Select(p => p.SessionProduction.GoutProduit).Distinct().ToList();
            cbTypeProduitPalette.Items.Add(new GoutProduit { ID = 0, Nom = "" });
            cbTypeProduitPalette.Items.AddRange(GoutsDispos.ToArray());


            if (GoutsDispos.Count == 1)
            {
                cbTypeProduitPalette.SelectedIndex = 1;
            }
            else
            {
                cbTypeProduitPalette.SelectedIndex = 0;
            }
        }

        private bool VerifNumeros()
        {
            string Numeros = txtNumeroPalette.Text.Trim();
            // var Numeros = txtPlaques.Text.Trim().Replace(" ", "");

            if (Numeros == string.Empty)
            {
                return true;
            }
            Regex regNumeros = new Regex(@"\d+([_,]\d*)*");
            Match m = regNumeros.Match(Numeros);

            return m.Success;


        }


        private void txtNumeroPalette_Leave(object sender, EventArgs e)
        {
            ProcessNumerosPalettes();

        }

        private bool ProcessNumerosPalettes()
        {
            if (!VerifNumeros())
            {
                //labError.Text = "Numéros palettes invalide.";
                txtListFound.Text = "";
                return false;
            }
            if (StockPalettesDispo.Count ==0)
            {
                return false;
            }

            //----- ,list to view in txtBox
            var Numeros = txtNumeroPalette.Text.Trim();

            List<StockPETPalette> PalettesDispoSelected = new List<StockPETPalette>();

            foreach (var paleteDispo in StockPalettesDispo)
            {
                string CommonNumerosPalette = FZBGsys.NsStock.FrNumerosPalettes.CommonNumeros(Numeros, paleteDispo.Numeros);
                if (CommonNumerosPalette != null)
                { //  seq selected
                    var stockPF = new StockPETPalette();
                    stockPF.Lot = paleteDispo.Lot;
                    stockPF.SessionProductionID = paleteDispo.SessionProductionID;
                    // stockPF.SetNumeros(CommonNumerosPalette);  dont use this will create a new Stock ion db !!!
                    stockPF.Numeros = CommonNumerosPalette;
                    stockPF.Nombre = CommonNumerosPalette.ToNombrePlalettes();



                    PalettesDispoSelected.Add(stockPF);
                }

            }
            txtListFound.Text = "";
            PalettesDispoSelected = PalettesDispoSelected.OrderBy(p => p.Lot).ToList();
            foreach (var item in PalettesDispoSelected)
            {

                txtListFound.Text += (item.Nombre.ToString() + " palette" + ((item.Nombre > 1) ? "s" : "") + " Lot " + item.Lot +
                    "  N° " + item.Numeros) + "\r\n";

            }
            var prix = db.PrixProduits.Single(p=>p.TypeClient = 0 && p.GoutProduitID = 
            var nombre = PalettesDispoSelected.Sum(p => p.Nombre);
            txtNombrePalette.Text = nombre.ToString();
            txtTotalPalette.Text = (nombre * PrixProduit. * Format.FardeauParPalette).ToString();
            return true;

        }

        private void btParcourirNumeros_Click(object sender, EventArgs e)
        {
            if (cbTypeProduitPalette.SelectedIndex < 1)
            {
                this.ShowWarning("Selectionner type produit avant de parcourir les numéros.");
                return;
            }

            // ------ get numeros from Palettes Dispo and group them into one Numeros string
            var palettesDispoNumeros = StockPalettesDispo.Select(p => p.Numeros).ToList().ToGroupedNumeros();
            var paletteSelectedNumeors = txtNumeroPalette.Text.Trim();

            paletteSelectedNumeors = FZBGsys.NsStock.FrNumerosPalettes.SelectPalettesDialog(
               SelectedDatePalette,
               SelectedTypeProduitPalette,
               palettesDispoNumeros,
               paletteSelectedNumeors);


            txtNumeroPalette.Text = paletteSelectedNumeors;

            if (!palettesDispoNumeros.ContainsNumeros(paletteSelectedNumeors))
            {
                // labError.Text = "un ou plusieurs Numeros palettes non disponibles";
            }
            else
            {
                // labError.Text = "";
                ProcessNumerosPalettes(); // txtPalettes leave
            }


        }


        public DateTime SelectedDatePalette { get; set; }

        private void cbTypeProduitPalette_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbTypeProduitPalette.SelectedIndex > 0)
            {
                this.SelectedTypeProduitPalette = (GoutProduit)cbTypeProduitPalette.SelectedItem;
                UpdatePaletteDispo();
            }
            else
            {
                this.SelectedTypeProduitPalette = null;
            }



        }
        //UpdateFardeauDispo
        private void UpdateFardeauDispo()
        {
            var query = db.StockPETFardeaux.Where(p =>
               p.EnStock == true);

            if (SelectedDateFardeaux != null)
            {
                query = query.Where(p => p.SessionProduction.HeureDemarrage == SelectedDateFardeaux);
            }
            if (SelectedTypeProduitFardeaux != null)
            {
                query = query.Where(p => p.SessionProduction.GoutProduitID == SelectedTypeProduitFardeaux.ID);
            }


            if (SelectedLotFardeaux != null)
            {
                query = query.Where(p => p.Lot == SelectedLotFardeaux);
            }
            this.StockFardeauxDispo = query.ToList();
        }


        private void UpdatePaletteDispo()
        {
            var query = db.StockPETPalettes.Where(p =>
               p.EnStock == true);

            if (SelectedDatePalette != null)
            {
                query = query.Where(p => p.DateProduction == SelectedDatePalette);
            }
            if (SelectedTypeProduitPalette != null)
            {
                query = query.Where(p => p.SessionProduction.GoutProduitID == SelectedTypeProduitPalette.ID);
            }

            this.StockPalettesDispo = query.ToList();
        }


        public GoutProduit SelectedTypeProduitPalette { get; set; }

        public List<StockPETPalette> StockPalettesDispo { get; set; }
        public List<StockPETFardeau> StockFardeauxDispo { get; set; }

        public Format Format { get; set; }

        private void btAjouter_Click(object sender, EventArgs e)
        {
            //  tcDataView.SelectedTab = (tcPaletteFardeau.SelectedTab == tpPalette) ? tpPaletteTransfert : tpFardeauTransfert;

            if (tcPaletteFardeau.SelectedTab == tpPalette)
            {
                // return;

                if (ProcessNumerosPalettes())
                {
                    AjouterPalette();
                    RefreshGrid();
                    UpdateContorls();

                }
                else
                {
                    this.ShowWarning("Quantite Non Valide");
                }
            }
            else if (tcPaletteFardeau.SelectedTab == tpFardeaux)
            {
                if (ProcessNombreFardeaux())
                {
                    AjouterFardeaux();
                    RefreshGrid();
                    UpdateContorls();

                }
                else
                {
                    this.ShowWarning("Quantite Non disponnible!");
                }

            }


        }

        private void UpdateContorls()
        {
            if (cbTypeProduitFardeaux.Items.Count != 0) cbTypeProduitFardeaux.SelectedIndex = 0;
            if (cbTypeProduitPalette.Items.Count != 0) cbTypeProduitPalette.SelectedIndex = 0;

            //            cbLotFardeaux.SelectedIndex = 0;

            txtNombreFardeaux.Text = "";
            txtNumeroPalette.Text = "";
            txtTotalFardeaux.Text = "";
            txtTotalPalette.Text = "";
            txtNombrePalette.Text = "";


        }

        private void AjouterFardeaux()
        {

            var Numeros = txtNumeroPalette.Text.Trim();

            var newTransfert = new TransfertFardeau();
            newTransfert.DateTransfert = dtDate.Value;

            newTransfert.StockFardeaux.Add(new StockFardeau()
            {
                DateEntree = newTransfert.DateTransfert,
                EnStock = true,
                Format = Format,
                GoutProduit = (GoutProduit)cbTypeProduitFardeaux.SelectedItem,
                Lot = cbLotFardeaux.SelectedItem.ToString(),
                Nombre = txtNombreFardeaux.Text.ParseToInt(),
                TotalBouteilles = Format.BouteillesParFardeau * txtNombreFardeaux.Text.ParseToInt(),

            });

            var SelectedfardeauDispo = StockFardeauxDispo.First(); // ---- test
            var NombreFardeauNeeded = txtNombreFardeaux.Text.ParseToInt();
            if (SelectedfardeauDispo.Nombre == NombreFardeauNeeded)
            {
                SelectedfardeauDispo.EnStock = false;
                SelectedfardeauDispo.DateSotie = newTransfert.DateTransfert;
                newTransfert.StockPETFardeau = SelectedfardeauDispo; // take all
            }
            else //-------------------------- detache
            {
                newTransfert.StockPETFardeau = new StockPETFardeau()
                {
                    DateSotie = newTransfert.DateTransfert,
                    EnStock = false,
                    SessionProduction = SelectedfardeauDispo.SessionProduction,
                    Lot = cbLotFardeaux.SelectedItem.ToString(),
                    Nombre = NombreFardeauNeeded,
                    TotalBouteilles = Format.BouteillesParFardeau * NombreFardeauNeeded,

                };

                SelectedfardeauDispo.Nombre -= NombreFardeauNeeded;
            }

            db.AddToTransfertFardeaux(newTransfert);
            ListFardeauToSave.Add(newTransfert);


        }

        private void AjouterPalette()
        {

            var Numeros = txtNumeroPalette.Text.Trim();
            foreach (var paleteDispo in StockPalettesDispo)
            {
                string CommonNumerosPalette = FZBGsys.NsStock.FrNumerosPalettes.CommonNumeros(Numeros, paleteDispo.Numeros);
                if (CommonNumerosPalette == null) continue;
                var CommonNombrePalette = CommonNumerosPalette.ToNombrePlalettes();

                var newTransfert = new TransfertPalette();
                newTransfert.DateTransfert = dtDate.Value;
                newTransfert.StockPalettes.Add(
                    new StockPalette()
                    {
                        DateEntree = newTransfert.DateTransfert,
                        EnStock = true,
                        Format = Format,
                        GoutProduit = (GoutProduit)cbTypeProduitPalette.SelectedItem,
                        Lot = paleteDispo.Lot,


                    }.SetNumeros(CommonNumerosPalette));

                if (CommonNumerosPalette.ToOrderedNumeros() == paleteDispo.Numeros.ToOrderedNumeros())
                {
                    paleteDispo.EnStock = false;
                    paleteDispo.DateSortie = newTransfert.DateTransfert;
                    newTransfert.StockPETPalette = paleteDispo; // take all
                }
                else //-------------------------- detache
                {
                    newTransfert.StockPETPalette = new StockPETPalette()

                    {
                        DateSortie = newTransfert.DateTransfert,
                        EnStock = false,
                        SessionProduction = paleteDispo.SessionProduction,
                        Lot = paleteDispo.Lot,

                    }.SetNumeros(CommonNumerosPalette);

                    paleteDispo.SetNumeros(paleteDispo.Numeros.RemoveNumeros(CommonNumerosPalette));

                }

                db.AddToTransfertPalettes(newTransfert);
                ListPalettesToSave.Add(newTransfert);



            }
        }

        private void RefreshGrid()
        {
            /*
            if (ListPalettesToSave != null)
            {
                dgvPlaette.DataSource = ListPalettesToSave.Select(p => new
                    {
                        Date = p.DateTransfert,
                        Produit = p.StockPETPalette.SessionProduction.GoutProduit.Nom,
                        Format = p.StockPETPalette.SessionProduction.Production.Format.Volume,
                        Lot = p.StockPETPalette.Lot,
                        Palettes = p.StockPETPalette.Nombre.ToString(),
                        Numéros = p.StockPETPalette.Numeros,
                        Fardeaux = "/",
                        Total = p.StockPETPalette.TotalBouteilles,

                    }).ToList();
            }

            if (ListFardeauToSave != null)
            {
                dgvFardeau.DataSource = ListFardeauToSave.Select(p => new
                   {
                       Date = p.DateTransfert,
                       Produit = p.StockPETFardeau.SessionProduction.GoutProduit.Nom,
                       Format = p.StockPETFardeau.SessionProduction.Production.Format.Volume,
                       Lot = p.StockPETFardeau.Lot,
                       Palettes = "/",
                       Numéros = "/",
                       Fardeaux = p.StockPETFardeau.Nombre,
                       Total = p.StockPETFardeau.TotalBouteilles,

                   }).ToList();
            }
            */

            // return;
            var ds = ListPalettesToSave.Select(p => new
            {
                Date = p.DateTransfert,
                Produit = p.StockPETPalette.SessionProduction.GoutProduit.Nom,
                Format = p.StockPETPalette.SessionProduction.Production.Format.Volume,
                Unite = "Palettes " + p.StockPETPalette.Numeros,
                Nombre = p.StockPETPalette.Nombre,
                Lot = p.StockPETPalette.Lot,
                Total = p.StockPETPalette.TotalBouteilles,


            }).ToList();


            ds.AddRange(ListFardeauToSave.Select(p => new
            {
                Date = p.DateTransfert,
                Produit = p.StockPETFardeau.SessionProduction.GoutProduit.Nom,
                Format = p.StockPETFardeau.SessionProduction.Production.Format.Volume,
                Unite = "Fardeaux ", //+ p.StockPETFardeau.Nombre,
                Nombre = p.StockPETFardeau.Nombre,
                Lot = p.StockPETFardeau.Lot,
                Total = p.StockPETFardeau.TotalBouteilles,

            }).ToList());


            dgv.DataSource = ds;



            UpdateTotal();

        }

        private void UpdateTotal()
        {
            labTotalPF.Text = "";

            var totalP = ListPalettesToSave.Sum(p => p.StockPETPalette.TotalBouteilles);
            var totalF = ListFardeauToSave.Sum(p => p.StockPETFardeau.TotalBouteilles);
            if (ListPalettesToSave.Count != 0)
            {
                labTotalPF.Text += ListPalettesToSave.Sum(p => p.StockPETPalette.Nombre) + " P   ";
            }
            if (ListFardeauToSave.Count != 0)
            {
                labTotalPF.Text += ListFardeauToSave.Sum(p => p.StockPETFardeau.Nombre) + " F";
            }

            labTotalBouteilles.Text = (totalF + totalP).ToString() + " bouteilles";

        }

        private void btAnnuler_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void FrTransfertProduiFini_FormClosed(object sender, FormClosedEventArgs e)
        {
            db.Dispose();
        }

        private void txtNumeroPalette_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (ch == '-')
            {
                e.KeyChar = '_';
                ch = '_';
            }
            if (txtNumeroPalette.Text.Trim().EndsWith("_") || txtNumeroPalette.Text.Trim().EndsWith(","))
            {
                if (!Char.IsDigit(ch) && ch != 8) e.Handled = true;

            }

            else
            {
                if (!Char.IsDigit(ch) && ch != 8 && ch != Char.Parse("_") && ch != Char.Parse(","))
                {
                    e.Handled = true;
                }
            }
        }

        private void btEnlever_Click(object sender, EventArgs e)
        {
            //  var selectedIndex = dgv.GetSelectedIndex();
            int PaletteIndex = -1;
            int FardeauxIndex = -1;

            for (int i = 0; i < dgv.Rows.Count; i++)
            {
                if (dgv.Rows[i].Cells["Unite"].Value.ToString().StartsWith("Fardeaux"))
                {
                    FardeauxIndex++;
                }

                if (dgv.Rows[i].Cells["Unite"].Value.ToString().StartsWith("Palettes"))
                {
                    PaletteIndex++;
                }


                if (dgv.Rows[i] == dgv.SelectedRows[0])
                {
                    break;
                }
            }


            if (dgv.SelectedRows[0].Cells["Unite"].Value.ToString().StartsWith("Palettes"))
            {
                var selectedIndex = PaletteIndex; //dgvPlaette.GetSelectedIndex();
                var transfert = this.ListPalettesToSave.ElementAt(selectedIndex);
                InputPalette(transfert);
                ListPalettesToSave.Remove(transfert);
                RemovePalette(transfert, db);

            }
            else if (dgv.SelectedRows[0].Cells["Unite"].Value.ToString().StartsWith("Fardeaux"))
            {
                var selectedIndex = FardeauxIndex;// dgvFardeau.GetSelectedIndex();
                var transfert = this.ListFardeauToSave.ElementAt(selectedIndex);
                InputFardeau(transfert);
                ListFardeauToSave.Remove(transfert);
                RemoveFardeau(transfert, db);
            }

            RefreshGrid();
            
        }

        private void InputFardeau(TransfertFardeau transfert)
        {
            tcPaletteFardeau.SelectedTab = tpFardeaux;
            dtDate.Value = transfert.DateTransfert.Value;
            dtDateProductionFardeaux.Value = transfert.StockPETFardeau.DateProduction.Value;

            cbTypeProduitFardeaux.SelectedItem = transfert.StockPETFardeau.SessionProduction.GoutProduit;
            cbLotFardeaux.SelectedItem = transfert.StockPETFardeau.Lot;
            txtNombreFardeaux.Text = transfert.StockPETFardeau.Nombre.Value.ToString();
            ProcessNombreFardeaux();
            txtNombreFardeaux.Select();
        }

        private void InputPalette(TransfertPalette transfert)
        {
            tcPaletteFardeau.SelectedTab = tpPalette;
            dtDate.Value = transfert.DateTransfert.Value;
            dtDateProductionPalette.Value = transfert.StockPETPalette.DateProduction.Value;

            cbTypeProduitPalette.SelectedItem = transfert.StockPETPalette.SessionProduction.GoutProduit;
            txtNumeroPalette.Text = transfert.StockPETPalette.Numeros;
            ProcessNumerosPalettes();
            txtNumeroPalette.Select();
        }



        public static void RemoveFardeau(TransfertFardeau SelectedTransfert, ModelEntities db)
        {

            var SelectedPETProduitFini = SelectedTransfert.StockPETFardeau;
            var existingInStock = SelectedTransfert.StockPETFardeau.SessionProduction.StockPETFardeaux.Where(p =>
                p.Lot == SelectedPETProduitFini.Lot && p.EnStock == true);

            if (existingInStock.Count() == 0)
            {
                SelectedPETProduitFini.EnStock = true;
                SelectedPETProduitFini.DateSotie = null;
            }
            else
            {
                var first = existingInStock.First();
  
                first.Nombre += SelectedPETProduitFini.Nombre;
                db.DeleteObject(SelectedPETProduitFini);
            }

            db.DeleteObject(SelectedTransfert);
        }

        public static void RemovePalette(TransfertPalette SelectedTransfert, ModelEntities db)
        {

            var SelectedPETProduitFini = SelectedTransfert.StockPETPalette;
            var existingInStock = SelectedTransfert.StockPETPalette.SessionProduction.StockPETPalettes.Where(p =>
                p.Lot == SelectedPETProduitFini.Lot && p.EnStock == true);

            if (existingInStock.Count() == 0)
            {
                SelectedPETProduitFini.EnStock = true;
                SelectedPETProduitFini.DateSortie = null;
            }
            else
            {
                var first = existingInStock.First();
                first.SetNumeros(first.Numeros.AddNumeros(SelectedPETProduitFini.Numeros));
                db.DeleteObject(SelectedPETProduitFini);
            }

            db.DeleteObject(SelectedTransfert);
        }


        private void btEnregistrer_Click(object sender, EventArgs e)
        {
            try
            {
                db.SaveChanges();
                Dispose();
            }
            catch (Exception exept)
            {

                this.ShowError(exept.AllMessages("impossible de sauvguarder!"));
            }
        }

        private void dtDateProductionFardeaux_ValueChanged(object sender, EventArgs e)
        {
            this.SelectedDateFardeaux = dtDateProductionFardeaux.Value.Date;
            this.SelectedTypeProduitFardeaux = null;
            UpdateFardeauDispo();
            cbTypeProduitFardeaux.Items.Clear();
            cbLotFardeaux.Items.Clear();
            if (StockFardeauxDispo.Count == 0)
            {
                return;
            }

            UpdateControlTypeProduitFardeau();
            //update lot cb
            UpdateLotControlFardeau();

            this.Format = StockFardeauxDispo.First().SessionProduction.Production.Format;
            txtFormatFardeaux.Text = Format.Volume;
        }

        private void UpdateControlTypeProduitFardeau()
        {
            //update type produit cb
            var GoutsDispos = StockFardeauxDispo.Select(p => p.SessionProduction.GoutProduit).Distinct().ToList();

            cbTypeProduitFardeaux.Items.Add(new GoutProduit { ID = 0, Nom = "" });
            cbTypeProduitFardeaux.Items.AddRange(GoutsDispos.ToArray());
            if (GoutsDispos.Count == 1)
            {
                cbTypeProduitFardeaux.SelectedIndex = 1;
            }
            else
            {
                cbTypeProduitFardeaux.SelectedIndex = 0;
            }
        }
        private void UpdateLotControlFardeau()
        {
            if (StockFardeauxDispo != null)
            {
                var LotsDispos = StockFardeauxDispo.Select(p => p.Lot).Distinct().ToList();
                cbLotFardeaux.Items.Clear();
                cbLotFardeaux.Items.Add("");
                cbLotFardeaux.Items.AddRange(LotsDispos.ToArray());
                if (LotsDispos.Count == 1)
                {
                    cbLotFardeaux.SelectedIndex = 1;
                }
                else
                {
                    cbLotFardeaux.SelectedIndex = 0;
                }
            }
        }





        public DateTime SelectedDateFardeaux { get; set; }
        public GoutProduit SelectedTypeProduitFardeaux { get; set; }

        private void txtNombreFardeaux_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != 8 && !char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtNombreFardeaux_TextChanged(object sender, EventArgs e)
        {
            ProcessNombreFardeaux();
        }

        private bool ProcessNombreFardeaux()
        {
            var need = txtNombreFardeaux.Text.ParseToInt();
            var dispo = StockFardeauxDispo.Sum(p => p.Nombre);
            if (need != null)
            {
                if (dispo.Value < need)
                {
                    txtTotalFardeaux.Text = "";
                    return false;
                }

                txtTotalFardeaux.Text = (need.Value * Format.BouteillesParFardeau.Value).ToString();
                return true;
            }

            txtTotalFardeaux.Text = "";
            return false;

        }


        public List<TransfertFardeau> ListFardeauToSave { get; set; }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void cbTypeProduitFardeaux_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cbTypeProduitFardeaux.SelectedIndex > 0)
            {
                if (cbLotFardeaux.Items.Count != 0)
                {
                    cbLotFardeaux.SelectedIndex = 0;
                }
                this.SelectedTypeProduitFardeaux = (GoutProduit)cbTypeProduitFardeaux.SelectedItem;
                UpdateFardeauDispo();

            }
            else
            {
                this.SelectedTypeProduitFardeaux = null;
            }

            UpdateLotControlFardeau();
        }

        private void cbLotFardeaux_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbLotFardeaux.SelectedIndex > 0)
            {
                this.SelectedLotFardeaux = cbLotFardeaux.SelectedItem.ToString();
                UpdateFardeauDispo();

            }
            else
            {
                this.SelectedLotFardeaux = null;
            }

            UpdateFardeauDispo();
        }

        public string SelectedLotFardeaux { get; set; }
    }
}
