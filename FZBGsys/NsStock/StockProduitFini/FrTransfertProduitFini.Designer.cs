﻿namespace FZBGsys.NsStock
{
    partial class FrTransfertProduitFini
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.xpannel1 = new System.Windows.Forms.Panel();
            this.btAnnuler = new System.Windows.Forms.Button();
            this.btEnregistrer = new System.Windows.Forms.Button();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.tcPaletteFardeau = new System.Windows.Forms.TabControl();
            this.tpPalette = new System.Windows.Forms.TabPage();
            this.txtListFound = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbTypeProduitPalette = new System.Windows.Forms.ComboBox();
            this.txtNumeroPalette = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNombrePalette = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtFormatPalette = new System.Windows.Forms.TextBox();
            this.btParcourirNumeros = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTotalPalette = new System.Windows.Forms.TextBox();
            this.dtDateProductionPalette = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.tpFardeaux = new System.Windows.Forms.TabPage();
            this.txtFormatFardeaux = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cbLotFardeaux = new System.Windows.Forms.ComboBox();
            this.cbTypeProduitFardeaux = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtNombreFardeaux = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtTotalFardeaux = new System.Windows.Forms.TextBox();
            this.dtDateProductionFardeaux = new System.Windows.Forms.DateTimePicker();
            this.label15 = new System.Windows.Forms.Label();
            this.btAjouter = new System.Windows.Forms.Button();
            this.btEnlever = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.labTotalPF = new System.Windows.Forms.Label();
            this.labTotalBouteilles = new System.Windows.Forms.Label();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.xpannel1.SuspendLayout();
            this.tcPaletteFardeau.SuspendLayout();
            this.tpPalette.SuspendLayout();
            this.tpFardeaux.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // xpannel1
            // 
            this.xpannel1.Controls.Add(this.btAnnuler);
            this.xpannel1.Controls.Add(this.btEnregistrer);
            this.xpannel1.Location = new System.Drawing.Point(716, 457);
            this.xpannel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.xpannel1.Name = "xpannel1";
            this.xpannel1.Size = new System.Drawing.Size(292, 43);
            this.xpannel1.TabIndex = 9;
            // 
            // btAnnuler
            // 
            this.btAnnuler.Location = new System.Drawing.Point(14, 3);
            this.btAnnuler.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(132, 33);
            this.btAnnuler.TabIndex = 4;
            this.btAnnuler.Text = "Annuler";
            this.btAnnuler.UseVisualStyleBackColor = true;
            this.btAnnuler.Click += new System.EventHandler(this.btAnnuler_Click);
            // 
            // btEnregistrer
            // 
            this.btEnregistrer.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEnregistrer.Location = new System.Drawing.Point(152, 3);
            this.btEnregistrer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btEnregistrer.Name = "btEnregistrer";
            this.btEnregistrer.Size = new System.Drawing.Size(127, 33);
            this.btEnregistrer.TabIndex = 3;
            this.btEnregistrer.Text = "Enregistrer";
            this.btEnregistrer.UseVisualStyleBackColor = true;
            this.btEnregistrer.Click += new System.EventHandler(this.btEnregistrer_Click);
            // 
            // dtDate
            // 
            this.dtDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDate.Location = new System.Drawing.Point(159, 29);
            this.dtDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(139, 22);
            this.dtDate.TabIndex = 11;
            this.dtDate.Value = new System.DateTime(2012, 5, 1, 0, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 31);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 17);
            this.label1.TabIndex = 12;
            this.label1.Text = "Date Transfert:";
            // 
            // tcPaletteFardeau
            // 
            this.tcPaletteFardeau.Controls.Add(this.tpPalette);
            this.tcPaletteFardeau.Controls.Add(this.tpFardeaux);
            this.tcPaletteFardeau.Location = new System.Drawing.Point(16, 75);
            this.tcPaletteFardeau.Margin = new System.Windows.Forms.Padding(4);
            this.tcPaletteFardeau.Name = "tcPaletteFardeau";
            this.tcPaletteFardeau.SelectedIndex = 0;
            this.tcPaletteFardeau.Size = new System.Drawing.Size(317, 379);
            this.tcPaletteFardeau.TabIndex = 13;
            // 
            // tpPalette
            // 
            this.tpPalette.BackColor = System.Drawing.SystemColors.Control;
            this.tpPalette.Controls.Add(this.txtListFound);
            this.tpPalette.Controls.Add(this.label4);
            this.tpPalette.Controls.Add(this.cbTypeProduitPalette);
            this.tpPalette.Controls.Add(this.txtNumeroPalette);
            this.tpPalette.Controls.Add(this.label5);
            this.tpPalette.Controls.Add(this.label6);
            this.tpPalette.Controls.Add(this.label3);
            this.tpPalette.Controls.Add(this.txtNombrePalette);
            this.tpPalette.Controls.Add(this.label18);
            this.tpPalette.Controls.Add(this.txtFormatPalette);
            this.tpPalette.Controls.Add(this.btParcourirNumeros);
            this.tpPalette.Controls.Add(this.label8);
            this.tpPalette.Controls.Add(this.label7);
            this.tpPalette.Controls.Add(this.txtTotalPalette);
            this.tpPalette.Controls.Add(this.dtDateProductionPalette);
            this.tpPalette.Controls.Add(this.label2);
            this.tpPalette.Location = new System.Drawing.Point(4, 25);
            this.tpPalette.Margin = new System.Windows.Forms.Padding(4);
            this.tpPalette.Name = "tpPalette";
            this.tpPalette.Padding = new System.Windows.Forms.Padding(4);
            this.tpPalette.Size = new System.Drawing.Size(309, 350);
            this.tpPalette.TabIndex = 0;
            this.tpPalette.Text = "Palettes";
            // 
            // txtListFound
            // 
            this.txtListFound.Location = new System.Drawing.Point(13, 180);
            this.txtListFound.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtListFound.Multiline = true;
            this.txtListFound.Name = "txtListFound";
            this.txtListFound.ReadOnly = true;
            this.txtListFound.Size = new System.Drawing.Size(279, 80);
            this.txtListFound.TabIndex = 46;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(207, 274);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 17);
            this.label4.TabIndex = 44;
            this.label4.Text = "Palettes";
            // 
            // cbTypeProduitPalette
            // 
            this.cbTypeProduitPalette.FormattingEnabled = true;
            this.cbTypeProduitPalette.Location = new System.Drawing.Point(77, 98);
            this.cbTypeProduitPalette.Margin = new System.Windows.Forms.Padding(4);
            this.cbTypeProduitPalette.Name = "cbTypeProduitPalette";
            this.cbTypeProduitPalette.Size = new System.Drawing.Size(157, 24);
            this.cbTypeProduitPalette.TabIndex = 43;
            this.cbTypeProduitPalette.SelectedIndexChanged += new System.EventHandler(this.cbTypeProduitPalette_SelectedIndexChanged);
            // 
            // txtNumeroPalette
            // 
            this.txtNumeroPalette.Location = new System.Drawing.Point(77, 133);
            this.txtNumeroPalette.Margin = new System.Windows.Forms.Padding(4);
            this.txtNumeroPalette.Name = "txtNumeroPalette";
            this.txtNumeroPalette.Size = new System.Drawing.Size(155, 22);
            this.txtNumeroPalette.TabIndex = 15;
            this.txtNumeroPalette.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumeroPalette_KeyPress);
            this.txtNumeroPalette.Leave += new System.EventHandler(this.txtNumeroPalette_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 101);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 17);
            this.label5.TabIndex = 39;
            this.label5.Text = "Produit:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 62);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 17);
            this.label6.TabIndex = 41;
            this.label6.Text = "Format:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 135);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 17);
            this.label3.TabIndex = 14;
            this.label3.Text = "Numeros:";
            // 
            // txtNombrePalette
            // 
            this.txtNombrePalette.Location = new System.Drawing.Point(108, 272);
            this.txtNombrePalette.Margin = new System.Windows.Forms.Padding(4);
            this.txtNombrePalette.Name = "txtNombrePalette";
            this.txtNombrePalette.ReadOnly = true;
            this.txtNombrePalette.Size = new System.Drawing.Size(84, 22);
            this.txtNombrePalette.TabIndex = 38;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(39, 274);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(62, 17);
            this.label18.TabIndex = 36;
            this.label18.Text = "Nombre:";
            // 
            // txtFormatPalette
            // 
            this.txtFormatPalette.Location = new System.Drawing.Point(77, 62);
            this.txtFormatPalette.Margin = new System.Windows.Forms.Padding(4);
            this.txtFormatPalette.Name = "txtFormatPalette";
            this.txtFormatPalette.ReadOnly = true;
            this.txtFormatPalette.Size = new System.Drawing.Size(63, 22);
            this.txtFormatPalette.TabIndex = 37;
            // 
            // btParcourirNumeros
            // 
            this.btParcourirNumeros.Location = new System.Drawing.Point(244, 130);
            this.btParcourirNumeros.Margin = new System.Windows.Forms.Padding(4);
            this.btParcourirNumeros.Name = "btParcourirNumeros";
            this.btParcourirNumeros.Size = new System.Drawing.Size(48, 25);
            this.btParcourirNumeros.TabIndex = 35;
            this.btParcourirNumeros.Text = "...";
            this.btParcourirNumeros.UseVisualStyleBackColor = true;
            this.btParcourirNumeros.Click += new System.EventHandler(this.btParcourirNumeros_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(201, 309);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 17);
            this.label8.TabIndex = 18;
            this.label8.Text = "Bouteilles";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(55, 309);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 17);
            this.label7.TabIndex = 17;
            this.label7.Text = "Total:";
            // 
            // txtTotalPalette
            // 
            this.txtTotalPalette.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalPalette.Location = new System.Drawing.Point(108, 306);
            this.txtTotalPalette.Margin = new System.Windows.Forms.Padding(4);
            this.txtTotalPalette.Name = "txtTotalPalette";
            this.txtTotalPalette.ReadOnly = true;
            this.txtTotalPalette.Size = new System.Drawing.Size(84, 23);
            this.txtTotalPalette.TabIndex = 15;
            // 
            // dtDateProductionPalette
            // 
            this.dtDateProductionPalette.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateProductionPalette.Location = new System.Drawing.Point(160, 26);
            this.dtDateProductionPalette.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDateProductionPalette.Name = "dtDateProductionPalette";
            this.dtDateProductionPalette.Size = new System.Drawing.Size(132, 22);
            this.dtDateProductionPalette.TabIndex = 11;
            this.dtDateProductionPalette.Value = new System.DateTime(2012, 5, 1, 0, 0, 0, 0);
            this.dtDateProductionPalette.ValueChanged += new System.EventHandler(this.dtDateProductionPalette_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 26);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 17);
            this.label2.TabIndex = 12;
            this.label2.Text = "Date Production:";
            // 
            // tpFardeaux
            // 
            this.tpFardeaux.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tpFardeaux.Controls.Add(this.txtFormatFardeaux);
            this.tpFardeaux.Controls.Add(this.label16);
            this.tpFardeaux.Controls.Add(this.cbLotFardeaux);
            this.tpFardeaux.Controls.Add(this.cbTypeProduitFardeaux);
            this.tpFardeaux.Controls.Add(this.label9);
            this.tpFardeaux.Controls.Add(this.label10);
            this.tpFardeaux.Controls.Add(this.label11);
            this.tpFardeaux.Controls.Add(this.label12);
            this.tpFardeaux.Controls.Add(this.label13);
            this.tpFardeaux.Controls.Add(this.txtNombreFardeaux);
            this.tpFardeaux.Controls.Add(this.label14);
            this.tpFardeaux.Controls.Add(this.txtTotalFardeaux);
            this.tpFardeaux.Controls.Add(this.dtDateProductionFardeaux);
            this.tpFardeaux.Controls.Add(this.label15);
            this.tpFardeaux.Location = new System.Drawing.Point(4, 25);
            this.tpFardeaux.Margin = new System.Windows.Forms.Padding(4);
            this.tpFardeaux.Name = "tpFardeaux";
            this.tpFardeaux.Padding = new System.Windows.Forms.Padding(4);
            this.tpFardeaux.Size = new System.Drawing.Size(309, 350);
            this.tpFardeaux.TabIndex = 1;
            this.tpFardeaux.Text = "Fardeaux";
            // 
            // txtFormatFardeaux
            // 
            this.txtFormatFardeaux.Location = new System.Drawing.Point(92, 133);
            this.txtFormatFardeaux.Margin = new System.Windows.Forms.Padding(4);
            this.txtFormatFardeaux.Name = "txtFormatFardeaux";
            this.txtFormatFardeaux.ReadOnly = true;
            this.txtFormatFardeaux.Size = new System.Drawing.Size(63, 22);
            this.txtFormatFardeaux.TabIndex = 38;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(190, 169);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(67, 17);
            this.label16.TabIndex = 34;
            this.label16.Text = "Fardeaux";
            // 
            // cbLotFardeaux
            // 
            this.cbLotFardeaux.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbLotFardeaux.FormattingEnabled = true;
            this.cbLotFardeaux.Location = new System.Drawing.Point(216, 130);
            this.cbLotFardeaux.Margin = new System.Windows.Forms.Padding(4);
            this.cbLotFardeaux.Name = "cbLotFardeaux";
            this.cbLotFardeaux.Size = new System.Drawing.Size(53, 24);
            this.cbLotFardeaux.TabIndex = 33;
            this.cbLotFardeaux.SelectedIndexChanged += new System.EventHandler(this.cbLotFardeaux_SelectedIndexChanged);
            // 
            // cbTypeProduitFardeaux
            // 
            this.cbTypeProduitFardeaux.FormattingEnabled = true;
            this.cbTypeProduitFardeaux.Location = new System.Drawing.Point(90, 79);
            this.cbTypeProduitFardeaux.Margin = new System.Windows.Forms.Padding(4);
            this.cbTypeProduitFardeaux.Name = "cbTypeProduitFardeaux";
            this.cbTypeProduitFardeaux.Size = new System.Drawing.Size(179, 24);
            this.cbTypeProduitFardeaux.TabIndex = 33;
            this.cbTypeProduitFardeaux.SelectedIndexChanged += new System.EventHandler(this.cbTypeProduitFardeaux_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(199, 248);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 17);
            this.label9.TabIndex = 32;
            this.label9.Text = "Bouteilles";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(53, 248);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(50, 17);
            this.label10.TabIndex = 31;
            this.label10.Text = "Total:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(25, 85);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 17);
            this.label11.TabIndex = 28;
            this.label11.Text = "Produit:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(28, 133);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 17);
            this.label12.TabIndex = 30;
            this.label12.Text = "Format:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(178, 133);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(32, 17);
            this.label13.TabIndex = 29;
            this.label13.Text = "Lot:";
            // 
            // txtNombreFardeaux
            // 
            this.txtNombreFardeaux.Location = new System.Drawing.Point(92, 169);
            this.txtNombreFardeaux.Margin = new System.Windows.Forms.Padding(4);
            this.txtNombreFardeaux.Name = "txtNombreFardeaux";
            this.txtNombreFardeaux.Size = new System.Drawing.Size(84, 22);
            this.txtNombreFardeaux.TabIndex = 25;
            this.txtNombreFardeaux.TextChanged += new System.EventHandler(this.txtNombreFardeaux_TextChanged);
            this.txtNombreFardeaux.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNombreFardeaux_KeyPress);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(22, 172);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(62, 17);
            this.label14.TabIndex = 22;
            this.label14.Text = "Nombre:";
            // 
            // txtTotalFardeaux
            // 
            this.txtTotalFardeaux.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalFardeaux.Location = new System.Drawing.Point(106, 244);
            this.txtTotalFardeaux.Margin = new System.Windows.Forms.Padding(4);
            this.txtTotalFardeaux.Name = "txtTotalFardeaux";
            this.txtTotalFardeaux.ReadOnly = true;
            this.txtTotalFardeaux.Size = new System.Drawing.Size(84, 23);
            this.txtTotalFardeaux.TabIndex = 23;
            // 
            // dtDateProductionFardeaux
            // 
            this.dtDateProductionFardeaux.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateProductionFardeaux.Location = new System.Drawing.Point(156, 23);
            this.dtDateProductionFardeaux.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDateProductionFardeaux.Name = "dtDateProductionFardeaux";
            this.dtDateProductionFardeaux.Size = new System.Drawing.Size(113, 22);
            this.dtDateProductionFardeaux.TabIndex = 19;
            this.dtDateProductionFardeaux.Value = new System.DateTime(2012, 6, 15, 0, 0, 0, 0);
            this.dtDateProductionFardeaux.ValueChanged += new System.EventHandler(this.dtDateProductionFardeaux_ValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(35, 25);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(114, 17);
            this.label15.TabIndex = 20;
            this.label15.Text = "Date Production:";
            // 
            // btAjouter
            // 
            this.btAjouter.Location = new System.Drawing.Point(197, 462);
            this.btAjouter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAjouter.Name = "btAjouter";
            this.btAjouter.Size = new System.Drawing.Size(133, 31);
            this.btAjouter.TabIndex = 18;
            this.btAjouter.Text = "Ajouter >>";
            this.btAjouter.UseVisualStyleBackColor = true;
            this.btAjouter.Click += new System.EventHandler(this.btAjouter_Click);
            // 
            // btEnlever
            // 
            this.btEnlever.Location = new System.Drawing.Point(341, 462);
            this.btEnlever.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btEnlever.Name = "btEnlever";
            this.btEnlever.Size = new System.Drawing.Size(111, 31);
            this.btEnlever.TabIndex = 19;
            this.btEnlever.Text = "<< Enlever";
            this.btEnlever.UseVisualStyleBackColor = true;
            this.btEnlever.Click += new System.EventHandler(this.btEnlever_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(482, 457);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(50, 17);
            this.label17.TabIndex = 21;
            this.label17.Text = "Total:";
            // 
            // labTotalPF
            // 
            this.labTotalPF.AutoSize = true;
            this.labTotalPF.Location = new System.Drawing.Point(538, 457);
            this.labTotalPF.Name = "labTotalPF";
            this.labTotalPF.Size = new System.Drawing.Size(0, 17);
            this.labTotalPF.TabIndex = 22;
            // 
            // labTotalBouteilles
            // 
            this.labTotalBouteilles.AutoSize = true;
            this.labTotalBouteilles.Location = new System.Drawing.Point(531, 479);
            this.labTotalBouteilles.Name = "labTotalBouteilles";
            this.labTotalBouteilles.Size = new System.Drawing.Size(0, 17);
            this.labTotalBouteilles.TabIndex = 23;
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(336, 34);
            this.dgv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 16;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(659, 416);
            this.dgv.TabIndex = 24;
            // 
            // FrTransfertProduitFini
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1017, 508);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.labTotalBouteilles);
            this.Controls.Add(this.labTotalPF);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.btEnlever);
            this.Controls.Add(this.btAjouter);
            this.Controls.Add(this.tcPaletteFardeau);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtDate);
            this.Controls.Add(this.xpannel1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrTransfertProduitFini";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Transfert Produit Fini";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrTransfertProduiFini_FormClosed);
            this.xpannel1.ResumeLayout(false);
            this.tcPaletteFardeau.ResumeLayout(false);
            this.tpPalette.ResumeLayout(false);
            this.tpPalette.PerformLayout();
            this.tpFardeaux.ResumeLayout(false);
            this.tpFardeaux.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel xpannel1;
        private System.Windows.Forms.Button btAnnuler;
        private System.Windows.Forms.Button btEnregistrer;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tcPaletteFardeau;
        private System.Windows.Forms.TabPage tpPalette;
        private System.Windows.Forms.TextBox txtNumeroPalette;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtDateProductionPalette;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tpFardeaux;
        private System.Windows.Forms.Button btAjouter;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btEnlever;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbTypeProduitPalette;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNombrePalette;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cbTypeProduitFardeaux;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtNombreFardeaux;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtTotalFardeaux;
        private System.Windows.Forms.DateTimePicker dtDateProductionFardeaux;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtTotalPalette;
        private System.Windows.Forms.Button btParcourirNumeros;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtListFound;
        private System.Windows.Forms.TextBox txtFormatPalette;
        private System.Windows.Forms.TextBox txtFormatFardeaux;
        private System.Windows.Forms.ComboBox cbLotFardeaux;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label labTotalPF;
        private System.Windows.Forms.Label labTotalBouteilles;
        private System.Windows.Forms.DataGridView dgv;
    }
}