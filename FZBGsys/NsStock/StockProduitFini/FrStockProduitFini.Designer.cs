﻿namespace FZBGsys.NsStock
{
    partial class FrStockProduitFini
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvProduction = new System.Windows.Forms.DataGridView();
            this.btImprimer = new System.Windows.Forms.Button();
            this.btFermer = new System.Windows.Forms.Button();
            this.btRecherche = new System.Windows.Forms.Button();
            this.btSelectionner = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbTypeProduit = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cbLot = new System.Windows.Forms.ComboBox();
            this.panConsome = new System.Windows.Forms.Panel();
            this.dtDateSortirAu = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.dtDateSortieDu = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbFormat = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chProduction = new System.Windows.Forms.CheckBox();
            this.chMagazin = new System.Windows.Forms.CheckBox();
            this.chDateEntree = new System.Windows.Forms.CheckBox();
            this.panDateProduction = new System.Windows.Forms.Panel();
            this.dtDateEntreeAu = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.dtDateEntreeDu = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpProduction = new System.Windows.Forms.TabPage();
            this.tpMagazin = new System.Windows.Forms.TabPage();
            this.dgvMagazin = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.chProduitConsome = new System.Windows.Forms.CheckBox();
            this.chProduitEnStock = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.labTotalPF = new System.Windows.Forms.Label();
            this.labTotalBouteilles = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduction)).BeginInit();
            this.panConsome.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panDateProduction.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tpProduction.SuspendLayout();
            this.tpMagazin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMagazin)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvProduction
            // 
            this.dgvProduction.AllowUserToAddRows = false;
            this.dgvProduction.AllowUserToDeleteRows = false;
            this.dgvProduction.AllowUserToResizeColumns = false;
            this.dgvProduction.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvProduction.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvProduction.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvProduction.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvProduction.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvProduction.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProduction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvProduction.Location = new System.Drawing.Point(4, 4);
            this.dgvProduction.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvProduction.MultiSelect = false;
            this.dgvProduction.Name = "dgvProduction";
            this.dgvProduction.ReadOnly = true;
            this.dgvProduction.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvProduction.RowHeadersVisible = false;
            this.dgvProduction.RowTemplate.Height = 16;
            this.dgvProduction.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProduction.Size = new System.Drawing.Size(711, 461);
            this.dgvProduction.TabIndex = 10;
            // 
            // btImprimer
            // 
            this.btImprimer.Location = new System.Drawing.Point(764, 539);
            this.btImprimer.Margin = new System.Windows.Forms.Padding(4);
            this.btImprimer.Name = "btImprimer";
            this.btImprimer.Size = new System.Drawing.Size(145, 33);
            this.btImprimer.TabIndex = 60;
            this.btImprimer.Text = "Imprimer La Liste";
            this.btImprimer.UseVisualStyleBackColor = true;
            this.btImprimer.Click += new System.EventHandler(this.btImprimer_Click);
            // 
            // btFermer
            // 
            this.btFermer.Location = new System.Drawing.Point(916, 539);
            this.btFermer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btFermer.Name = "btFermer";
            this.btFermer.Size = new System.Drawing.Size(139, 34);
            this.btFermer.TabIndex = 57;
            this.btFermer.Text = "Fermer";
            this.btFermer.UseVisualStyleBackColor = true;
            this.btFermer.Click += new System.EventHandler(this.btFermer_Click);
            // 
            // btRecherche
            // 
            this.btRecherche.Location = new System.Drawing.Point(15, 537);
            this.btRecherche.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btRecherche.Name = "btRecherche";
            this.btRecherche.Size = new System.Drawing.Size(147, 34);
            this.btRecherche.TabIndex = 58;
            this.btRecherche.Text = "Recherche";
            this.btRecherche.UseVisualStyleBackColor = true;
            this.btRecherche.Click += new System.EventHandler(this.btRecherche_Click_1);
            // 
            // btSelectionner
            // 
            this.btSelectionner.Location = new System.Drawing.Point(328, 537);
            this.btSelectionner.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btSelectionner.Name = "btSelectionner";
            this.btSelectionner.Size = new System.Drawing.Size(147, 34);
            this.btSelectionner.TabIndex = 58;
            this.btSelectionner.Text = "Selectionner";
            this.btSelectionner.UseVisualStyleBackColor = true;
            this.btSelectionner.Click += new System.EventHandler(this.btSelectionner_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 6);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 17);
            this.label9.TabIndex = 68;
            this.label9.Text = "Date Sortie";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 111);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 17);
            this.label1.TabIndex = 61;
            this.label1.Text = "Produit:";
            // 
            // cbTypeProduit
            // 
            this.cbTypeProduit.FormattingEnabled = true;
            this.cbTypeProduit.Location = new System.Drawing.Point(97, 108);
            this.cbTypeProduit.Margin = new System.Windows.Forms.Padding(4);
            this.cbTypeProduit.Name = "cbTypeProduit";
            this.cbTypeProduit.Size = new System.Drawing.Size(157, 24);
            this.cbTypeProduit.TabIndex = 62;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(72, 186);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(32, 17);
            this.label13.TabIndex = 64;
            this.label13.Text = "Lot:";
            // 
            // cbLot
            // 
            this.cbLot.FormattingEnabled = true;
            this.cbLot.Location = new System.Drawing.Point(119, 182);
            this.cbLot.Margin = new System.Windows.Forms.Padding(4);
            this.cbLot.Name = "cbLot";
            this.cbLot.Size = new System.Drawing.Size(69, 24);
            this.cbLot.TabIndex = 65;
            // 
            // panConsome
            // 
            this.panConsome.Controls.Add(this.dtDateSortirAu);
            this.panConsome.Controls.Add(this.label9);
            this.panConsome.Controls.Add(this.label3);
            this.panConsome.Controls.Add(this.dtDateSortieDu);
            this.panConsome.Controls.Add(this.label6);
            this.panConsome.Location = new System.Drawing.Point(39, 58);
            this.panConsome.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panConsome.Name = "panConsome";
            this.panConsome.Size = new System.Drawing.Size(213, 100);
            this.panConsome.TabIndex = 67;
            this.panConsome.Visible = false;
            // 
            // dtDateSortirAu
            // 
            this.dtDateSortirAu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateSortirAu.Location = new System.Drawing.Point(63, 65);
            this.dtDateSortirAu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDateSortirAu.Name = "dtDateSortirAu";
            this.dtDateSortirAu.Size = new System.Drawing.Size(139, 22);
            this.dtDateSortirAu.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 17);
            this.label3.TabIndex = 20;
            this.label3.Text = "Au:";
            // 
            // dtDateSortieDu
            // 
            this.dtDateSortieDu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateSortieDu.Location = new System.Drawing.Point(63, 37);
            this.dtDateSortieDu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDateSortieDu.Name = "dtDateSortieDu";
            this.dtDateSortieDu.Size = new System.Drawing.Size(139, 22);
            this.dtDateSortieDu.TabIndex = 1;
            this.dtDateSortieDu.Value = new System.DateTime(2012, 5, 1, 0, 0, 0, 0);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 37);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 17);
            this.label6.TabIndex = 18;
            this.label6.Text = "Du:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 145);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 63;
            this.label2.Text = "Format:";
            // 
            // cbFormat
            // 
            this.cbFormat.FormattingEnabled = true;
            this.cbFormat.Location = new System.Drawing.Point(97, 142);
            this.cbFormat.Margin = new System.Windows.Forms.Padding(4);
            this.cbFormat.Name = "cbFormat";
            this.cbFormat.Size = new System.Drawing.Size(93, 24);
            this.cbFormat.TabIndex = 66;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chProduction);
            this.groupBox1.Controls.Add(this.chMagazin);
            this.groupBox1.Controls.Add(this.chDateEntree);
            this.groupBox1.Controls.Add(this.cbTypeProduit);
            this.groupBox1.Controls.Add(this.cbFormat);
            this.groupBox1.Controls.Add(this.panDateProduction);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.cbLot);
            this.groupBox1.Location = new System.Drawing.Point(15, 25);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(291, 329);
            this.groupBox1.TabIndex = 69;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Produit Fini";
            // 
            // chProduction
            // 
            this.chProduction.AutoSize = true;
            this.chProduction.Checked = true;
            this.chProduction.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chProduction.Location = new System.Drawing.Point(36, 64);
            this.chProduction.Margin = new System.Windows.Forms.Padding(4);
            this.chProduction.Name = "chProduction";
            this.chProduction.Size = new System.Drawing.Size(137, 21);
            this.chProduction.TabIndex = 2;
            this.chProduction.Text = "Stock Production";
            this.chProduction.UseVisualStyleBackColor = true;
            this.chProduction.CheckedChanged += new System.EventHandler(this.chProduitConsome_CheckedChanged);
            // 
            // chMagazin
            // 
            this.chMagazin.AutoSize = true;
            this.chMagazin.Checked = true;
            this.chMagazin.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chMagazin.Location = new System.Drawing.Point(36, 36);
            this.chMagazin.Margin = new System.Windows.Forms.Padding(4);
            this.chMagazin.Name = "chMagazin";
            this.chMagazin.Size = new System.Drawing.Size(180, 21);
            this.chMagazin.TabIndex = 1;
            this.chMagazin.Text = "Stock Magazin Principal";
            this.chMagazin.UseVisualStyleBackColor = true;
            // 
            // chDateEntree
            // 
            this.chDateEntree.AutoSize = true;
            this.chDateEntree.Location = new System.Drawing.Point(40, 229);
            this.chDateEntree.Margin = new System.Windows.Forms.Padding(4);
            this.chDateEntree.Name = "chDateEntree";
            this.chDateEntree.Size = new System.Drawing.Size(106, 21);
            this.chDateEntree.TabIndex = 68;
            this.chDateEntree.Text = "Date Entree";
            this.chDateEntree.UseVisualStyleBackColor = true;
            this.chDateEntree.CheckedChanged += new System.EventHandler(this.chDateProduction_CheckedChanged);
            // 
            // panDateProduction
            // 
            this.panDateProduction.Controls.Add(this.dtDateEntreeAu);
            this.panDateProduction.Controls.Add(this.label5);
            this.panDateProduction.Controls.Add(this.dtDateEntreeDu);
            this.panDateProduction.Controls.Add(this.label7);
            this.panDateProduction.Location = new System.Drawing.Point(36, 254);
            this.panDateProduction.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panDateProduction.Name = "panDateProduction";
            this.panDateProduction.Size = new System.Drawing.Size(213, 63);
            this.panDateProduction.TabIndex = 67;
            this.panDateProduction.Visible = false;
            // 
            // dtDateEntreeAu
            // 
            this.dtDateEntreeAu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateEntreeAu.Location = new System.Drawing.Point(57, 31);
            this.dtDateEntreeAu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDateEntreeAu.Name = "dtDateEntreeAu";
            this.dtDateEntreeAu.Size = new System.Drawing.Size(139, 22);
            this.dtDateEntreeAu.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 17);
            this.label5.TabIndex = 20;
            this.label5.Text = "Au:";
            // 
            // dtDateEntreeDu
            // 
            this.dtDateEntreeDu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateEntreeDu.Location = new System.Drawing.Point(57, 2);
            this.dtDateEntreeDu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDateEntreeDu.Name = "dtDateEntreeDu";
            this.dtDateEntreeDu.Size = new System.Drawing.Size(139, 22);
            this.dtDateEntreeDu.TabIndex = 1;
            this.dtDateEntreeDu.Value = new System.DateTime(2012, 5, 1, 0, 0, 0, 0);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(1, 2);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 17);
            this.label7.TabIndex = 18;
            this.label7.Text = "Du:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpProduction);
            this.tabControl1.Controls.Add(this.tpMagazin);
            this.tabControl1.Location = new System.Drawing.Point(328, 25);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(727, 498);
            this.tabControl1.TabIndex = 71;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tpProduction
            // 
            this.tpProduction.Controls.Add(this.dgvProduction);
            this.tpProduction.Location = new System.Drawing.Point(4, 25);
            this.tpProduction.Margin = new System.Windows.Forms.Padding(4);
            this.tpProduction.Name = "tpProduction";
            this.tpProduction.Padding = new System.Windows.Forms.Padding(4);
            this.tpProduction.Size = new System.Drawing.Size(719, 469);
            this.tpProduction.TabIndex = 0;
            this.tpProduction.Text = "Stock Production";
            this.tpProduction.UseVisualStyleBackColor = true;
            // 
            // tpMagazin
            // 
            this.tpMagazin.Controls.Add(this.dgvMagazin);
            this.tpMagazin.Location = new System.Drawing.Point(4, 25);
            this.tpMagazin.Margin = new System.Windows.Forms.Padding(4);
            this.tpMagazin.Name = "tpMagazin";
            this.tpMagazin.Padding = new System.Windows.Forms.Padding(4);
            this.tpMagazin.Size = new System.Drawing.Size(719, 469);
            this.tpMagazin.TabIndex = 1;
            this.tpMagazin.Text = "Stock Magazin Principal";
            this.tpMagazin.UseVisualStyleBackColor = true;
            // 
            // dgvMagazin
            // 
            this.dgvMagazin.AllowUserToAddRows = false;
            this.dgvMagazin.AllowUserToDeleteRows = false;
            this.dgvMagazin.AllowUserToResizeColumns = false;
            this.dgvMagazin.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvMagazin.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvMagazin.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvMagazin.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvMagazin.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvMagazin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMagazin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMagazin.Location = new System.Drawing.Point(4, 4);
            this.dgvMagazin.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvMagazin.MultiSelect = false;
            this.dgvMagazin.Name = "dgvMagazin";
            this.dgvMagazin.ReadOnly = true;
            this.dgvMagazin.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvMagazin.RowHeadersVisible = false;
            this.dgvMagazin.RowTemplate.Height = 16;
            this.dgvMagazin.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMagazin.Size = new System.Drawing.Size(711, 461);
            this.dgvMagazin.TabIndex = 11;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.chProduitConsome);
            this.groupBox3.Controls.Add(this.chProduitEnStock);
            this.groupBox3.Controls.Add(this.panConsome);
            this.groupBox3.Location = new System.Drawing.Point(16, 361);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(284, 162);
            this.groupBox3.TabIndex = 72;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Etat du stock";
            // 
            // chProduitConsome
            // 
            this.chProduitConsome.AutoSize = true;
            this.chProduitConsome.Location = new System.Drawing.Point(139, 31);
            this.chProduitConsome.Margin = new System.Windows.Forms.Padding(4);
            this.chProduitConsome.Name = "chProduitConsome";
            this.chProduitConsome.Size = new System.Drawing.Size(133, 21);
            this.chProduitConsome.TabIndex = 2;
            this.chProduitConsome.Text = "Transféré / Sorti";
            this.chProduitConsome.UseVisualStyleBackColor = true;
            this.chProduitConsome.CheckedChanged += new System.EventHandler(this.chProduitConsome_CheckedChanged);
            // 
            // chProduitEnStock
            // 
            this.chProduitEnStock.AutoSize = true;
            this.chProduitEnStock.Checked = true;
            this.chProduitEnStock.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chProduitEnStock.Location = new System.Drawing.Point(43, 31);
            this.chProduitEnStock.Margin = new System.Windows.Forms.Padding(4);
            this.chProduitEnStock.Name = "chProduitEnStock";
            this.chProduitEnStock.Size = new System.Drawing.Size(86, 21);
            this.chProduitEnStock.TabIndex = 1;
            this.chProduitEnStock.Text = "En Stock";
            this.chProduitEnStock.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(549, 532);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 17);
            this.label4.TabIndex = 73;
            this.label4.Text = "Total:";
            // 
            // labTotalPF
            // 
            this.labTotalPF.AutoSize = true;
            this.labTotalPF.Location = new System.Drawing.Point(607, 532);
            this.labTotalPF.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labTotalPF.Name = "labTotalPF";
            this.labTotalPF.Size = new System.Drawing.Size(0, 17);
            this.labTotalPF.TabIndex = 74;
            // 
            // labTotalBouteilles
            // 
            this.labTotalBouteilles.AutoSize = true;
            this.labTotalBouteilles.Location = new System.Drawing.Point(611, 553);
            this.labTotalBouteilles.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labTotalBouteilles.Name = "labTotalBouteilles";
            this.labTotalBouteilles.Size = new System.Drawing.Size(16, 17);
            this.labTotalBouteilles.TabIndex = 74;
            this.labTotalBouteilles.Text = "  ";
            // 
            // FrStockProduitFini
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1068, 581);
            this.Controls.Add(this.labTotalBouteilles);
            this.Controls.Add(this.labTotalPF);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btImprimer);
            this.Controls.Add(this.btFermer);
            this.Controls.Add(this.btSelectionner);
            this.Controls.Add(this.btRecherche);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrStockProduitFini";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Stock Produit Fini";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrStockPETProduitFini_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduction)).EndInit();
            this.panConsome.ResumeLayout(false);
            this.panConsome.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panDateProduction.ResumeLayout(false);
            this.panDateProduction.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tpProduction.ResumeLayout(false);
            this.tpMagazin.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMagazin)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvProduction;
        private System.Windows.Forms.Button btImprimer;
        private System.Windows.Forms.Button btFermer;
        private System.Windows.Forms.Button btRecherche;
        private System.Windows.Forms.Button btSelectionner;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbTypeProduit;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cbLot;
        private System.Windows.Forms.Panel panConsome;
        private System.Windows.Forms.DateTimePicker dtDateSortirAu;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtDateSortieDu;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbFormat;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpProduction;
        private System.Windows.Forms.TabPage tpMagazin;
        private System.Windows.Forms.CheckBox chDateEntree;
        private System.Windows.Forms.Panel panDateProduction;
        private System.Windows.Forms.DateTimePicker dtDateEntreeAu;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtDateEntreeDu;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox chProduitConsome;
        private System.Windows.Forms.CheckBox chProduitEnStock;
        private System.Windows.Forms.DataGridView dgvMagazin;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labTotalPF;
        private System.Windows.Forms.CheckBox chProduction;
        private System.Windows.Forms.CheckBox chMagazin;
        private System.Windows.Forms.Label labTotalBouteilles;
    }
}