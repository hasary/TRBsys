﻿namespace NsTRBsys
{
    partial class FrRegistration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtIDMachine = new System.Windows.Forms.TextBox();
            this.txtUSB = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.cbRegMAC = new System.Windows.Forms.CheckBox();
            this.cbRegUSB = new System.Windows.Forms.CheckBox();
            this.cbRegDMA = new System.Windows.Forms.CheckBox();
            this.dtDateExpire = new System.Windows.Forms.DateTimePicker();
            this.cbDrive = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtIDMachine
            // 
            this.txtIDMachine.Location = new System.Drawing.Point(147, 27);
            this.txtIDMachine.Name = "txtIDMachine";
            this.txtIDMachine.ReadOnly = true;
            this.txtIDMachine.Size = new System.Drawing.Size(274, 22);
            this.txtIDMachine.TabIndex = 1;
            // 
            // txtUSB
            // 
            this.txtUSB.Location = new System.Drawing.Point(146, 63);
            this.txtUSB.Name = "txtUSB";
            this.txtUSB.ReadOnly = true;
            this.txtUSB.Size = new System.Drawing.Size(191, 22);
            this.txtUSB.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(292, 154);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(129, 30);
            this.button1.TabIndex = 3;
            this.button1.Text = "Enregistrer";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cbRegMAC
            // 
            this.cbRegMAC.AutoSize = true;
            this.cbRegMAC.Location = new System.Drawing.Point(28, 27);
            this.cbRegMAC.Name = "cbRegMAC";
            this.cbRegMAC.Size = new System.Drawing.Size(100, 21);
            this.cbRegMAC.TabIndex = 4;
            this.cbRegMAC.Text = "Machine ID";
            this.cbRegMAC.UseVisualStyleBackColor = true;
            // 
            // cbRegUSB
            // 
            this.cbRegUSB.AutoSize = true;
            this.cbRegUSB.Location = new System.Drawing.Point(28, 65);
            this.cbRegUSB.Name = "cbRegUSB";
            this.cbRegUSB.Size = new System.Drawing.Size(102, 21);
            this.cbRegUSB.TabIndex = 4;
            this.cbRegUSB.Text = "USB Fourni";
            this.cbRegUSB.UseVisualStyleBackColor = true;
            // 
            // cbRegDMA
            // 
            this.cbRegDMA.AutoSize = true;
            this.cbRegDMA.Location = new System.Drawing.Point(28, 106);
            this.cbRegDMA.Name = "cbRegDMA";
            this.cbRegDMA.Size = new System.Drawing.Size(103, 21);
            this.cbRegDMA.TabIndex = 4;
            this.cbRegDMA.Text = "Date Expire";
            this.cbRegDMA.UseVisualStyleBackColor = true;
            // 
            // dtDateExpire
            // 
            this.dtDateExpire.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtDateExpire.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateExpire.Location = new System.Drawing.Point(147, 104);
            this.dtDateExpire.Name = "dtDateExpire";
            this.dtDateExpire.Size = new System.Drawing.Size(137, 22);
            this.dtDateExpire.TabIndex = 6;
            // 
            // cbDrive
            // 
            this.cbDrive.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDrive.FormattingEnabled = true;
            this.cbDrive.Location = new System.Drawing.Point(354, 61);
            this.cbDrive.Name = "cbDrive";
            this.cbDrive.Size = new System.Drawing.Size(67, 24);
            this.cbDrive.TabIndex = 7;
            this.cbDrive.SelectedIndexChanged += new System.EventHandler(this.cbDrive_SelectedIndexChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(184, 154);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(96, 30);
            this.button2.TabIndex = 8;
            this.button2.Text = "Annuler";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // FrRegistration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(449, 205);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.cbDrive);
            this.Controls.Add(this.dtDateExpire);
            this.Controls.Add(this.cbRegDMA);
            this.Controls.Add(this.cbRegUSB);
            this.Controls.Add(this.cbRegMAC);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtUSB);
            this.Controls.Add(this.txtIDMachine);
            this.Name = "FrRegistration";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Numéros de Série";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtIDMachine;
        private System.Windows.Forms.TextBox txtUSB;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox cbRegMAC;
        private System.Windows.Forms.CheckBox cbRegUSB;
        private System.Windows.Forms.CheckBox cbRegDMA;
        private System.Windows.Forms.DateTimePicker dtDateExpire;
        private System.Windows.Forms.ComboBox cbDrive;
        private System.Windows.Forms.Button button2;
    }
}