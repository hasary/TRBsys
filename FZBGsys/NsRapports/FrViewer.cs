﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NsTRBsys.NsRapports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;


using System.IO;
using System.Diagnostics;


namespace NsTRBsys.NsRapports
{
    public partial class FrViewer : Form
    {



        public FrViewer(bool tree = false)
        {
            InitializeComponent();
            if (tree)
            {
                crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.GroupTree;
            }
        }

        public static bool ExportPDF = false;
        public string extention { get; set; }
        public static string ExportFileName { get; set; }

        #region repport methods

        internal static void PrintReleverClient(Client Client, decimal? soldeBugin, DateTime From, DateTime To)
        {

            ReleveClient report = new ReleveClient();
            report.SetParameterValue("@from", From);
            report.SetParameterValue("@to", To);
            report.SetParameterValue("SoldeBegin", (soldeBugin != null) ? soldeBugin : 0);
            report.SetParameterValue("NomClient", Client.Nom);
            report.SetParameterValue("@ClientID", Client.ID);
            report.SetParameterValue("NoSolde", soldeBugin == null);
            report.SetParameterValue("LocalNom", Tools.LocalNom);
            PrintReport(report);
        }

        internal static void PrintReleverPartnaire(Client Client, decimal? soldeBugin, DateTime From, DateTime To)
        {

            CR_RelevePartenaire report = new CR_RelevePartenaire();
            report.SetParameterValue("@from", From);
            report.SetParameterValue("@to", To);
            report.SetParameterValue("SoldeBegin", (soldeBugin != null) ? soldeBugin : 0);
            report.SetParameterValue("NomClient", Client.Nom);
            report.SetParameterValue("@ClientID", Client.ID);
            report.SetParameterValue("NoSolde", soldeBugin == null);
            report.SetParameterValue("LocalNom", Tools.LocalNom);
            PrintReport(report);
        }



        internal static void PrintReleverFourni(Fournisseur Fourni, decimal? soldeBugin, DateTime From, DateTime To)
        {

            CR_ReleveFournisseur report = new CR_ReleveFournisseur();
            report.SetParameterValue("@from", From);
            report.SetParameterValue("@FouniID", Fourni.ID);
            report.SetParameterValue("@to", To);
            report.SetParameterValue("SoldeBegin", (soldeBugin != null) ? soldeBugin : 0);
            report.SetParameterValue("NomClient", Fourni.Nom);
            report.SetParameterValue("NoSolde", soldeBugin == null);
            report.SetParameterValue("LocalNom", Tools.LocalNom);
            PrintReport(report);
        }

        internal static void PrintVentes(DateTime From, DateTime to)
        {
            CR_ListeVentes report = new CR_ListeVentes();
            report.SetParameterValue("@UID", 1);
            report.SetParameterValue("@from", From);
            report.SetParameterValue("@to", to);
            report.SetParameterValue("LocalNom", Tools.LocalNom);
            report.SetParameterValue("LocalAdresse", Tools.LocalAdresse);
            PrintReport(report);

        }


        //--------------------------------------------------------Facture
        internal static void PrintFacture(int BonID, string TotalFactTxt, Destination Destination)
        {

            CR_FactureMoh report = new CR_FactureMoh();
            report.SetParameterValue("@ID", BonID);
            report.SetParameterValue("TotalFactureTxt", TotalFactTxt);

            report.SetParameterValue("LocalNom", Tools.LocalNom);
            report.SetParameterValue("LocalAdresse", Tools.LocalAdresse);
            report.SetParameterValue("LocalRegistre", Tools.LocalRegistre);
            report.SetParameterValue("LocalFiscal", Tools.LocalFiscal);
            report.SetParameterValue("LocalArticle", Tools.LocalArticle);
            
            if (Destination != null)
            {
                report.SetParameterValue("FakeNom", Destination.NomRaisonSocial);
                report.SetParameterValue("FakeRegistre", Destination.NoRegistre);
                report.SetParameterValue("FakeArticle", Destination.NoArticle);
                report.SetParameterValue("FakeFiscal", Destination.NoIdentifiant); 
            }
            
            report.SetParameterValue("UseFakeData", true);

            //   report.SetParameterValue("RemiseTxt", remisetxt);
            PrintReport(report);

        }
        //----------------------------------------------------Bon Livraison 2

        internal static void PrintLivraison(int BonID, string Nom)
        {
            CR_LivraisonMoh report = new CR_LivraisonMoh();
            report.SetParameterValue("@ID", BonID);
            report.SetParameterValue("TotalFactureTxt", "");
            
            report.SetParameterValue("UseFakeData", true);
            report.SetParameterValue("LocalNom", Tools.LocalNom);
            report.SetParameterValue("LocalAdresse", Tools.LocalAdresse);

           
                report.SetParameterValue("FakeNom", Nom); 
           




            //   report.SetParameterValue("RemiseTxt", remisetxt);
            PrintReport(report);

        }



        #endregion
        #region output methods
        public static string ExportToPDF(ReportClass report)
        {

            try
            {
                // System.IO.FileInfo fn =  new System.IO.FileInfo(report.FileName);
                //string fileName = System.IO.Path.GetTempPath() + report.GetClassName().Replace("Actions.Rapports.", "") + ".pdf";
                string fileName = System.IO.Path.GetTempPath() + ExportFileName + ".pdf";
                ExportOptions CrExportOptions;
                DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();

                CrDiskFileDestinationOptions.DiskFileName = fileName;
                CrExportOptions = report.ExportOptions;

                {
                    CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;

                    CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                    CrExportOptions.FormatOptions = CrFormatTypeOptions;
                }
                report.Export();
                return CrDiskFileDestinationOptions.DiskFileName;
            }
            catch (Exception ex)
            {
                return null;
                //MessageBox.Show(ex.ToString());
            }

        }
        public static string ExportToPDF(ReportDocument report)
        {

            try
            {
                // System.IO.FileInfo fn =  new System.IO.FileInfo(report.FileName);
                //string fileName = System.IO.Path.GetTempPath() + report.GetClassName().Replace("Actions.Rapports.", "") + sufix + ".pdf";
                string fileName = System.IO.Path.GetTempPath() + ExportFileName + ".pdf";
                ExportOptions CrExportOptions;
                DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                CrDiskFileDestinationOptions.DiskFileName = fileName;
                CrExportOptions = report.ExportOptions;
                {
                    CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                    CrExportOptions.FormatOptions = CrFormatTypeOptions;
                }
                report.Export();
                return CrDiskFileDestinationOptions.DiskFileName;
            }
            catch (Exception ex)
            {
                return null;
                //MessageBox.Show(ex.ToString());
            }

        }



        private static void PrintReport(ReportClass report, bool tree = false)
        {
            //report.Refresh();
            TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
            TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
            ConnectionInfo crConnectionInfo = new ConnectionInfo();
            Tables CrTables;
            //  FrViewer.ExportFileName = null;
            string utilisateur = null;
            using (ModelEntities db = new ModelEntities())
            {
                //  var section =
                //  utilisateur = db.ApplicationUtilisateurs.Single(u => u.ID == Tools.CurrentUserID).Employer.Section.Employer.Nom;

                crConnectionInfo.ServerName = Tools.ServerName;
                crConnectionInfo.DatabaseName = Tools.DatabaseName;
                crConnectionInfo.UserID = "sa";
                crConnectionInfo.Password = Tools.passwrod;

            }



            CrTables = report.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
            {
                crtableLogoninfo = CrTable.LogOnInfo;
                crtableLogoninfo.ConnectionInfo = crConnectionInfo;
                CrTable.ApplyLogOnInfo(crtableLogoninfo);
            }

            try
            {
                report.SetParameterValue("UtilisateurNom", utilisateur);
            }
            catch (Exception)
            {


            }

            var form = new FrViewer(false);
            //ExportPDF = false;
            form.crystalReportViewer1.ReportSource = report;
            if (!ExportPDF)
            {
                form.ShowDialog();
            }
            else
            {
                FrViewer.ExportFileName = ExportToPDF(report);
            }

            report.Dispose();
        }
        private static void PrintReport(ReportDocument report)
        {
            //report.Refresh();
            TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
            TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
            ConnectionInfo crConnectionInfo = new ConnectionInfo();
            Tables CrTables;

            string utilisateur = null;
            using (ModelEntities db = new ModelEntities())
            {
                utilisateur = db.ApplicationUtilisateurs.Single(u => u.ID == Tools.CurrentUserID).UserName;

                crConnectionInfo.ServerName = Tools.ServerName;
                crConnectionInfo.DatabaseName = Tools.DatabaseName;
                crConnectionInfo.UserID = "sa";
                crConnectionInfo.Password = Tools.passwrod;

            }



            CrTables = report.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
            {
                crtableLogoninfo = CrTable.LogOnInfo;
                crtableLogoninfo.ConnectionInfo = crConnectionInfo;
                CrTable.ApplyLogOnInfo(crtableLogoninfo);
            }

            report.SetParameterValue("UtilisateurNom", utilisateur);
            var form = new FrViewer();
            //  report.VerifyDatabase();

            form.crystalReportViewer1.ReportSource = report;
            form.crystalReportViewer1.ReportSource = report;
            if (!ExportPDF)
            {
                form.ShowDialog();
            }
            else
            {
                FrViewer.ExportFileName = ExportToPDF(report);
            }
        }


        #endregion

        internal static void PrintSituationComerciale(DateTime to)
        {
            if (to == null)
            {
                to = Tools.GetServerDateTime().Date;
            }
            using (var db = new ModelEntities())
            {
                var clientsRep = db.Clients.Where(p => p.ID != 0 && p.Active == true).ToList();
                Tools.ClearPrintTemp();
                int i = 0;
                foreach (var client in clientsRep)
                {
                    if (client.ID != 3296)
                    {
                        // continue;
                    }

                    i++;
                    FrMain.GenerateSoldeClient(db, to, client);

                    var dateDebut = Tools.DateDebutOperations;
                    //------------------------------------------------------------------------------  
                    var dernierRecuV = client.Operations.Where(p => p.Sen == "C" &&
                        p.Date <= to && p.Date >= dateDebut).OrderByDescending(p => p.Date).FirstOrDefault();

                    var dernierRecuF = client.Factures.Where(p => p.Date >= dateDebut).OrderByDescending(p => p.Date).FirstOrDefault();


                    DateTime? dernierRecuDate = null;
                    decimal? dernierRecuMontant = null;

                    if (dernierRecuV == null && dernierRecuF == null)
                    {

                    }
                    else

                        if (dernierRecuV == null && dernierRecuF != null)
                        {
                            dernierRecuDate = dernierRecuF.Date;
                            dernierRecuMontant = dernierRecuF.MontantTotalAchat;
                        }
                        else if (dernierRecuV != null && dernierRecuF == null)
                        {
                            dernierRecuDate = dernierRecuV.Date;
                            dernierRecuMontant = dernierRecuV.Montant;
                        }
                        else if (dernierRecuV.Date >= dernierRecuF.Date)
                        {
                            dernierRecuDate = dernierRecuV.Date;
                            dernierRecuMontant = dernierRecuV.Montant;
                        }
                        else if (dernierRecuV.Date < dernierRecuF.Date)
                        {
                            dernierRecuDate = dernierRecuF.Date;
                            dernierRecuMontant = dernierRecuF.MontantTotalAchat;
                        }
                    //---------------------------------------------------------------------------------------



                    var dernierEnvoiV = client.Operations.Where(p => p.Sen == "D" &&
                      p.Date <= to && p.Date >= dateDebut).OrderByDescending(p => p.Date).FirstOrDefault();

                    var dernierEnvoiF = client.Factures1.Where(p => p.Date >= dateDebut).OrderByDescending(p => p.Date).FirstOrDefault();


                    DateTime? dernierEnvoiDate = null;
                    decimal? dernierEnvoiMontant = null;


                    if (dernierEnvoiV == null && dernierEnvoiF == null)
                    {

                    }
                    else
                        if (dernierEnvoiV == null && dernierEnvoiF != null)
                        {
                            dernierEnvoiDate = dernierEnvoiF.Date;
                            dernierEnvoiMontant = dernierEnvoiF.MontantTotalFacture;
                        }
                        else if (dernierEnvoiV != null && dernierEnvoiF == null)
                        {
                            dernierEnvoiDate = dernierEnvoiV.Date;
                            dernierEnvoiMontant = dernierEnvoiV.Montant;
                        }
                        else if (dernierEnvoiV.Date >= dernierEnvoiF.Date)
                        {
                            dernierEnvoiDate = dernierEnvoiV.Date;
                            dernierEnvoiMontant = dernierEnvoiV.Montant;
                        }
                        else if (dernierEnvoiV.Date < dernierEnvoiF.Date)
                        {
                            dernierEnvoiDate = dernierEnvoiF.Date;
                            dernierEnvoiMontant = dernierEnvoiF.MontantTotalFacture;
                        }
                    /*

                                        if (dernierRecuV == null && dernierEnvoiF != null)
                                        {
                                            DateDernierMouvement = dernierEnvoiF.Date;
                                        }
                                        else if (dernierEnvoiF == null && dernierRecuV != null)
                                        {
                                            DateDernierMouvement = dernierRecuV.Date;
                                        }
                                        else if (dernierRecuV != null && dernierEnvoiF != null)
                                        {
                                            DateDernierMouvement = (DateTime.Compare(dernierEnvoiF.Date.Value, dernierRecuV.Date.Value) > 0) ? dernierEnvoiF.Date.Value : dernierRecuV.Date.Value;
                                        }
                                        else
                                        {
                                            DateDernierMouvement = null;
                                        }*/

                    var DateNow = Tools.GetServerDateTime();

                    db.AddToPrintTemps(new PrintTemp()
                    {
                        UID = Tools.CurrentUserID,
                        Nom = client.NomClientAndParent(),
                        MontantC = (client.Solde > 0) ? client.Solde : 0,
                        MontantD = (client.Solde < 0) ? client.Solde : 0,
                        val3 = (dernierRecuDate != null) ? dernierRecuDate.Value.ToShortDateString() : "",
                        val1 = (dernierRecuMontant != null) ? dernierRecuMontant.ToString() : "0",
                        val4 = (dernierEnvoiDate != null) ? dernierEnvoiDate.Value.ToShortDateString() : "",
                        val2 = (dernierEnvoiMontant != null) ? dernierEnvoiMontant.ToString() : "0",
                        Etat = "Clients"//(client.take == true) ? "Clients Actifs (durant ces derniers  " + Tools.MonthBeforeClientNonActif + " mois)" : "Clients non actifs (pas d'activité depuis " + Tools.MonthBeforeClientNonActif + " mois)",
                    });



                }

                var founisseurs = db.Fournisseurs.Where(p => p.ID > 0);

                foreach (var founi in founisseurs)
                {
                    FrMain.GenerateSoldeFourni(db, DateTime.Now, founi);
                    var dernierVersF = founi.Operations.OrderByDescending(p => p.Date).FirstOrDefault();
                    var dernierFactF = founi.Factures.OrderByDescending(p => p.Date).FirstOrDefault();
                    db.AddToPrintTemps(new PrintTemp()
                                 {
                                     UID = Tools.CurrentUserID,
                                     Nom = founi.Nom,
                                     MontantD = (founi.Solde > 0) ? founi.Solde : 0,
                                     MontantC = (founi.Solde < 0) ? founi.Solde : 0,
                                     val3 = (dernierVersF != null) ? dernierVersF.Date.Value.ToShortDateString() : "",
                                     val1 = (dernierVersF != null) ? dernierVersF.Montant.ToString() : "0",
                                     val4 = (dernierFactF != null) ? dernierFactF.Date.Value.ToShortDateString() : "",
                                     val2 = (dernierFactF != null) ? dernierFactF.MontantTotalAchat.ToString() : "0",
                                     Etat = "Fournisseurs"//(client.take == true) ? "Clients Actifs (durant ces derniers  " + Tools.MonthBeforeClientNonActif + " mois)" : "Clients non actifs (pas d'activité depuis " + Tools.MonthBeforeClientNonActif + " mois)",
                                 });
                  
                }
                db.SaveChangeTry();
            }

            NsRapports.CR_SituationComercial report = new NsRapports.CR_SituationComercial();
            report.SetParameterValue("@UID", Tools.CurrentUserID);
            report.SetParameterValue("DateTxt", to.ToShortDateString());
            report.SetParameterValue("LocalNom", Tools.LocalNom);

            PrintReport(report);
        }
    }
}
