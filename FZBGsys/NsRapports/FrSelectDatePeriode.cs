﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NsTRBsys.NsRapport
{
    public partial class FrSelectDatePeriode : Form
    {
        public FrSelectDatePeriode()
        {
            InitializeComponent();
            InitialiseDate();

            //   DateTimePicker dateTimePicker1 = new DateTimePicker();


        }

        private void InitialiseDate()
        {


            dateTimePickerAu.Value = DateTime.Now;
            dateTimePickerDu.Value = Tools.DateDebutOperations;
            dateTimePickerJourn.Visible = false;
            dateTimePickerMonth.Visible = false;
            panelPeriode.Visible = true;

        

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            dateTimePickerMonth.Visible = radioButtonMon.Checked;
        }

        private void radioButtonPeriode_CheckedChanged(object sender, EventArgs e)
        {
            panelPeriode.Visible = radioButtonPeriode.Checked;
        }

        private void radioButtonDate_CheckedChanged(object sender, EventArgs e)
        {
            dateTimePickerJourn.Visible = radioButtonDate.Checked;
        }

        public static DateTime? From { get; set; }
        public static DateTime? To { get; set; }
        private void button1_Click(object sender, EventArgs e)
        {
            //  progressBar1.Visible = true;
            button1.Enabled = false;
            DateTime du = DateTime.Now;
            DateTime au = DateTime.Now;

            if (radioButtonDate.Checked)
            {
                du = au = dateTimePickerJourn.Value;

            }

            if (radioButtonPeriode.Checked)
            {
                du = dateTimePickerDu.Value;
                au = dateTimePickerAu.Value;
            }
            if (radioButtonMon.Checked)
            {
                var dt = dateTimePickerMonth.Value;
                au = new DateTime(dt.Year, dt.Month, DateTime.DaysInMonth(dt.Year, dt.Month));
                du = new DateTime(dt.Year, dt.Month, 1);
            }

            //   Actions.Rapports.FrViewer.PrintRecapShowroom(du, au);
            From = du;
            To = au;
            //    progressBar1.Visible = false;
            button1.Enabled = true;

            Dispose();

        }


        public static DateTime?[] SelectPeriode(bool MonthOnly = false)
        {


            FrSelectDatePeriode form = new FrSelectDatePeriode();
            if (MonthOnly)
            {
                FrSelectDatePeriode.From = null;
                FrSelectDatePeriode.To = null;
                form.radioButtonMon.Checked = true;
                form.radioButtonDate.Enabled = false;
                form.radioButtonPeriode.Enabled = false;
            }
            form.ShowDialog();
            if (FrSelectDatePeriode.From == null || FrSelectDatePeriode.To == null)
            {
                return null;
            }
            return new DateTime?[2] { FrSelectDatePeriode.From, FrSelectDatePeriode.To };


        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }
    }
}
