﻿namespace NsTRBsys
{
    partial class FrParametres
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dtDebutOperrations = new System.Windows.Forms.DateTimePicker();
            this.txtLocalAdress = new NsTRBsys.TextBoxx();
            this.label3 = new System.Windows.Forms.Label();
            this.txtLocalNom = new NsTRBsys.TextBoxx();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtLocalRegistre = new NsTRBsys.TextBoxx();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtLocalFiscal = new NsTRBsys.TextBoxx();
            this.txtLocalArticle = new NsTRBsys.TextBoxx();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtFakeNom = new NsTRBsys.TextBoxx();
            this.txtFakeRegistre = new NsTRBsys.TextBoxx();
            this.txtFackeFiscal = new NsTRBsys.TextBoxx();
            this.txtFakeArticle = new NsTRBsys.TextBoxx();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.panSave = new System.Windows.Forms.Panel();
            this.txtSavePathFile = new System.Windows.Forms.TextBox();
            this.cbModeSauveguarde = new System.Windows.Forms.ComboBox();
            this.button3 = new System.Windows.Forms.Button();
            this.chActiveSave = new System.Windows.Forms.CheckBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.textBoxx4 = new NsTRBsys.TextBoxx();
            this.label16 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtPasseActu = new NsTRBsys.TextBoxx();
            this.txtNouvPasse = new NsTRBsys.TextBoxx();
            this.txtNouvPasse2 = new NsTRBsys.TextBoxx();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.panSave.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(523, 256);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dtDebutOperrations);
            this.tabPage1.Controls.Add(this.txtLocalAdress);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.txtLocalNom);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.txtLocalRegistre);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.txtLocalFiscal);
            this.tabPage1.Controls.Add(this.txtLocalArticle);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Size = new System.Drawing.Size(515, 227);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Mes Informations";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dtDebutOperrations
            // 
            this.dtDebutOperrations.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDebutOperrations.Location = new System.Drawing.Point(187, 101);
            this.dtDebutOperrations.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDebutOperrations.Name = "dtDebutOperrations";
            this.dtDebutOperrations.Size = new System.Drawing.Size(144, 22);
            this.dtDebutOperrations.TabIndex = 18;
            this.dtDebutOperrations.Value = new System.DateTime(2014, 1, 1, 0, 0, 0, 0);
            // 
            // txtLocalAdress
            // 
            this.txtLocalAdress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocalAdress.Location = new System.Drawing.Point(187, 47);
            this.txtLocalAdress.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLocalAdress.Multiline = true;
            this.txtLocalAdress.Name = "txtLocalAdress";
            this.txtLocalAdress.Size = new System.Drawing.Size(322, 48);
            this.txtLocalAdress.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 17);
            this.label3.TabIndex = 16;
            this.label3.Text = "Adresse:";
            // 
            // txtLocalNom
            // 
            this.txtLocalNom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocalNom.Location = new System.Drawing.Point(187, 17);
            this.txtLocalNom.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLocalNom.Name = "txtLocalNom";
            this.txtLocalNom.Size = new System.Drawing.Size(322, 22);
            this.txtLocalNom.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 17);
            this.label1.TabIndex = 14;
            this.label1.Text = "Nom  de la société";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(21, 103);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(153, 17);
            this.label6.TabIndex = 9;
            this.label6.Text = "Date début operations:";
            // 
            // txtLocalRegistre
            // 
            this.txtLocalRegistre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocalRegistre.Location = new System.Drawing.Point(187, 130);
            this.txtLocalRegistre.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLocalRegistre.Name = "txtLocalRegistre";
            this.txtLocalRegistre.Size = new System.Drawing.Size(239, 22);
            this.txtLocalRegistre.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 130);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "N° Registre Comerce:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 160);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "N° Fiscale:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 190);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "N° Article:";
            // 
            // txtLocalFiscal
            // 
            this.txtLocalFiscal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocalFiscal.Location = new System.Drawing.Point(187, 159);
            this.txtLocalFiscal.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLocalFiscal.Name = "txtLocalFiscal";
            this.txtLocalFiscal.Size = new System.Drawing.Size(239, 22);
            this.txtLocalFiscal.TabIndex = 12;
            // 
            // txtLocalArticle
            // 
            this.txtLocalArticle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocalArticle.Location = new System.Drawing.Point(187, 187);
            this.txtLocalArticle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLocalArticle.Name = "txtLocalArticle";
            this.txtLocalArticle.Size = new System.Drawing.Size(239, 22);
            this.txtLocalArticle.TabIndex = 11;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.txtFakeNom);
            this.tabPage2.Controls.Add(this.txtFakeRegistre);
            this.tabPage2.Controls.Add(this.txtFackeFiscal);
            this.tabPage2.Controls.Add(this.txtFakeArticle);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage2.Size = new System.Drawing.Size(515, 227);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Facture de Route (registre client)";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(208, 158);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 17);
            this.label12.TabIndex = 28;
            this.label12.Text = "+W";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(208, 103);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 17);
            this.label11.TabIndex = 27;
            this.label11.Text = "+W";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(147, 17);
            this.label7.TabIndex = 24;
            this.label7.Text = "Nom  / Raison Sociale";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 100);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(145, 17);
            this.label8.TabIndex = 19;
            this.label8.Text = "N° Registre Comerce:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 129);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 17);
            this.label9.TabIndex = 20;
            this.label9.Text = "N° Fiscale:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 158);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 17);
            this.label10.TabIndex = 18;
            this.label10.Text = "N° Article:";
            // 
            // txtFakeNom
            // 
            this.txtFakeNom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFakeNom.Location = new System.Drawing.Point(179, 15);
            this.txtFakeNom.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtFakeNom.Name = "txtFakeNom";
            this.txtFakeNom.Size = new System.Drawing.Size(305, 22);
            this.txtFakeNom.TabIndex = 25;
            // 
            // txtFakeRegistre
            // 
            this.txtFakeRegistre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFakeRegistre.Location = new System.Drawing.Point(241, 100);
            this.txtFakeRegistre.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtFakeRegistre.Name = "txtFakeRegistre";
            this.txtFakeRegistre.Size = new System.Drawing.Size(243, 22);
            this.txtFakeRegistre.TabIndex = 23;
            // 
            // txtFackeFiscal
            // 
            this.txtFackeFiscal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFackeFiscal.Location = new System.Drawing.Point(241, 128);
            this.txtFackeFiscal.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtFackeFiscal.Name = "txtFackeFiscal";
            this.txtFackeFiscal.Size = new System.Drawing.Size(243, 22);
            this.txtFackeFiscal.TabIndex = 22;
            // 
            // txtFakeArticle
            // 
            this.txtFakeArticle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFakeArticle.Location = new System.Drawing.Point(241, 156);
            this.txtFakeArticle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtFakeArticle.Name = "txtFakeArticle";
            this.txtFakeArticle.Size = new System.Drawing.Size(178, 22);
            this.txtFakeArticle.TabIndex = 21;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.panSave);
            this.tabPage4.Controls.Add(this.chActiveSave);
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(515, 227);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Sauveguarde";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // panSave
            // 
            this.panSave.Controls.Add(this.txtSavePathFile);
            this.panSave.Controls.Add(this.cbModeSauveguarde);
            this.panSave.Controls.Add(this.button3);
            this.panSave.Location = new System.Drawing.Point(40, 72);
            this.panSave.Name = "panSave";
            this.panSave.Size = new System.Drawing.Size(450, 72);
            this.panSave.TabIndex = 4;
            this.panSave.Visible = false;
            // 
            // txtSavePathFile
            // 
            this.txtSavePathFile.Location = new System.Drawing.Point(12, 41);
            this.txtSavePathFile.Name = "txtSavePathFile";
            this.txtSavePathFile.ReadOnly = true;
            this.txtSavePathFile.Size = new System.Drawing.Size(335, 22);
            this.txtSavePathFile.TabIndex = 3;
            // 
            // cbModeSauveguarde
            // 
            this.cbModeSauveguarde.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbModeSauveguarde.FormattingEnabled = true;
            this.cbModeSauveguarde.Items.AddRange(new object[] {
            "Au démarrage du système",
            "Au Fermeture du système"});
            this.cbModeSauveguarde.Location = new System.Drawing.Point(12, 5);
            this.cbModeSauveguarde.Name = "cbModeSauveguarde";
            this.cbModeSauveguarde.Size = new System.Drawing.Size(335, 24);
            this.cbModeSauveguarde.TabIndex = 1;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(353, 37);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(93, 30);
            this.button3.TabIndex = 2;
            this.button3.Text = "Parcourir...";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // chActiveSave
            // 
            this.chActiveSave.AutoSize = true;
            this.chActiveSave.Location = new System.Drawing.Point(40, 35);
            this.chActiveSave.Name = "chActiveSave";
            this.chActiveSave.Size = new System.Drawing.Size(259, 21);
            this.chActiveSave.TabIndex = 0;
            this.chActiveSave.Text = "Activer la Sauveguarde automatique";
            this.chActiveSave.UseVisualStyleBackColor = true;
            this.chActiveSave.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.textBoxx4);
            this.tabPage3.Controls.Add(this.label16);
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.label15);
            this.tabPage3.Controls.Add(this.txtPasseActu);
            this.tabPage3.Controls.Add(this.txtNouvPasse);
            this.tabPage3.Controls.Add(this.txtNouvPasse2);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage3.Size = new System.Drawing.Size(515, 227);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Mot de passe";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // textBoxx4
            // 
            this.textBoxx4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxx4.Location = new System.Drawing.Point(203, 36);
            this.textBoxx4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxx4.Name = "textBoxx4";
            this.textBoxx4.ReadOnly = true;
            this.textBoxx4.Size = new System.Drawing.Size(239, 22);
            this.textBoxx4.TabIndex = 19;
            this.textBoxx4.Text = "admin";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(36, 36);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(60, 17);
            this.label16.TabIndex = 15;
            this.label16.Text = "Compte:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(36, 81);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(139, 17);
            this.label13.TabIndex = 15;
            this.label13.Text = "Mot de passe actuel:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(36, 108);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(158, 17);
            this.label14.TabIndex = 16;
            this.label14.Text = "Nouveau mot de passe:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(36, 137);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(154, 17);
            this.label15.TabIndex = 14;
            this.label15.Text = "Retapez mot de passe:";
            // 
            // txtPasseActu
            // 
            this.txtPasseActu.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPasseActu.Location = new System.Drawing.Point(203, 79);
            this.txtPasseActu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPasseActu.Name = "txtPasseActu";
            this.txtPasseActu.Size = new System.Drawing.Size(239, 22);
            this.txtPasseActu.TabIndex = 19;
            this.txtPasseActu.UseSystemPasswordChar = true;
            // 
            // txtNouvPasse
            // 
            this.txtNouvPasse.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNouvPasse.Location = new System.Drawing.Point(203, 107);
            this.txtNouvPasse.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNouvPasse.Name = "txtNouvPasse";
            this.txtNouvPasse.Size = new System.Drawing.Size(239, 22);
            this.txtNouvPasse.TabIndex = 18;
            this.txtNouvPasse.UseSystemPasswordChar = true;
            // 
            // txtNouvPasse2
            // 
            this.txtNouvPasse2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNouvPasse2.Location = new System.Drawing.Point(203, 135);
            this.txtNouvPasse2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNouvPasse2.Name = "txtNouvPasse2";
            this.txtNouvPasse2.Size = new System.Drawing.Size(239, 22);
            this.txtNouvPasse2.TabIndex = 17;
            this.txtNouvPasse2.UseSystemPasswordChar = true;
            // 
            // button1
            // 
            this.button1.Image = global::NsTRBsys.Properties.Resources.icon_16_allow;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(405, 277);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(125, 34);
            this.button1.TabIndex = 1;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Image = global::NsTRBsys.Properties.Resources.icon_16_deny;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(275, 277);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(125, 34);
            this.button2.TabIndex = 2;
            this.button2.Text = "Annuler";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // FrParametres
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(547, 323);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrParametres";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Parametres";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.panSave.ResumeLayout(false);
            this.panSave.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private TextBoxx txtLocalAdress;
        private System.Windows.Forms.Label label3;
        private TextBoxx txtLocalNom;
        private System.Windows.Forms.Label label1;
        private TextBoxx txtLocalRegistre;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private TextBoxx txtLocalFiscal;
        private TextBoxx txtLocalArticle;
        private TextBoxx txtFakeNom;
        private System.Windows.Forms.Label label7;
        private TextBoxx txtFakeRegistre;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private TextBoxx txtFackeFiscal;
        private TextBoxx txtFakeArticle;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TabPage tabPage3;
        private TextBoxx textBoxx4;
        private System.Windows.Forms.Label label16;
        private TextBoxx txtPasseActu;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private TextBoxx txtNouvPasse;
        private TextBoxx txtNouvPasse2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dtDebutOperrations;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TextBox txtSavePathFile;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ComboBox cbModeSauveguarde;
        private System.Windows.Forms.CheckBox chActiveSave;
        private System.Windows.Forms.Panel panSave;
    }
}