﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Security.Cryptography;
using System.Management;
namespace NsTRBsys
{
    #region tools
    public static class Tools
    {
        public static int CurrentUserID;

        //----------------------------------------- contants
        public static bool AutoCreateTransfertFromProductionMP = false;
        public static bool AutoCreateTransfertFromProductionPF = false;

        //   public static string Svp = "EBs1qVXvzTc4F6Q8axQDKA==";
        //public static string Srp = "6kZSsAsD2f9vrxvF1HmgB7kkbA14mXzpesWRvYSUD2o=";
        public static string SuperRegistratorPassword = "SNKneogeo190584";
        public static string SuperAdminPassword = "snkneogeo190584";
        public static bool SuperAdmin = false;

        public static string ServerName = @"localhost\sqlhome";
        public static string DatabaseName = "TRBdb";
        public static string passwrod = "SNKbegin=1973";
        public static DateTime DateDebutOperations = new DateTime(2014, 08, 01);

        public static string LocalNom { get; set; }
        public static string LocalAdresse { get; set; }
        public static string LocalRegistre { get; set; }
        public static string LocalFiscal { get; set; }
        public static string LocalArticle { get; set; }

        public static string FakeNom { get; set; }
        public static string FakeRegistre { get; set; }
        public static string FakeFiscal { get; set; }
        public static string FakeArticle { get; set; }

        public static bool AutoBackup { get; set; }
        public static string AutoBackupPath { get; set; }
        public static string AutoBackupMethod { get; set; }




        //-------------------------------------------------------------------

        public static void loadParams()
        {

            using (var db = new ModelEntities())
            {
                var parms = db.ApplicationParams;


                DateDebutOperations = DateTime.Parse(parms.Single(p => p.ParamName == "DateDebutOperations").ParamValue);
                LocalNom = parms.Single(p => p.ParamName == "LocalNom").ParamValue;
                LocalAdresse = parms.Single(p => p.ParamName == "LocalAdresse").ParamValue;
                LocalRegistre = parms.Single(p => p.ParamName == "LocalRegistre").ParamValue;
                LocalFiscal = parms.Single(p => p.ParamName == "LocalFiscal").ParamValue;
                LocalArticle = parms.Single(p => p.ParamName == "LocalArticle").ParamValue;

                FakeNom = parms.Single(p => p.ParamName == "FakeNom").ParamValue;
                FakeRegistre = parms.Single(p => p.ParamName == "FakeRegistre").ParamValue;
                FakeFiscal = parms.Single(p => p.ParamName == "FakeFiscal").ParamValue;
                FakeArticle = parms.Single(p => p.ParamName == "FakeArticle").ParamValue;

                AutoBackup = parms.Single(p => p.ParamName == "AutoBackup").ParamValue == "1";
                AutoBackupMethod = parms.Single(p => p.ParamName == "AutoBackupMethod").ParamValue;
                AutoBackupPath = parms.Single(p => p.ParamName == "AutoBackupPath").ParamValue;


            }




        }

        internal static void ShowError(string message)
        {
            MessageBox.Show(message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        internal static void ShowMessage(string message)
        {
            MessageBox.Show(message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        internal static void ShowWarning(string message)
        {
            MessageBox.Show(message, "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public static bool? isBoss { get; set; }

        internal static void ClearPrintTemp()
        {
            SqlConnection conn = null;
            try
            {
                conn = new
                    SqlConnection("Server=" + Tools.ServerName + ";DataBase=" + Tools.DatabaseName + ";User ID=sa;password=" + Tools.passwrod + "");
                conn.Open();
                SqlCommand cmd = new SqlCommand(
                    "DELETE FROM PrintTemp WHERE UID = " + Tools.CurrentUserID, conn);
                //  cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }

            }
        }



        public static string Encrypt(string data)
        {
            if (string.IsNullOrEmpty(data))
                return string.Empty;

            byte[] Value = Encoding.UTF8.GetBytes(data);
            SymmetricAlgorithm mCSP = new RijndaelManaged();
            mCSP.Key = _key;
            mCSP.IV = _initVector;
            using (ICryptoTransform ct = mCSP.CreateEncryptor(mCSP.Key, mCSP.IV))
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, ct, CryptoStreamMode.Write))
                    {
                        cs.Write(Value, 0, Value.Length);
                        cs.FlushFinalBlock();
                        cs.Close();
                        return Convert.ToBase64String(ms.ToArray());
                        // return data;
                    }
                }
            }
        }

        #region Encryption
        private static readonly byte[] _key = { 0xA1, 0xF1, 0xA6, 0xBB, 0xA2, 0x5A, 0x37, 0x6F, 0x81, 0x2E, 0x17, 0x41, 0x72, 0x2C, 0x43, 0x27 };
        private static readonly byte[] _initVector = { 0xE1, 0xF1, 0xA6, 0xBB, 0xA9, 0x5B, 0x31, 0x2F, 0x81, 0x2E, 0x17, 0x4C, 0xA2, 0x81, 0x53, 0x61 };


        public static string Decrypt(string Value)
        {

            SymmetricAlgorithm mCSP;
            ICryptoTransform ct = null;
            MemoryStream ms = null;
            CryptoStream cs = null;
            byte[] byt;
            byte[] _result;

            mCSP = new RijndaelManaged();

            try
            {
                mCSP.Key = _key;
                mCSP.IV = _initVector;
                ct = mCSP.CreateDecryptor(mCSP.Key, mCSP.IV);
                byt = Convert.FromBase64String(Value);
                ms = new MemoryStream();
                cs = new CryptoStream(ms, ct, CryptoStreamMode.Write);
                cs.Write(byt, 0, byt.Length);
                cs.FlushFinalBlock();

                cs.Close();
                _result = ms.ToArray();
            }
            catch
            {
                _result = null;
            }
            finally
            {

                if (ct != null)
                    ct.Dispose();
                if (ms != null)
                    ms.Dispose();
                if (cs != null)
                    cs.Dispose();


            }
            if (_result == null) return null;
            return ASCIIEncoding.UTF8.GetString(_result);

            // return Value;
        }
        internal static string GetMachineMAC()
        {
            string cma = string.Empty;

            foreach (System.Net.NetworkInformation.NetworkInterface nic in System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces())
            {
                if (nic.OperationalStatus == System.Net.NetworkInformation.OperationalStatus.Up)
                {
                    cma += nic.GetPhysicalAddress().ToString();
                    break;
                }
            }
            return cma;
        }

        public static bool isRegistredUSBDrive(string dbRegUSB)
        {
            //var dbRegUSB = db.ApplicationParams.Single(p => p.ParamName == "RegUSB").ParamValue;
            USBSerialNumber ser = new USBSerialNumber();

            foreach (System.IO.DriveInfo driveInfo in System.IO.DriveInfo.GetDrives())
            {
                if (driveInfo.DriveType == System.IO.DriveType.Removable)
                {
                    var serials = ser.getSerialNumberFromDriveLetter(driveInfo.Name.Substring(0, 1));
                    var encrypted = Tools.Encrypt(serials);
                    return dbRegUSB == encrypted; // cracked if true

                }

            }

            return false;

        }
        public static bool isRegistredMac(string dbRegMAC)
        {

            //------------------------------------------------ Machine Adresse mac
            // var dbRegMAC = db.ApplicationParams.SingleOrDefault(p => p.ParamName == "RegMAC").ParamValue;
            if (dbRegMAC != null)
            {
                var cma1 = dbRegMAC;
                var cma2 = Tools.Encrypt(Tools.GetMachineMAC());
                return cma1 == cma2;
            }
            else
            {
                return false;

            }

            //------------------------------------------------- Expiration Date
        }
        public static bool isExpired(ModelEntities db, string dbRegDMA)
        {

            //var dbRegDMA = db.ApplicationParams.SingleOrDefault(p => p.ParamName == "RegDMA").ParamValue;
            if (dbRegDMA != null)
            {
                DateTime ema;
                DateTime.TryParse(Tools.Decrypt(dbRegDMA), out ema);
                if (ema.Date == DateTime.Parse("01/01/000001"))
                {
                    return true;
                }
                bool Expired = ema.CompareTo(DateTime.Now) <= 0;
                if (db.Operations.Count() != 0)
                {
                    var Xnow = db.Operations.Max(p => p.InscriptionDate);
                    Expired &= ema.CompareTo(Xnow) <= 0;
                }
                if (db.Factures.Count() != 0)
                {
                    var Ynow = db.Factures.Max(p => p.InscriptionTime);
                    Expired &= ema.CompareTo(Ynow) <= 0;
                }

                return Expired;

            }
            else
            {

                return true;
            }


        }



        public static void BackupDataBase(string fileName = null, bool silent = false)
        {

            SqlConnection conn = null;

            try
            {
                conn = new
                       SqlConnection(@"Server=localhost\SQLHOME" + ";DataBase=" + "TRBdb" + ";User ID=" + "sa" + ";password=" + "SNKbegin=1973" + "");
                conn.Open();
                SqlCommand cmd = new SqlCommand("USE [master]", conn);
                cmd.ExecuteNonQuery();

                if (fileName == null)
                {
                    SaveFileDialog dialog = new SaveFileDialog();
                    dialog.Filter = "Fichier Sauveguarde *.dms|*.dms";
                    dialog.FileName = "TRBdb_data.dms";
                    var res = dialog.ShowDialog();
                    if (res == DialogResult.OK)
                    {
                        fileName = dialog.FileName;

                    }
                    else
                    {
                        return;
                    }
                }
                var commande = "BACKUP DATABASE [TRBdb] TO  DISK = '" + fileName + "' WITH NOFORMAT, INIT,  NAME = 'TRBdb-Complète Base de données Sauvegarde', SKIP, NOREWIND, NOUNLOAD,  STATS = 10";
                SqlCommand cmd2 = new SqlCommand(commande, conn);


                cmd2.ExecuteNonQuery();
                if (!silent) Tools.ShowMessage("Suaveguarde réussie");
            }
            catch (Exception ex)
            {
                Tools.ShowError(ex.AllMessages("Impossible de lancer la sauveguarde"));
            }


            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }

            }



        }

        internal static void RestoreDataBase(bool create = false)
        {

            string fileName = "";
            SqlConnection conn = null;

            try
            {
                conn = new
                       SqlConnection(@"Server=localhost\SQLHOME" + ";DataBase=" + "master" + ";User ID=" + "sa" + ";password=" + "SNKbegin=1973" + "");
                conn.Open();
                SqlCommand cmd = new SqlCommand("USE [master]", conn);
                cmd.ExecuteNonQuery();
                if (create && File.Exists("empty.dms"))
                {
                    FileInfo fn = new FileInfo("empty.dms");
                    fileName = fn.FullName;
                }
                else
                {
                    OpenFileDialog dialog = new OpenFileDialog();
                    dialog.Filter = "Fichier Sauveguarde *.dms|*.dms";
                    dialog.FileName = "TRBdb_data.dms";
                    var res = dialog.ShowDialog();
                    if (res == DialogResult.OK)
                    {
                        fileName = dialog.FileName;

                    }
                    else
                    {
                        return;
                    }

                }
               // var com = "DELETE DATABASE TRBsys";
               // SqlCommand cmd0 = new SqlCommand(com, conn);

                var commande = "RESTORE DATABASE [" + Tools.DatabaseName + "]  FROM  DISK = '" + fileName + "' WITH  FILE = 1,  NOUNLOAD, REPLACE,  STATS = 10";
                SqlCommand cmd2 = new SqlCommand(commande, conn);
                if (create || Tools.ConfirmWarning("Confirmer la restauration des données??\nATTENTION: tout les données actuels seront EFFACES et remplacés par ceux du fichier selectionné!!!"))
                {
                    var single = "ALTER DATABASE ["+Tools.DatabaseName+"] SET SINGLE_USER WITH ROLLBACK IMMEDIATE";
                    SqlCommand cmds = new SqlCommand(single, conn);
                    if (!create) cmds.ExecuteNonQuery();

                    cmd2.ExecuteNonQuery();


                    single = "ALTER DATABASE [" + Tools.DatabaseName + "]  SET MULTI_USER";
                    cmds = new SqlCommand(single, conn);
                    if (!create) cmds.ExecuteNonQuery();
                    Tools.ShowMessage("Restauration réussie.");
                    if (!create) Application.Restart();
                }




            }
            catch (Exception ex)
            {
                Tools.ShowError(ex.AllMessages("Impossible de restaurer"));
            }


            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }

            }


        }

        internal static bool ConfirmWarning(string p)
        {
            DialogResult dl = MessageBox.Show(p, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            return (dl == DialogResult.Yes);
        }

        internal static bool ConfirmMessage(string p)
        {
            DialogResult dl = MessageBox.Show(p, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

            return (dl == DialogResult.Yes);
        }


        internal static void InitiliseDb()
        {
            SqlConnection conn = null;
            try
            {
                conn = new
                 SqlConnection(@"Data Source=localhost\sqlhome" +
                ";Initial Catalog=master" +
                ";User ID=sa" +
                ";Password='SNKbegin=1973" +
                "';MultipleActiveResultSets=True");

                conn.Open();
                SqlCommand cmd = new SqlCommand(
                   "use TRBdb", conn);

                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("TRBdb"))
                {
                    RestoreDataBase(true);
                }


            }

            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }

            }
        }

        public static DateTime GetServerDateTime()
        {
            string connStr = "";
            //= "Data Source=VIRTUALXP-44208;Initial Catalog=MarBlida;Integrated Security=true";// "Data Source=apex2006sql;Initial Catalog=Leather;Integrated Security=True;";

            using (ModelEntities db = new ModelEntities())
            {
                connStr = "Data Source=" + db.Connection.DataSource + ";Initial Catalog=" + db.Connection.Database +
                    ";User ID=sa;Password='" + Tools.passwrod + "';MultipleActiveResultSets=True";// "Data Source=apex2006sql;Initial Catalog=Leather;Integrated Security=True;";

            }

            const string query = "Select GetDate()";
            DataTable result = new DataTable();
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        result.Load(dr);
                    }
                }
            }

            var date = result.Rows[0][0].ToString();
            CurrentDateTime = DateTime.Parse(date);
            return DateTime.Parse(date);
        }

        public static int? GetMinutesBetween2Times(DateTime fin, DateTime debut)
        {
            if (fin.CompareTo(debut) > 0)
            {
                var result = (int)fin.TimeOfDay.TotalMinutes - (int)debut.TimeOfDay.TotalMinutes;

                if (result <= 0)
                {
                    return null;
                }
                return result;
            }
            else
            {

                if (fin.Hour > 12 || debut.Hour < 12)
                {
                    return null;
                }

                var xfin = fin.AddHours(12);
                var xdebut = debut.AddHours(-12);


                var result = (int)xfin.TimeOfDay.TotalMinutes - (int)xdebut.TimeOfDay.TotalMinutes;

                if (result <= 0)
                {
                    return null;
                }
                return result;
            }
        }

        public static DateTime? CurrentDateTime { get; set; }

        internal static DateTime GetFillDateFromDateTime(DateTime? Date, TimeSpan? HoursMinutes)
        {


            return new DateTime(Date.Value.Year, Date.Value.Month, Date.Value.Day, HoursMinutes.Value.Hours, HoursMinutes.Value.Minutes, 0);
        }



        public static bool Registred { get; set; }

        public static bool Expired { get; set; }


    }

    public class USBSerialNumber
    {

        string _serialNumber;
        string _driveLetter;
        public string getSerialNumberFromDriveLetter(string driveLetter)
        {
            this._driveLetter = driveLetter.ToUpper();

            if (!this._driveLetter.Contains(":"))
            {
                this._driveLetter += ":";
            }

            matchDriveLetterWithSerial();

            return this._serialNumber;
        }


        public List<string> getUSBAllSerials()
        {
            UsbSerials = new System.Collections.Generic.List<string>();
            matchDriveLetterWithSerial();

            return this.UsbSerials;
        }

        private void matchDriveLetterWithSerial()
        {

            string[] diskArray;
            string driveNumber;
            string driveLetter;

            ManagementObjectSearcher searcher1 = new ManagementObjectSearcher("SELECT * FROM Win32_LogicalDiskToPartition");
            foreach (ManagementObject dm in searcher1.Get())
            {
                diskArray = null;
                driveLetter = getValueInQuotes(dm["Dependent"].ToString());
                if (driveLetter == this._driveLetter)
                {
                    diskArray = getValueInQuotes(dm["Antecedent"].ToString()).Split(',');
                    driveNumber = diskArray[0].Remove(0, 6).Trim();
                    this.deviceID = driveNumber;
                    break;
                }

            }
            /* This is where we get the drive serial */
            ManagementObjectSearcher disks = new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive WHERE InterfaceType = 'USB'");
            foreach (ManagementObject disk in disks.Get())
            {
                var diskName = disk["Name"].ToString();
                var diskInterface = disk["InterfaceType"].ToString();
                // var diskLettre = disk["Dependent"].ToString();

                if (diskName.Contains("PHYSICALDRIVE" + deviceID.ToString()))
                {
                    this._serialNumber = parseSerialFromDeviceID(disk["PNPDeviceID"].ToString());
                    return;
                }
            }


        }
        public List<string> UsbSerials { get; set; }
        private string parseSerialFromDeviceID(string deviceId)
        {
            string[] splitDeviceId = deviceId.Split('\\');
            string[] serialArray;
            string serial;
            int arrayLen = splitDeviceId.Length - 1;

            serialArray = splitDeviceId[arrayLen].Split('&');
            serial = serialArray[0];

            return serial;
        }

        private string getValueInQuotes(string inValue)
        {
            string parsedValue = "";

            int posFoundStart = 0;
            int posFoundEnd = 0;

            posFoundStart = inValue.IndexOf("\"");
            posFoundEnd = inValue.IndexOf("\"", posFoundStart + 1);

            parsedValue = inValue.Substring(posFoundStart + 1, (posFoundEnd - posFoundStart) - 1);

            return parsedValue;
        }


        public string deviceID { get; set; }
    }
        #endregion

    public class TextBoxx : TextBox
    {
        public TextBoxx()
        {

            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;

        }

    }
    #endregion

    #region enums

    public enum EnTypeBon
    {
        Bon_Attribution = 1,
        Bon_Sortie = 2,
        Bon_Livraison = 3,
        Facture = 4,

    }

    /*
    public enum EnTypeClient
    {
        Divers = 0,
        SuperGrossiste = 1,
        Gratuit = 2,
        Grossiste = 3,
    }
     * */

    public enum EnModePayement
    {
        Especes = 1,
        Avance = 2,
        Virement = 3,
        Chèque = 4,
        Traite = 5,
        a_Terme = 6,
    }
    public enum EnUniteProduit
    {
        Palettes = 1,
        Fardeaux = 2

    }
    public enum EnEtatBonAttribution
    {
        EnInstance = 1,
        Chargé = 2,
        Livré = 3,
        Annulé = 0,

    }

    public enum EnFormat
    {
        Format1L = 1,
        Format1_5L = 2,
        Format2L = 3,
    }


    public enum EnTypeOperation
    {
        Recu_de_Versement = 1,
        Versement_ENVOI = 2,

    }
    public enum EnClassRessource
    {
        Bouteillage = 1,
        Siroperie = 2,
        Embalage = 3

    }
    public enum EnSection
    {

        Administration = 1,
        Direction = 2,
        Unité_PET = 3,
        Unité_Canette = 4,
        Magazin = 5,
        Autre = 6,
        Comercial = 7,
        Extern = 100,



    }
    public enum EnTypeRessource
    {
        Preforme = 1,
        Sucre = 2,
        Film_Thermofusible = 3,
        Film_Etirable = 4,
        Arrome = 5,
        Etiquette = 6,
        Bouchon = 7,
        Cole = 8,
        Bouteille_Verre = 9,
        Cannete = 10,
        Aspartam = 11,
        Acide_Citrique = 12,
        Consontre_Orange = 13,
        CO2 = 14,
        InterCalaire = 15,
        Colorant = 16,
        Benzoate = 17,
        Barquette = 18,
        Produit_Chimique = 19,
        Solvant = 20,
        Encre = 21,

        Prod_Chimique_Promain = 23,
        Prod_Chimique_Proneige_AL = 24,
        Prod_Chimique_Proneige_AC = 25,
        Prod_Chimique_OXIPRO_AS = 26,
        Prod_Chimique_Javel = 27,
        Prod_Chimique_Proflow = 28,
        Prod_Chimique_Acide_Chlorhydrique = 29,



    }
    #endregion

    #region Extentions
    public static class MyExtensions
    {/*
        public static void SetDateKeepTime(this DateTime? subject, DateTime dateToSet)
        {
            subject = Tools.GetFillDateFromDateTime(dateToSet, subject.Value.TimeOfDay);
        }

        public static void SetDateAndTime(this DateTime? subject, DateTime dateToSet, DateTime timeToSet)
        {
            subject = Tools.GetFillDateFromDateTime(dateToSet, timeToSet.TimeOfDay);
        }

        public static void SetTimeKeepDate(this DateTime? subject, DateTime TimeToSet)
        {
            subject = Tools.GetFillDateFromDateTime(subject, TimeToSet.TimeOfDay);
        }

        public static void SetDateKeepTime(this DateTime subject, DateTime dateToSet)
        {
            var newDate = Tools.GetFillDateFromDateTime(dateToSet, subject.TimeOfDay);
            subject.Date = newDate.Date;
        }

        public static void SetDateAndTime(this DateTime subject, DateTime dateToSet, DateTime timeToSet)
        {
            subject = Tools.GetFillDateFromDateTime(dateToSet, timeToSet.TimeOfDay);
        }

        public static void SetTimeKeepDate(this DateTime subject, DateTime TimeToSet)
        {
            subject = Tools.GetFillDateFromDateTime(subject, TimeToSet.TimeOfDay);
        }

        */

        public static bool SaveChangeTry(this ModelEntities db)
        {

            try
            {
                db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {

                Tools.ShowError(e.AllMessages("Impossible d'enregistrer"));
                return false;
            }

        }

        public static string ToAffInt(this decimal? deci, bool hideZero = false)
        {
            if (deci == null) return "";
            if (deci == 0 && hideZero) return "";
            return ((decimal)deci).ToString("N2").Replace(",0000", "").Replace(",000", "").Replace(",00", "");
        }

        public static string ToAffInt(this decimal deci, bool hideZero = false)
        {
            //  if (deci == null) return "";
            if (deci == 0 && hideZero) return "";
            return deci.ToString("N2").Replace(",0000", "").Replace(",000", "").Replace(",00", "");
        }

        public static string ToModePayementTxt(this int? modeInt)
        {
            if (modeInt == null)
            {
                return "à terme";
            }
            switch ((EnModePayement)modeInt)
            {
                case EnModePayement.Especes:
                    return "Especes";
                case EnModePayement.Avance:
                    return "Avance";
                case EnModePayement.Virement:
                    return "Virement";
                case EnModePayement.Chèque:
                    return "Chèque";
                case EnModePayement.Traite:
                    return "Traite";
                case EnModePayement.a_Terme:
                    return "à terme";
                default:
                    return "";
            }

        }

        public static string ToEtatBonAttributionTxt(this int? noBon)
        {
            switch ((EnEtatBonAttribution)noBon)
            {
                case EnEtatBonAttribution.EnInstance:
                    return "En Instance";
                case EnEtatBonAttribution.Livré:
                    return "Livré";
                case EnEtatBonAttribution.Annulé:
                    return "Annulé";
                case EnEtatBonAttribution.Chargé:
                    return "Chargé";
                default:
                    return "";
            }

        }

        public static string ToUniteProduitTxt(this int? unite)
        {
            switch ((EnUniteProduit)unite)
            {
                case EnUniteProduit.Palettes:
                    return "Palettes";
                case EnUniteProduit.Fardeaux:
                    return "Fardeaux";

                default: return "";

            }


        }

        public static string MinutesToHoursMinutesTxt(this int? minutes)
        {
            string result = "";
            if (minutes == null)
            {
                return "";
            }
            if (minutes == 0)
            {
                return "0 min";
            }
            var hour = (minutes / 60);
            var restMinutes = (minutes % 60);

            if (hour != 0)
            {
                result += hour.ToString() + " h ";
            }

            if (restMinutes != 0)
            {
                result += restMinutes.ToString() + " min";
            }

            return result;

        }
        public static int? GetTotalMinutesFromHeureTxt(this string heuretxt)
        {

            int? result = 0;

            if (!heuretxt.Contains("h"))
            {
                result = heuretxt.Replace("min", "").Replace(" ", "").ParseToInt();
            }
            else if (!heuretxt.Contains("min"))
            {
                result = 60 * heuretxt.Replace("h", "").Replace(" ", "").ParseToInt();
            }
            else
            {

                try
                {
                    string[] split = heuretxt.Replace(" ", "").Replace("h", "#").Replace("min", "").Split('#');

                    result = split[0].ParseToInt().Value * 60 + split[1].ParseToInt().Value;

                }
                catch (Exception)
                {

                    result = null;
                }
            }
            return result;
        }


        /*    public static bool EqualsRessource(this Ressource rs, Ressource ers)
            {
                return
                    rs.TypeRessourceID == ers.TypeRessourceID &&
                    rs.FournisseurID == ers.FournisseurID &&
                    rs.TypePreformeID == ers.TypePreformeID &&
                    rs.TypeArromeID == ers.TypeArromeID &&
                    rs.FormatID == ers.FormatID &&
                    rs.TypeBouchonID == ers.TypeBouchonID &&
                    rs.TypeCole == ers.TypeCole &&
                    rs.IntercalaireFormat == ers.IntercalaireFormat &&
                    rs.CouleurBouchon == ers.CouleurBouchon &&
                    rs.TypeColorantID == ers.TypeColorantID &&
                    rs.TypeEtiquetteID == ers.TypeEtiquetteID &&
                    rs.EpaisseurFilmThermo == ers.EpaisseurFilmThermo &&
                    rs.GoutProduitID == ers.GoutProduitID &&
                    rs.TypeProduitChimiqueID == ers.TypeProduitChimiqueID;
            }
         */
        public static string AllMessages(this Exception e, string firstMessage)
        {
            var message = firstMessage;
            var exeption = e;
            while (exeption != null)
            {
                message += "\n-> " + exeption.Message;
                exeption = exeption.InnerException;
            }

            return message;
        }



        #region DataGridView
        public static int? GetSelectedID(this DataGridView dgv, string columName = null)
        {
            if (dgv.RowCount == 0 || dgv.SelectedRows.Count == 0)
            {
                //  Tools.ShowError("Aucun élément n'est séléctionné");
                return null;
            }
            var name = (columName == null) ? "ID" : columName;
            return int.Parse(dgv.SelectedRows[0].Cells[name].Value.ToString());

        }
        public static int? GetSelectedIndex(this DataGridView dgv, bool showError = true)
        {
            if (dgv.RowCount == 0 || dgv.SelectedRows.Count == 0)
            {
                if (showError)
                {
                    Tools.ShowError("Aucun élément n'est séléctionné");
                }
                return null;
            }
            return dgv.SelectedRows[0].Index;

        }
        public static void HideIDColumn(this DataGridView dgv, string columName = null)
        {
            var name = (columName == null) ? "ID" : columName;
            dgv.Columns[name].Visible = false;
        }
        #endregion

        public static int? ParseToInt(this string number, bool acceptZero = true)
        {

            int outInt = 0;

            if (int.TryParse(number, out outInt))
            {
                if (outInt == 0)
                {
                    if (acceptZero)
                    {
                        return 0;
                    }
                    else
                    {
                        return null;
                    }
                }
                return outInt;
            }
            else
            {
                return null;
            }

        }
        public static decimal? ParseToDec(this string number, bool acceptZero = true)
        {

            decimal outDec = 0;

            if (decimal.TryParse(number, out outDec))
            {
                if (outDec == 0)
                {
                    if (acceptZero)
                    {
                        return 0;
                    }
                    else
                    {
                        return null;
                    }
                }
                return outDec;
            }
            else
            {
                return null;
            }

        }

        #region Dialog messages
        public static void ShowInformation(this Form form, string message)
        {
            MessageBox.Show(message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        public static void ShowError(this Form form, string message)
        {
            MessageBox.Show(message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        public static void ShowWarning(this Form form, string message)
        {
            MessageBox.Show(message, "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        public static bool ConfirmWarning(this Form form, string p)
        {

            DialogResult dl = MessageBox.Show(p, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            return (dl == DialogResult.Yes);

        }

        public static string NomClientAndParent(this Client client)
        {


            return client.Nom;// +" [" + client.ClientParent.Nom + "]";



        }

        public static bool ConfirmInformation(this Form form, string p)
        {

            DialogResult dl = MessageBox.Show(p, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

            return (dl == DialogResult.Yes);

        }
        #endregion

        #region Text

        public static string ToSectionTxt(this EnSection section)
        {


            switch (section)
            {
                case EnSection.Administration: return "Administration";

                case EnSection.Direction:
                    return "Administration";
                case EnSection.Unité_PET:
                    return "Unité PET";
                case EnSection.Unité_Canette:
                    return "Unité Canette";
                case EnSection.Magazin:
                    return "Magazin";
                case EnSection.Autre:
                    return "Autres Destinations";
                case EnSection.Comercial:
                    return "Comercial";
                case EnSection.Extern:
                    return "Externe";
                default:
                    return "";
            }
        }


        #endregion






    }
    #endregion

}
