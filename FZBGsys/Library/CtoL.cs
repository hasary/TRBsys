﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NsTRBsys
{
    class CtoL
    {
        private static string bas(long b)
        {
            switch (b)
            {
                case 1: return "un ";
                case 2: return "deux ";
                case 3: return "trois ";
                case 4: return "quatre ";
                case 5: return "cinq ";
                case 6: return "six ";
                case 7: return "sept ";
                case 8: return "huit ";
                case 9: return "neuf ";
                case 10: return "dix ";
                case 11: return "onze ";
                case 12: return "douze ";
                case 13: return "treize ";
                case 14: return "quatorze ";
                case 15: return "quinze ";
                case 16: return "seize ";
                case 17: return "dix-sept ";
                case 18: return "dix-huit ";
                case 19: return "dix-neuf ";
                case 20: return "vingt ";
                case 30: return "trente ";
                case 40: return "quarante ";
                case 50: return "cinquante ";
                case 60: return "soixante ";
                case 80: return "quatre-vingt ";
                case 100: return "cent ";

                default: return "";
            }
        }

        public static string convert(Int64 n)
        {
            string lett = "";
            if (n == 0) return "zero ";
            lett += (n / 1000000000 != 0) ? lett += convert(n / 1000000000) + "milliard" + ((n / 1000000000 - 1 != 0) ? "s " : " ") : "";
            n %= 1000000000;
            lett += (n / 1000000 != 0) ? convert(n / 1000000) + "million" + ((n / 1000000 - 1 != 0) ? "s " : " ") : "";
            n %= 1000000;
            lett += (n / 1000 != 0) ? ((n / 1000 != 1) ? convert(n / 1000) : "") + "mille " : "";
            n %= 1000;
            lett += (n / 100 > 1) ? bas(n / 100) : "";
            lett += (n / 100 != 0) ? bas(100) : "";
            lett += ((n / 10 % 10 - 7) * (n / 10 % 10 - 9) * (n / 10 % 10 - 1) * (n / 10 % 10) != 0) ? bas(n / 10 % 10 * 10) : bas(n / 10 % 10 * 10 - 10);
            lett += (n % 10 - 1 != 0) ? "" : ((n / 10 % 10 < 2) ? "" : "et ");
            lett += ((n / 10 % 10 - 7) * (n / 10 % 10 - 9) * (n / 10 % 10 - 1) != 0) ? bas(n % 10) : bas(n % 10 + 10);///

            return lett;
        }

        public static string convertMontant(decimal montant)
        {
            string txt = "";
        
            int dinards = (int)decimal.Floor(montant);
            decimal centimes = Math.Abs((int)montant - montant) * 100;

            txt += convert(dinards) + " dinars";
            if (centimes == 0)
            {
                txt += ".";
            }
            else
            {
                txt += " ," + convert((int)centimes) + " centimes.";
            }


            return txt;
        }

    }
}
