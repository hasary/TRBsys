﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;

namespace FZBGsys.NsStock
{
    public partial class FrArrivage : Form
    {
        ModelEntities db = new ModelEntities();

        public FrArrivage()
        {
            InitializeComponent();
            InitialiseControls();
            this.CurrentDateTime = Tools.GetServerDateTime();
        }

        private void InitialiseControls()
        {


            //---------------------- Fournisseur
            cbFournisseur.Items.Add(new Fournisseur { ID = 0, Nom = "" });
            // cbFournisseur.Items.AddRange(db.Fournisseurs.Where(p => p.ID > 0 ).ToArray());
            cbFournisseur.DisplayMember = "Nom";
            cbFournisseur.ValueMember = "ID";
            cbFournisseur.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFournisseur.SelectedIndex = 0;

            //---------------------- TypeArome
            cbArrome.Items.Add(new TypeArrome { ID = 0, Nom = "" });
            cbArrome.Items.AddRange(db.TypeArromes.Where(p => p.ID > 0).ToArray());
            cbArrome.DisplayMember = "Nom";
            cbArrome.ValueMember = "ID";
            cbArrome.DropDownStyle = ComboBoxStyle.DropDownList;
            cbArrome.SelectedItem = null;


            //---------------------- Gamme Bouteille
            cbGamme.Items.Add(new PreformeGamme { ID = 0, GRS = "" });
            cbGamme.Items.AddRange(db.PreformeGammes.Where(p => p.ID > 0).ToArray());
            cbGamme.DisplayMember = "GRS";
            cbGamme.ValueMember = "ID";
            cbGamme.DropDownStyle = ComboBoxStyle.DropDownList;
            cbGamme.SelectedItem = null;



            //---------------------- Format (Bouteilles)
            cbFormat.Items.Add(new Format { ID = 0, Volume = "" });
            cbFormat.Items.AddRange(db.Formats.Where(p => p.ID > 0).ToArray());
            cbFormat.DisplayMember = "Volume";
            cbFormat.ValueMember = "ID";
            cbFormat.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFormat.SelectedItem = null;


            //---------------------- Format (intercalaire)
            cbFormatIntercalaire.Items.Add("");
            cbFormatIntercalaire.Items.Add("Format 1000");
            cbFormatIntercalaire.Items.Add("Format 1200");
            cbFormatIntercalaire.SelectedIndex = 0;
            cbFormatIntercalaire.DropDownStyle = ComboBoxStyle.DropDownList;


            //---------------------- Colorant
            cbTypeColorant.Items.Add(new TypeColorant { ID = 0, Code = "" });
            cbTypeColorant.Items.AddRange(db.TypeColorants.Where(p => p.ID > 0).ToArray());
            cbTypeColorant.DisplayMember = "Code";
            cbTypeColorant.ValueMember = "ID";
            cbTypeColorant.DropDownStyle = ComboBoxStyle.DropDownList;
            cbTypeColorant.SelectedItem = null;




            //-------------------------------------  Cole
            cbTypeCole.Items.Add("");
            cbTypeCole.Items.Add("Chad");
            cbTypeCole.Items.Add("Froid");
            cbTypeCole.SelectedIndex = 0;
            //-------------------------------------


            //---------------------- Couleur (Bouchon)
            cbCouleurBouchon.Items.Add("");
            cbCouleurBouchon.Items.Add("Bleu");
            cbCouleurBouchon.Items.Add("Blanc");
            cbCouleurBouchon.Items.Add("Rouge");
            cbCouleurBouchon.Items.Add("Noir");
            cbCouleurBouchon.Items.Add("Orange");
            cbCouleurBouchon.Items.Add("Jaune");
            cbCouleurBouchon.SelectedIndex = 0;

            // - ----------------------------- Type bouchon

            cbTypeBouchon.Items.Add(new TypeBouchon { ID = 0, Nom = "" });
            cbTypeBouchon.Items.AddRange(db.TypeBouchons.Where(p => p.ID > 0).ToArray());
            cbTypeBouchon.DisplayMember = "Nom";
            cbTypeBouchon.ValueMember = "ID";
            cbTypeBouchon.DropDownStyle = ComboBoxStyle.DropDownList;
            cbTypeBouchon.SelectedItem = null;


            // - ----------------------------- Type Etiquette

            cbTypeEtiquette.Items.Add(new TypeEtiquette { ID = 0, Nom = "" });
            cbTypeEtiquette.Items.AddRange(db.TypeEtiquettes.Where(p => p.ID > 0).ToArray());
            cbTypeEtiquette.DisplayMember = "Nom";
            cbTypeEtiquette.ValueMember = "ID";
            cbTypeEtiquette.DropDownStyle = ComboBoxStyle.DropDownList;
            cbTypeEtiquette.SelectedItem = null;


            //---------------------- Categorie (type Ressource) ---------------------------------------------------------------
            cbCategorie.Items.Add(new TypeRessource { ID = 0, Nom = "" });
            cbCategorie.Items.AddRange(db.TypeRessources.Where(p => p.ID > 0).ToArray());
            cbCategorie.DisplayMember = "Nom";
            cbCategorie.ValueMember = "ID";
            cbCategorie.DropDownStyle = ComboBoxStyle.DropDownList;
            cbCategorie.SelectedIndex = 0;




        }



        private void cbCategorie_SelectedIndexChanged(object sender, EventArgs e)
        {
            var ressource = (TypeRessource)cbCategorie.SelectedItem;
            labUniteDeMesure.Text = ressource.UniteDeMesure;
            labUniteDePiece.Text = ressource.UniteDePiece;
            if (ressource != null)
            {

                cbFournisseur.Items.Clear();
                cbFournisseur.Items.Add(new Fournisseur { ID = 0, Nom = "" });
                cbFournisseur.Items.AddRange(db.Fournisseurs.ToList().Where(p => p.ID > 0 && p.TypeRessources.Contains(ressource)).ToArray());

            }

            Clear();


            if (cbCategorie.SelectedItem == null)
            {
                return;
            }

            switch ((EnTypeRessource)((TypeRessource)cbCategorie.SelectedItem).ID)
            {
                case EnTypeRessource.Preforme:
                    panGamme.Visible = true;
                    // panFormat.Visible = true;

                    break;
                case EnTypeRessource.Sucre:
                    break;
                case EnTypeRessource.FilmThermo:
                    break;
                case EnTypeRessource.Film:
                    break;
                case EnTypeRessource.Arrome:
                    panTypeArome.Visible = true;
                    break;
                case EnTypeRessource.Etiquette:
                    panTypeArome.Visible = true;
                    panFormat.Visible = true;
                    panTypeEtiquette.Visible = true;
                    break;
                case EnTypeRessource.Intercalaire:
                    panInterCalaireFormat.Visible = true;
                    break;
                case EnTypeRessource.Bouchon:
                    panBouchon.Visible = true;
                    break;
                case EnTypeRessource.Cole:
                    PanCole.Visible = true;
                    break;
                case EnTypeRessource.Bouteille_Verre:
                    panFormat.Visible = true;
                    break;
                case EnTypeRessource.Cannete:
                    panTypeArome.Visible = true;
                    panFormat.Visible = true;
                    break;
                case EnTypeRessource.Aspartam:
                    break;
                case EnTypeRessource.AcideCitrique:
                    break;
                case EnTypeRessource.Consontré:
                    break;
                case EnTypeRessource.CO2:
                    break;
                case EnTypeRessource.Colorant:
                    panTypeColorant.Visible = true;
                    break;
                default:
                    break;
            }
        }

        private void Clear()
        {
            panBouchon.Visible = false;
            PanCole.Visible = false;
            panFormat.Visible = false;
            panGamme.Visible = false;
            panInterCalaireFormat.Visible = false;
            panTypeArome.Visible = false;
            panTypeColorant.Visible = false;
            panTypeEtiquette.Visible = false;
            cbArrome.SelectedIndex = 0;
            cbCouleurBouchon.SelectedIndex = 0;
            cbFormat.SelectedIndex = 0;
            cbFormatIntercalaire.SelectedIndex = 0;
            cbFournisseur.SelectedIndex = 0;
            cbGamme.SelectedIndex = 0;
            cbTypeBouchon.SelectedIndex = 0;
            cbTypeCole.SelectedIndex = 0;
            txtPiece.Text = "";
            txtQuantite.Text = "";

        }

        private void FrArrivage_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        private void txtPiece_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != 8 && !Char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private bool isValidAll()
        {
            string message = "";

            if (cbFournisseur.SelectedIndex == 0)
            {
                message += "Selectionner un fournisseur!";
            }

            if (cbCategorie.SelectedIndex == 0)
            {
                message += "\nSelectionner une Categorie!";
            }

            if (cbArrome.SelectedIndex < 1 && panTypeArome.Visible)
            {
                message += "\nSelectionner un Type Arrome!";
            }


            if (cbFormat.SelectedIndex < 1 && panFormat.Visible)
            {
                message += "\nSelectionner un Format!";
            }

            if (chDateFab.Checked && dtFab.Value >= dtDate.Value)
            {
                message += "\nVerifiez les dates !!";
            }

            if (cbGamme.SelectedIndex < 1 && panGamme.Visible)
            {
                message += "\nSelectionner une Gamme!";
            }

            if (cbTypeBouchon.SelectedIndex < 1 && panBouchon.Visible)
            {
                message += "\nSelectionner un Type Bouchon!";
            }

            if (cbCouleurBouchon.SelectedIndex < 1 && panBouchon.Visible)
            {
                message += "\nSelectionner une Couleur!";
            }

            if (cbFormatIntercalaire.SelectedIndex < 1 && panInterCalaireFormat.Visible)
            {
                message += "\nSelectionner un Type intercalaire!";
            }

            if (cbTypeCole.SelectedIndex < 1 && PanCole.Visible)
            {
                message += "\nSelectionner un Type Cole!";
            }
            decimal outdec;
            int outInt;

            if (!decimal.TryParse(txtQuantite.Text.Trim(), out outdec))
            {
                message += "\nQunatite non valide!";
            }

            if (!int.TryParse(txtPiece.Text.Trim(), out outInt))
            {
                message += "\nNombre non valide!";
            }

            if (message != "")
            {
                this.ShowWarning(message);
                return false;
            }

            return true;
        }


        private void txtQuantite_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != 8 && e.KeyChar != ',' && e.KeyChar != '.' && !Char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.KeyChar = ',';
            }
        }

        private void btAjouter_Click(object sender, EventArgs e)
        {
            if (!isValidAll())
            {
                return;
            }

            //gig  //-- Enlever ne supprime pas quand enregistrer !!!!
            //
            // -----------------------------------------

            var ressource = CreateOrGetRessource();


            var newArrivage = new Arrivage();

            newArrivage.Ressource = ressource;
            newArrivage.DateArrivage = dtDate.Value;
            if (chDateFab.Checked) newArrivage.DateFabrication = dtFab.Value.Date;
            newArrivage.Quantite = decimal.Parse(txtQuantite.Text.Trim());
            newArrivage.Piece = int.Parse(txtPiece.Text.Trim());

            ListToSave.Add(newArrivage);
            db.AddToArrivages(newArrivage);

            cbCategorie.SelectedIndex = 0;
            Clear();

            RefreshGrid();

            // -----------------------------------------



        }

        private Ressource CreateOrGetRessource()
        {

            var existingDB = db.Ressources.ToList().Where(rs =>
                rs.TypeRessourceID == ((TypeRessource)cbCategorie.SelectedItem).ID &&
                rs.FournisseurID == ((Fournisseur)cbFournisseur.SelectedItem).ID &&
               (!panGamme.Visible || rs.PreformeGammeID == ((PreformeGamme)cbGamme.SelectedItem).ID) &&

                (!panTypeArome.Visible || rs.TypeArromeID == ((TypeArrome)cbArrome.SelectedItem).ID) &&
                (!panFormat.Visible || rs.FormatID == ((Format)cbFormat.SelectedItem).ID) &&
                (!panBouchon.Visible || rs.TypeBouchonID == ((TypeBouchon)cbTypeBouchon.SelectedItem).ID) &&
                (!PanCole.Visible || rs.TypeCole == cbTypeCole.SelectedItem.ToString()) &&
                (!panInterCalaireFormat.Visible || rs.IntercalaireFormat == cbFormatIntercalaire.SelectedItem.ToString()) &&
                (!panBouchon.Visible || rs.CouleurBouchon == cbCouleurBouchon.SelectedItem.ToString()) &&
                (!panTypeColorant.Visible || rs.TypeColorantID == ((TypeColorant)cbTypeColorant.SelectedItem).ID) &&
                (!panTypeEtiquette.Visible || rs.TypeEtiquetteID == ((TypeEtiquette)cbTypeEtiquette.SelectedItem).ID)

                );

            if (existingDB.Count() == 1)
            {
                return existingDB.First();
            }
            else if (existingDB.Count() > 1)
            {
                this.ShowError("Multiple Ressources Exists");
                return null;
            }

            var existingList = ListToSave.ToList().Where(rs =>
               rs.Ressource.TypeRessourceID == ((TypeRessource)cbCategorie.SelectedItem).ID &&
               rs.Ressource.FournisseurID == ((Fournisseur)cbFournisseur.SelectedItem).ID &&
               (!panGamme.Visible || rs.Ressource.PreformeGammeID == ((PreformeGamme)cbGamme.SelectedItem).ID) &&

               (!panTypeArome.Visible || rs.Ressource.TypeArromeID == ((TypeArrome)cbArrome.SelectedItem).ID) &&
               (!panFormat.Visible || rs.Ressource.FormatID == ((Format)cbFormat.SelectedItem).ID) &&
               (!panBouchon.Visible || rs.Ressource.TypeBouchonID == ((TypeBouchon)cbTypeBouchon.SelectedItem).ID) &&
               (!PanCole.Visible || rs.Ressource.TypeCole == cbTypeCole.SelectedItem.ToString()) &&
               (!panInterCalaireFormat.Visible || rs.Ressource.IntercalaireFormat == cbFormatIntercalaire.SelectedItem.ToString()) &&
               (!panBouchon.Visible || rs.Ressource.CouleurBouchon == cbCouleurBouchon.SelectedItem.ToString()) &&
               (!panTypeColorant.Visible || rs.Ressource.TypeColorantID == ((TypeColorant)cbTypeColorant.SelectedItem).ID) &&
               (!panTypeEtiquette.Visible || rs.Ressource.TypeEtiquetteID == ((TypeEtiquette)cbTypeEtiquette.SelectedItem).ID)

               );


            if (existingList.Count() == 1)
            {
                return existingList.First().Ressource;
            }

            var ressource = new Ressource();
            ressource.FournisseurID = ((Fournisseur)cbFournisseur.SelectedItem).ID;
            if (panBouchon.Visible) ressource.CouleurBouchon = cbCouleurBouchon.SelectedItem.ToString();
            if (panFormat.Visible) ressource.Format = (Format)cbFormat.SelectedItem;
            if (panInterCalaireFormat.Visible) ressource.IntercalaireFormat = cbFormatIntercalaire.SelectedItem.ToString();
            if (panGamme.Visible) ressource.PreformeGamme = (PreformeGamme)cbGamme.SelectedItem;
            if (panTypeArome.Visible) ressource.TypeArrome = (TypeArrome)cbArrome.SelectedItem; else ressource.TypeArromeID = 0;
            if (panTypeColorant.Visible) ressource.TypeColorant = (TypeColorant)cbArrome.SelectedItem; else ressource.TypeColorantID = 0;
            if (panTypeEtiquette.Visible) ressource.TypeEtiquette = (TypeEtiquette)cbTypeEtiquette.SelectedItem;

            if (panBouchon.Visible) ressource.TypeBouchon = (TypeBouchon)cbTypeBouchon.SelectedItem;
            if (PanCole.Visible) ressource.TypeCole = cbTypeCole.SelectedItem.ToString();

            ressource.TypeRessource = (TypeRessource)cbCategorie.SelectedItem;

            return ressource;
        }

        private List<Arrivage> ListToSave = new List<Arrivage>();


        private void RefreshGrid()
        {
            dgv.DataSource = ListToSave.Select(p => new
            {
                ID = p.ID,
                Date = p.DateArrivage.Value.ToShortDateString(),
                Fournisseur = p.Ressource.Fournisseur.Nom,
                Descption = p.Ressource.ToDescription(),
                Fabriqué = (p.DateFabrication != null) ? p.DateFabrication.Value.ToShortDateString() : "",
                Nombre = p.Piece + " " + p.Ressource.TypeRessource.UniteDePiece + "s",
                Quantite = p.Quantite + " " + p.Ressource.TypeRessource.UniteDeMesure
            }
            ).ToList();

            dgv.Columns["ID"].Visible = false;
        }

        private void btEnlever_Click(object sender, EventArgs e)
        {
            var index = dgv.SelectedRows[0].Index;
            var toDel = ListToSave.ElementAt(index);
            ListToSave.Remove(toDel);
            db.DeleteObject(toDel.Ressource);
            db.DeleteObject(toDel);

            RefreshGrid();
        }

        private void btAnnuler_Click(object sender, EventArgs e)
        {
            if (ListToSave == null || ListToSave.Count == 0) Dispose();
            else
                if (this.ConfirmWarning("Annuler tout et fermer ?\n(ATTENTION: rien ne sera enregistré!)"))
                {
                    Dispose();
                }
        }

        private void btEnregistrer_Click(object sender, EventArgs e)
        {
            if (ListToSave == null || ListToSave.Count == 0)
            {
                this.ShowInformation("La liste est vide, remplire d'abord avec \"Ajouter\" ");
                return;
            }


                  FZBGsys.NsStock.FrStockMatiere.AddRangeToStockMT(ListToSave, db);

            try
            {
                db.SaveChanges();
                Dispose();
            }
            catch (Exception exp)
            {

                this.ShowError(exp.AllMessages("Impossible d'enregistrer:"));
            }


        }



        public DateTime CurrentDateTime { get; set; }

        private void chDateFab_CheckedChanged(object sender, EventArgs e)
        {
            dtFab.Visible = chDateFab.Checked;
        }

        private void txtPiece_TextChanged(object sender, EventArgs e)
        {
            var ressource = (TypeRessource)cbCategorie.SelectedItem;

            if (ressource.QuantiteParPieceFix == true)
            {
                txtQuantite.Text = (ressource.QuantiteParPiece * txtPiece.Text.ParseToInt()).ToString();
            }

        }
    }
}
