﻿namespace FZBGsys.Stock
{
    partial class FrTransfert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.labUniteDePiece = new System.Windows.Forms.Label();
            this.btAjouter = new System.Windows.Forms.Button();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.txtPiece = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btEnlever = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.labUniteDeMesure = new System.Windows.Forms.Label();
            this.txtQuantite = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.xpannel1 = new System.Windows.Forms.Panel();
            this.btAnnuler = new System.Windows.Forms.Button();
            this.btEnregistrer = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btOuvrirStock = new System.Windows.Forms.Button();
            this.txtMatiere = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDateFabriq = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDateArrivage = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtAromsa = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPiceEnStock = new System.Windows.Forms.TextBox();
            this.labUniteDePiece2 = new System.Windows.Forms.Label();
            this.txtQttEnStock = new System.Windows.Forms.TextBox();
            this.labUniteDeMesure2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.xpannel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(319, 24);
            this.dgv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 16;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(548, 467);
            this.dgv.TabIndex = 7;
            // 
            // labUniteDePiece
            // 
            this.labUniteDePiece.AutoSize = true;
            this.labUniteDePiece.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labUniteDePiece.Location = new System.Drawing.Point(250, 429);
            this.labUniteDePiece.Name = "labUniteDePiece";
            this.labUniteDePiece.Size = new System.Drawing.Size(56, 17);
            this.labUniteDePiece.TabIndex = 24;
            this.labUniteDePiece.Text = "boxBid";
            // 
            // btAjouter
            // 
            this.btAjouter.Location = new System.Drawing.Point(180, 501);
            this.btAjouter.Name = "btAjouter";
            this.btAjouter.Size = new System.Drawing.Size(133, 31);
            this.btAjouter.TabIndex = 28;
            this.btAjouter.Text = "Ajouter >>";
            this.btAjouter.UseVisualStyleBackColor = true;
            // 
            // dtDate
            // 
            this.dtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDate.Location = new System.Drawing.Point(165, 24);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(132, 22);
            this.dtDate.TabIndex = 18;
            // 
            // txtPiece
            // 
            this.txtPiece.Location = new System.Drawing.Point(154, 429);
            this.txtPiece.Name = "txtPiece";
            this.txtPiece.Size = new System.Drawing.Size(90, 22);
            this.txtPiece.TabIndex = 26;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(69, 432);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 17);
            this.label10.TabIndex = 25;
            this.label10.Text = "Nombre:";
            // 
            // btEnlever
            // 
            this.btEnlever.Location = new System.Drawing.Point(319, 501);
            this.btEnlever.Name = "btEnlever";
            this.btEnlever.Size = new System.Drawing.Size(111, 31);
            this.btEnlever.TabIndex = 20;
            this.btEnlever.Text = "<< Enlever";
            this.btEnlever.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 17);
            this.label1.TabIndex = 19;
            this.label1.Text = "Date Transfert:";
            // 
            // labUniteDeMesure
            // 
            this.labUniteDeMesure.AutoSize = true;
            this.labUniteDeMesure.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labUniteDeMesure.Location = new System.Drawing.Point(250, 467);
            this.labUniteDeMesure.Name = "labUniteDeMesure";
            this.labUniteDeMesure.Size = new System.Drawing.Size(31, 17);
            this.labUniteDeMesure.TabIndex = 23;
            this.labUniteDeMesure.Text = "UM";
            // 
            // txtQuantite
            // 
            this.txtQuantite.Location = new System.Drawing.Point(154, 464);
            this.txtQuantite.Name = "txtQuantite";
            this.txtQuantite.Size = new System.Drawing.Size(90, 22);
            this.txtQuantite.TabIndex = 27;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(65, 467);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 17);
            this.label4.TabIndex = 22;
            this.label4.Text = "Quantite:";
            // 
            // xpannel1
            // 
            this.xpannel1.Controls.Add(this.btAnnuler);
            this.xpannel1.Controls.Add(this.btEnregistrer);
            this.xpannel1.Location = new System.Drawing.Point(531, 498);
            this.xpannel1.Name = "xpannel1";
            this.xpannel1.Size = new System.Drawing.Size(336, 43);
            this.xpannel1.TabIndex = 21;
            // 
            // btAnnuler
            // 
            this.btAnnuler.Location = new System.Drawing.Point(68, 3);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(132, 33);
            this.btAnnuler.TabIndex = 4;
            this.btAnnuler.Text = "Annuler";
            this.btAnnuler.UseVisualStyleBackColor = true;
            // 
            // btEnregistrer
            // 
            this.btEnregistrer.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEnregistrer.Location = new System.Drawing.Point(206, 3);
            this.btEnregistrer.Name = "btEnregistrer";
            this.btEnregistrer.Size = new System.Drawing.Size(127, 33);
            this.btEnregistrer.TabIndex = 3;
            this.btEnregistrer.Text = "Enregistrer";
            this.btEnregistrer.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labUniteDePiece2);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtPiceEnStock);
            this.groupBox1.Controls.Add(this.labUniteDeMesure2);
            this.groupBox1.Controls.Add(this.txtDateArrivage);
            this.groupBox1.Controls.Add(this.txtQttEnStock);
            this.groupBox1.Controls.Add(this.txtDateFabriq);
            this.groupBox1.Controls.Add(this.txtAromsa);
            this.groupBox1.Controls.Add(this.txtMatiere);
            this.groupBox1.Controls.Add(this.btOuvrirStock);
            this.groupBox1.Location = new System.Drawing.Point(20, 71);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(293, 343);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Matiere Premiere";
            // 
            // btOuvrirStock
            // 
            this.btOuvrirStock.Location = new System.Drawing.Point(98, 33);
            this.btOuvrirStock.Name = "btOuvrirStock";
            this.btOuvrirStock.Size = new System.Drawing.Size(126, 28);
            this.btOuvrirStock.TabIndex = 1;
            this.btOuvrirStock.Text = "Ouvrir le Stock...";
            this.btOuvrirStock.UseVisualStyleBackColor = true;
            // 
            // txtMatiere
            // 
            this.txtMatiere.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMatiere.Location = new System.Drawing.Point(19, 99);
            this.txtMatiere.Name = "txtMatiere";
            this.txtMatiere.ReadOnly = true;
            this.txtMatiere.Size = new System.Drawing.Size(258, 22);
            this.txtMatiere.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Matiere";
            // 
            // txtDateFabriq
            // 
            this.txtDateFabriq.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDateFabriq.Location = new System.Drawing.Point(134, 194);
            this.txtDateFabriq.Name = "txtDateFabriq";
            this.txtDateFabriq.ReadOnly = true;
            this.txtDateFabriq.Size = new System.Drawing.Size(90, 22);
            this.txtDateFabriq.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 194);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 17);
            this.label5.TabIndex = 3;
            this.label5.Text = "Date Fabrication";
            // 
            // txtDateArrivage
            // 
            this.txtDateArrivage.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDateArrivage.Location = new System.Drawing.Point(134, 230);
            this.txtDateArrivage.Name = "txtDateArrivage";
            this.txtDateArrivage.ReadOnly = true;
            this.txtDateArrivage.Size = new System.Drawing.Size(90, 22);
            this.txtDateArrivage.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 230);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 17);
            this.label6.TabIndex = 3;
            this.label6.Text = "Date Arrivage";
            // 
            // txtAromsa
            // 
            this.txtAromsa.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAromsa.Location = new System.Drawing.Point(19, 149);
            this.txtAromsa.Name = "txtAromsa";
            this.txtAromsa.ReadOnly = true;
            this.txtAromsa.Size = new System.Drawing.Size(258, 22);
            this.txtAromsa.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 129);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 17);
            this.label7.TabIndex = 3;
            this.label7.Text = "Fournisseur";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Selection:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 270);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(122, 17);
            this.label8.TabIndex = 5;
            this.label8.Text = "Quantite En Stock";
            // 
            // txtPiceEnStock
            // 
            this.txtPiceEnStock.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPiceEnStock.Location = new System.Drawing.Point(134, 270);
            this.txtPiceEnStock.Name = "txtPiceEnStock";
            this.txtPiceEnStock.ReadOnly = true;
            this.txtPiceEnStock.Size = new System.Drawing.Size(90, 22);
            this.txtPiceEnStock.TabIndex = 2;
            // 
            // labUniteDePiece2
            // 
            this.labUniteDePiece2.AutoSize = true;
            this.labUniteDePiece2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labUniteDePiece2.Location = new System.Drawing.Point(230, 273);
            this.labUniteDePiece2.Name = "labUniteDePiece2";
            this.labUniteDePiece2.Size = new System.Drawing.Size(56, 17);
            this.labUniteDePiece2.TabIndex = 25;
            this.labUniteDePiece2.Text = "boxBid";
            // 
            // txtQttEnStock
            // 
            this.txtQttEnStock.Location = new System.Drawing.Point(134, 298);
            this.txtQttEnStock.Name = "txtQttEnStock";
            this.txtQttEnStock.ReadOnly = true;
            this.txtQttEnStock.Size = new System.Drawing.Size(90, 22);
            this.txtQttEnStock.TabIndex = 27;
            // 
            // labUniteDeMesure2
            // 
            this.labUniteDeMesure2.AutoSize = true;
            this.labUniteDeMesure2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labUniteDeMesure2.Location = new System.Drawing.Point(230, 301);
            this.labUniteDeMesure2.Name = "labUniteDeMesure2";
            this.labUniteDeMesure2.Size = new System.Drawing.Size(31, 17);
            this.labUniteDeMesure2.TabIndex = 23;
            this.labUniteDeMesure2.Text = "UM";
            // 
            // FrTransfert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(879, 553);
            this.Controls.Add(this.dtDate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.labUniteDePiece);
            this.Controls.Add(this.btAjouter);
            this.Controls.Add(this.txtPiece);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.btEnlever);
            this.Controls.Add(this.labUniteDeMesure);
            this.Controls.Add(this.txtQuantite);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.xpannel1);
            this.Controls.Add(this.dgv);
            this.Name = "FrTransfert";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Transfert Matrire Première";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.xpannel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Label labUniteDePiece;
        private System.Windows.Forms.Button btAjouter;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.TextBox txtPiece;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btEnlever;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labUniteDeMesure;
        private System.Windows.Forms.TextBox txtQuantite;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel xpannel1;
        private System.Windows.Forms.Button btAnnuler;
        private System.Windows.Forms.Button btEnregistrer;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labUniteDePiece2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPiceEnStock;
        private System.Windows.Forms.Label labUniteDeMesure2;
        private System.Windows.Forms.TextBox txtDateArrivage;
        private System.Windows.Forms.TextBox txtQttEnStock;
        private System.Windows.Forms.TextBox txtDateFabriq;
        private System.Windows.Forms.TextBox txtAromsa;
        private System.Windows.Forms.TextBox txtMatiere;
        private System.Windows.Forms.Button btOuvrirStock;
    }
}