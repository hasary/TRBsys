﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsStock
{
    public partial class FrStockMatiere : Form
    {
        ModelEntities db = new ModelEntities();
        public FrStockMatiere()
        {
            InitializeComponent();
            InitializeControls();
        }

        private void InitializeControls()
        {
            //----------------------- Fournisseur --------------------------------------------------
            cbFournisseur.Items.Add(new Fournisseur { ID = 0, Nom = "[Tout]" });
            cbFournisseur.Items.AddRange(db.Fournisseurs.Where(p => p.ID > 0).ToArray());
            cbFournisseur.DisplayMember = "Nom";
            cbFournisseur.ValueMember = "ID";
            cbFournisseur.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFournisseur.SelectedIndex = 0;


            //---------------------- Categorie (type Ressource) -------------------------------------
            cbCategorie.Items.Add(new TypeRessource { ID = 0, Nom = "[Tout]" });
            cbCategorie.Items.AddRange(db.TypeRessources.Where(p => p.ID > 0).ToArray());
            cbCategorie.DisplayMember = "Nom";
            cbCategorie.ValueMember = "ID";
            cbCategorie.DropDownStyle = ComboBoxStyle.DropDownList;
            cbCategorie.SelectedIndex = 0;


            //----------------------------------------------------------------------------------------
        }

        private void FrListArrivage_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }
        private void btRecherche_Click(object sender, EventArgs e)
        {
            string filterCat = "", filterFour = "", filterDate = "";
            ListFound = null;
            IQueryable<StockMatiere> result = db.StockMatieres;


            if (chConsome.Checked && !chEnstock.Checked)
            {
                result = result.Where(p => p.DateSortie <= dtAu.Value.Date && p.DateSortie >= dtDu.Value.Date);
                filterDate = (dtDu.Value.Date == dtAu.Value.Date) ?
                    "sortie le " + dtDu.Value.ToShortDateString() : "sortie entre " + dtDu.Value.Date.ToShortDateString() + " et " + dtAu.Value.ToShortDateString();
            }
            else if (!chConsome.Checked && chEnstock.Checked)
            {
                result = result.Where(p => p.EnStock == true);
                filterDate = "Matière en Stock uniquement";

            }
            else if (chConsome.Checked && chEnstock.Checked)
            {
                result = result.Where(p => p.EnStock == true || (p.DateSortie <= dtAu.Value.Date && p.DateSortie >= dtDu.Value.Date));
                filterDate = "Matière en Stock ou ";
                filterDate += (dtDu.Value.Date == dtAu.Value.Date) ?
                    "sortie le " + dtDu.Value.ToShortDateString() : "sortie entre " + dtDu.Value.Date.ToShortDateString() + " et " + dtAu.Value.ToShortDateString();
            }
            else
            {
                return;
            }

            if (cbCategorie.SelectedIndex != 0)
            {
                //var idCat = int.Parse(cbCategorie.SelectedValue.ToString());
                var selected = (TypeRessource)cbCategorie.SelectedItem;
                result = result.Where(p => p.Ressource.TypeRessourceID == selected.ID);
                filterCat = selected.Nom;

            }


            if (cbFournisseur.SelectedIndex != 0)
            {
                //var idCat = int.Parse(cbCategorie.SelectedValue.ToString());
                var selected = (Fournisseur)cbFournisseur.SelectedItem;
                filterFour = "fournisseur " + selected.Nom;
                result = result.Where(p => p.Ressource.FournisseurID == selected.ID);

            }

            this.ListFound = result.ToList();

            RrefreshGrid();
            labFilterTxt.Text = filterDate + " " + filterCat + " " + filterFour;

        }

        private void RrefreshGrid()
        {
            dgv.DataSource = ListFound.Select(p => new
            {
                ID = p.ID,
                Descption = p.Ressource.ToDescription(),
                Fournisseur = p.Ressource.Fournisseur.Nom,

                Nombre = p.Piece.Value + " " + p.Ressource.TypeRessource.UniteDePiece + "s",
                Quantite = p.Quantite.ToAffInt() + " " + p.Ressource.TypeRessource.UniteDeMesure,
                Etat = (p.EnStock.Value) ? "En Stock" : "Sortie: " + p.DateSortie.Value.ToShortDateString(),
            }
              ).ToList();

            dgv.Columns["ID"].Visible = false;
        }

        public List<StockMatiere> ListFound { get; set; }
        private void btDelete_Click(object sender, EventArgs e)
        {
            var id = dgv.GetSelectedID();

            var toDel = db.Arrivages.SingleOrDefault(p => p.ID == id);
            if (toDel != null)
            {
                if (this.ConfirmWarning("Confirmer suppression élement?"))
                {
                    try
                    {
                        db.DeleteObject(toDel);
                        db.SaveChanges();
                    }
                    catch (Exception exp)
                    {

                        this.ShowError(exp.AllMessages("Impossible de supprimer un element!"));
                    }
                }
            }
            else
            {
                this.ShowError("element introuvable!");
            }



            var index = dgv.SelectedRows[0].Index;
            ListFound.RemoveAt(index);


        }
        private void btPrint_Click(object sender, EventArgs e)
        {
            if (ListFound == null || ListFound.Count == 0)
            {
                this.ShowInformation("Il n'y a rien à imprimer!");
                return;
            }

            Tools.ClearPrintTemp(); // ------------------------------------------- must clear printTemp before

            foreach (var stock in ListFound)
            {
                db.AddToPrintTemps(new PrintTemp()
                {
                    Date = stock.DateSortie,
                    Description = stock.Ressource.ToDescription(),
                    Nom = stock.Ressource.Fournisseur.Nom,
                    Piece = stock.Piece,
                    Quantite = stock.Quantite,
                    UID = Tools.CurrentUserID,
                    val1 = stock.Ressource.TypeRessource.UniteDeMesure,
                    val2 = stock.Ressource.TypeRessource.UniteDePiece,
                    EnStock = stock.EnStock,
                    val3 = (stock.EnStock == true) ? "En Stock" : " Sortie le: " + stock.DateSortie.Value.ToShortDateString()
                });


            }

            db.SaveChanges();

            FZBGsys.NsRapports.FrViewer.PrintListStockMT(labFilterTxt.Text);
        }
        private void chDate_CheckedChanged(object sender, EventArgs e)
        {
            panDate.Visible = chConsome.Checked;
        }
        private void btFermer_Click(object sender, EventArgs e)
        {
            Dispose();
        }
        private void chConsome_CheckedChanged(object sender, EventArgs e)
        {
            panDate.Visible = chConsome.Checked;
        }


        #region gestion stock
        internal static void AddToStockMT(Arrivage ar, ModelEntities db, DateTime? dt = null)
        {
            var existings = db.StockMatieres.ToList().Where(p => ar.Ressource.EqualsRessource(p.Ressource) && p.EnStock == true);

            if (existings.Count() == 0) //  create new Stock
            {
                db.AddToStockMatieres(new StockMatiere()
                {
                    Ressource = ar.Ressource,
                    Quantite = ar.Quantite,
                    Piece = (ar.Ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece) ? ar.Piece : null,
                    EnStock = true,

                });

            }
            else if (existings.Count() == 1)
            {
                var stock = existings.First();
                stock.Quantite += ar.Quantite;

                if (ar.Ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece) stock.Piece += ar.Piece;

            }
            else // 
            {
                Tools.ShowError("plusieurs stock meme categorie");
                return;
            }

        }
        internal static void AddRangeToStockMT(List<Arrivage> ListToSave, ModelEntities db)
        {
            foreach (var item in ListToSave)
            {
                AddToStockMT(item, db);
            }
        }
        internal static void AddToStockMT(StockMatiere ar, ModelEntities db, DateTime? dt = null)
        {
            var existings = db.StockMatieres.ToList().Where(p => ar.Ressource.EqualsRessource(p.Ressource) && p.EnStock == true);

            if (existings.Count() == 0) //  create new Stock
            {
                db.AddToStockMatieres(new StockMatiere()
                {
                    Ressource = ar.Ressource.CloneRessource(),
                    Quantite = ar.Quantite,
                    Piece = (ar.Ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece) ? ar.Piece : null,
                    EnStock = true,
                    DateFabrication = ar.DateFabrication,
                });

            }
            else if (existings.Count() == 1)
            {
                var stock = existings.First();
                stock.Quantite += ar.Quantite;
                if (ar.Ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece) stock.Piece += ar.Piece;

            }
            else // 
            {
                Tools.ShowError("plusieurs stock meme categorie");
                return;
            }

        }
        internal static void AddRangeToStockMT(List<StockMatiere> ListToSave, ModelEntities db)
        {
            foreach (var item in ListToSave)
            {
                AddToStockMT(item, db);
            }
        }
        internal static void RemoveFromStockMT(StockMatiere mat, ModelEntities db, DateTime? DateSortie = null)
        {
            if (DateSortie == null)
            {
                DateSortie = Tools.CurrentDateTime;
            }

            var existings = db.StockMatieres.ToList().Where(p => mat.Ressource.EqualsRessource(p.Ressource) && p.EnStock == true /*&& p.DateFabrication == mat.DateFabrication*/);
            var count = existings.Count();
            if (count == 0)      //------ not found
            {
                Tools.ShowError("Produit introuvable en Stock!!");
            }
            else if (count == 1) //------ found 
            {
                var stock = existings.First();
                if (stock.Quantite < mat.Quantite)
                {
                    Tools.ShowError("Quantite Produit En Stock insuffisante !");
                }

                else if (stock.Quantite == mat.Quantite)
                {
                    stock.DateSortie = DateSortie;
                    stock.EnStock = false;
                }
                else // stock >  demane
                {
                    stock.Quantite -= mat.Quantite;

                    if ((stock.Ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece))
                    {// attention arrome cant not be Piece but only quantite
                        stock.Piece -= mat.Piece;
                    }

                    db.AddToStockMatieres(new StockMatiere()
                    {
                        EnStock = false,
                        DateSortie = DateSortie,
                        Piece = (stock.Ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece) ? mat.Piece : null,
                        Quantite = mat.Quantite,
                        Ressource = mat.Ressource.CloneRessource(),
                    });


                }
            }
            else // impossible
            {
                Tools.ShowError("Plusieurs stock matiere en stock meme categorie");
            }


        }
        internal static void RemoveRangeFromStockMT(List<StockMatiere> list, ModelEntities db)
        {
            foreach (var item in list)
            {
                RemoveFromStockMT(item, db);
            }

        }
        internal static void RemoveFromStockMT(Arrivage mat, ModelEntities db) // annulage
        {
            var existings = db.StockMatieres.ToList().Where(p => mat.Ressource.EqualsRessource(p.Ressource) && p.EnStock == true);
            var count = existings.Count();
            if (count == 0)      //------ not found
            {
                Tools.ShowError("Produit introuvable en Stock!!");
            }
            else if (count == 1) //------ found 
            {
                var stock = existings.First();
                if (stock.Quantite < mat.Quantite)
                {
                    Tools.ShowError("Quantite Produit En Stock insuffisante !");
                }

                else if (stock.Quantite == mat.Quantite)
                {

                    db.DeleteObject(stock.Ressource);
                    db.DeleteObject(stock);
                }
                else // stock >  demane
                {
                    stock.Quantite -= mat.Quantite;
                    if (stock.Ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece) stock.Piece -= mat.Piece;

                }
            }
            else // impossible
            {
                Tools.ShowError("Plusieurs stock matiere en stock meme categorie");
            }


        }
        internal static void RemoveRangeFromStockMT(List<Arrivage> list, ModelEntities db)
        {
            foreach (var item in list)
            {
                RemoveFromStockMT(item, db);
            }

        }







        #endregion
    }
}
