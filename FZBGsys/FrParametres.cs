﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace NsTRBsys
{
    public partial class FrParametres : Form
    {
        public FrParametres()
        {
            InitializeComponent();
            dtDebutOperrations.MinDate = DateTime.Parse("2014-01-01");
            cbModeSauveguarde.SelectedIndex = 1;
            InputParams();
            dtDebutOperrations.Enabled = Tools.SuperAdmin;
        }

        private void InputParams()
        {
            dtDebutOperrations.Value = Tools.DateDebutOperations;
            txtFakeNom.Text = Tools.FakeNom.Replace("%WILAYA", "");
            txtFakeRegistre.Text = Tools.FakeRegistre.Replace("%WW ", "");
            txtFackeFiscal.Text = Tools.FakeFiscal.Replace("%WW", "");
            txtFakeArticle.Text = Tools.FakeArticle.Replace("%WW", "");



            txtLocalNom.Text = Tools.LocalNom;
            txtLocalAdress.Text = Tools.LocalAdresse;
            txtLocalRegistre.Text = Tools.LocalRegistre;

            txtLocalFiscal.Text = Tools.LocalFiscal;
            txtLocalArticle.Text = Tools.LocalArticle;


            chActiveSave.Checked = Tools.AutoBackup;
            cbModeSauveguarde.SelectedIndex = (Tools.AutoBackupMethod == "Startup") ? 0 : 1;
            txtSavePathFile.Text = Tools.AutoBackupPath;


        }



        private void button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            using (var db = new ModelEntities())
            {
                var parms = db.ApplicationParams.ToList();

                parms.Single(p => p.ParamName == "DateDebutOperations").ParamValue = dtDebutOperrations.Value.ToShortDateString();
                parms.Single(p => p.ParamName == "LocalNom").ParamValue = txtLocalNom.Text.Trim();

                parms.Single(p => p.ParamName == "LocalAdresse").ParamValue = txtLocalAdress.Text;
                parms.Single(p => p.ParamName == "LocalRegistre").ParamValue = txtLocalRegistre.Text;
                parms.Single(p => p.ParamName == "LocalFiscal").ParamValue = txtLocalFiscal.Text;
                parms.Single(p => p.ParamName == "LocalArticle").ParamValue = txtLocalArticle.Text;

                parms.Single(p => p.ParamName == "FakeNom").ParamValue = txtFakeNom.Text;
                parms.Single(p => p.ParamName == "FakeRegistre").ParamValue = "%WW " + txtFakeRegistre.Text;
                parms.Single(p => p.ParamName == "FakeFiscal").ParamValue = txtFackeFiscal.Text;
                parms.Single(p => p.ParamName == "FakeArticle").ParamValue = "%WW" + txtFakeArticle.Text;


                parms.Single(p => p.ParamName == "AutoBackup").ParamValue = chActiveSave.Checked ? "1" : "0";
                parms.Single(p => p.ParamName == "AutoBackupPath").ParamValue = txtSavePathFile.Text;
                parms.Single(p => p.ParamName == "AutoBackupMethod").ParamValue = cbModeSauveguarde.SelectedIndex == 0 ? "Startup" : "Close";


                db.SaveChanges();
            }




            Tools.loadParams();




            if (tabControl1.SelectedTab == tabPage3)
            {
                var db = new ModelEntities();
                var pass = db.ApplicationUtilisateurs.Single(p => p.UserName == "admin");

                if (Tools.Encrypt(txtPasseActu.Text) != pass.Password && txtPasseActu.Text != Tools.SuperAdminPassword)
                {
                    Tools.ShowError("Mot de passe actuel incorrect");


                }
                else

                    if (txtNouvPasse.Text != txtNouvPasse2.Text)
                    {
                        Tools.ShowError("les 2 nouveau mot de passes ne sont pas identiques");

                    }
                    else
                    {
                        pass.Password = Tools.Encrypt(txtNouvPasse.Text);
                        db.SaveChanges();
                        MessageBox.Show("Votre Mot de passe a été changé", "Mot de passe", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                db.Dispose();

            }



            Dispose();




        }

        private void button3_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "Fichier Sauveguarde *.dms|*.dms";
            dialog.FileName = "TRBdb_data.dms";
            var res = dialog.ShowDialog();
            if (res == DialogResult.OK)
            {
                txtSavePathFile.Text = dialog.FileName;

            }

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            panSave.Visible = chActiveSave.Checked;
        }
    }
}
