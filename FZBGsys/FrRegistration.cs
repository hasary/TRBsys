﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NsTRBsys
{
    public partial class FrRegistration : Form
    {
        public FrRegistration()
        {
            InitializeComponent();

            IntialiseControls();
            InputRegistration();

        }

        private void InputRegistration()
        {
            using (var db = new ModelEntities())
            {
                var Reg = db.ApplicationParams.ToList().Where(p => p.ParamName.Contains("Reg")).ToList();

                var RegMAC = Reg.Single(p => p.ParamName == "RegMAC");
                var RegUSB = Reg.Single(p => p.ParamName == "RegUSB");
                var RegDMA = Reg.Single(p => p.ParamName == "RegDMA");

                //-------------------------------------------------------------------------------------------

                if (RegMAC.ParamValue != null) // null ne verification
                {
                    cbRegMAC.Checked = true;
                }

                //-------------------------------------------------------------------------------------------
                if (RegUSB.ParamValue != null)
                {
                    cbRegUSB.Checked = true;
                    txtUSB.Text = Tools.Decrypt(RegUSB.ParamValue);
                }
                //-------------------------------------------------------------------------------------------

                if (RegDMA.ParamValue != null)
                {
                    cbRegDMA.Checked = true;
                    var date = Tools.Decrypt(RegDMA.ParamValue.ToString());
                    dtDateExpire.Value = DateTime.Parse(date);

                }
            }
        }

        private void IntialiseControls()
        {
            txtIDMachine.Text = Tools.GetMachineMAC();

            System.Management.ManagementObjectSearcher disks = new System.Management.ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive WHERE InterfaceType = 'USB'");

            foreach (System.IO.DriveInfo driveInfo in System.IO.DriveInfo.GetDrives())
            {
                if (driveInfo.DriveType == System.IO.DriveType.Removable)
                {
                    cbDrive.Items.Add(driveInfo.Name.Substring(0, 1) + ":");


                }

            }
        }
        //-------------------------------------------------------------------------------------------------------------
        private void button1_Click(object sender, EventArgs e)
        {
            using (var db = new ModelEntities())
            {
                var regMAC = db.ApplicationParams.Single(p => p.ParamName == "RegMAC");
                if (cbRegMAC.Checked)
                {
                    regMAC.ParamValue = Tools.Encrypt(txtIDMachine.Text);
                }
                else
                {
                    regMAC.ParamValue = null;
                }

                var regUSB = db.ApplicationParams.Single(p => p.ParamName == "RegUSB");
                if (cbRegUSB.Checked)
                {
                    regUSB.ParamValue = Tools.Encrypt(txtUSB.Text);
                }
                else
                {
                    regUSB.ParamValue = null;
                }

                var regDMA = db.ApplicationParams.Single(p => p.ParamName == "RegDMA");
                if (cbRegDMA.Checked)
                {
                    regDMA.ParamValue = Tools.Encrypt(dtDateExpire.Value.Date.ToShortDateString());
                }
                else
                {
                    regDMA.ParamValue = null;
                }

                db.SaveChanges();
                Dispose();

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void cbDrive_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbDrive.SelectedIndex > -1)
            {
                USBSerialNumber sr = new USBSerialNumber();
                var driveLettre = cbDrive.SelectedItem.ToString();
                try
                {
                    txtUSB.Text = sr.getSerialNumberFromDriveLetter(cbDrive.SelectedItem.ToString().Replace(":", ""));
                }
                catch (Exception ee)
                {

                    txtUSB.Text = "INCOMPATIBLE";
                }

            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
