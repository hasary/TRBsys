﻿namespace NsTRBsys.NsComercial
{
    partial class FrListDestination
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.panelSelect = new System.Windows.Forms.Panel();
            this.buttonSelectOK = new System.Windows.Forms.Button();
            this.buttonAnnuler = new System.Windows.Forms.Button();
            this.panelEdit = new System.Windows.Forms.Panel();
            this.buttonModifier = new System.Windows.Forms.Button();
            this.buttonSupprimer = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.labNomClient = new System.Windows.Forms.Label();
            this.buttonNouveau = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panelSelect.SuspendLayout();
            this.panelEdit.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(12, 40);
            this.dgv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 16;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(712, 204);
            this.dgv.TabIndex = 12;
            this.dgv.DoubleClick += new System.EventHandler(this.dgv_DoubleClick);
            // 
            // panelSelect
            // 
            this.panelSelect.Controls.Add(this.buttonSelectOK);
            this.panelSelect.Controls.Add(this.buttonAnnuler);
            this.panelSelect.Location = new System.Drawing.Point(12, 248);
            this.panelSelect.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelSelect.Name = "panelSelect";
            this.panelSelect.Size = new System.Drawing.Size(255, 42);
            this.panelSelect.TabIndex = 15;
            this.panelSelect.Visible = false;
            // 
            // buttonSelectOK
            // 
            this.buttonSelectOK.Image = global::NsTRBsys.Properties.Resources.icon_16_allow;
            this.buttonSelectOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonSelectOK.Location = new System.Drawing.Point(3, 2);
            this.buttonSelectOK.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSelectOK.Name = "buttonSelectOK";
            this.buttonSelectOK.Size = new System.Drawing.Size(116, 31);
            this.buttonSelectOK.TabIndex = 2;
            this.buttonSelectOK.Text = "Selectionner";
            this.buttonSelectOK.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonSelectOK.UseVisualStyleBackColor = true;
            this.buttonSelectOK.Click += new System.EventHandler(this.buttonSelectOK_Click);
            // 
            // buttonAnnuler
            // 
            this.buttonAnnuler.Image = global::NsTRBsys.Properties.Resources.icon_16_deny;
            this.buttonAnnuler.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAnnuler.Location = new System.Drawing.Point(125, 2);
            this.buttonAnnuler.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonAnnuler.Name = "buttonAnnuler";
            this.buttonAnnuler.Size = new System.Drawing.Size(105, 31);
            this.buttonAnnuler.TabIndex = 2;
            this.buttonAnnuler.Text = "Annuler";
            this.buttonAnnuler.UseVisualStyleBackColor = true;
            this.buttonAnnuler.Click += new System.EventHandler(this.buttonAnnuler_Click);
            // 
            // panelEdit
            // 
            this.panelEdit.Controls.Add(this.buttonModifier);
            this.panelEdit.Controls.Add(this.buttonSupprimer);
            this.panelEdit.Location = new System.Drawing.Point(495, 252);
            this.panelEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelEdit.Name = "panelEdit";
            this.panelEdit.Size = new System.Drawing.Size(228, 42);
            this.panelEdit.TabIndex = 14;
            // 
            // buttonModifier
            // 
            this.buttonModifier.Image = global::NsTRBsys.Properties.Resources.icon_16_edit;
            this.buttonModifier.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonModifier.Location = new System.Drawing.Point(16, 2);
            this.buttonModifier.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonModifier.Name = "buttonModifier";
            this.buttonModifier.Size = new System.Drawing.Size(100, 31);
            this.buttonModifier.TabIndex = 2;
            this.buttonModifier.Text = "Modifier";
            this.buttonModifier.UseVisualStyleBackColor = true;
            this.buttonModifier.Click += new System.EventHandler(this.buttonModifier_Click);
            // 
            // buttonSupprimer
            // 
            this.buttonSupprimer.Image = global::NsTRBsys.Properties.Resources.icon_16_delete;
            this.buttonSupprimer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonSupprimer.Location = new System.Drawing.Point(123, 2);
            this.buttonSupprimer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSupprimer.Name = "buttonSupprimer";
            this.buttonSupprimer.Size = new System.Drawing.Size(103, 31);
            this.buttonSupprimer.TabIndex = 2;
            this.buttonSupprimer.Text = "Supprimer";
            this.buttonSupprimer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonSupprimer.UseVisualStyleBackColor = true;
            this.buttonSupprimer.Click += new System.EventHandler(this.buttonSupprimer_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 17);
            this.label1.TabIndex = 16;
            this.label1.Text = "Compte Client:";
            // 
            // labNomClient
            // 
            this.labNomClient.AutoSize = true;
            this.labNomClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labNomClient.Location = new System.Drawing.Point(120, 12);
            this.labNomClient.Name = "labNomClient";
            this.labNomClient.Size = new System.Drawing.Size(0, 17);
            this.labNomClient.TabIndex = 16;
            // 
            // buttonNouveau
            // 
            this.buttonNouveau.Image = global::NsTRBsys.Properties.Resources.icon_16_new1;
            this.buttonNouveau.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonNouveau.Location = new System.Drawing.Point(382, 254);
            this.buttonNouveau.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonNouveau.Name = "buttonNouveau";
            this.buttonNouveau.Size = new System.Drawing.Size(107, 31);
            this.buttonNouveau.TabIndex = 13;
            this.buttonNouveau.Text = "Nouvelle";
            this.buttonNouveau.UseVisualStyleBackColor = true;
            this.buttonNouveau.Click += new System.EventHandler(this.buttonNouveau_Click);
            // 
            // FrListDestination
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(736, 307);
            this.Controls.Add(this.labNomClient);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panelSelect);
            this.Controls.Add(this.panelEdit);
            this.Controls.Add(this.buttonNouveau);
            this.Controls.Add(this.dgv);
            this.Name = "FrListDestination";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Liste des Destinations";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrListDestination_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panelSelect.ResumeLayout(false);
            this.panelEdit.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Panel panelSelect;
        private System.Windows.Forms.Button buttonSelectOK;
        private System.Windows.Forms.Button buttonAnnuler;
        private System.Windows.Forms.Panel panelEdit;
        private System.Windows.Forms.Button buttonModifier;
        private System.Windows.Forms.Button buttonSupprimer;
        private System.Windows.Forms.Button buttonNouveau;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labNomClient;
    }
}