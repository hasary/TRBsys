﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsComercial
{
    public partial class FrListBonAttribution : Form
    {
        ModelEntities db = new ModelEntities();
        public List<BonAttribution> ListFound { get; set; }
        public FrListBonAttribution()
        {
            InitializeComponent();
        }

        private void chRechAvancee_CheckedChanged(object sender, EventArgs e)
        {
            panRechAvance.Enabled = chRechAvancee.Checked;
        }

        private void FrListBonAttribution_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        private void btRecherche_Click(object sender, EventArgs e)
        {
            Recherche();

        }

        private void Recherche()
        {
            IQueryable<BonAttribution> result = db.BonAttributions;

            if (!chRechAvancee.Checked)
            {
                var date = dtDate.Value.Date;
                result = result.Where(p => p.Date == date);

            }
            else
            {
                var dateDu = dtDateDu.Value.Date;
                var dateAu = dtDateAu.Value.Date;
                result = result.Where(p => p.Date >= dateDu && p.Date <= dateAu);

                var ID = txtNoBon.Text.ParseToInt();
                if (ID != null)
                {
                    result = result.Where(p => p.ID == ID);
                }

                var client = txtClient.Text.Trim();

                if (client != "")
                {
                    result = result.Where(p => p.Client.Nom.Contains(client));
                }

                if (!chEnInstance.Checked)
                {
                    result = result.Where(p => p.Etat != (int)EnEtatBonAttribution.EnInstance);
                }

                if (!chAnnulé.Checked)
                {
                    result = result.Where(p => p.Etat != (int)EnEtatBonAttribution.Annulé);
                }

                if (!chEnInstance.Checked)
                {
                    result = result.Where(p => p.Etat != (int)EnEtatBonAttribution.Livré);
                }

            }

            ListFound = result.ToList();

            RefreshGrid();
        }

        //-------------------------------------------------------------------------------------------------------------
        private void RefreshGrid()
        {
            dgv.DataSource = ListFound.Select(p => new
            {
                ID = p.ID,
                Date = p.Date.Value.ToShortDateString(),
                Numero = p.ID.ToString().PadLeft(4, '0'),
                Client = p.Client.Nom + ((p.Client.ClientParentID != 0) ? " [" + p.Client.ClientParent.Nom + "]" : ""),
                Quantite = p.Ventes.Sum(s => s.NombrePalettes) + " P " + p.Ventes.Sum(s => s.NombreFardeaux) + " F ",
                Total = p.Ventes.Sum(s => s.TotalFardeaux) + " Fardeaux  " + p.Ventes.Sum(s => s.TotalBouteilles) + " Bouteilles",
                Etat = p.Etat.ToEtatBonAttributionTxt(),

            }).ToList();

            dgv.HideIDColumn();
        }
        //------------------------------------------------------------------------------------------------------------
        private void btImprimeBon_Click(object sender, EventArgs e)
        {
            var id = dgv.GetSelectedID();
            if (id != null)
            {
                FZBGsys.NsRapports.FrViewer.PrintBonAttribution(id.Value);
            }
        }

        private void modifierLeBonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var id = dgv.GetSelectedID();

            new FrBonAttribution(id).ShowDialog();


        }

        private void annulerLeBonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var id = dgv.GetSelectedID();
            var selected = db.BonAttributions.Single(p => p.ID == id);
            if (selected.Etat != (int)EnEtatBonAttribution.EnInstance)
            {
                this.ShowWarning("Ce Bon est déja " + selected.Etat.ToEtatBonAttributionTxt() + " ! ");
                return;
            }

            if (this.ConfirmWarning("Confirmer annulation du Bon N° " + id + " ?"))
            {
                selected.Etat = (int)EnEtatBonAttribution.Annulé;
                db.SaveChanges();
                Recherche();
            }


        }

        private void btFermer_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btImprimerList_Click(object sender, EventArgs e)
        {

        }

        private void dgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //  cMenu.Enabled = dgv.SelectedRows.Count == 0;

        }

        private void dgv_SelectionChanged(object sender, EventArgs e)
        {
            var selectedIndex = dgv.GetSelectedIndex(false);
            if (selectedIndex != null)
            {
                var selected = ListFound.ElementAt(selectedIndex.Value);
                tsmBonLivraison.Visible = selected.BonLivraisons.Count == 1;
                tsmBonSortie.Visible = selected.BonSorties.Count == 1;
                tsmFacture.Visible = selected.Factures.Count == 1;
            }
        }

        private void imprimerBonDeSortieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var ID = ListFound.ElementAt(dgv.GetSelectedIndex().Value).BonSorties.First().ID;

            FZBGsys.NsRapports.FrViewer.PrintBonLivraison(ID);



        }

        private void imprimerBonLivraisonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var ID = ListFound.ElementAt(dgv.GetSelectedIndex().Value).BonLivraisons.First().ID;

            FZBGsys.NsRapports.FrViewer.PrintBonLivraison(ID);


        }

        private void imprimerLaFactureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var facture = ListFound.ElementAt(dgv.GetSelectedIndex().Value).Factures.First();
            var totalTxt = CtoL.convertMontant(facture.MontantTotalFacture.Value);
            var remiseTxt = (facture.MontantTotalRemise == 0) ? "" : "(remise comprise)";
            FZBGsys.NsRapports.FrViewer.PrintFacture(facture.ID, facture.ModePayement.ToModePayementTxt(), totalTxt, remiseTxt);


        }

        private void modifierLaFactureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var id = ListFound.ElementAt(dgv.GetSelectedIndex().Value).Factures.First().ID;
            new FZBGsys.NsComercial.FrFacture(id).ShowDialog();
        }

        private void annulerLaFactureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var facture = ListFound.ElementAt(dgv.GetSelectedIndex().Value).Factures.First();
            if (this.ConfirmWarning("Confirmer suppression Facture N° " + facture.ID + "?"))
            {
                db.DeleteObject(facture);
                db.SaveChanges();
                Recherche();
            }
        }

        private void modifierLivraisonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var id = ListFound.ElementAt(dgv.GetSelectedIndex().Value).BonLivraisons.First().ID;
            new FZBGsys.NsComercial.FrBonLivraison(id).ShowDialog();
        }

        private void annulerLivraisonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var livraison = ListFound.ElementAt(dgv.GetSelectedIndex().Value).BonLivraisons.First();
            if (this.ConfirmWarning("Confirmer suppression Livraison N° " + livraison.ID + "?"))
            {
                db.DeleteObject(livraison);
                db.SaveChanges();
                Recherche();
            }

        }

        private void modifierBonDeSortieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var id = ListFound.ElementAt(dgv.GetSelectedIndex().Value).BonSorties.First().ID;
            new FZBGsys.NsComercial.FrBonSortie(id).ShowDialog();
        }

        private void annulerBonDeSortieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var sortie = ListFound.ElementAt(dgv.GetSelectedIndex().Value).BonSorties.First();
            var BonAttribution = sortie.BonAttribution;
            if (BonAttribution.BonLivraisons.Count != 0)
            {
                this.ShowWarning("Impossible de supprimer Bon Sortie car le Bon d'attribution associé est Livré,\nSupprimez d'abord le Bon de Livraison.");
                return;
            }

            if (BonAttribution.Factures.Count != 0)
            {
                this.ShowWarning("Impossible de supprimer Bon Sortie car le Bon d'attribution associé est Facturé,\nSupprimez d'abord le la Facture.");
                return;
            }


            if (this.ConfirmWarning("Confirmer annulation Sortie N° " + sortie.ID + "?"))
            {
                var ventesDetails = sortie.VenteDetails.ToList();
                foreach (var details in ventesDetails)
                {
                    db.DeleteObject(details);
                }

                
                sortie.BonAttribution.Etat = (int)EnEtatBonAttribution.EnInstance;
                db.DeleteObject(sortie);
                db.SaveChanges();
                Recherche();
            }
        }


    }
}
