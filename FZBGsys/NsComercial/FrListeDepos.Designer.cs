﻿namespace NsTRBsys.NsComercial
{
    partial class FrListeDepos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.btNouv = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btmodif = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(23, 26);
            this.dgv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 16;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(562, 380);
            this.dgv.TabIndex = 12;
            // 
            // btNouv
            // 
            this.btNouv.Image = global::NsTRBsys.Properties.Resources.icon_16_new1;
            this.btNouv.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btNouv.Location = new System.Drawing.Point(23, 424);
            this.btNouv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btNouv.Name = "btNouv";
            this.btNouv.Size = new System.Drawing.Size(112, 36);
            this.btNouv.TabIndex = 15;
            this.btNouv.Text = "Nouveau";
            this.btNouv.UseVisualStyleBackColor = true;
            this.btNouv.Click += new System.EventHandler(this.btNouv_Click);
            // 
            // button1
            // 
            this.button1.Image = global::NsTRBsys.Properties.Resources.icon_16_delete;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(265, 424);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(121, 36);
            this.button1.TabIndex = 14;
            this.button1.Text = "Supprimer";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btmodif
            // 
            this.btmodif.Image = global::NsTRBsys.Properties.Resources.icon_16_edit;
            this.btmodif.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btmodif.Location = new System.Drawing.Point(142, 424);
            this.btmodif.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btmodif.Name = "btmodif";
            this.btmodif.Size = new System.Drawing.Size(117, 36);
            this.btmodif.TabIndex = 13;
            this.btmodif.Text = "Modifier";
            this.btmodif.UseVisualStyleBackColor = true;
            this.btmodif.Click += new System.EventHandler(this.btmodif_Click);
            // 
            // FrListeDepos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(612, 471);
            this.Controls.Add(this.btNouv);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btmodif);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FrListeDepos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Liste des dépos";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btNouv;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btmodif;
    }
}