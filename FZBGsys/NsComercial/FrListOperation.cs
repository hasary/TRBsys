﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NsTRBsys.NsComercial
{
    public partial class FrListOperation : Form
    {
        ModelEntities db = new ModelEntities();
        List<Operation> ListFound;
        public FrListOperation()
        {

            InitializeComponent();
            cbTypeOperation.SelectedIndex = 0;
            //  chRechAvancee.Checked = true;

            cbDepartFournisseur.Items.Add(new Fournisseur { ID = 0, Nom = "[tout]" });
            cbDepartFournisseur.Items.AddRange(db.Fournisseurs.Where(p => p.ID > 0 && p.Active == true).ToArray());
            cbDepartFournisseur.SelectedIndex = 0;
            cbDepartFournisseur.ValueMember = "ID";
            cbDepartFournisseur.DisplayMember = "Nom";
            cbDepartFournisseur.DropDownStyle = ComboBoxStyle.DropDownList;
            dtDateDu.MinDate = Tools.DateDebutOperations;
            dtDateDu.Value = Tools.DateDebutOperations;

            Recherche();
        }

        private void FrListOperation_FormClosed(object sender, FormClosedEventArgs e)
        {
            db.Dispose();
        }

        private void btRecherche_Click(object sender, EventArgs e)
        {
            Recherche();


        }

        private void Recherche()
        {
            IQueryable<Operation> result = db.Operations;

            if (!chRechAvancee.Checked)
            {
                var date = dtDate.Value.Date;
                result = result.Where(p => p.Date == date);
            }
            else
            {
                var dateFrom = dtDateDu.Value.Date;
                var dateTo = dtDateAu.Value.Date;
                result = result.Where(p => p.Date >= dateFrom && p.Date <= dateTo);

                if (cbTypeOperation.SelectedIndex != 0)
                {
                    result = result.Where(p => p.TypeOperaiton == cbTypeOperation.SelectedIndex);
                }

                if (panFacture.Visible = true && txtNoFacture.Text.Trim().ParseToInt() != null)
                {
                    var factureID = txtNoFacture.Text.Trim().ParseToInt();
                    result = result.Where(p => p.FactureID == factureID);
                }

                if (txtClient.Text.Trim() != "")
                {
                    var cli = txtClient.Text;//.Trim().ToUpper();
                    result = result.Where(p => p.Client.Nom == cli);
                }

                if (cbDepartFournisseur.SelectedIndex > 0)
                {
                    var fourniID = ((Fournisseur)cbDepartFournisseur.SelectedItem).ID;
                    result = result.Where(p => p.FournisseurID == fourniID);
                }


            }

            ListFound = result.ToList();
            RefreshGrid();

        }

        private void RefreshGrid()
        {
            dgv.DataSource = ListFound.Select(p => new
            {
                ID = p.ID,
                Date = p.Date,
                Type = ((p.TypeOperaiton == (int)EnTypeOperation.Versement_ENVOI) ? "Envoi Versement" : "Reçu Versement") +
                ((p.FournisseurID != null) ? " Fournisseur" : (p.Client.isPartenaire == true ? " Partenaire" : " Client")),

                Nom = (p.Client != null) ? p.Client.Nom : p.Fournisseur.Nom,
                Libele = p.Lib,

                Montant = p.Montant.ToAffInt(),
              //  Inscription = " le " + p.InscriptionDate.Value.ToString("dd/MM/yyyy à hh:mm "),
                UID = p.InscriptionUID,

            }).ToList();

            dgv.HideIDColumn();
            dgv.HideIDColumn("UID");
        }



        private void btAnnuler_Click(object sender, EventArgs e)
        {
            var id = dgv.GetSelectedID();

            if (id != null)
            {
                var toDel = ListFound.Single(p => p.ID == id.Value);

                if (this.ConfirmWarning("Etes-vous sur de vouloir supprimer l'operation ?\n" +


                    toDel.Lib + " " + toDel.Montant + " DA"))
                {
                    var Client = toDel.Client;
                    var four = toDel.Fournisseur;


                    ListFound.Remove(toDel);
                    db.DeleteObject(toDel);
                    db.SaveChangeTry();


                    if (Client != null)
                    {
                        FrMain.GenerateSoldeClient(db, DateTime.Now, Client);
                    }

                    if (four != null)
                    {
                        FrMain.GenerateSoldeFourni(db, DateTime.Now, four);
                    }

                    db.SaveChanges();

                    RefreshGrid();
                }
            }



        }

        private void chRechAvancee_CheckedChanged(object sender, EventArgs e)
        {
            panRechAvance.Enabled = chRechAvancee.Checked;
        }

        private void btFermer_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btImprimer_Click(object sender, EventArgs e)
        {

        }

        private void cbTypeOperation_SelectedIndexChanged(object sender, EventArgs e)
        {
            panFacture.Visible = true; //(EnTypeOpration)cbTypeOperation.SelectedIndex == EnTypeOpration.Payement_Espèces; 
        }

        private void btParcouClient_Click(object sender, EventArgs e)
        {
            var Client = NsTRBsys.NsComercial.NsClient.FrList.SelectClientDialog(db);

            if (Client != null)
            {
                txtClient.Text = Client.Nom;
                cbDepartFournisseur.SelectedIndex = 0;
                //  cbWilaya.SelectedItem = Client.Wilaya;
            }
        }

        private void cbDepartFournisseur_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbDepartFournisseur.SelectedIndex == 0)
            {
                
            }
            else if (cbDepartFournisseur.SelectedIndex > 0)
            {
                txtClient.Text = "";
            }
        }
    }
}
