﻿namespace NsTRBsys.NsComercial
{
    partial class FrListOperation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.panRechAvance = new System.Windows.Forms.Panel();
            this.cbDepartFournisseur = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btParcouClient = new System.Windows.Forms.Button();
            this.panFacture = new System.Windows.Forms.Panel();
            this.txtNoFacture = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbTypeOperation = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtClient = new System.Windows.Forms.TextBox();
            this.panDate = new System.Windows.Forms.Panel();
            this.dtDateAu = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.dtDateDu = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.chRechAvancee = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panEdit = new System.Windows.Forms.Panel();
            this.btAnnuler = new System.Windows.Forms.Button();
            this.btRecherche = new System.Windows.Forms.Button();
            this.btFermer = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panRechAvance.SuspendLayout();
            this.panFacture.SuspendLayout();
            this.panDate.SuspendLayout();
            this.panEdit.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(232, 9);
            this.dgv.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 16;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(566, 365);
            this.dgv.TabIndex = 11;
            // 
            // dtDate
            // 
            this.dtDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDate.Location = new System.Drawing.Point(96, 19);
            this.dtDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(114, 20);
            this.dtDate.TabIndex = 68;
            // 
            // panRechAvance
            // 
            this.panRechAvance.Controls.Add(this.cbDepartFournisseur);
            this.panRechAvance.Controls.Add(this.label8);
            this.panRechAvance.Controls.Add(this.btParcouClient);
            this.panRechAvance.Controls.Add(this.panFacture);
            this.panRechAvance.Controls.Add(this.cbTypeOperation);
            this.panRechAvance.Controls.Add(this.label7);
            this.panRechAvance.Controls.Add(this.label6);
            this.panRechAvance.Controls.Add(this.txtClient);
            this.panRechAvance.Controls.Add(this.panDate);
            this.panRechAvance.Controls.Add(this.label2);
            this.panRechAvance.Enabled = false;
            this.panRechAvance.Location = new System.Drawing.Point(9, 79);
            this.panRechAvance.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panRechAvance.Name = "panRechAvance";
            this.panRechAvance.Size = new System.Drawing.Size(218, 287);
            this.panRechAvance.TabIndex = 67;
            // 
            // cbDepartFournisseur
            // 
            this.cbDepartFournisseur.FormattingEnabled = true;
            this.cbDepartFournisseur.Location = new System.Drawing.Point(14, 166);
            this.cbDepartFournisseur.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbDepartFournisseur.Name = "cbDepartFournisseur";
            this.cbDepartFournisseur.Size = new System.Drawing.Size(194, 21);
            this.cbDepartFournisseur.TabIndex = 94;
            this.cbDepartFournisseur.SelectedIndexChanged += new System.EventHandler(this.cbDepartFournisseur_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 147);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 13);
            this.label8.TabIndex = 93;
            this.label8.Text = "Fournisseur:";
            // 
            // btParcouClient
            // 
            this.btParcouClient.Image = global::NsTRBsys.Properties.Resources.icon_16_module;
            this.btParcouClient.Location = new System.Drawing.Point(169, 113);
            this.btParcouClient.Name = "btParcouClient";
            this.btParcouClient.Size = new System.Drawing.Size(34, 20);
            this.btParcouClient.TabIndex = 92;
            this.btParcouClient.UseVisualStyleBackColor = true;
            this.btParcouClient.Click += new System.EventHandler(this.btParcouClient_Click);
            // 
            // panFacture
            // 
            this.panFacture.Controls.Add(this.txtNoFacture);
            this.panFacture.Controls.Add(this.label3);
            this.panFacture.Location = new System.Drawing.Point(59, 46);
            this.panFacture.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panFacture.Name = "panFacture";
            this.panFacture.Size = new System.Drawing.Size(140, 29);
            this.panFacture.TabIndex = 73;
            this.panFacture.Visible = false;
            // 
            // txtNoFacture
            // 
            this.txtNoFacture.Enabled = false;
            this.txtNoFacture.Location = new System.Drawing.Point(75, 7);
            this.txtNoFacture.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtNoFacture.Name = "txtNoFacture";
            this.txtNoFacture.Size = new System.Drawing.Size(55, 20);
            this.txtNoFacture.TabIndex = 26;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 10);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "N° Facture:";
            // 
            // cbTypeOperation
            // 
            this.cbTypeOperation.FormattingEnabled = true;
            this.cbTypeOperation.Items.AddRange(new object[] {
            "[Tout]",
            "Versements (Envois) ",
            "Reçu de Versement "});
            this.cbTypeOperation.Location = new System.Drawing.Point(62, 22);
            this.cbTypeOperation.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbTypeOperation.Name = "cbTypeOperation";
            this.cbTypeOperation.Size = new System.Drawing.Size(138, 21);
            this.cbTypeOperation.TabIndex = 30;
            this.cbTypeOperation.Visible = false;
            this.cbTypeOperation.SelectedIndexChanged += new System.EventHandler(this.cbTypeOperation_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 24);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 29;
            this.label7.Text = "Type:";
            this.label7.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 93);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 13);
            this.label6.TabIndex = 27;
            this.label6.Text = "Client / partenaire:";
            // 
            // txtClient
            // 
            this.txtClient.Location = new System.Drawing.Point(14, 114);
            this.txtClient.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtClient.Name = "txtClient";
            this.txtClient.ReadOnly = true;
            this.txtClient.Size = new System.Drawing.Size(151, 20);
            this.txtClient.TabIndex = 26;
            // 
            // panDate
            // 
            this.panDate.Controls.Add(this.dtDateAu);
            this.panDate.Controls.Add(this.label5);
            this.panDate.Controls.Add(this.dtDateDu);
            this.panDate.Controls.Add(this.label4);
            this.panDate.Location = new System.Drawing.Point(62, 210);
            this.panDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panDate.Name = "panDate";
            this.panDate.Size = new System.Drawing.Size(145, 57);
            this.panDate.TabIndex = 24;
            // 
            // dtDateAu
            // 
            this.dtDateAu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateAu.Location = new System.Drawing.Point(31, 31);
            this.dtDateAu.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtDateAu.Name = "dtDateAu";
            this.dtDateAu.Size = new System.Drawing.Size(105, 20);
            this.dtDateAu.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1, 32);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Au:";
            // 
            // dtDateDu
            // 
            this.dtDateDu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateDu.Location = new System.Drawing.Point(31, 8);
            this.dtDateDu.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtDateDu.Name = "dtDateDu";
            this.dtDateDu.Size = new System.Drawing.Size(105, 20);
            this.dtDateDu.TabIndex = 1;
            this.dtDateDu.Value = new System.DateTime(2014, 5, 5, 0, 0, 0, 0);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1, 10);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Du:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 225);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Période:";
            // 
            // chRechAvancee
            // 
            this.chRechAvancee.AutoSize = true;
            this.chRechAvancee.Location = new System.Drawing.Point(9, 58);
            this.chRechAvancee.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chRechAvancee.Name = "chRechAvancee";
            this.chRechAvancee.Size = new System.Drawing.Size(125, 17);
            this.chRechAvancee.TabIndex = 66;
            this.chRechAvancee.Text = "Recherche Avancée";
            this.chRechAvancee.UseVisualStyleBackColor = true;
            this.chRechAvancee.CheckedChanged += new System.EventHandler(this.chRechAvancee_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 65;
            this.label1.Text = "Date :";
            // 
            // panEdit
            // 
            this.panEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panEdit.Controls.Add(this.btAnnuler);
            this.panEdit.Location = new System.Drawing.Point(232, 384);
            this.panEdit.Name = "panEdit";
            this.panEdit.Size = new System.Drawing.Size(176, 36);
            this.panEdit.TabIndex = 72;
            // 
            // btAnnuler
            // 
            this.btAnnuler.Image = global::NsTRBsys.Properties.Resources.icon_16_delete;
            this.btAnnuler.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btAnnuler.Location = new System.Drawing.Point(2, 2);
            this.btAnnuler.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(130, 29);
            this.btAnnuler.TabIndex = 58;
            this.btAnnuler.Text = "Supprimer l\'operation";
            this.btAnnuler.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btAnnuler.UseVisualStyleBackColor = true;
            this.btAnnuler.Click += new System.EventHandler(this.btAnnuler_Click);
            // 
            // btRecherche
            // 
            this.btRecherche.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btRecherche.Image = global::NsTRBsys.Properties.Resources.icon_16_search;
            this.btRecherche.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btRecherche.Location = new System.Drawing.Point(9, 390);
            this.btRecherche.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btRecherche.Name = "btRecherche";
            this.btRecherche.Size = new System.Drawing.Size(110, 28);
            this.btRecherche.TabIndex = 71;
            this.btRecherche.Text = "Recherche";
            this.btRecherche.UseVisualStyleBackColor = true;
            this.btRecherche.Click += new System.EventHandler(this.btRecherche_Click);
            // 
            // btFermer
            // 
            this.btFermer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btFermer.Location = new System.Drawing.Point(693, 384);
            this.btFermer.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btFermer.Name = "btFermer";
            this.btFermer.Size = new System.Drawing.Size(104, 32);
            this.btFermer.TabIndex = 69;
            this.btFermer.Text = "Fermer";
            this.btFermer.UseVisualStyleBackColor = true;
            this.btFermer.Click += new System.EventHandler(this.btFermer_Click);
            // 
            // FrListOperation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(806, 433);
            this.Controls.Add(this.panEdit);
            this.Controls.Add(this.btRecherche);
            this.Controls.Add(this.btFermer);
            this.Controls.Add(this.dtDate);
            this.Controls.Add(this.panRechAvance);
            this.Controls.Add(this.chRechAvancee);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgv);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FrListOperation";
            this.Text = "Liste des versements";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrListOperation_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panRechAvance.ResumeLayout(false);
            this.panRechAvance.PerformLayout();
            this.panFacture.ResumeLayout(false);
            this.panFacture.PerformLayout();
            this.panDate.ResumeLayout(false);
            this.panDate.PerformLayout();
            this.panEdit.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.Panel panRechAvance;
        private System.Windows.Forms.ComboBox cbTypeOperation;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtClient;
        private System.Windows.Forms.TextBox txtNoFacture;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panDate;
        private System.Windows.Forms.DateTimePicker dtDateAu;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtDateDu;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chRechAvancee;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panFacture;
        private System.Windows.Forms.Panel panEdit;
        private System.Windows.Forms.Button btAnnuler;
        private System.Windows.Forms.Button btRecherche;
        private System.Windows.Forms.Button btFermer;
        private System.Windows.Forms.Button btParcouClient;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbDepartFournisseur;
    }
}