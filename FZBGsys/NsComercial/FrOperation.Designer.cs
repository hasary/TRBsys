﻿namespace FZBGsys.NsComercial
{
    partial class FrOperation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSolde = new System.Windows.Forms.TextBox();
            this.txtParent = new System.Windows.Forms.TextBox();
            this.txtClient = new System.Windows.Forms.TextBox();
            this.btParcourrirClient = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labMontantRecuOperation = new System.Windows.Forms.Label();
            this.txtMontantReçuOperation = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbTypeOperation = new System.Windows.Forms.ComboBox();
            this.btEnlever = new System.Windows.Forms.Button();
            this.btAjouter = new System.Windows.Forms.Button();
            this.tcOperation = new System.Windows.Forms.TabControl();
            this.tpOperation = new System.Windows.Forms.TabPage();
            this.panOperation = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNouveauSolde = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tpPayement = new System.Windows.Forms.TabPage();
            this.panPayment = new System.Windows.Forms.Panel();
            this.txtMontantRecuPayement = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtMontantFacture = new System.Windows.Forms.TextBox();
            this.labMontantRecupayement = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.btAfficher = new System.Windows.Forms.Button();
            this.cbDocument = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtNoBon = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.btEnregistrer = new System.Windows.Forms.Button();
            this.btAnnuler = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.tcOperation.SuspendLayout();
            this.tpOperation.SuspendLayout();
            this.panOperation.SuspendLayout();
            this.tpPayement.SuspendLayout();
            this.panPayment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtSolde);
            this.groupBox1.Controls.Add(this.txtParent);
            this.groupBox1.Controls.Add(this.txtClient);
            this.groupBox1.Controls.Add(this.btParcourrirClient);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(12, 49);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(366, 119);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Client";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(46, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 17);
            this.label1.TabIndex = 28;
            this.label1.Text = "Solde:";
            // 
            // txtSolde
            // 
            this.txtSolde.Location = new System.Drawing.Point(109, 81);
            this.txtSolde.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSolde.Name = "txtSolde";
            this.txtSolde.ReadOnly = true;
            this.txtSolde.Size = new System.Drawing.Size(197, 22);
            this.txtSolde.TabIndex = 27;
            // 
            // txtParent
            // 
            this.txtParent.Location = new System.Drawing.Point(109, 55);
            this.txtParent.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtParent.Name = "txtParent";
            this.txtParent.ReadOnly = true;
            this.txtParent.Size = new System.Drawing.Size(197, 22);
            this.txtParent.TabIndex = 25;
            // 
            // txtClient
            // 
            this.txtClient.Location = new System.Drawing.Point(37, 29);
            this.txtClient.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtClient.Name = "txtClient";
            this.txtClient.ReadOnly = true;
            this.txtClient.Size = new System.Drawing.Size(269, 22);
            this.txtClient.TabIndex = 26;
            // 
            // btParcourrirClient
            // 
            this.btParcourrirClient.Location = new System.Drawing.Point(312, 29);
            this.btParcourrirClient.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btParcourrirClient.Name = "btParcourrirClient";
            this.btParcourrirClient.Size = new System.Drawing.Size(43, 25);
            this.btParcourrirClient.TabIndex = 24;
            this.btParcourrirClient.Text = "...";
            this.btParcourrirClient.UseVisualStyleBackColor = true;
            this.btParcourrirClient.Click += new System.EventHandler(this.btParcourrirClient_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(320, 84);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 17);
            this.label7.TabIndex = 5;
            this.label7.Text = "DA";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(312, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "DA";
            // 
            // labMontantRecuOperation
            // 
            this.labMontantRecuOperation.AutoSize = true;
            this.labMontantRecuOperation.Font = new System.Drawing.Font("Arial Narrow", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labMontantRecuOperation.Location = new System.Drawing.Point(94, 37);
            this.labMontantRecuOperation.Name = "labMontantRecuOperation";
            this.labMontantRecuOperation.Size = new System.Drawing.Size(245, 17);
            this.labMontantRecuOperation.TabIndex = 4;
            this.labMontantRecuOperation.Text = "trente huit milles trois cent quatre vingt dix-neuf ";
            // 
            // txtMontantReçuOperation
            // 
            this.txtMontantReçuOperation.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMontantReçuOperation.Location = new System.Drawing.Point(144, 3);
            this.txtMontantReçuOperation.Name = "txtMontantReçuOperation";
            this.txtMontantReçuOperation.Size = new System.Drawing.Size(154, 22);
            this.txtMontantReçuOperation.TabIndex = 3;
            this.txtMontantReçuOperation.TextChanged += new System.EventHandler(this.txtMontantReçuOperation_TextChanged);
            this.txtMontantReçuOperation.Enter += new System.EventHandler(this.txtMontantReçuOperation_Enter);
            this.txtMontantReçuOperation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMontantReçuOperation_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(17, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Montant reçu:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Type:";
            // 
            // cbTypeOperation
            // 
            this.cbTypeOperation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTypeOperation.FormattingEnabled = true;
            this.cbTypeOperation.Items.AddRange(new object[] {
            "",
            "Reglement Créance",
            "Avance",
            "Payement Espèces..."});
            this.cbTypeOperation.Location = new System.Drawing.Point(145, 14);
            this.cbTypeOperation.Name = "cbTypeOperation";
            this.cbTypeOperation.Size = new System.Drawing.Size(157, 24);
            this.cbTypeOperation.TabIndex = 0;
            this.cbTypeOperation.SelectedIndexChanged += new System.EventHandler(this.cbTypeOperation_SelectedIndexChanged);
            // 
            // btEnlever
            // 
            this.btEnlever.Location = new System.Drawing.Point(384, 385);
            this.btEnlever.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btEnlever.Name = "btEnlever";
            this.btEnlever.Size = new System.Drawing.Size(123, 33);
            this.btEnlever.TabIndex = 6;
            this.btEnlever.Text = "<<Enlevez";
            this.btEnlever.UseVisualStyleBackColor = true;
            this.btEnlever.Click += new System.EventHandler(this.btEnlever_Click);
            // 
            // btAjouter
            // 
            this.btAjouter.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAjouter.Location = new System.Drawing.Point(251, 385);
            this.btAjouter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAjouter.Name = "btAjouter";
            this.btAjouter.Size = new System.Drawing.Size(127, 33);
            this.btAjouter.TabIndex = 5;
            this.btAjouter.Text = "Ajouter >>";
            this.btAjouter.UseVisualStyleBackColor = true;
            this.btAjouter.Click += new System.EventHandler(this.btAjouter_Click);
            // 
            // tcOperation
            // 
            this.tcOperation.Controls.Add(this.tpOperation);
            this.tcOperation.Controls.Add(this.tpPayement);
            this.tcOperation.Location = new System.Drawing.Point(12, 178);
            this.tcOperation.Name = "tcOperation";
            this.tcOperation.SelectedIndex = 0;
            this.tcOperation.Size = new System.Drawing.Size(366, 202);
            this.tcOperation.TabIndex = 7;
            this.tcOperation.SelectedIndexChanged += new System.EventHandler(this.tcOperation_SelectedIndexChanged);
            // 
            // tpOperation
            // 
            this.tpOperation.BackColor = System.Drawing.SystemColors.Control;
            this.tpOperation.Controls.Add(this.panOperation);
            this.tpOperation.Controls.Add(this.cbTypeOperation);
            this.tpOperation.Controls.Add(this.label2);
            this.tpOperation.Location = new System.Drawing.Point(4, 25);
            this.tpOperation.Name = "tpOperation";
            this.tpOperation.Padding = new System.Windows.Forms.Padding(3);
            this.tpOperation.Size = new System.Drawing.Size(358, 173);
            this.tpOperation.TabIndex = 0;
            this.tpOperation.Text = "Operation";
            // 
            // panOperation
            // 
            this.panOperation.Controls.Add(this.label6);
            this.panOperation.Controls.Add(this.txtMontantReçuOperation);
            this.panOperation.Controls.Add(this.label3);
            this.panOperation.Controls.Add(this.txtNouveauSolde);
            this.panOperation.Controls.Add(this.labMontantRecuOperation);
            this.panOperation.Controls.Add(this.label5);
            this.panOperation.Controls.Add(this.label8);
            this.panOperation.Enabled = false;
            this.panOperation.Location = new System.Drawing.Point(1, 58);
            this.panOperation.Name = "panOperation";
            this.panOperation.Size = new System.Drawing.Size(354, 98);
            this.panOperation.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(9, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 17);
            this.label6.TabIndex = 28;
            this.label6.Text = "Nouveau Solde:";
            // 
            // txtNouveauSolde
            // 
            this.txtNouveauSolde.Location = new System.Drawing.Point(144, 70);
            this.txtNouveauSolde.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNouveauSolde.Name = "txtNouveauSolde";
            this.txtNouveauSolde.ReadOnly = true;
            this.txtNouveauSolde.Size = new System.Drawing.Size(154, 22);
            this.txtNouveauSolde.TabIndex = 27;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(312, 73);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(27, 17);
            this.label8.TabIndex = 5;
            this.label8.Text = "DA";
            // 
            // tpPayement
            // 
            this.tpPayement.BackColor = System.Drawing.SystemColors.Control;
            this.tpPayement.Controls.Add(this.panPayment);
            this.tpPayement.Controls.Add(this.btAfficher);
            this.tpPayement.Controls.Add(this.cbDocument);
            this.tpPayement.Controls.Add(this.label11);
            this.tpPayement.Controls.Add(this.txtNoBon);
            this.tpPayement.Controls.Add(this.label12);
            this.tpPayement.Location = new System.Drawing.Point(4, 25);
            this.tpPayement.Name = "tpPayement";
            this.tpPayement.Padding = new System.Windows.Forms.Padding(3);
            this.tpPayement.Size = new System.Drawing.Size(358, 173);
            this.tpPayement.TabIndex = 1;
            this.tpPayement.Text = "Payement Espèce";
            // 
            // panPayment
            // 
            this.panPayment.Controls.Add(this.txtMontantRecuPayement);
            this.panPayment.Controls.Add(this.label10);
            this.panPayment.Controls.Add(this.label13);
            this.panPayment.Controls.Add(this.txtMontantFacture);
            this.panPayment.Controls.Add(this.labMontantRecupayement);
            this.panPayment.Controls.Add(this.label15);
            this.panPayment.Enabled = false;
            this.panPayment.Location = new System.Drawing.Point(1, 50);
            this.panPayment.Name = "panPayment";
            this.panPayment.Size = new System.Drawing.Size(354, 117);
            this.panPayment.TabIndex = 13;
            // 
            // txtMontantRecuPayement
            // 
            this.txtMontantRecuPayement.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMontantRecuPayement.Location = new System.Drawing.Point(154, 44);
            this.txtMontantRecuPayement.Name = "txtMontantRecuPayement";
            this.txtMontantRecuPayement.Size = new System.Drawing.Size(155, 22);
            this.txtMontantRecuPayement.TabIndex = 38;
            this.txtMontantRecuPayement.TextChanged += new System.EventHandler(this.txtMontantRecuPayement_TextChanged);
            this.txtMontantRecuPayement.Enter += new System.EventHandler(this.txtMontantRecuPayement_Enter);
            this.txtMontantRecuPayement.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMontantReçuOperation_KeyPress);
            this.txtMontantRecuPayement.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtMontantRecuPayement_KeyUp);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 13);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(115, 17);
            this.label10.TabIndex = 36;
            this.label10.Text = "Montant Facture:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(315, 47);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(27, 17);
            this.label13.TabIndex = 40;
            this.label13.Text = "DA";
            // 
            // txtMontantFacture
            // 
            this.txtMontantFacture.Location = new System.Drawing.Point(154, 12);
            this.txtMontantFacture.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtMontantFacture.Name = "txtMontantFacture";
            this.txtMontantFacture.ReadOnly = true;
            this.txtMontantFacture.Size = new System.Drawing.Size(155, 22);
            this.txtMontantFacture.TabIndex = 27;
            // 
            // labMontantRecupayement
            // 
            this.labMontantRecupayement.AutoSize = true;
            this.labMontantRecupayement.Font = new System.Drawing.Font("Arial Narrow", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labMontantRecupayement.Location = new System.Drawing.Point(97, 81);
            this.labMontantRecupayement.Name = "labMontantRecupayement";
            this.labMontantRecupayement.Size = new System.Drawing.Size(245, 17);
            this.labMontantRecupayement.TabIndex = 39;
            this.labMontantRecupayement.Text = "trente huit milles trois cent quatre vingt dix-neuf ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(16, 48);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(108, 17);
            this.label15.TabIndex = 37;
            this.label15.Text = "Montant reçu:";
            // 
            // btAfficher
            // 
            this.btAfficher.Location = new System.Drawing.Point(316, 18);
            this.btAfficher.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAfficher.Name = "btAfficher";
            this.btAfficher.Size = new System.Drawing.Size(35, 25);
            this.btAfficher.TabIndex = 35;
            this.btAfficher.Text = ">>";
            this.btAfficher.UseVisualStyleBackColor = true;
            this.btAfficher.Click += new System.EventHandler(this.btAfficher_Click);
            // 
            // cbDocument
            // 
            this.cbDocument.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDocument.FormattingEnabled = true;
            this.cbDocument.Items.AddRange(new object[] {
            "Facture",
            "Attribution"});
            this.cbDocument.Location = new System.Drawing.Point(95, 19);
            this.cbDocument.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbDocument.Name = "cbDocument";
            this.cbDocument.Size = new System.Drawing.Size(95, 24);
            this.cbDocument.TabIndex = 34;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 22);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 17);
            this.label11.TabIndex = 33;
            this.label11.Text = "Document:";
            // 
            // txtNoBon
            // 
            this.txtNoBon.Location = new System.Drawing.Point(237, 19);
            this.txtNoBon.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNoBon.Name = "txtNoBon";
            this.txtNoBon.Size = new System.Drawing.Size(73, 22);
            this.txtNoBon.TabIndex = 32;
            this.txtNoBon.Enter += new System.EventHandler(this.txtNoBon_Enter);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(197, 21);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(28, 17);
            this.label12.TabIndex = 31;
            this.label12.Text = "N°:";
            // 
            // dtDate
            // 
            this.dtDate.Location = new System.Drawing.Point(118, 17);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(200, 22);
            this.dtDate.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(49, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Date:";
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(384, 17);
            this.dgv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 16;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(675, 364);
            this.dgv.TabIndex = 10;
            // 
            // btEnregistrer
            // 
            this.btEnregistrer.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEnregistrer.Location = new System.Drawing.Point(932, 385);
            this.btEnregistrer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btEnregistrer.Name = "btEnregistrer";
            this.btEnregistrer.Size = new System.Drawing.Size(127, 33);
            this.btEnregistrer.TabIndex = 11;
            this.btEnregistrer.Text = "Enregistrer";
            this.btEnregistrer.UseVisualStyleBackColor = true;
            this.btEnregistrer.Click += new System.EventHandler(this.buttonEnregistrer_Click);
            // 
            // btAnnuler
            // 
            this.btAnnuler.Location = new System.Drawing.Point(803, 385);
            this.btAnnuler.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(123, 33);
            this.btAnnuler.TabIndex = 12;
            this.btAnnuler.Text = "Annuler";
            this.btAnnuler.UseVisualStyleBackColor = true;
            this.btAnnuler.Click += new System.EventHandler(this.buttonAnnuler_Click);
            // 
            // FrOperation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1095, 429);
            this.Controls.Add(this.btAnnuler);
            this.Controls.Add(this.btEnregistrer);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dtDate);
            this.Controls.Add(this.tcOperation);
            this.Controls.Add(this.btEnlever);
            this.Controls.Add(this.btAjouter);
            this.Controls.Add(this.groupBox1);
            this.Name = "FrOperation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Verssement ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrOperation_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tcOperation.ResumeLayout(false);
            this.tpOperation.ResumeLayout(false);
            this.tpOperation.PerformLayout();
            this.panOperation.ResumeLayout(false);
            this.panOperation.PerformLayout();
            this.tpPayement.ResumeLayout(false);
            this.tpPayement.PerformLayout();
            this.panPayment.ResumeLayout(false);
            this.panPayment.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtParent;
        private System.Windows.Forms.TextBox txtClient;
        private System.Windows.Forms.Button btParcourrirClient;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSolde;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labMontantRecuOperation;
        private System.Windows.Forms.TextBox txtMontantReçuOperation;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbTypeOperation;
        private System.Windows.Forms.Button btEnlever;
        private System.Windows.Forms.Button btAjouter;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabControl tcOperation;
        private System.Windows.Forms.TabPage tpOperation;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNouveauSolde;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TabPage tpPayement;
        private System.Windows.Forms.TextBox txtMontantRecuPayement;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label labMontantRecupayement;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtMontantFacture;
        private System.Windows.Forms.Button btAfficher;
        private System.Windows.Forms.ComboBox cbDocument;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtNoBon;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Button btEnregistrer;
        private System.Windows.Forms.Button btAnnuler;
        private System.Windows.Forms.Panel panPayment;
        private System.Windows.Forms.Panel panOperation;
    }
}