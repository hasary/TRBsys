﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NsTRBsys.NsComercial
{
    public partial class FrListDestination : Form
    {
        ModelEntities db = new ModelEntities();
        public FrListDestination(int ClientID, bool isSelectionDialog = false, int? AllReadySelectedDestinationID = null)
        {
            InitializeComponent();
            panelSelect.Visible = isSelectionDialog;
            this.Client = db.Clients.Single(p => p.ID == ClientID);
            labNomClient.Text = Client.Nom;
            RefreshGrid();
            if (AllReadySelectedDestinationID == null)
            {
                var defDestination = Client.Destinations.SingleOrDefault(p => p.IsDefault == true);
                if (defDestination != null)
                {
                    SelectDestinationID(defDestination.ID);
                }
            }
            else
            {
                SelectDestinationID(AllReadySelectedDestinationID.Value);
            }
        }

        public void SelectDestinationID(int ID)
        {
            DataGridViewRow dd = null;
            dgv.ClearSelection();
            return;
            for (int i = 0; i < dgv.Rows.Count; i++)
            {
                if (dgv.Rows[i].Cells["ID"].Value.ToString().ParseToInt() == ID)
                {
                    dgv.Rows[i].Selected = true;
                    dd = dgv.Rows[i];
                }

            }
            dgv.Refresh();

        }
        public static Destination SelectionDialog(int ClientID, int? AllReadySelectedDestinationID = null)
        {

            FrListDestination l = new FrListDestination(ClientID, true, AllReadySelectedDestinationID);
            //int selectionID = 0;


            //   l.panelEdit.Visible = true;
            l.ShowDialog();

            return ReturnDestination;
        }


        private void RefreshGrid()
        {
            dgv.DataSource = Client.Destinations.Select(p =>
                new
                {
                    ID = p.ID,
                    Type = (p.IsReal == true) ? "Réel" : "",
                    Wilaya = p.Wilaya.Nom,
                    Raison_Sociale = p.NomRaisonSocial,
                    Adresse = p.Adresse,
                    Registre = p.NoRegistre,
                    Ident_Fiscal = p.NoIdentifiant,
                    Article = p.NoArticle,

                }).ToList();

            dgv.HideIDColumn();
        }



        public Client Client { get; set; }

        private void FrListDestination_FormClosed(object sender, FormClosedEventArgs e)
        {
            //db.Dispose();
        }

        private void buttonNouveau_Click(object sender, EventArgs e)
        {

            new FrDestinationEdit(Client.ID).ShowDialog();
            db.Refresh(System.Data.Objects.RefreshMode.StoreWins, db.Destinations);
            RefreshGrid();

        }

        private void buttonModifier_Click(object sender, EventArgs e)
        {
            var id = dgv.GetSelectedID();
            if (id != null)
            {
                var toEdit = Client.Destinations.Single(p => p.ID == id);
                if (toEdit.IsReal == true)
                {
                    new NsTRBsys.NsComercial.NsClient.FrEdit(Client.ID, false, "", "", false, true).ShowDialog();
                }
                else
                {
                    new FrDestinationEdit(Client.ID, id).ShowDialog();
                }

                db.Refresh(System.Data.Objects.RefreshMode.StoreWins, db.Destinations);
                RefreshGrid();
                SelectDestinationID(id.Value);
            }
        }

        private void buttonSupprimer_Click(object sender, EventArgs e)
        {
            var id = dgv.GetSelectedID();
            if (id != null)
            {
                var toDel = db.Destinations.Single(p => p.ID == id);
                if (toDel.Factures.Count() != 0)
                {
                    Tools.ShowError("Impossible de supprimer un objet utilisé");
                    return;
                }
                db.DeleteObject(toDel);
                db.Refresh(System.Data.Objects.RefreshMode.StoreWins, db.Destinations);
                RefreshGrid();
            }
        }

        private void buttonAnnuler_Click(object sender, EventArgs e)
        {
            ReturnDestination = null;
            Dispose();

        }

        private void buttonSelectOK_Click(object sender, EventArgs e)
        {
            var id = dgv.GetSelectedID();
            if (id != null)
            {
                ReturnDestination = db.Destinations.Single(p => p.ID == id);
                if (ReturnDestination.IsReal == true)
                {
                    if (string.IsNullOrEmpty(ReturnDestination.NoRegistre) ||
                       string.IsNullOrEmpty(ReturnDestination.NoArticle) ||
                       string.IsNullOrEmpty(ReturnDestination.NoIdentifiant))
                    {
                        new NsTRBsys.NsComercial.NsClient.FrEdit(Client.ID, false, "", "", true, true).ShowDialog();
                        db.Refresh(System.Data.Objects.RefreshMode.StoreWins, ReturnDestination);
                        db.Refresh(System.Data.Objects.RefreshMode.StoreWins, Client);
                        RefreshGrid();
                        SelectDestinationID(ReturnDestination.ID);
                    }
                    Dispose();

                }
                else
                {
                    Dispose();
                }

            }
            else
            {
                ReturnDestination = null;

                Dispose();
            }

        }



        public static Destination ReturnDestination { get; set; }

        private void dgv_DoubleClick(object sender, EventArgs e)
        {
            var id = dgv.GetSelectedID();
            if (id != null)
            {
                ReturnDestination = db.Destinations.Single(p => p.ID == id);
                if (ReturnDestination.IsReal == true)
                {
                    if (ReturnDestination.NoRegistre == null || ReturnDestination.NoArticle == null || ReturnDestination.NoIdentifiant == null)
                    {
                        new NsTRBsys.NsComercial.NsClient.FrEdit(Client.ID, false, "", "", true, true).ShowDialog();
                        db.Refresh(System.Data.Objects.RefreshMode.StoreWins, ReturnDestination);
                        RefreshGrid();
                        SelectDestinationID(ReturnDestination.ID);
                    }
                    Dispose();

                }
                else
                {
                    Dispose();
                }

            }
            else
            {
                ReturnDestination = null;

                Dispose();
            }
        }
    }
}
