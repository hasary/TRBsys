﻿using System.Windows.Forms;
namespace NsTRBsys.NsComercial
{
    partial class FrVersementFournisseur
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.LabelLettreDA = new System.Windows.Forms.Label();
            this.DA = new System.Windows.Forms.Label();
            this.txtmontantVers = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbFournisseur = new System.Windows.Forms.ComboBox();
            this.labSoldeFourni = new System.Windows.Forms.Label();
            this.txtNumCompte = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtObs = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cbModePayement = new System.Windows.Forms.ComboBox();
            this.labNewSolde = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpFournisseur = new System.Windows.Forms.TabPage();
            this.tpPartenaire = new System.Windows.Forms.TabPage();
            this.cbPartenaire = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.labSoldePartenaire = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tpFournisseur.SuspendLayout();
            this.tpPartenaire.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.LabelLettreDA);
            this.groupBox1.Controls.Add(this.DA);
            this.groupBox1.Controls.Add(this.txtmontantVers);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(19, 162);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(444, 101);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Versement";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(40, 28);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 17);
            this.label8.TabIndex = 2;
            this.label8.Text = "Montant:";
            // 
            // LabelLettreDA
            // 
            this.LabelLettreDA.AutoSize = true;
            this.LabelLettreDA.Font = new System.Drawing.Font("Book Antiqua", 10.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelLettreDA.Location = new System.Drawing.Point(19, 63);
            this.LabelLettreDA.Name = "LabelLettreDA";
            this.LabelLettreDA.Size = new System.Drawing.Size(14, 22);
            this.LabelLettreDA.TabIndex = 48;
            this.LabelLettreDA.Text = ".";
            // 
            // DA
            // 
            this.DA.AutoSize = true;
            this.DA.Location = new System.Drawing.Point(325, 33);
            this.DA.Name = "DA";
            this.DA.Size = new System.Drawing.Size(39, 17);
            this.DA.TabIndex = 1;
            this.DA.Text = "D.A.";
            // 
            // txtmontantVers
            // 
            this.txtmontantVers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtmontantVers.Location = new System.Drawing.Point(117, 28);
            this.txtmontantVers.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtmontantVers.Name = "txtmontantVers";
            this.txtmontantVers.Size = new System.Drawing.Size(205, 22);
            this.txtmontantVers.TabIndex = 0;
            this.txtmontantVers.TextChanged += new System.EventHandler(this.txtmontantVers_TextChanged);
            this.txtmontantVers.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtmontantVers_KeyPress);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = global::NsTRBsys.Properties.Resources.icon_16_allow;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(344, 433);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(119, 30);
            this.button1.TabIndex = 1;
            this.button1.Text = "Enregistrer";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Image = global::NsTRBsys.Properties.Resources.icon_16_deny;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(225, 433);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(112, 30);
            this.button2.TabIndex = 2;
            this.button2.Text = "Fermer";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // dtDate
            // 
            this.dtDate.Location = new System.Drawing.Point(225, 19);
            this.dtDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(238, 22);
            this.dtDate.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(175, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Date:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(53, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Solde:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 274);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 17);
            this.label6.TabIndex = 9;
            this.label6.Text = "Nouveau Solde:";
            // 
            // cbFournisseur
            // 
            this.cbFournisseur.FormattingEnabled = true;
            this.cbFournisseur.Location = new System.Drawing.Point(52, 12);
            this.cbFournisseur.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbFournisseur.Name = "cbFournisseur";
            this.cbFournisseur.Size = new System.Drawing.Size(313, 24);
            this.cbFournisseur.TabIndex = 42;
            this.cbFournisseur.SelectedIndexChanged += new System.EventHandler(this.cbFournisseur_SelectedIndexChanged);
            // 
            // labSoldeFourni
            // 
            this.labSoldeFourni.AutoSize = true;
            this.labSoldeFourni.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labSoldeFourni.Location = new System.Drawing.Point(116, 50);
            this.labSoldeFourni.Name = "labSoldeFourni";
            this.labSoldeFourni.Size = new System.Drawing.Size(17, 17);
            this.labSoldeFourni.TabIndex = 41;
            this.labSoldeFourni.Text = "0";
            // 
            // txtNumCompte
            // 
            this.txtNumCompte.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNumCompte.Location = new System.Drawing.Point(273, 47);
            this.txtNumCompte.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNumCompte.Name = "txtNumCompte";
            this.txtNumCompte.Size = new System.Drawing.Size(47, 22);
            this.txtNumCompte.TabIndex = 6;
            this.txtNumCompte.Visible = false;
            this.txtNumCompte.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtmontantVers_KeyPress);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.txtObs);
            this.groupBox3.Location = new System.Drawing.Point(20, 327);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(443, 92);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Observation";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 27);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(361, 17);
            this.label9.TabIndex = 42;
            this.label9.Text = "Informations supplementaire à noter pour ce versement.";
            // 
            // txtObs
            // 
            this.txtObs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtObs.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtObs.Location = new System.Drawing.Point(19, 47);
            this.txtObs.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtObs.Multiline = true;
            this.txtObs.Name = "txtObs";
            this.txtObs.Size = new System.Drawing.Size(408, 27);
            this.txtObs.TabIndex = 0;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(59, 299);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(109, 17);
            this.label11.TabIndex = 32;
            this.label11.Text = "Mode paiement:";
            this.label11.Visible = false;
            // 
            // cbModePayement
            // 
            this.cbModePayement.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbModePayement.FormattingEnabled = true;
            this.cbModePayement.Items.AddRange(new object[] {
            "Espèces"});
            this.cbModePayement.Location = new System.Drawing.Point(187, 295);
            this.cbModePayement.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbModePayement.Name = "cbModePayement";
            this.cbModePayement.Size = new System.Drawing.Size(152, 24);
            this.cbModePayement.TabIndex = 31;
            this.cbModePayement.Visible = false;
            // 
            // labNewSolde
            // 
            this.labNewSolde.AutoSize = true;
            this.labNewSolde.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labNewSolde.Location = new System.Drawing.Point(127, 274);
            this.labNewSolde.Name = "labNewSolde";
            this.labNewSolde.Size = new System.Drawing.Size(17, 17);
            this.labNewSolde.TabIndex = 41;
            this.labNewSolde.Text = "0";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpFournisseur);
            this.tabControl1.Controls.Add(this.tpPartenaire);
            this.tabControl1.Location = new System.Drawing.Point(18, 50);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(445, 105);
            this.tabControl1.TabIndex = 43;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tpFournisseur
            // 
            this.tpFournisseur.BackColor = System.Drawing.Color.LemonChiffon;
            this.tpFournisseur.Controls.Add(this.cbFournisseur);
            this.tpFournisseur.Controls.Add(this.label4);
            this.tpFournisseur.Controls.Add(this.labSoldeFourni);
            this.tpFournisseur.Controls.Add(this.txtNumCompte);
            this.tpFournisseur.Location = new System.Drawing.Point(4, 25);
            this.tpFournisseur.Name = "tpFournisseur";
            this.tpFournisseur.Padding = new System.Windows.Forms.Padding(3);
            this.tpFournisseur.Size = new System.Drawing.Size(437, 76);
            this.tpFournisseur.TabIndex = 0;
            this.tpFournisseur.Text = "Fournisseur";
            // 
            // tpPartenaire
            // 
            this.tpPartenaire.BackColor = System.Drawing.Color.LemonChiffon;
            this.tpPartenaire.Controls.Add(this.cbPartenaire);
            this.tpPartenaire.Controls.Add(this.label2);
            this.tpPartenaire.Controls.Add(this.labSoldePartenaire);
            this.tpPartenaire.Location = new System.Drawing.Point(4, 25);
            this.tpPartenaire.Name = "tpPartenaire";
            this.tpPartenaire.Padding = new System.Windows.Forms.Padding(3);
            this.tpPartenaire.Size = new System.Drawing.Size(437, 76);
            this.tpPartenaire.TabIndex = 1;
            this.tpPartenaire.Text = "Partenaire";
            // 
            // cbPartenaire
            // 
            this.cbPartenaire.FormattingEnabled = true;
            this.cbPartenaire.Location = new System.Drawing.Point(50, 13);
            this.cbPartenaire.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbPartenaire.Name = "cbPartenaire";
            this.cbPartenaire.Size = new System.Drawing.Size(313, 24);
            this.cbPartenaire.TabIndex = 46;
            this.cbPartenaire.SelectedIndexChanged += new System.EventHandler(this.cbPartenaire_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(49, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 17);
            this.label2.TabIndex = 44;
            this.label2.Text = "Solde:";
            // 
            // labSoldePartenaire
            // 
            this.labSoldePartenaire.AutoSize = true;
            this.labSoldePartenaire.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labSoldePartenaire.Location = new System.Drawing.Point(103, 48);
            this.labSoldePartenaire.Name = "labSoldePartenaire";
            this.labSoldePartenaire.Size = new System.Drawing.Size(17, 17);
            this.labSoldePartenaire.TabIndex = 45;
            this.labSoldePartenaire.Text = "0";
            // 
            // FrVersementFournisseur
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LemonChiffon;
            this.ClientSize = new System.Drawing.Size(486, 474);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.labNewSolde);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cbModePayement);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtDate);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrVersementFournisseur";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Versement Compte Fournisseur ou Partenaire";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tpFournisseur.ResumeLayout(false);
            this.tpFournisseur.PerformLayout();
            this.tpPartenaire.ResumeLayout(false);
            this.tpPartenaire.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label DA;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label LabelLettreDA;
        private TextBox txtmontantVers;
        private TextBox txtNumCompte;
        private TextBox txtObs;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbModePayement;
        private Label labSoldeFourni;
        private Label labNewSolde;
        private ComboBox cbFournisseur;
        private TabControl tabControl1;
        private TabPage tpFournisseur;
        private TabPage tpPartenaire;
        private ComboBox cbPartenaire;
        private Label label2;
        private Label labSoldePartenaire;
    }
}