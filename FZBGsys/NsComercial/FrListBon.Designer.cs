﻿namespace NsTRBsys.NsComercial
{
    partial class FrListBon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.chRechAvancee = new System.Windows.Forms.CheckBox();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.panDate = new System.Windows.Forms.Panel();
            this.dtDateAu = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.dtDateDu = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.panRechAvance = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.cbWilaya = new System.Windows.Forms.ComboBox();
            this.btCli = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtClient = new System.Windows.Forms.TextBox();
            this.txtNoBon = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tcDocument = new System.Windows.Forms.TabControl();
            this.tpFacture = new System.Windows.Forms.TabPage();
            this.dgvFacture = new System.Windows.Forms.DataGridView();
            this.btModifie = new System.Windows.Forms.Button();
            this.panEdit = new System.Windows.Forms.Panel();
            this.btAnnuler = new System.Windows.Forms.Button();
            this.labTotalBon = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.afficherLeContenueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifierLeBonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.annulerLeBonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmBonSortie = new System.Windows.Forms.ToolStripMenuItem();
            this.imprimerBonDeSortieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifierBonDeSortieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.annulerBonDeSortieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmBonLivraison = new System.Windows.Forms.ToolStripMenuItem();
            this.imprimerBonLivraisonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifierLivraisonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.annulerLivraisonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFacture = new System.Windows.Forms.ToolStripMenuItem();
            this.imprimerLaFactureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifierLaFactureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.annulerLaFactureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.btImprimerLiv = new System.Windows.Forms.Button();
            this.btImprimerFact = new System.Windows.Forms.Button();
            this.btRecherche = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.panDate.SuspendLayout();
            this.panRechAvance.SuspendLayout();
            this.tcDocument.SuspendLayout();
            this.tpFacture.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFacture)).BeginInit();
            this.panEdit.SuspendLayout();
            this.cMenu.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 17);
            this.label1.TabIndex = 11;
            this.label1.Text = "Date Création:";
            // 
            // chRechAvancee
            // 
            this.chRechAvancee.AutoSize = true;
            this.chRechAvancee.Location = new System.Drawing.Point(16, 68);
            this.chRechAvancee.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chRechAvancee.Name = "chRechAvancee";
            this.chRechAvancee.Size = new System.Drawing.Size(158, 21);
            this.chRechAvancee.TabIndex = 61;
            this.chRechAvancee.Text = "Recherche Avancée";
            this.chRechAvancee.UseVisualStyleBackColor = true;
            this.chRechAvancee.CheckedChanged += new System.EventHandler(this.chRechAvancee_CheckedChanged);
            // 
            // dtDate
            // 
            this.dtDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDate.Location = new System.Drawing.Point(125, 27);
            this.dtDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(127, 22);
            this.dtDate.TabIndex = 64;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 220);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "Date Facturation:";
            // 
            // panDate
            // 
            this.panDate.Controls.Add(this.dtDateAu);
            this.panDate.Controls.Add(this.label5);
            this.panDate.Controls.Add(this.dtDateDu);
            this.panDate.Controls.Add(this.label4);
            this.panDate.Location = new System.Drawing.Point(59, 243);
            this.panDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panDate.Name = "panDate";
            this.panDate.Size = new System.Drawing.Size(213, 70);
            this.panDate.TabIndex = 24;
            // 
            // dtDateAu
            // 
            this.dtDateAu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateAu.Location = new System.Drawing.Point(63, 38);
            this.dtDateAu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDateAu.Name = "dtDateAu";
            this.dtDateAu.Size = new System.Drawing.Size(139, 22);
            this.dtDateAu.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 17);
            this.label5.TabIndex = 20;
            this.label5.Text = "Au:";
            // 
            // dtDateDu
            // 
            this.dtDateDu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateDu.Location = new System.Drawing.Point(63, 10);
            this.dtDateDu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDateDu.Name = "dtDateDu";
            this.dtDateDu.Size = new System.Drawing.Size(139, 22);
            this.dtDateDu.TabIndex = 1;
            this.dtDateDu.Value = new System.DateTime(2014, 1, 1, 0, 0, 0, 0);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 17);
            this.label4.TabIndex = 18;
            this.label4.Text = "Du:";
            // 
            // panRechAvance
            // 
            this.panRechAvance.Controls.Add(this.label7);
            this.panRechAvance.Controls.Add(this.cbWilaya);
            this.panRechAvance.Controls.Add(this.btCli);
            this.panRechAvance.Controls.Add(this.label6);
            this.panRechAvance.Controls.Add(this.txtClient);
            this.panRechAvance.Controls.Add(this.txtNoBon);
            this.panRechAvance.Controls.Add(this.label3);
            this.panRechAvance.Controls.Add(this.panDate);
            this.panRechAvance.Controls.Add(this.label2);
            this.panRechAvance.Enabled = false;
            this.panRechAvance.Location = new System.Drawing.Point(16, 95);
            this.panRechAvance.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panRechAvance.Name = "panRechAvance";
            this.panRechAvance.Size = new System.Drawing.Size(291, 337);
            this.panRechAvance.TabIndex = 62;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(24, 161);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 17);
            this.label7.TabIndex = 30;
            this.label7.Text = "Destination";
            // 
            // cbWilaya
            // 
            this.cbWilaya.FormattingEnabled = true;
            this.cbWilaya.Location = new System.Drawing.Point(122, 158);
            this.cbWilaya.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbWilaya.Name = "cbWilaya";
            this.cbWilaya.Size = new System.Drawing.Size(139, 24);
            this.cbWilaya.TabIndex = 29;
            // 
            // btCli
            // 
            this.btCli.Location = new System.Drawing.Point(232, 119);
            this.btCli.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btCli.Name = "btCli";
            this.btCli.Size = new System.Drawing.Size(39, 23);
            this.btCli.TabIndex = 28;
            this.btCli.Text = "...";
            this.btCli.UseVisualStyleBackColor = true;
            this.btCli.Click += new System.EventHandler(this.btCli_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 94);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(204, 17);
            this.label6.TabIndex = 27;
            this.label6.Text = "Destination Client /  Partenaire:";
            // 
            // txtClient
            // 
            this.txtClient.Location = new System.Drawing.Point(11, 119);
            this.txtClient.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtClient.Name = "txtClient";
            this.txtClient.ReadOnly = true;
            this.txtClient.Size = new System.Drawing.Size(213, 22);
            this.txtClient.TabIndex = 26;
            this.txtClient.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtClient_KeyPress);
            // 
            // txtNoBon
            // 
            this.txtNoBon.Location = new System.Drawing.Point(109, 22);
            this.txtNoBon.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNoBon.Name = "txtNoBon";
            this.txtNoBon.Size = new System.Drawing.Size(72, 22);
            this.txtNoBon.TabIndex = 26;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(65, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 17);
            this.label3.TabIndex = 25;
            this.label3.Text = "N°:";
            // 
            // tcDocument
            // 
            this.tcDocument.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcDocument.Controls.Add(this.tpFacture);
            this.tcDocument.Location = new System.Drawing.Point(313, 20);
            this.tcDocument.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tcDocument.Name = "tcDocument";
            this.tcDocument.SelectedIndex = 0;
            this.tcDocument.Size = new System.Drawing.Size(849, 448);
            this.tcDocument.TabIndex = 65;
            this.tcDocument.SelectedIndexChanged += new System.EventHandler(this.tcDocument_SelectedIndexChanged);
            // 
            // tpFacture
            // 
            this.tpFacture.Controls.Add(this.dgvFacture);
            this.tpFacture.Location = new System.Drawing.Point(4, 25);
            this.tpFacture.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tpFacture.Name = "tpFacture";
            this.tpFacture.Size = new System.Drawing.Size(841, 419);
            this.tpFacture.TabIndex = 3;
            this.tpFacture.Text = "Facture";
            this.tpFacture.UseVisualStyleBackColor = true;
            // 
            // dgvFacture
            // 
            this.dgvFacture.AllowUserToAddRows = false;
            this.dgvFacture.AllowUserToDeleteRows = false;
            this.dgvFacture.AllowUserToResizeColumns = false;
            this.dgvFacture.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvFacture.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvFacture.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvFacture.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvFacture.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvFacture.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFacture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFacture.Location = new System.Drawing.Point(0, 0);
            this.dgvFacture.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvFacture.MultiSelect = false;
            this.dgvFacture.Name = "dgvFacture";
            this.dgvFacture.ReadOnly = true;
            this.dgvFacture.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvFacture.RowHeadersVisible = false;
            this.dgvFacture.RowTemplate.Height = 16;
            this.dgvFacture.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFacture.Size = new System.Drawing.Size(841, 419);
            this.dgvFacture.TabIndex = 11;
            this.dgvFacture.SelectionChanged += new System.EventHandler(this.dgvFacture_SelectionChanged);
            // 
            // btModifie
            // 
            this.btModifie.Location = new System.Drawing.Point(15, 7);
            this.btModifie.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btModifie.Name = "btModifie";
            this.btModifie.Size = new System.Drawing.Size(97, 34);
            this.btModifie.TabIndex = 58;
            this.btModifie.Text = "Modifier";
            this.btModifie.UseVisualStyleBackColor = true;
            this.btModifie.Click += new System.EventHandler(this.btModifieBon_Click);
            // 
            // panEdit
            // 
            this.panEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panEdit.Controls.Add(this.btAnnuler);
            this.panEdit.Controls.Add(this.btModifie);
            this.panEdit.Location = new System.Drawing.Point(864, 510);
            this.panEdit.Margin = new System.Windows.Forms.Padding(4);
            this.panEdit.Name = "panEdit";
            this.panEdit.Size = new System.Drawing.Size(293, 44);
            this.panEdit.TabIndex = 66;
            // 
            // btAnnuler
            // 
            this.btAnnuler.Image = global::NsTRBsys.Properties.Resources.icon_16_delete;
            this.btAnnuler.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btAnnuler.Location = new System.Drawing.Point(181, 7);
            this.btAnnuler.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(107, 34);
            this.btAnnuler.TabIndex = 58;
            this.btAnnuler.Text = "Supprimer";
            this.btAnnuler.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btAnnuler.UseVisualStyleBackColor = true;
            this.btAnnuler.Click += new System.EventHandler(this.btAnnuler_Click);
            // 
            // labTotalBon
            // 
            this.labTotalBon.AutoSize = true;
            this.labTotalBon.Location = new System.Drawing.Point(71, 10);
            this.labTotalBon.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labTotalBon.Name = "labTotalBon";
            this.labTotalBon.Size = new System.Drawing.Size(0, 17);
            this.labTotalBon.TabIndex = 60;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(8, 10);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 17);
            this.label8.TabIndex = 59;
            this.label8.Text = "Total:";
            // 
            // afficherLeContenueToolStripMenuItem
            // 
            this.afficherLeContenueToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.afficherLeContenueToolStripMenuItem.Name = "afficherLeContenueToolStripMenuItem";
            this.afficherLeContenueToolStripMenuItem.Size = new System.Drawing.Size(214, 24);
            this.afficherLeContenueToolStripMenuItem.Text = "Afficher le Contenu";
            // 
            // modifierLeBonToolStripMenuItem
            // 
            this.modifierLeBonToolStripMenuItem.Name = "modifierLeBonToolStripMenuItem";
            this.modifierLeBonToolStripMenuItem.Size = new System.Drawing.Size(214, 24);
            this.modifierLeBonToolStripMenuItem.Text = "Modifier le Bon";
            this.modifierLeBonToolStripMenuItem.Click += new System.EventHandler(this.modifierLeBonToolStripMenuItem_Click);
            // 
            // annulerLeBonToolStripMenuItem
            // 
            this.annulerLeBonToolStripMenuItem.Name = "annulerLeBonToolStripMenuItem";
            this.annulerLeBonToolStripMenuItem.Size = new System.Drawing.Size(214, 24);
            this.annulerLeBonToolStripMenuItem.Text = "Annuler le Bon";
            this.annulerLeBonToolStripMenuItem.Click += new System.EventHandler(this.annulerLeBonToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(211, 6);
            // 
            // tsmBonSortie
            // 
            this.tsmBonSortie.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.imprimerBonDeSortieToolStripMenuItem,
            this.modifierBonDeSortieToolStripMenuItem,
            this.annulerBonDeSortieToolStripMenuItem});
            this.tsmBonSortie.Name = "tsmBonSortie";
            this.tsmBonSortie.Size = new System.Drawing.Size(214, 24);
            this.tsmBonSortie.Text = "Bon de Sortie";
            // 
            // imprimerBonDeSortieToolStripMenuItem
            // 
            this.imprimerBonDeSortieToolStripMenuItem.Name = "imprimerBonDeSortieToolStripMenuItem";
            this.imprimerBonDeSortieToolStripMenuItem.Size = new System.Drawing.Size(233, 24);
            this.imprimerBonDeSortieToolStripMenuItem.Text = "Imprimer Bon de Sortie";
            this.imprimerBonDeSortieToolStripMenuItem.Click += new System.EventHandler(this.imprimerBonDeSortieToolStripMenuItem_Click);
            // 
            // modifierBonDeSortieToolStripMenuItem
            // 
            this.modifierBonDeSortieToolStripMenuItem.Name = "modifierBonDeSortieToolStripMenuItem";
            this.modifierBonDeSortieToolStripMenuItem.Size = new System.Drawing.Size(233, 24);
            this.modifierBonDeSortieToolStripMenuItem.Text = "Modifier Bon de Sortie";
            this.modifierBonDeSortieToolStripMenuItem.Click += new System.EventHandler(this.modifierBonDeSortieToolStripMenuItem_Click);
            // 
            // annulerBonDeSortieToolStripMenuItem
            // 
            this.annulerBonDeSortieToolStripMenuItem.Name = "annulerBonDeSortieToolStripMenuItem";
            this.annulerBonDeSortieToolStripMenuItem.Size = new System.Drawing.Size(233, 24);
            this.annulerBonDeSortieToolStripMenuItem.Text = "Annuler Bon de Sortie";
            this.annulerBonDeSortieToolStripMenuItem.Click += new System.EventHandler(this.annulerBonDeSortieToolStripMenuItem_Click);
            // 
            // tsmBonLivraison
            // 
            this.tsmBonLivraison.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.imprimerBonLivraisonToolStripMenuItem,
            this.modifierLivraisonToolStripMenuItem,
            this.annulerLivraisonToolStripMenuItem});
            this.tsmBonLivraison.Name = "tsmBonLivraison";
            this.tsmBonLivraison.Size = new System.Drawing.Size(214, 24);
            this.tsmBonLivraison.Text = "Bon de Livraison";
            // 
            // imprimerBonLivraisonToolStripMenuItem
            // 
            this.imprimerBonLivraisonToolStripMenuItem.Name = "imprimerBonLivraisonToolStripMenuItem";
            this.imprimerBonLivraisonToolStripMenuItem.Size = new System.Drawing.Size(231, 24);
            this.imprimerBonLivraisonToolStripMenuItem.Text = "Imprimer Bon Livraison";
            this.imprimerBonLivraisonToolStripMenuItem.Click += new System.EventHandler(this.imprimerBonLivraisonToolStripMenuItem_Click);
            // 
            // modifierLivraisonToolStripMenuItem
            // 
            this.modifierLivraisonToolStripMenuItem.Name = "modifierLivraisonToolStripMenuItem";
            this.modifierLivraisonToolStripMenuItem.Size = new System.Drawing.Size(231, 24);
            this.modifierLivraisonToolStripMenuItem.Text = "Modifier Livraison";
            this.modifierLivraisonToolStripMenuItem.Click += new System.EventHandler(this.modifierLivraisonToolStripMenuItem_Click);
            // 
            // annulerLivraisonToolStripMenuItem
            // 
            this.annulerLivraisonToolStripMenuItem.Name = "annulerLivraisonToolStripMenuItem";
            this.annulerLivraisonToolStripMenuItem.Size = new System.Drawing.Size(231, 24);
            this.annulerLivraisonToolStripMenuItem.Text = "Annuler Livraison";
            this.annulerLivraisonToolStripMenuItem.Click += new System.EventHandler(this.annulerLivraisonToolStripMenuItem_Click);
            // 
            // tsmFacture
            // 
            this.tsmFacture.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.imprimerLaFactureToolStripMenuItem,
            this.modifierLaFactureToolStripMenuItem,
            this.annulerLaFactureToolStripMenuItem});
            this.tsmFacture.Name = "tsmFacture";
            this.tsmFacture.Size = new System.Drawing.Size(214, 24);
            this.tsmFacture.Text = "Facture";
            // 
            // imprimerLaFactureToolStripMenuItem
            // 
            this.imprimerLaFactureToolStripMenuItem.Name = "imprimerLaFactureToolStripMenuItem";
            this.imprimerLaFactureToolStripMenuItem.Size = new System.Drawing.Size(207, 24);
            this.imprimerLaFactureToolStripMenuItem.Text = "Imprimer la Facture";
            this.imprimerLaFactureToolStripMenuItem.Click += new System.EventHandler(this.imprimerLaFactureToolStripMenuItem_Click);
            // 
            // modifierLaFactureToolStripMenuItem
            // 
            this.modifierLaFactureToolStripMenuItem.Name = "modifierLaFactureToolStripMenuItem";
            this.modifierLaFactureToolStripMenuItem.Size = new System.Drawing.Size(207, 24);
            this.modifierLaFactureToolStripMenuItem.Text = "Modifier la Facture";
            this.modifierLaFactureToolStripMenuItem.Click += new System.EventHandler(this.modifierLaFactureToolStripMenuItem_Click);
            // 
            // annulerLaFactureToolStripMenuItem
            // 
            this.annulerLaFactureToolStripMenuItem.Name = "annulerLaFactureToolStripMenuItem";
            this.annulerLaFactureToolStripMenuItem.Size = new System.Drawing.Size(207, 24);
            this.annulerLaFactureToolStripMenuItem.Text = "Annuler la Facture";
            this.annulerLaFactureToolStripMenuItem.Click += new System.EventHandler(this.annulerLaFactureToolStripMenuItem_Click);
            // 
            // cMenu
            // 
            this.cMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.afficherLeContenueToolStripMenuItem,
            this.modifierLeBonToolStripMenuItem,
            this.annulerLeBonToolStripMenuItem,
            this.toolStripSeparator1,
            this.tsmBonSortie,
            this.tsmBonLivraison,
            this.tsmFacture});
            this.cMenu.Name = "cMenu";
            this.cMenu.Size = new System.Drawing.Size(215, 154);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.Controls.Add(this.labTotalBon);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Location = new System.Drawing.Point(319, 474);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(497, 33);
            this.panel1.TabIndex = 12;
            // 
            // btImprimerLiv
            // 
            this.btImprimerLiv.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btImprimerLiv.Image = global::NsTRBsys.Properties.Resources.icon_16_levels;
            this.btImprimerLiv.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btImprimerLiv.Location = new System.Drawing.Point(533, 519);
            this.btImprimerLiv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btImprimerLiv.Name = "btImprimerLiv";
            this.btImprimerLiv.Size = new System.Drawing.Size(113, 34);
            this.btImprimerLiv.TabIndex = 58;
            this.btImprimerLiv.Text = "Livraison";
            this.btImprimerLiv.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btImprimerLiv.UseVisualStyleBackColor = true;
            this.btImprimerLiv.Click += new System.EventHandler(this.button1_Click);
            // 
            // btImprimerFact
            // 
            this.btImprimerFact.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btImprimerFact.Image = global::NsTRBsys.Properties.Resources.icon_16_component;
            this.btImprimerFact.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btImprimerFact.Location = new System.Drawing.Point(413, 519);
            this.btImprimerFact.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btImprimerFact.Name = "btImprimerFact";
            this.btImprimerFact.Size = new System.Drawing.Size(105, 34);
            this.btImprimerFact.TabIndex = 58;
            this.btImprimerFact.Text = "Facture";
            this.btImprimerFact.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btImprimerFact.UseVisualStyleBackColor = true;
            this.btImprimerFact.Click += new System.EventHandler(this.btImprimer_Click);
            // 
            // btRecherche
            // 
            this.btRecherche.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btRecherche.Image = global::NsTRBsys.Properties.Resources.icon_16_search;
            this.btRecherche.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btRecherche.Location = new System.Drawing.Point(16, 517);
            this.btRecherche.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btRecherche.Name = "btRecherche";
            this.btRecherche.Size = new System.Drawing.Size(125, 34);
            this.btRecherche.TabIndex = 63;
            this.btRecherche.Text = "Recherche";
            this.btRecherche.UseVisualStyleBackColor = true;
            this.btRecherche.Click += new System.EventHandler(this.btRecherche_Click);
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(327, 527);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 17);
            this.label9.TabIndex = 67;
            this.label9.Text = "Imprimer:";
            // 
            // FrListBon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1177, 571);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.btImprimerFact);
            this.Controls.Add(this.btImprimerLiv);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tcDocument);
            this.Controls.Add(this.dtDate);
            this.Controls.Add(this.panEdit);
            this.Controls.Add(this.panRechAvance);
            this.Controls.Add(this.btRecherche);
            this.Controls.Add(this.chRechAvancee);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrListBon";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Liste des documents";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrListBonAttribution_FormClosing);
            this.Shown += new System.EventHandler(this.FrListBon_Shown);
            this.panDate.ResumeLayout(false);
            this.panDate.PerformLayout();
            this.panRechAvance.ResumeLayout(false);
            this.panRechAvance.PerformLayout();
            this.tcDocument.ResumeLayout(false);
            this.tpFacture.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFacture)).EndInit();
            this.panEdit.ResumeLayout(false);
            this.cMenu.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chRechAvancee;
        private System.Windows.Forms.Button btRecherche;
        private System.Windows.Forms.Button btImprimerFact;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panDate;
        private System.Windows.Forms.DateTimePicker dtDateAu;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtDateDu;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panRechAvance;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtClient;
        private System.Windows.Forms.TextBox txtNoBon;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabControl tcDocument;
        private System.Windows.Forms.TabPage tpFacture;
        private System.Windows.Forms.DataGridView dgvFacture;
        private System.Windows.Forms.Button btModifie;
        private System.Windows.Forms.Button btAnnuler;
        private System.Windows.Forms.Panel panEdit;
        private System.Windows.Forms.ContextMenuStrip cMenu;
        private System.Windows.Forms.ToolStripMenuItem afficherLeContenueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modifierLeBonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem annulerLeBonToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem tsmBonSortie;
        private System.Windows.Forms.ToolStripMenuItem imprimerBonDeSortieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modifierBonDeSortieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem annulerBonDeSortieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmBonLivraison;
        private System.Windows.Forms.ToolStripMenuItem imprimerBonLivraisonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modifierLivraisonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem annulerLivraisonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmFacture;
        private System.Windows.Forms.ToolStripMenuItem imprimerLaFactureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modifierLaFactureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem annulerLaFactureToolStripMenuItem;
        private System.Windows.Forms.Label labTotalBon;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btImprimerLiv;
        private System.Windows.Forms.Button btCli;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbWilaya;
        private System.Windows.Forms.Label label9;
    }
}