﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NsTRBsys.NsComercial
{
    public partial class FrSelectDateFourni : Form
    {
        ModelEntities db = new ModelEntities();
        public FrSelectDateFourni()
        {
            InitializeComponent();
            InitialiseDate();

            cbFournisseur.Items.Add(new Fournisseur { ID = 0, Nom = "" });
            cbFournisseur.Items.AddRange(db.Fournisseurs.Where(p => p.Active == true).ToArray());
            cbFournisseur.SelectedIndex = (cbFournisseur.Items.Count != 2) ? 0 : 1;
            cbFournisseur.ValueMember = "ID";
            cbFournisseur.DisplayMember = "Nom";
            cbFournisseur.DropDownStyle = ComboBoxStyle.DropDownList;


            SelectionDone = false;
            //   txtFournisseur.Text = Founi.Nom;
            //   DateTimePicker dateTimePicker1 = new DateTimePicker();


        }

        private void InitialiseDate()
        {
            string[] monthText = { "Ja", "" };
            Dictionary<string, string> months = new Dictionary<string, string>();

            DateTime fin = DateTime.Now;
            DateTime debut = Tools.DateDebutOperations;
            dateTimePickerDu.Value = debut;
            dateTimePickerDu.MinDate = Tools.DateDebutOperations;
            for (DateTime i = debut; i < fin; i = i.AddMonths(1))
            {
                // months.Add(i.Month,
            }

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            dateTimePickerMonth.Visible = radioButtonMon.Checked;
        }

        private void radioButtonPeriode_CheckedChanged(object sender, EventArgs e)
        {
            panelPeriode.Visible = radioButtonPeriode.Checked;
        }

        private void radioButtonDate_CheckedChanged(object sender, EventArgs e)
        {
            dateTimePickerJourn.Visible = radioButtonDate.Checked;
        }

        public static DateTime From { get; set; }
        public static DateTime To { get; set; }
        public static bool SelectionDone = false;
        private void button1_Click(object sender, EventArgs e)
        {
            SelectionDone = true;
            button1.Enabled = false;
            DateTime du = DateTime.Now;
            DateTime au = DateTime.Now;

            if (radioButtonDate.Checked)
            {
                du = au = dateTimePickerJourn.Value;

            }

            if (radioButtonPeriode.Checked)
            {
                du = dateTimePickerDu.Value;
                au = dateTimePickerAu.Value;
            }
            if (radioButtonMon.Checked)
            {
                var dt = dateTimePickerMonth.Value;
                au = new DateTime(dt.Year, dt.Month, DateTime.DaysInMonth(dt.Year, dt.Month));
                du = new DateTime(dt.Year, dt.Month, 1);
            }

            //   Actions.Rapports.FrViewer.PrintRecapShowroom(du, au);
            From = du;
            if (From < Tools.DateDebutOperations)
            {
                From = Tools.DateDebutOperations;
            }
            To = au;
            //    progressBar1.Visible = false;

            decimal? soldeBugin = 0;// Founi.SoldeHistory;
            var dateHi = Tools.DateDebutOperations;// Founi.DateHistory;
           
         
                var tmp = Founi.Factures.Where(p => p.Date >= dateHi && p.Date < From).Sum(p => p.MontantTotalAchat);
                if (tmp != null)
                {
                    soldeBugin -= tmp;
                }

                // soldeBugin += Client.FactureRetours.Where(p =>  p.Etat != (int)EnEtatFacture.Annulée && p.BonRetour.Etat != (int)EnEtatBonAttribution.Annulé && p.Date > dateHi && p.Date < From).Sum(p => p.MontantTTC);
                var tmp2 = Founi.Operations.Where(p => p.Sen == "D" && p.Date >= dateHi && p.Date < From).Sum(p => p.Montant);
                if (tmp2 != null)
                {
                    soldeBugin -= tmp2;
                }

                var tmp3 = Founi.Operations.Where(p => p.Sen == "C" && p.Date >= dateHi && p.Date < From).Sum(p => p.Montant);
                if (tmp3 != null)
                {
                    soldeBugin += tmp3;
                }
          

            NsRapports.FrViewer.PrintReleverFourni(Founi, soldeBugin, From, To);
            // Dispose();
            button1.Enabled = true;

        }




        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }



        //   public Client Client { get; set; }

        private void FrSelectDateClient_FormClosed(object sender, FormClosedEventArgs e)
        {
            db.Dispose();
        }

        public Fournisseur Founi { get; set; }

        private void cbFournisseur_SelectedIndexChanged(object sender, EventArgs e)
        {
            Founi = (Fournisseur)cbFournisseur.SelectedItem;
            if (Founi != null && Founi.ID == 0)
            {
                Founi = null;
            }
        }
    }
}
