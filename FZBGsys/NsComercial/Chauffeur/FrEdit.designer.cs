﻿namespace NsTRBsys.NsComercial.NsChauffeur
{
    partial class FrEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonEnregistre = new System.Windows.Forms.Button();
            this.buttonAnnuler = new System.Windows.Forms.Button();
            this.Activite = new System.Windows.Forms.Label();
            this.txtPermi = new NsTRBsys.TextBoxx();
            this.txtMatricule = new NsTRBsys.TextBoxx();
            this.txtDatePC = new NsTRBsys.TextBoxx();
            this.txtNomraison = new NsTRBsys.TextBoxx();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTel = new NsTRBsys.TextBoxx();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonEnregistre
            // 
            this.buttonEnregistre.Image = global::NsTRBsys.Properties.Resources.icon_16_allow;
            this.buttonEnregistre.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonEnregistre.Location = new System.Drawing.Point(309, 249);
            this.buttonEnregistre.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonEnregistre.Name = "buttonEnregistre";
            this.buttonEnregistre.Size = new System.Drawing.Size(104, 33);
            this.buttonEnregistre.TabIndex = 0;
            this.buttonEnregistre.Text = "Enregistrer";
            this.buttonEnregistre.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonEnregistre.UseVisualStyleBackColor = true;
            this.buttonEnregistre.Click += new System.EventHandler(this.buttonEnregistrerEmployer_Click);
            // 
            // buttonAnnuler
            // 
            this.buttonAnnuler.Image = global::NsTRBsys.Properties.Resources.icon_16_deny;
            this.buttonAnnuler.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAnnuler.Location = new System.Drawing.Point(196, 249);
            this.buttonAnnuler.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonAnnuler.Name = "buttonAnnuler";
            this.buttonAnnuler.Size = new System.Drawing.Size(107, 33);
            this.buttonAnnuler.TabIndex = 1;
            this.buttonAnnuler.Text = "Annuler";
            this.buttonAnnuler.UseVisualStyleBackColor = true;
            this.buttonAnnuler.Click += new System.EventHandler(this.buttonAnnulerEmployer_Click);
            // 
            // Activite
            // 
            this.Activite.AutoSize = true;
            this.Activite.Location = new System.Drawing.Point(25, 169);
            this.Activite.Name = "Activite";
            this.Activite.Size = new System.Drawing.Size(109, 17);
            this.Activite.TabIndex = 22;
            this.Activite.Text = "Numero Permis:";
            // 
            // txtPermi
            // 
            this.txtPermi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPermi.Location = new System.Drawing.Point(135, 166);
            this.txtPermi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPermi.Name = "txtPermi";
            this.txtPermi.Size = new System.Drawing.Size(218, 22);
            this.txtPermi.TabIndex = 19;
            // 
            // txtMatricule
            // 
            this.txtMatricule.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMatricule.Location = new System.Drawing.Point(135, 60);
            this.txtMatricule.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtMatricule.Name = "txtMatricule";
            this.txtMatricule.Size = new System.Drawing.Size(218, 22);
            this.txtMatricule.TabIndex = 21;
            // 
            // txtDatePC
            // 
            this.txtDatePC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDatePC.Location = new System.Drawing.Point(135, 203);
            this.txtDatePC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDatePC.Name = "txtDatePC";
            this.txtDatePC.Size = new System.Drawing.Size(218, 22);
            this.txtDatePC.TabIndex = 18;
            // 
            // txtNomraison
            // 
            this.txtNomraison.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNomraison.Location = new System.Drawing.Point(135, 31);
            this.txtNomraison.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNomraison.Name = "txtNomraison";
            this.txtNomraison.Size = new System.Drawing.Size(218, 22);
            this.txtNomraison.TabIndex = 17;
            this.txtNomraison.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNomraison_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 17);
            this.label3.TabIndex = 16;
            this.label3.Text = "Immatriculation:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 206);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 17);
            this.label2.TabIndex = 14;
            this.label2.Text = "Livré le:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 17);
            this.label1.TabIndex = 13;
            this.label1.Text = "Nom / Prenom:";
            // 
            // txtTel
            // 
            this.txtTel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTel.Location = new System.Drawing.Point(135, 90);
            this.txtTel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTel.Name = "txtTel";
            this.txtTel.Size = new System.Drawing.Size(218, 22);
            this.txtTel.TabIndex = 25;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 90);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 17);
            this.label5.TabIndex = 24;
            this.label5.Text = "N° Telephone:";
            // 
            // FrEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 304);
            this.Controls.Add(this.txtTel);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Activite);
            this.Controls.Add(this.txtPermi);
            this.Controls.Add(this.txtMatricule);
            this.Controls.Add(this.txtDatePC);
            this.Controls.Add(this.txtNomraison);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonAnnuler);
            this.Controls.Add(this.buttonEnregistre);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrEdit";
            this.Text = "Modifier Chauffeur";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AdminEmployerNew_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonEnregistre;
        private System.Windows.Forms.Button buttonAnnuler;
        private System.Windows.Forms.Label Activite;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private TextBoxx txtPermi;
        private TextBoxx txtMatricule;
        private TextBoxx txtDatePC;
        private TextBoxx txtNomraison;
        private TextBoxx txtTel;
        private System.Windows.Forms.Label label5;
    }
}