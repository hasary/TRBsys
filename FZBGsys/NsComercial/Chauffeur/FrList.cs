﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Objects;

namespace NsTRBsys.NsComercial.NsChauffeur
{


    public partial class FrList : Form
    {
        ModelEntities db;
        public bool AddAucuneSelection { get; set; }

        public FrList()
        {
            db = new ModelEntities();
            this.InitType(false); // normal list
        }

        public FrList(bool IsSelectionDialog, ModelEntities db, Client client = null)
        {
            this.client = client;
            this.db = db;
            this.InitType(IsSelectionDialog);
        }

        private void InitType(bool IsSelectionDialog)
        {
            this.IsSelectionDialog = IsSelectionDialog;


            InitializeComponent();
            panelEdit.Visible = !IsSelectionDialog;
            panelSelect.Visible = IsSelectionDialog;
            if (IsSelectionDialog)
            {
                //    txtNomCli.Text = client.NomClientAndParent();
                //   txtNomCli.ReadOnly = true;

            }
            InitializeData();

        }


        private void InitializeData()
        {
            if (client != null)
            {
                RefreshGrid(String.Empty, client.ID, client.Nom);
            }
            else
            {
                RefreshGrid(String.Empty);

            }

        }

        private void RefreshGrid(string NomFilter, int? ClientID = null, string nomcli = "")
        {

            var Chaffeurs = db.Chauffeurs.Where(e => e.ID != 0);
            if (!string.IsNullOrEmpty(NomFilter))
            {
                Chaffeurs = Chaffeurs.Where(emp => emp.Nom.Contains(NomFilter));
            }

            //  List<MarbreBLIDA.Client> ListClientsList = null;

            if (!IsSelectionDialog)
            {
                dgv1.DataSource = Chaffeurs.ToList().OrderBy(data => data.Nom).Take(100).Select(
                          data => new
                          {
                              Numero = data.ID,
                              Nom = data.Nom,
                              Tetephone = data.NoTelephone,
                              Matricule = data.DefaultMatricule,
                              Permis = data.NumeroPC + " du " + data.DatePC,


                          }).ToList();
            }
            else
            {
                dgv1.DataSource = Chaffeurs.ToList().OrderBy(data => data.Nom).Take(100).Select(
                       data => new
                       {
                           Numero = data.ID,
                           Nom = data.Nom,
                           Tetephone = data.NoTelephone,
                           Matricule = data.DefaultMatricule,
                           Permis = data.NumeroPC + " du " + data.DatePC,

                       }).ToList();
            }

            dgv1.Columns[0].Visible = false;
        }
        public static Chauffeur SelectChaffeurDialog(Client client, ModelEntities db)
        {
            FrList e =
            new FrList(true, db, client);

            // e.buttonNouveau.Visible = false;
            e.ShowDialog();

            return ReturnChaffeur;

        }
        private void AdminClient_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }
        private void buttonOKClient_Click(object sender, EventArgs e)
        {

            if (!AddAucuneSelection || dgv1.SelectedRows[0].Index != 0)
            {
                var id = int.Parse(dgv1.SelectedRows[0].Cells[0].Value.ToString());
                ReturnChaffeur = db.Chauffeurs.Single(em => em.ID == id);
            }
            else
                ReturnChaffeur = null; // en cas de selection aucun
            Dispose();
        }
        private static Chauffeur ReturnChaffeur { get; set; }
        public bool IsSelectionDialog { get; set; } // if control is called for selection only (not for editing)
        private void buttonAnnuler_Click(object sender, EventArgs e)
        {
            ReturnChaffeur = null;
            Dispose();
        }
        private void buttonNewClient_Click(object sender, EventArgs e)
        {
            if (IsSelectionDialog)
            {
                ReturnChaffeur = FrCreate.CreateChaffeurDialog(db, client);

                Dispose(); // so return to Utilisateur Liste Selection,

            }
            else
            {
                new FrCreate(null, false).ShowDialog();

                InitializeData();
            }

        }
        private void buttonModifier_Click(object sender, EventArgs e)
        {
            var matricule = int.Parse(dgv1.SelectedRows[0].Cells[0].Value.ToString());
            var selectedChauffeur = db.Chauffeurs.Single(em => em.ID == matricule);
            db.Refresh(RefreshMode.StoreWins, selectedChauffeur);
            new FrEdit(selectedChauffeur.ID).ShowDialog();



            InitializeData();
        }
        private void buttonSupprimer_Click(object sender, EventArgs e)
        {
            var matricule = int.Parse(dgv1.SelectedRows[0].Cells[0].Value.ToString());
            var selectedChauffeur = db.Chauffeurs.Single(em => em.ID == matricule);

            if (selectedChauffeur.Factures.Count != 0)
            {
                this.ShowWarning("impossible de supprimer chaffeur car il existes des bon de livraison liés.");
                return;
            }
            if (this.ConfirmWarning("Supprimer le Chaffeur " + selectedChauffeur.Nom + "?"))
            {
                db.Chauffeurs.DeleteObject(selectedChauffeur);
                db.SaveChanges();
                InitializeData();

            }
        }

        private void txtNom_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLower(e.KeyChar))
            {
                e.KeyChar = char.ToUpper(e.KeyChar);
            }
        }

        private void buttonSelectOK_Click(object sender, EventArgs e)
        {
            if (!IsSelectionDialog) return;
            if (dgv1.SelectedRows.Count == 1)
            {
                var matricule = int.Parse(dgv1.SelectedRows[0].Cells[0].Value.ToString());
                var selectedClient = db.Chauffeurs.Single(em => em.ID == matricule);
                ReturnChaffeur = selectedClient;
            }
            else
            {
                ReturnChaffeur = null;
            }
            Dispose();
        }

        private void FrList_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!IsSelectionDialog) db.Dispose();
        }

        private void txtNom_TextChanged(object sender, EventArgs e)
        {
            //     txtNomCli.Text = "";
            RefreshGrid(txtNom.Text.ToUpper());
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        public Client client { get; set; }

        //    public string clientNom { get; set; }

        private void txtNomCli_TextChanged(object sender, EventArgs e)
        {
            txtNom.Text = "";
            RefreshGrid(null, null, "");
        }

        private void txtNomCli_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLower(e.KeyChar))
            {
                e.KeyChar = char.ToUpper(e.KeyChar);
            }
        }
    }
}
