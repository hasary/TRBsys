﻿namespace NsTRBsys.NsComercial.NsChauffeur
{
    partial class FrList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv1 = new System.Windows.Forms.DataGridView();
            this.buttonNouveau = new System.Windows.Forms.Button();
            this.buttonSupprimer = new System.Windows.Forms.Button();
            this.buttonModifier = new System.Windows.Forms.Button();
            this.panelEdit = new System.Windows.Forms.Panel();
            this.panelSelect = new System.Windows.Forms.Panel();
            this.buttonSelectOK = new System.Windows.Forms.Button();
            this.buttonAnnuler = new System.Windows.Forms.Button();
            this.txtNom = new NsTRBsys.TextBoxx();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgv1)).BeginInit();
            this.panelEdit.SuspendLayout();
            this.panelSelect.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv1
            // 
            this.dgv1.AllowUserToAddRows = false;
            this.dgv1.AllowUserToDeleteRows = false;
            this.dgv1.AllowUserToOrderColumns = true;
            this.dgv1.AllowUserToResizeColumns = false;
            this.dgv1.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv1.Location = new System.Drawing.Point(12, 62);
            this.dgv1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgv1.MultiSelect = false;
            this.dgv1.Name = "dgv1";
            this.dgv1.ReadOnly = true;
            this.dgv1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv1.RowHeadersVisible = false;
            this.dgv1.RowTemplate.Height = 16;
            this.dgv1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv1.Size = new System.Drawing.Size(664, 379);
            this.dgv1.TabIndex = 0;
            // 
            // buttonNouveau
            // 
            this.buttonNouveau.Image = global::NsTRBsys.Properties.Resources.icon_16_new1;
            this.buttonNouveau.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonNouveau.Location = new System.Drawing.Point(335, 450);
            this.buttonNouveau.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonNouveau.Name = "buttonNouveau";
            this.buttonNouveau.Size = new System.Drawing.Size(107, 31);
            this.buttonNouveau.TabIndex = 1;
            this.buttonNouveau.Text = "Nouveau";
            this.buttonNouveau.UseVisualStyleBackColor = true;
            this.buttonNouveau.Click += new System.EventHandler(this.buttonNewClient_Click);
            // 
            // buttonSupprimer
            // 
            this.buttonSupprimer.Image = global::NsTRBsys.Properties.Resources.icon_16_delete;
            this.buttonSupprimer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonSupprimer.Location = new System.Drawing.Point(123, 2);
            this.buttonSupprimer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSupprimer.Name = "buttonSupprimer";
            this.buttonSupprimer.Size = new System.Drawing.Size(103, 31);
            this.buttonSupprimer.TabIndex = 2;
            this.buttonSupprimer.Text = "Supprimer";
            this.buttonSupprimer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonSupprimer.UseVisualStyleBackColor = true;
            this.buttonSupprimer.Click += new System.EventHandler(this.buttonSupprimer_Click);
            // 
            // buttonModifier
            // 
            this.buttonModifier.Image = global::NsTRBsys.Properties.Resources.icon_16_edit;
            this.buttonModifier.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonModifier.Location = new System.Drawing.Point(16, 2);
            this.buttonModifier.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonModifier.Name = "buttonModifier";
            this.buttonModifier.Size = new System.Drawing.Size(100, 31);
            this.buttonModifier.TabIndex = 2;
            this.buttonModifier.Text = "Modifier";
            this.buttonModifier.UseVisualStyleBackColor = true;
            this.buttonModifier.Click += new System.EventHandler(this.buttonModifier_Click);
            // 
            // panelEdit
            // 
            this.panelEdit.Controls.Add(this.buttonModifier);
            this.panelEdit.Controls.Add(this.buttonSupprimer);
            this.panelEdit.Location = new System.Drawing.Point(448, 450);
            this.panelEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelEdit.Name = "panelEdit";
            this.panelEdit.Size = new System.Drawing.Size(228, 42);
            this.panelEdit.TabIndex = 3;
            // 
            // panelSelect
            // 
            this.panelSelect.Controls.Add(this.buttonSelectOK);
            this.panelSelect.Controls.Add(this.buttonAnnuler);
            this.panelSelect.Location = new System.Drawing.Point(12, 446);
            this.panelSelect.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelSelect.Name = "panelSelect";
            this.panelSelect.Size = new System.Drawing.Size(255, 42);
            this.panelSelect.TabIndex = 3;
            this.panelSelect.Visible = false;
            // 
            // buttonSelectOK
            // 
            this.buttonSelectOK.Image = global::NsTRBsys.Properties.Resources.icon_16_allow;
            this.buttonSelectOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonSelectOK.Location = new System.Drawing.Point(3, 2);
            this.buttonSelectOK.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSelectOK.Name = "buttonSelectOK";
            this.buttonSelectOK.Size = new System.Drawing.Size(116, 31);
            this.buttonSelectOK.TabIndex = 2;
            this.buttonSelectOK.Text = "Selectionner";
            this.buttonSelectOK.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonSelectOK.UseVisualStyleBackColor = true;
            this.buttonSelectOK.Click += new System.EventHandler(this.buttonSelectOK_Click);
            // 
            // buttonAnnuler
            // 
            this.buttonAnnuler.Image = global::NsTRBsys.Properties.Resources.icon_16_deny;
            this.buttonAnnuler.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAnnuler.Location = new System.Drawing.Point(125, 2);
            this.buttonAnnuler.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonAnnuler.Name = "buttonAnnuler";
            this.buttonAnnuler.Size = new System.Drawing.Size(87, 31);
            this.buttonAnnuler.TabIndex = 2;
            this.buttonAnnuler.Text = "Annuler";
            this.buttonAnnuler.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonAnnuler.UseVisualStyleBackColor = true;
            this.buttonAnnuler.Click += new System.EventHandler(this.buttonAnnuler_Click);
            // 
            // txtNom
            // 
            this.txtNom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNom.Location = new System.Drawing.Point(197, 21);
            this.txtNom.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNom.Name = "txtNom";
            this.txtNom.Size = new System.Drawing.Size(199, 22);
            this.txtNom.TabIndex = 10;
            this.txtNom.TextChanged += new System.EventHandler(this.txtNom_TextChanged);
            this.txtNom.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNom_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(180, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "Recherche Nom Chauffeur:";
            // 
            // FrList
            // 
            this.AcceptButton = this.buttonSelectOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(688, 508);
            this.Controls.Add(this.panelSelect);
            this.Controls.Add(this.txtNom);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panelEdit);
            this.Controls.Add(this.buttonNouveau);
            this.Controls.Add(this.dgv1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Liste des Chaffeurs";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrList_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgv1)).EndInit();
            this.panelEdit.ResumeLayout(false);
            this.panelSelect.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv1;
        private System.Windows.Forms.Button buttonNouveau;
        private System.Windows.Forms.Button buttonSupprimer;
        private System.Windows.Forms.Button buttonModifier;
        private System.Windows.Forms.Panel panelEdit;
        private System.Windows.Forms.Panel panelSelect;
        private System.Windows.Forms.Button buttonSelectOK;
        private System.Windows.Forms.Button buttonAnnuler;
        private System.Windows.Forms.Label label2;
        private TextBoxx txtNom;
    }
}