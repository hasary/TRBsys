﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace NsTRBsys.NsComercial.NsChauffeur
{
    public partial class FrEdit : Form
    {
        public static Chauffeur ReturnChaffeur { get; set; }
        public bool IsSelectionDialogReturn { get; set; }
        private ModelEntities db = new ModelEntities();
        public FrEdit(int IDChaffeur)
        {
            this.IDChaffeur = IDChaffeur;
            InitializeComponent();
            InitializeData();
            IsSelectionDialogReturn = false;
        }

        private void InitializeData()
        {

            ReturnChaffeur = db.Chauffeurs.Single(c => c.ID == this.IDChaffeur);
         //   this.Client = ReturnChaffeur.Client;
        //    txtMatricule.Text = ReturnChaffeur.Client.Nom;
            txtNomraison.Text = ReturnChaffeur.Nom;
            txtPermi.Text = ReturnChaffeur.NumeroPC;
            txtDatePC.Text = ReturnChaffeur.DatePC;
            txtMatricule.Text = ReturnChaffeur.DefaultMatricule;
            txtTel.Text = ReturnChaffeur.NoTelephone;
        }
        private void buttonEnregistrerEmployer_Click(object sender, EventArgs e)
        {
            if (isValidAll())
            {

                ReturnChaffeur.Nom = txtNomraison.Text.Trim();
              //  ReturnChaffeur.Client = this.Client;
                ReturnChaffeur.NumeroPC = txtPermi.Text.Trim();
                ReturnChaffeur.DatePC = txtDatePC.Text.Trim();
                ReturnChaffeur.DefaultMatricule = txtMatricule.Text.Trim();
                ReturnChaffeur.NoTelephone = txtTel.Text.Trim();

                db.SaveChanges();

                Dispose();

            }
        }
        private bool isValidAll()
        {
            string ErrorMessage = String.Empty;


            if (txtNomraison.Text.Trim().Length < 1 || txtNomraison.Text.Length > 10)
            {

                ErrorMessage += "\nCe Nom est Invalide (doit faire entre 2 et 10 caracteres).";
            }
            else
            {
                var matricule = txtNomraison.Text.Trim();
                var Uti = db.Chauffeurs.FirstOrDefault(em => em.Nom == matricule && em.ID != IDChaffeur);

                if (Uti != null)
                {
                    ErrorMessage += "\nCe Nom existe déja dans la liste des Chaffeur";

                }
            }


            if (ErrorMessage != String.Empty)
            {
                Tools.ShowError(ErrorMessage);
                // MessageBox.Show(ErrorMessage, "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }
        private void AdminEmployerNew_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }
        private void buttonAnnulerEmployer_Click(object sender, EventArgs e)
        {
            ReturnChaffeur = null;
            Dispose();
        }

        public int IDChaffeur { get; set; }

        public Client Client { get; set; }

        private void button1_Click(object sender, EventArgs e)
        {

            this.Client = NsTRBsys.NsComercial.NsClient.FrList.SelectClientDialog(db);
            if (Client != null)
            {
           //     txtClientNom.Text = Client.Nom;

            }
        }

        private void txtNomraison_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLower(e.KeyChar))
            {
                e.KeyChar = char.ToUpper(e.KeyChar);
            }
        }
    }
}
