﻿using System.Windows.Forms;
namespace NsTRBsys.NsComercial
{
    partial class FrVersementCompte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.LabelLettreDA = new System.Windows.Forms.Label();
            this.DA = new System.Windows.Forms.Label();
            this.txtmontantVers = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.labSolde = new System.Windows.Forms.Label();
            this.txtNumCompte = new System.Windows.Forms.TextBox();
            this.txtNomCli = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtObs = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cbModePayement = new System.Windows.Forms.ComboBox();
            this.labNewSolde = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.LabelLettreDA);
            this.groupBox1.Controls.Add(this.DA);
            this.groupBox1.Controls.Add(this.txtmontantVers);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(18, 162);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(492, 101);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Versement";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(92, 28);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 17);
            this.label8.TabIndex = 2;
            this.label8.Text = "Montant:";
            // 
            // LabelLettreDA
            // 
            this.LabelLettreDA.AutoSize = true;
            this.LabelLettreDA.Font = new System.Drawing.Font("Book Antiqua", 10.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelLettreDA.Location = new System.Drawing.Point(71, 63);
            this.LabelLettreDA.Name = "LabelLettreDA";
            this.LabelLettreDA.Size = new System.Drawing.Size(14, 22);
            this.LabelLettreDA.TabIndex = 48;
            this.LabelLettreDA.Text = ".";
            // 
            // DA
            // 
            this.DA.AutoSize = true;
            this.DA.Location = new System.Drawing.Point(390, 30);
            this.DA.Name = "DA";
            this.DA.Size = new System.Drawing.Size(39, 17);
            this.DA.TabIndex = 1;
            this.DA.Text = "D.A.";
            // 
            // txtmontantVers
            // 
            this.txtmontantVers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtmontantVers.Location = new System.Drawing.Point(169, 28);
            this.txtmontantVers.Name = "txtmontantVers";
            this.txtmontantVers.Size = new System.Drawing.Size(204, 22);
            this.txtmontantVers.TabIndex = 0;
            this.txtmontantVers.TextChanged += new System.EventHandler(this.txtmontantVers_TextChanged);
            this.txtmontantVers.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtmontantVers_KeyPress);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = global::NsTRBsys.Properties.Resources.icon_16_allow;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(388, 451);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(119, 30);
            this.button1.TabIndex = 1;
            this.button1.Text = "Enregistrer";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Image = global::NsTRBsys.Properties.Resources.icon_16_deny;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(270, 451);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(112, 30);
            this.button2.TabIndex = 2;
            this.button2.Text = "Fermer";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // dtDate
            // 
            this.dtDate.Location = new System.Drawing.Point(135, 29);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(240, 22);
            this.dtDate.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Date Versement:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "N°Compte:";
            this.label2.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "Client:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(40, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Solde:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 274);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 17);
            this.label6.TabIndex = 9;
            this.label6.Text = "Nouveau Solde:";
            // 
            // button3
            // 
            this.button3.Image = global::NsTRBsys.Properties.Resources.icon_16_module;
            this.button3.Location = new System.Drawing.Point(400, 21);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 10;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.labSolde);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.txtNumCompte);
            this.groupBox2.Controls.Add(this.txtNomCli);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(18, 57);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(492, 99);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Compte";
            // 
            // labSolde
            // 
            this.labSolde.AutoSize = true;
            this.labSolde.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labSolde.Location = new System.Drawing.Point(109, 55);
            this.labSolde.Name = "labSolde";
            this.labSolde.Size = new System.Drawing.Size(17, 17);
            this.labSolde.TabIndex = 41;
            this.labSolde.Text = "0";
            // 
            // txtNumCompte
            // 
            this.txtNumCompte.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNumCompte.Location = new System.Drawing.Point(419, 51);
            this.txtNumCompte.Name = "txtNumCompte";
            this.txtNumCompte.Size = new System.Drawing.Size(48, 22);
            this.txtNumCompte.TabIndex = 6;
            this.txtNumCompte.Visible = false;
            this.txtNumCompte.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtmontantVers_KeyPress);
            this.txtNumCompte.Leave += new System.EventHandler(this.txtNumCompte_Leave);
            // 
            // txtNomCli
            // 
            this.txtNomCli.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNomCli.Location = new System.Drawing.Point(112, 21);
            this.txtNomCli.Name = "txtNomCli";
            this.txtNomCli.ReadOnly = true;
            this.txtNomCli.Size = new System.Drawing.Size(280, 22);
            this.txtNomCli.TabIndex = 7;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(154, 266);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(339, 17);
            this.label10.TabIndex = 40;
            this.label10.Text = "Cliquer sur le button \"Parcourir\" pour afficher la liste ";
            this.label10.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.txtObs);
            this.groupBox3.Location = new System.Drawing.Point(20, 347);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(487, 92);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Observation";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 27);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(361, 17);
            this.label9.TabIndex = 42;
            this.label9.Text = "Informations supplementaire à noter pour ce versement.";
            // 
            // txtObs
            // 
            this.txtObs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtObs.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtObs.Location = new System.Drawing.Point(19, 47);
            this.txtObs.Multiline = true;
            this.txtObs.Name = "txtObs";
            this.txtObs.Size = new System.Drawing.Size(454, 27);
            this.txtObs.TabIndex = 0;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(59, 311);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(113, 17);
            this.label11.TabIndex = 32;
            this.label11.Text = "Mode payement:";
            this.label11.Visible = false;
            // 
            // cbModePayement
            // 
            this.cbModePayement.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbModePayement.FormattingEnabled = true;
            this.cbModePayement.Items.AddRange(new object[] {
            "Espèces"});
            this.cbModePayement.Location = new System.Drawing.Point(187, 308);
            this.cbModePayement.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbModePayement.Name = "cbModePayement";
            this.cbModePayement.Size = new System.Drawing.Size(152, 24);
            this.cbModePayement.TabIndex = 31;
            this.cbModePayement.Visible = false;
            // 
            // labNewSolde
            // 
            this.labNewSolde.AutoSize = true;
            this.labNewSolde.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labNewSolde.Location = new System.Drawing.Point(127, 274);
            this.labNewSolde.Name = "labNewSolde";
            this.labNewSolde.Size = new System.Drawing.Size(17, 17);
            this.labNewSolde.TabIndex = 41;
            this.labNewSolde.Text = "0";
            // 
            // FrVersementCompte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Azure;
            this.ClientSize = new System.Drawing.Size(533, 501);
            this.Controls.Add(this.labNewSolde);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cbModePayement);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtDate);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox1);
            this.Name = "FrVersementCompte";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Versement Compte Client / Partenaire";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label DA;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label LabelLettreDA;
        private TextBox txtmontantVers;
        private TextBox txtNumCompte;
        private TextBox txtNomCli;
        private TextBox txtObs;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbModePayement;
        private Label labSolde;
        private Label labNewSolde;
    }
}