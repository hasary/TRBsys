﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NsTRBsys.NsComercial
{
    public partial class FrNEDepos : Form
    {
        ModelEntities db = new ModelEntities();
        public FrNEDepos(int? EditID = null)
        {
            InitializeComponent();
            initializeContorls();

            if (EditID != null)
            {
               
                this.Depo = db.Depoes.Single(p => p.ID == EditID);
                txtAdress.Text = Depo.Adresse;
                txtNom.Text = Depo.Nom;
                txtResponsable.Text = Depo.AgentRespensable;
                txtTelephone.Text = Depo.NoTelephone;
              
                chActif.Checked = Depo.publish.Value;
                RefreshGrid();
            }
            else
            {
                tabControl1.TabPages.Remove(tabPage2);
            }

            initializeContorls();
            if (EditID != null)
            {
                cbWilaya.SelectedItem = Depo.Wilaya;
            } 
            
        }

        private void RefreshGrid()
        {
          

            if (this.Depo != null)
            {

                dgv.DataSource = Depo.StockProduits.Select(p => new
                {
                    ID = p.ID,
                    Produit = p.GoutProduit.Nom,
                    Format = p.Format.Volume,
                    Palettes = p.QuantitePalette.ToString()
                }).ToList();

                dgv.HideIDColumn();
            }
        }

        private void initializeContorls()
        {
            cbWilaya.Items.Add(new Wilaya { ID = 0, Nom = "" });
            cbWilaya.Items.AddRange(db.Wilayas.Where(p => p.ID > 0).ToArray());
            cbWilaya.SelectedIndex = 0;
            cbWilaya.ValueMember = "ID";
            cbWilaya.DisplayMember = "Nom";

            if (Depo != null)
            {
                var GoutID = db.PrixProduits.Select(p => p.GoutProduitID).ToList();
                var FormatID = db.PrixProduits.Select(p => p.FormatID).ToList();

                cbGoutProduit.Items.Add(new GoutProduit { ID = 0, Nom = "" });
                cbGoutProduit.Items.AddRange(db.GoutProduits.Where(p => p.ID > 0 && GoutID.Contains(p.ID)).ToArray());
                cbGoutProduit.SelectedIndex = 0;
                cbGoutProduit.ValueMember = "ID";
                cbGoutProduit.DisplayMember = "Nom";
                cbGoutProduit.DropDownStyle = ComboBoxStyle.DropDownList;

                //---------------------- Format (Bouteilles)
                cbFormat.Items.Add(new Format { ID = 0, Volume = "" });
                cbFormat.Items.AddRange(db.Formats.Where(p => p.ID > 0 && FormatID.Contains(p.ID)).ToArray());
                cbFormat.DisplayMember = "Volume";
                cbFormat.ValueMember = "ID";
                cbFormat.DropDownStyle = ComboBoxStyle.DropDownList;
                cbFormat.SelectedIndex = 0;
                //-------------------------------------------------------------- 
            }

        }

        public Depo Depo { get; set; }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void FrNEDepos_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        private void btAnnuler_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btEnregistre_Click(object sender, EventArgs e)
        {
            if (Depo == null)
            {
                Depo = new Depo();
                db.AddToDepoes(Depo);
            }

            Depo.Adresse = txtAdress.Text;
            Depo.AgentRespensable = txtResponsable.Text;
            Depo.NoTelephone = txtTelephone.Text;
            Depo.Nom = txtNom.Text;
            Depo.Wilaya = (Wilaya)cbWilaya.SelectedItem;
            Depo.publish = chActif.Checked;

            db.SaveChanges();

            Dispose();

        }

        private void btAjouter_Click(object sender, EventArgs e)
        {
            if (Depo == null)
            {
                Depo = new Depo();
            }
            var format = (Format)cbFormat.SelectedItem;
            var gout = (GoutProduit)cbGoutProduit.SelectedItem;
            var existes = Depo.StockProduits.SingleOrDefault(p => p.FormatID == format.ID && p.GoutProduitID == gout.ID);
            if (existes == null)
            {
                Depo.StockProduits.Add(new StockProduit { FormatID = format.ID, GoutProduitID = gout.ID, QuantitePalette = txtQttPal.Text.ParseToInt() });

            }
            else
            {
                existes.QuantitePalette = txtQttPal.Text.ParseToInt();
            }
            txtQttPal.Text = "";
           //  db.SaveChanges();
            RefreshGrid();
        }

        private void btEnlever_Click(object sender, EventArgs e)
        {
            var id = dgv.GetSelectedID(); 
            if (id != null)
            {

                var toDel = db.StockProduits.Single(p => p.ID == id);
                db.DeleteObject(toDel); 
            }
            //  db.SaveChanges();
            RefreshGrid();
        }
    }
}
