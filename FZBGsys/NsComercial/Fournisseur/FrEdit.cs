﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NsTRBsys;

namespace NsTRBsys.NsComercial.NsFournisseur
{
    public partial class FrEdit : Form
    {
        public static Fournisseur ReturnFournisseur { get; set; }
        public bool IsSelectionDialogReturn { get; set; }
        private ModelEntities db = new ModelEntities();
        public FrEdit(int IDFournisseur, bool isCreationTemp = false, string TempNom = "", string TempAdresse = "")
        {
            this.isCreationTemp = isCreationTemp;
            this.IDFournisseur = IDFournisseur;
            InitializeComponent();
            /*
                        cbTypeFournisseur.Items.Clear();
                        //    cbTypeFournisseur.Items.Add(new TypeFournisseur { ID = 0, Nom = "" });
                        //    cbTypeFournisseur.Items.AddRange(db.TypeFournisseurs.Where(p => p.ID > 0).ToArray());
                        cbTypeFournisseur.DropDownStyle = ComboBoxStyle.DropDownList;
                        cbTypeFournisseur.SelectedIndex = 0;
                        cbTypeFournisseur.ValueMember = "ID";
                        cbTypeFournisseur.DisplayMember = "Nom";

            */

            InitializeData(TempNom, TempAdresse);
            IsSelectionDialogReturn = false;
        }

        private void InitializeData(string TempNom = "", string TempAdresse = "")
        {
            Canceled = null;
            if (!isCreationTemp)
            {
                ReturnFournisseur = db.Fournisseurs.Single(c => c.ID == this.IDFournisseur);
            }
            else
            {
                ReturnFournisseur = new Fournisseur() { Nom = TempNom, Adresse = TempAdresse, NoFiscale = "", NoArticle = "", NoRegistre = "", Telephone = "" };
            }
            txtAdresse.Text = ReturnFournisseur.Adresse;
            txtNomraison.Text = ReturnFournisseur.Nom;
            txtTelephone.Text = ReturnFournisseur.Telephone;
            txtRegistre.Text = ReturnFournisseur.NoRegistre;
            txtArticle.Text = ReturnFournisseur.NoArticle;
            txtFiscal.Text = ReturnFournisseur.NoFiscale;

            //  cbTypeFournisseur.SelectedItem = ReturnFournisseur.TypeFournisseur;

            cbWilaya.Items.Add(new Wilaya { ID = 0, Nom = "" });
            cbWilaya.Items.AddRange(db.Wilayas.Where(p => p.ID > 0).ToArray());
            cbWilaya.SelectedItem = ReturnFournisseur.Wilaya;
            cbWilaya.ValueMember = "ID";
            cbWilaya.DisplayMember = "Nom";



        }
        private void buttonEnregistrerEmployer_Click(object sender, EventArgs e)
        {
            if (isValidAll())
            {

                ReturnFournisseur.Nom = txtNomraison.Text.Trim();
                ReturnFournisseur.Adresse = txtAdresse.Text.Trim();
                ReturnFournisseur.Telephone = txtTelephone.Text.Trim();
                ReturnFournisseur.Wilaya = (Wilaya)cbWilaya.SelectedItem;


                ReturnFournisseur.NoRegistre = txtRegistre.Text.Trim().ToUpper();
                ReturnFournisseur.NoArticle = txtArticle.Text.Trim().ToUpper();
                ReturnFournisseur.NoFiscale = txtFiscal.Text.Trim();

                db.SaveChanges();
                Canceled = false;
                Dispose();

            }
        }

        public static Fournisseur createFournisseurTemp(string tempNom, string tempAdre)
        {

            new FrEdit(0, true, tempNom, tempAdre).ShowDialog();
            return ReturnFournisseur;


        }

        private bool isValidAll()
        {
            string ErrorMessage = String.Empty;


            if (txtNomraison.Text.Trim().Length < 1 || txtNomraison.Text.Length > 70)
            {

                ErrorMessage += "\nCe Nom est Invalide (doit faire entre 2 et 70 caracteres).";
            }
            else
            {
                var matricule = txtNomraison.Text.Trim();
                var Uti = db.Fournisseurs.FirstOrDefault(em => em.Nom == matricule && em.ID != IDFournisseur);

                if (Uti != null)
                {
                    ErrorMessage += "\nCe Nom existe déja dans la liste des Fournisseur";

                }
            }

            if (cbWilaya.SelectedIndex == 0)
            {
                //  ErrorMessage += "\nVeillez selectionner wilaya!";
            }
            /*    if (cbTypeFournisseur.SelectedIndex < 1)
                {
                    ErrorMessage += "\nVeillez selectionner un type Fournisseur";
                }
    */
            if (panReg.Visible)
            {
                //  informations registre commerce
            }

            if (chIsParent.Checked && cbParentFournisseur.SelectedIndex == 0)
            {
                ErrorMessage += "\nVeillez spécifier ou selectionné le nom du parent";
            }

            if (ErrorMessage != String.Empty)
            {
                Tools.ShowError(ErrorMessage);
                // MessageBox.Show(ErrorMessage, "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }
        private void AdminEmployerNew_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Canceled == null)
            {
                Canceled = true;
            }
            db.Dispose();
        }
        private void buttonAnnulerEmployer_Click(object sender, EventArgs e)
        {
            ReturnFournisseur = null;

            Dispose();
        }

        public int IDFournisseur { get; set; }

        public static bool? Canceled { get; set; }

        public bool isCreationTemp { get; set; }



        private void chIsParent_CheckedChanged(object sender, EventArgs e)
        {
            cbParentFournisseur.Visible = chIsParent.Checked;
        }

        private void cbTypeFournisseur_SelectedIndexChanged(object sender, EventArgs e)
        {
            /* if (cbTypeFournisseur.SelectedIndex > 0)
             {
                 //      panReg.Visible = ((TypeFournisseur)cbTypeFournisseur.SelectedItem).HasRegistre.Value;
             }
             */
        }

        private void cbParentFournisseur_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLower(e.KeyChar))
            {
                e.KeyChar = char.ToUpper(e.KeyChar);
            }
        }
    }
}
