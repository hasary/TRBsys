﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Objects;

namespace NsTRBsys.NsComercial.NsFournisseur
{


    public partial class FrList : Form
    {
        ModelEntities db;
        public bool AddAucuneSelection { get; set; }

        public FrList()
        {
            db = new ModelEntities();
            this.InitType(false); // normal list
        }
        public bool ActiveOnly = false;
        public FrList(bool IsSelectionDialog, ModelEntities db, bool ActiveOnly = false)
        {
            this.db = db;
            this.ActiveOnly = ActiveOnly;
            this.InitType(IsSelectionDialog);

            txtNom.Select();


        }

        private void InitType(bool IsSelectionDialog)
        {
            this.IsSelectionDialog = IsSelectionDialog;


            InitializeComponent();
            panelEdit.Visible = !IsSelectionDialog;
            panelSelect.Visible = IsSelectionDialog;
            if (IsSelectionDialog)
            {
                this.AcceptButton = btSelectOK;
                this.CancelButton = btAnnuler;
            }
            else
            {
                this.AcceptButton = buttonModifier;
            }
            InitializeData();

        }


        private void InitializeData()
        {
            cbWilaya.Items.Add(new Wilaya { ID = 0, Nom = "(tout) " });
            cbWilaya.Items.AddRange(db.Wilayas.Where(p => p.ID > 0).ToArray());
            cbWilaya.SelectedIndex = 0;
            cbWilaya.ValueMember = "ID";
            cbWilaya.DisplayMember = "Nom";

            AutoCompleteStringCollection acsc = new AutoCompleteStringCollection();
            cbWilaya.AutoCompleteCustomSource = acsc;
            cbWilaya.AutoCompleteMode = AutoCompleteMode.Suggest;
            cbWilaya.AutoCompleteSource = AutoCompleteSource.ListItems;




            RefreshGrid(String.Empty); // 0 means all sections



        }

        private void RefreshGrid(string NomFilter)
        {
            /*if (IsSelectionDialog && NomFilter != String.Empty)
            {
                return;
            }

           */

            var Fournisseurs = db.Fournisseurs.Where(e => e.ID != 0);
            if (ActiveOnly)
            {
                Fournisseurs = Fournisseurs.Where(p => p.Active == true);
            }

            if (cbWilaya.SelectedIndex != 0)
            {
                var index = cbWilaya.SelectedIndex;
                Fournisseurs = Fournisseurs.Where(p => p.WilayaID == index);
            }
            Fournisseurs = Fournisseurs.Where(emp => emp.Nom.Contains(NomFilter));
            /*  if (parentFilter != "")
              {
                  Fournisseurs = Fournisseurs.Where(emp => emp.FournisseurParent.Nom.Contains(parentFilter));
              }*/

            //  List<MarbreBLIDA.Fournisseur> ListFournisseursList = null;

            if (!IsSelectionDialog)
            {
                dgv1.DataSource = Fournisseurs.OrderBy(data => data.Nom).Take(100).ToList().Select(
                         data => new
                         {

                             Numero = data.ID,
                             Nom = data.Nom,
                             Telephone = data.Telephone,
                             Wilaya = (data.WilayaID != 0) ? data.Wilaya.Nom : "/",
                             Solde = data.Solde.ToAffInt(),
                             Adresse = data.Adresse,

                             Registre = (data.NoRegistre == null) ? "/" : data.NoRegistre,
                         }).ToList();
            }
            else
            {
                dgv1.DataSource = Fournisseurs.OrderBy(data => data.Nom).Take(100).ToList().Select(
                        data => new
                        {
                            // Type = data.TypeFournisseur.Nom,
                            Numero = data.ID,
                            Nom = data.Nom,
                            Telephone = data.Telephone,
                            Wilaya = data.Wilaya.Nom,
                            Solde = data.Solde.ToAffInt(),
                            Adresse = data.Adresse,
                            Etat = data.Active.Value ? "Actif" : "Désactivé",

                        }).ToList();
            }

            // = ListFournisseursList;
            dgv1.HideIDColumn("Numero");

            for (int i = 0; i < dgv1.RowCount; i++)
            {
                var sld = dgv1.Rows[i].Cells["Solde"].Value.ToString().ParseToDec();

                if (sld < 0)
                {
             //       dgv1.Rows[i].Cells["Solde"].Style.BackColor = Color.Red;
                }
                else if (sld > 0)
                {
              //      dgv1.Rows[i].Cells["Solde"].Style.BackColor = Color.Green;
                }
            }

        }
        public static Fournisseur SelectFournisseurDialog(ModelEntities db, bool activeOnly = false)
        {


            FrList e =
            new FrList(true, db, activeOnly);

            //   e.buttonNouveau.Visible = false;
            e.ShowDialog();

            return ReturnFournisseur;

        }
        private void AdminFournisseur_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }
        private void buttonOKFournisseur_Click(object sender, EventArgs e)
        {

            if (!AddAucuneSelection || dgv1.SelectedRows[0].Index != 0)
            {
                var matricule = dgv1.SelectedRows[0].Cells[0].Value.ToString();
                ReturnFournisseur = db.Fournisseurs.Single(em => em.Nom == matricule);
            }
            else
                ReturnFournisseur = null; // en cas de selection aucun
            Dispose();
        }
        private static Fournisseur ReturnFournisseur { get; set; }
        public bool IsSelectionDialog { get; set; } // if control is called for selection only (not for editing)
        private void buttonAnnuler_Click(object sender, EventArgs e)
        {
            ReturnFournisseur = null;
            Dispose();
        }
        private void buttonNewFournisseur_Click(object sender, EventArgs e)
        {
            if (IsSelectionDialog)
            {
                ReturnFournisseur = FrCreate.CreateFournisseurDialog(db);

                Dispose(); // so return to Utilisateur Liste Selection,

            }
            else
            {
                new FrCreate(null, false).ShowDialog();

                InitializeData();
            }

        }
        private void buttonModifier_Click(object sender, EventArgs e)
        {
            var id = dgv1.GetSelectedID("Numero");
            var selectedFournisseur = db.Fournisseurs.Single(em => em.ID == id);

            new FrEdit(selectedFournisseur.ID).ShowDialog();
            db.Refresh(RefreshMode.StoreWins, selectedFournisseur);


            InitializeData();
        }
        private void buttonSupprimer_Click(object sender, EventArgs e)
        {
            var id = dgv1.GetSelectedID("Numero");
            var selectedFournisseur = db.Fournisseurs.Single(em => em.ID == id);
            if (this.ConfirmWarning("Supprimer le Fournisseur " + selectedFournisseur.Nom + "?"))
            {
                db.Fournisseurs.DeleteObject(selectedFournisseur);
                db.SaveChanges();
                InitializeData();
            }
        }







        private void txtNom_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLower(e.KeyChar))
            {
                e.KeyChar = char.ToUpper(e.KeyChar);
            }
        }

        private void buttonSelectOK_Click(object sender, EventArgs e)
        {
            if (!IsSelectionDialog) return;
            if (dgv1.SelectedRows.Count == 1)
            {
                var id = dgv1.GetSelectedID("Numero");
                var selectedFournisseur = db.Fournisseurs.Single(em => em.ID == id);
                ReturnFournisseur = selectedFournisseur;
            }
            else
            {
                ReturnFournisseur = null;
            }
            Dispose();
        }

        private void FrList_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!IsSelectionDialog) db.Dispose();
        }

        private void txtNom_TextChanged(object sender, EventArgs e)
        {
            RefreshGrid(txtNom.Text.ToUpper());
        }

        private void txtParent_TextChanged(object sender, EventArgs e)
        {
            //   txtNom.Text = "";
            //     InitialiseGridView(txtNom.Text.ToUpper(), txtParent.Text.ToUpper());
        }

        private void dgv1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((Keys)e.KeyChar == Keys.Enter)
            {
                //buttonOKFournisseur_Click(sender, e);

            }
        }
    }
}
