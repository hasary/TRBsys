﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NsTRBsys;

namespace NsTRBsys.NsComercial.NsFournisseur
{
    public partial class FrCreate : Form
    {
        public static Fournisseur ReturnFournisseur { get; set; }
        public bool IsSelectionDialogReturn { get; set; }
        private ModelEntities db;
        public FrCreate(ModelEntities db = null, bool IsSelectionDialogReturn = false)
        {
            this.db = db;
            if (db == null) this.db = new ModelEntities();
            InitializeComponent();
            this.IsSelectionDialogReturn = IsSelectionDialogReturn;
            InitializeData();
            //this.IsSelectionDialogReturn = IsSelectionDialogReturn;
        }
        public static Fournisseur CreateFournisseurDialog(ModelEntities db)
        {
            FrCreate e =
            new FrCreate(db, true);
            //          e.IsSelectionDialogReturn = true;
            e.ShowDialog();

            return ReturnFournisseur;

        }
        private void InitializeData()
        {/*
            cbTypeFournisseur.Items.Clear();
            //   cbTypeFournisseur.Items.Add(new TypeFournisseur { ID = 0, Nom = "" });
            //   cbTypeFournisseur.Items.AddRange(db.TypeFournisseurs.Where(p => p.ID > 0).ToArray());
            cbTypeFournisseur.DropDownStyle = ComboBoxStyle.DropDownList;
            // cbTypeFournisseur.SelectedIndex = 0;
            cbTypeFournisseur.ValueMember = "ID";
            cbTypeFournisseur.DisplayMember = "Nom";
            */
            /* chAdd.Visible = true;
             if (IsSelectionDialogReturn)
             {
                 chAdd.Enabled = true;
                 chAdd.Checked = false;

             }
             else {
                 chAdd.Enabled = false;
                 chAdd.Checked = true;
            
             }*/

            cbWilaya.Items.Add(new Wilaya { ID = 0, Nom = "" });
            cbWilaya.Items.AddRange(db.Wilayas.Where(p => p.ID > 0).ToArray());
            cbWilaya.SelectedIndex = 0;
            cbWilaya.ValueMember = "ID";
            cbWilaya.DisplayMember = "Nom";


            
          /*  cbParentFournisseur.SelectedIndex = 0;
            cbParentFournisseur.ValueMember = "ID";
            cbParentFournisseur.DisplayMember = "Nom";
            //     cbParentFournisseur.DropDownStyle = ComboBoxStyle.DropDownList;
            */

        }
        private void buttonEnregistrerEmployer_Click(object sender, EventArgs e)
        {
            if (isValidAll())
            {

                ReturnFournisseur = new Fournisseur()
                 {

                     //   EquipeID = ((MarbreBLIDA.Equipe)cbEquipe.SelectedItem).ID,
                     Nom = txtNomraison.Text.Trim().ToUpper(),
                     Adresse = txtAdresse.Text.Trim().ToUpper(),
                     Wilaya = (Wilaya)cbWilaya.SelectedItem,
                     Telephone = txtTelephone.Text.Trim().ToUpper(),
                     Solde = 0,
                   //  SoldeHistory = 0,
                  //   DateHistory = DateTime.Parse("2013-01-01"),
                     Active =  true,
                 };
                //
                //         ReturnFournisseur.TypeFournisseur = (TypeFournisseur)cbTypeFournisseur.SelectedItem;
                
                    ReturnFournisseur.NoRegistre = txtRegistre.Text.Trim().ToUpper();
                    ReturnFournisseur.NoArticle = txtArticle.Text.Trim().ToUpper();
                    ReturnFournisseur.NoFiscale = txtFiscal.Text.Trim();
              

                if (!IsSelectionDialogReturn)
                {
                    db.AddToFournisseurs(ReturnFournisseur);

                    db.SaveChanges();
                }
                /*  else if (chAdd.Checked)
                  {
                      db.AddToFournisseurs(ReturnFournisseur);

                      db.SaveChanges();

                  }
  */
                Dispose();

            }
        }
        private bool isValidAll()
        {
            string ErrorMessage = String.Empty;


            if (txtNomraison.Text.Trim().Length < 1 || txtNomraison.Text.Length > 100)
            {

                ErrorMessage += "\nCe Nom est Invalide (doit faire entre 2 et 100 caracteres).";
            }
            else
            {
                var matricule = txtNomraison.Text.Trim();
                var Uti = db.Fournisseurs.FirstOrDefault(em => em.Nom == matricule);

                if (Uti != null)
                {
                    ErrorMessage += "\nCe Nom existe déja dans la liste des Fournisseurs";

                }
            }

            if (cbWilaya.SelectedIndex == 0)
            {
             //   ErrorMessage += "\nVeillez selectionner wilaya!";
            }
            /*    if (cbTypeFournisseur.SelectedIndex < 1)
                {
                    ErrorMessage += "\nVeillez selectionner un type Fournisseur";
                }
                */
            if (panReg.Visible)
            {
                //  informations registre commerce
            }

            if (chIsParent.Checked && cbParentFournisseur.SelectedIndex == 0)
            {
               // ErrorMessage += "\nVeillez spécifier ou selectionné le nom du parent";
            }

            if (ErrorMessage != String.Empty)
            {
                Tools.ShowError(ErrorMessage);
                // MessageBox.Show(ErrorMessage, "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }
        private void AdminEmployerNew_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!IsSelectionDialogReturn)
            {
                db.Dispose();
            }
        }
        private void buttonAnnulerEmployer_Click(object sender, EventArgs e)
        {
            ReturnFournisseur = null;
            Dispose();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void chIsParent_CheckedChanged(object sender, EventArgs e)
        {
            cbParentFournisseur.Enabled = chIsParent.Checked;
        }

        private void txtNomraison_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLower(e.KeyChar))
            {
                e.KeyChar = char.ToUpper(e.KeyChar);
            }
        }

        private void cbTypeFournisseur_SelectedIndexChanged(object sender, EventArgs e)
        {
            /* if (cbTypeFournisseur.SelectedIndex > 0)
             {
                 //     panReg.Visible = ((TypeFournisseur)cbTypeFournisseur.SelectedItem).HasRegistre.Value;
             }*/
        }


    }
}
