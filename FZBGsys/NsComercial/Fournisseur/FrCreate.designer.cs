﻿namespace NsTRBsys.NsComercial.NsFournisseur
{
    partial class FrCreate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonEnregistre = new System.Windows.Forms.Button();
            this.buttonAnnuler = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Activite = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbParentFournisseur = new System.Windows.Forms.ComboBox();
            this.chIsParent = new System.Windows.Forms.CheckBox();
            this.txtTelephone = new NsTRBsys.TextBoxx();
            this.txtAdresse = new NsTRBsys.TextBoxx();
            this.txtArticle = new NsTRBsys.TextBoxx();
            this.txtFiscal = new NsTRBsys.TextBoxx();
            this.txtRegistre = new NsTRBsys.TextBoxx();
            this.txtNomraison = new NsTRBsys.TextBoxx();
            this.panReg = new System.Windows.Forms.Panel();
            this.tb = new System.Windows.Forms.TabControl();
            this.tpInformation = new System.Windows.Forms.TabPage();
            this.label7 = new System.Windows.Forms.Label();
            this.cbWilaya = new System.Windows.Forms.ComboBox();
            this.tpDossier = new System.Windows.Forms.TabPage();
            this.panReg.SuspendLayout();
            this.tb.SuspendLayout();
            this.tpInformation.SuspendLayout();
            this.tpDossier.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonEnregistre
            // 
            this.buttonEnregistre.Image = global::NsTRBsys.Properties.Resources.icon_16_allow;
            this.buttonEnregistre.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonEnregistre.Location = new System.Drawing.Point(316, 276);
            this.buttonEnregistre.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonEnregistre.Name = "buttonEnregistre";
            this.buttonEnregistre.Size = new System.Drawing.Size(139, 33);
            this.buttonEnregistre.TabIndex = 0;
            this.buttonEnregistre.Text = "Enregistrer";
            this.buttonEnregistre.UseVisualStyleBackColor = true;
            this.buttonEnregistre.Click += new System.EventHandler(this.buttonEnregistrerEmployer_Click);
            // 
            // buttonAnnuler
            // 
            this.buttonAnnuler.Image = global::NsTRBsys.Properties.Resources.icon_16_deny;
            this.buttonAnnuler.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAnnuler.Location = new System.Drawing.Point(191, 276);
            this.buttonAnnuler.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonAnnuler.Name = "buttonAnnuler";
            this.buttonAnnuler.Size = new System.Drawing.Size(120, 33);
            this.buttonAnnuler.TabIndex = 1;
            this.buttonAnnuler.Text = "Annuler";
            this.buttonAnnuler.UseVisualStyleBackColor = true;
            this.buttonAnnuler.Click += new System.EventHandler(this.buttonAnnulerEmployer_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nom / Raison Sociale:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "N° Registre Comerce:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(89, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Adresse:";
            // 
            // Activite
            // 
            this.Activite.AutoSize = true;
            this.Activite.Location = new System.Drawing.Point(13, 171);
            this.Activite.Name = "Activite";
            this.Activite.Size = new System.Drawing.Size(140, 17);
            this.Activite.TabIndex = 10;
            this.Activite.Text = "Contact / Telephone:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "N° Fiscale:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 17);
            this.label5.TabIndex = 3;
            this.label5.Text = "N° Article:";
            // 
            // cbParentFournisseur
            // 
            this.cbParentFournisseur.Enabled = false;
            this.cbParentFournisseur.FormattingEnabled = true;
            this.cbParentFournisseur.Location = new System.Drawing.Point(18, 186);
            this.cbParentFournisseur.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbParentFournisseur.Name = "cbParentFournisseur";
            this.cbParentFournisseur.Size = new System.Drawing.Size(10, 24);
            this.cbParentFournisseur.TabIndex = 12;
            this.cbParentFournisseur.Visible = false;
            this.cbParentFournisseur.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNomraison_KeyPress);
            // 
            // chIsParent
            // 
            this.chIsParent.AutoSize = true;
            this.chIsParent.Location = new System.Drawing.Point(43, 186);
            this.chIsParent.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chIsParent.Name = "chIsParent";
            this.chIsParent.Size = new System.Drawing.Size(100, 21);
            this.chIsParent.TabIndex = 13;
            this.chIsParent.Text = "Pariné par ";
            this.chIsParent.UseVisualStyleBackColor = true;
            this.chIsParent.Visible = false;
            this.chIsParent.CheckedChanged += new System.EventHandler(this.chIsParent_CheckedChanged);
            // 
            // txtTelephone
            // 
            this.txtTelephone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTelephone.Location = new System.Drawing.Point(173, 166);
            this.txtTelephone.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTelephone.Name = "txtTelephone";
            this.txtTelephone.Size = new System.Drawing.Size(239, 22);
            this.txtTelephone.TabIndex = 7;
            // 
            // txtAdresse
            // 
            this.txtAdresse.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAdresse.Location = new System.Drawing.Point(173, 50);
            this.txtAdresse.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtAdresse.Multiline = true;
            this.txtAdresse.Name = "txtAdresse";
            this.txtAdresse.Size = new System.Drawing.Size(239, 60);
            this.txtAdresse.TabIndex = 7;
            this.txtAdresse.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNomraison_KeyPress);
            // 
            // txtArticle
            // 
            this.txtArticle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtArticle.Location = new System.Drawing.Point(175, 66);
            this.txtArticle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtArticle.Name = "txtArticle";
            this.txtArticle.Size = new System.Drawing.Size(239, 22);
            this.txtArticle.TabIndex = 7;
            // 
            // txtFiscal
            // 
            this.txtFiscal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFiscal.Location = new System.Drawing.Point(175, 38);
            this.txtFiscal.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtFiscal.Name = "txtFiscal";
            this.txtFiscal.Size = new System.Drawing.Size(239, 22);
            this.txtFiscal.TabIndex = 7;
            // 
            // txtRegistre
            // 
            this.txtRegistre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRegistre.Location = new System.Drawing.Point(175, 10);
            this.txtRegistre.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtRegistre.Name = "txtRegistre";
            this.txtRegistre.Size = new System.Drawing.Size(239, 22);
            this.txtRegistre.TabIndex = 7;
            // 
            // txtNomraison
            // 
            this.txtNomraison.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNomraison.Location = new System.Drawing.Point(173, 14);
            this.txtNomraison.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNomraison.Name = "txtNomraison";
            this.txtNomraison.Size = new System.Drawing.Size(239, 22);
            this.txtNomraison.TabIndex = 6;
            this.txtNomraison.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNomraison_KeyPress);
            // 
            // panReg
            // 
            this.panReg.Controls.Add(this.txtRegistre);
            this.panReg.Controls.Add(this.label2);
            this.panReg.Controls.Add(this.label4);
            this.panReg.Controls.Add(this.label5);
            this.panReg.Controls.Add(this.txtFiscal);
            this.panReg.Controls.Add(this.txtArticle);
            this.panReg.Location = new System.Drawing.Point(3, 47);
            this.panReg.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panReg.Name = "panReg";
            this.panReg.Size = new System.Drawing.Size(427, 95);
            this.panReg.TabIndex = 14;
            // 
            // tb
            // 
            this.tb.Controls.Add(this.tpInformation);
            this.tb.Controls.Add(this.tpDossier);
            this.tb.Location = new System.Drawing.Point(12, 12);
            this.tb.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb.Name = "tb";
            this.tb.SelectedIndex = 0;
            this.tb.Size = new System.Drawing.Size(443, 250);
            this.tb.TabIndex = 17;
            // 
            // tpInformation
            // 
            this.tpInformation.Controls.Add(this.label7);
            this.tpInformation.Controls.Add(this.cbWilaya);
            this.tpInformation.Controls.Add(this.label1);
            this.tpInformation.Controls.Add(this.label3);
            this.tpInformation.Controls.Add(this.txtNomraison);
            this.tpInformation.Controls.Add(this.txtAdresse);
            this.tpInformation.Controls.Add(this.txtTelephone);
            this.tpInformation.Controls.Add(this.Activite);
            this.tpInformation.Location = new System.Drawing.Point(4, 25);
            this.tpInformation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tpInformation.Name = "tpInformation";
            this.tpInformation.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tpInformation.Size = new System.Drawing.Size(435, 221);
            this.tpInformation.TabIndex = 1;
            this.tpInformation.Text = "Informations Fournisseur";
            this.tpInformation.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(99, 130);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 17);
            this.label7.TabIndex = 12;
            this.label7.Text = "Wilaya:";
            // 
            // cbWilaya
            // 
            this.cbWilaya.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWilaya.FormattingEnabled = true;
            this.cbWilaya.Location = new System.Drawing.Point(173, 127);
            this.cbWilaya.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbWilaya.Name = "cbWilaya";
            this.cbWilaya.Size = new System.Drawing.Size(239, 24);
            this.cbWilaya.TabIndex = 11;
            // 
            // tpDossier
            // 
            this.tpDossier.Controls.Add(this.chIsParent);
            this.tpDossier.Controls.Add(this.panReg);
            this.tpDossier.Controls.Add(this.cbParentFournisseur);
            this.tpDossier.Location = new System.Drawing.Point(4, 25);
            this.tpDossier.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tpDossier.Name = "tpDossier";
            this.tpDossier.Size = new System.Drawing.Size(435, 221);
            this.tpDossier.TabIndex = 2;
            this.tpDossier.Text = "Dossier Comercial";
            this.tpDossier.UseVisualStyleBackColor = true;
            // 
            // FrCreate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(476, 324);
            this.Controls.Add(this.tb);
            this.Controls.Add(this.buttonAnnuler);
            this.Controls.Add(this.buttonEnregistre);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrCreate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nouveau Fournisseur";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AdminEmployerNew_FormClosing);
            this.panReg.ResumeLayout(false);
            this.panReg.PerformLayout();
            this.tb.ResumeLayout(false);
            this.tpInformation.ResumeLayout(false);
            this.tpInformation.PerformLayout();
            this.tpDossier.ResumeLayout(false);
            this.tpDossier.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonEnregistre;
        private System.Windows.Forms.Button buttonAnnuler;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label Activite;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private TextBoxx txtNomraison;
        private TextBoxx txtRegistre;
        private TextBoxx txtAdresse;
        private TextBoxx txtTelephone;
        private TextBoxx txtFiscal;
        private TextBoxx txtArticle;
        private System.Windows.Forms.ComboBox cbParentFournisseur;
        private System.Windows.Forms.CheckBox chIsParent;
        private System.Windows.Forms.Panel panReg;
        private System.Windows.Forms.TabControl tb;
        private System.Windows.Forms.TabPage tpInformation;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbWilaya;
        private System.Windows.Forms.TabPage tpDossier;
    }
}