﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NsTRBsys.NsComercial
{
    public partial class FrVersementFournisseur : Form
    {
        ModelEntities db = new ModelEntities();
        public FrVersementFournisseur()
        {
            InitializeComponent();
            InitialiseData();
        }

        private void InitialiseData()
        {
            cbFournisseur.Items.Add(new Fournisseur { ID = 0, Nom = "" });
            cbFournisseur.Items.AddRange(db.Fournisseurs.Where(p => p.Active == true).ToArray());
            cbFournisseur.SelectedIndex = (cbFournisseur.Items.Count != 2) ? 0 : 1;
            cbFournisseur.ValueMember = "ID";
            cbFournisseur.DisplayMember = "Nom";
            cbFournisseur.DropDownStyle = ComboBoxStyle.DropDownList;

            cbPartenaire.Items.Add(new Client { ID = 0, Nom = "" });
            cbPartenaire.Items.AddRange(db.Clients.Where(p => p.Active == true && p.isPartenaire == true).ToArray());
            cbPartenaire.SelectedIndex = (cbPartenaire.Items.Count != 2) ? 0 : 1;
            cbPartenaire.ValueMember = "ID";
            cbPartenaire.DisplayMember = "Nom";
            cbPartenaire.DropDownStyle = ComboBoxStyle.DropDownList;

        }

        private void button3_Click(object sender, EventArgs e)
        {

            if (Fournisseur != null)
            {
                txtNumCompte.Text = Fournisseur.ID.ToString().PadLeft(4, '0');
                // txtNomCli.Text = FournisseurMoh.Nom + "   (" + FournisseurMoh.Wilaya.Nom + ")";
                labSoldeFourni.Text = (Fournisseur.Solde == null) ? "" : Fournisseur.Solde.Value.ToAffInt() + " DA";
                if (Fournisseur.Solde.Value < 0)
                {
                    labSoldeFourni.ForeColor = Color.Red;

                }
                else if (Fournisseur.Solde.Value > 0)
                {
                    labSoldeFourni.ForeColor = Color.Green;
                }
                else
                {
                    labSoldeFourni.ForeColor = Color.Black;
                }

                txtmontantVers.Text = "";
                txtmontantVers.Focus();
            }

        }

        private void txtmontantVers_TextChanged(object sender, EventArgs e)
        {
            decimal montant = 0;

            if (decimal.TryParse(txtmontantVers.Text.Trim().Replace(" ", "").Trim(), out montant))
            {
                if (Fournisseur != null) labNewSolde.Text = (Fournisseur.Solde.Value + montant).ToAffInt() + " DA";
                if (Partenaire != null) labNewSolde.Text = (Partenaire.Solde.Value - montant).ToAffInt() + " DA";
                LabelLettreDA.Text = CtoL.convertMontant(Math.Abs(montant));
            }

            else
            {
                labNewSolde.Text = "";
                LabelLettreDA.Text = "";
            }

            UpdateSoldeColor(labNewSolde);


        }

        public Fournisseur Fournisseur { get; set; }

        private void button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        public bool IsValidAll()
        {
            string ErrorMessage = "";

            if (Fournisseur == null && Partenaire == null)
            {
                ErrorMessage += "Veuillez séléctionner un Fournisseur ou  un Partenaire!";
            }

            decimal solde = 0;
            if (txtmontantVers.Text.Trim().Replace(" ", "") != "" && !decimal.TryParse(txtmontantVers.Text.Trim().Replace(" ", ""), out solde))
            {
                ErrorMessage += "\nCe montant est Invalide!!";
            }

            if (ErrorMessage != "")
            {
                Tools.ShowError(ErrorMessage);
                return false;
            }

            return true;

        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (!IsValidAll())
            {
                return;
            }

            if (tabControl1.SelectedTab == tpFournisseur)
            {
                Partenaire = null;
            }
            else
            {
                Fournisseur = null;
            }
            var montant = int.Parse(txtmontantVers.Text.Trim().Replace(" ", "").Trim());

            string message = "Confirmer le Versement du Compte ?";
            if (Fournisseur != null) message += "\n\nFournisseur : " + Fournisseur.Nom + "\n\nVersement: " + montant.ToString("N2");
            if (Partenaire != null) message += "\n\nPartenaire : " + Partenaire.Nom + "\n\nVersement: " + montant.ToString("N2");


            if (this.ConfirmInformation(message))
            {
                var CurrentDateTime = Tools.GetServerDateTime();
                Operation op = new Operation()
                {
                    Fournisseur = Fournisseur,
                    Client = Partenaire,
                    Date = dtDate.Value,
                    Montant = montant,
                    TypeOperaiton = (int)EnTypeOperation.Versement_ENVOI,
                    InscriptionDate = CurrentDateTime,
                    InscriptionUID = Tools.CurrentUserID,
                    Sen = Fournisseur != null ? "C" : "D",
                    Lib = txtObs.Text,
                    


                };

                db.AddToOperations(op);
                if (Fournisseur != null) FrMain.GenerateSoldeFourni(db, CurrentDateTime, Fournisseur);
                if (Partenaire != null) FrMain.GenerateSoldeClient(db, CurrentDateTime, Partenaire);


                db.SaveChanges();


                //  db.AddToOperations(op);
                if (Fournisseur != null)
                {
                    db.Refresh(System.Data.Objects.RefreshMode.StoreWins, Fournisseur);
                    this.ShowInformation("Versement " + Fournisseur.Nom + " Effectué avec succés!!\n\nNouv Solde: " + Fournisseur.Solde);
                }
                if (Partenaire != null)
                {
                    db.Refresh(System.Data.Objects.RefreshMode.StoreWins, Partenaire);
                    this.ShowInformation("Versement " + Partenaire.Nom + " Effectué avec succés!!\n\nNouv Solde: " + Partenaire.Solde);
                }


                txtmontantVers.Text = "";
                this.Fournisseur = null;
                this.Partenaire = null;
                //txtNomCli.Text = "";
                txtNumCompte.Text = "";
                labSoldeFourni.Text = "";
                labNewSolde.Text = "";

                //   Dispose();
            }

        }

        private void txtmontantVers_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!Char.IsDigit(ch) && ch != 8 && ch != '-')
            {
                e.Handled = true;
            }
        }



        private void cbFournisseur_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbFournisseur.SelectedIndex > 0)
            {
                Fournisseur = (Fournisseur)cbFournisseur.SelectedItem;
                labSoldeFourni.Text = Fournisseur.Solde.ToAffInt() + " DA";

            }
            else
            {
                Fournisseur = null;
                labSoldeFourni.Text = "";
            }

            UpdateSoldeColor(labSoldeFourni);
            txtmontantVers.Text = "";
        }

        private void UpdateSoldeColor(Label lab)
        {
            lab.ForeColor = Color.Black;
            var solde = lab.Text.ParseToDec();
            if (solde == null)
            {
                return;
            }

            if (solde > 0)
            {
                lab.ForeColor = Color.Green;
            }
            else if (solde < 0)
            {

                lab.ForeColor = Color.Red;
            }

        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbFournisseur.SelectedIndex = cbPartenaire.SelectedIndex = 0;
            txtmontantVers.Text = "";
            txtObs.Text = "";
        }

        private void cbPartenaire_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cbPartenaire.SelectedIndex > 0)
            {
                Partenaire = (Client)cbPartenaire.SelectedItem;
                labSoldePartenaire.Text = Partenaire.Solde.ToAffInt() + " DA";

            }
            else
            {
                Partenaire = null;
                labSoldePartenaire.Text = "";
            }

            UpdateSoldeColor(labSoldePartenaire);
            txtmontantVers.Text = "";
        }

        public Client Partenaire { get; set; }
    }
}
