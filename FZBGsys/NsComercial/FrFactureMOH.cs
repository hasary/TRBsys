﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NsTRBsys.NsComercial
{
    public partial class FrFactureMO : Form
    {
        ModelEntities db = new ModelEntities();
        public FrFactureMO(int? BonID = null)
        {
            InitializeComponent();
            InitialiseControls();


            if (BonID != null)
            {
                Facture = db.Factures.Single(p => p.ID == BonID);
                this.Edit = true;
                groupBox2.Enabled = false;
                InputFacture();

            }
            else
            {
                this.Edit = false;
            }
        }



        private void InputFacture()
        {
            dtDate.Value = Facture.Date.Value;
            DestinationClientPartenaire = Facture.Client1;
            if (DestinationClientPartenaire != null)
            {
                LoadDestinationClient();
            }

            DestinationDepo = Facture.Depo1;
            if (DestinationDepo != null)
            {
                LoadDestinationDepo();
            }

            DepartPartenaire = Facture.Client;
            if (DepartPartenaire != null)
            {
                LoadDepartPartenaire();
            }

            DepartFournisseur = Facture.Fournisseur;
            if (DepartFournisseur != null)
            {
                LoadDepartFournisseur();
            }

            DepartDepo = Facture.Depo;
            if (DepartDepo != null)
            {
                LoadDepartDepo();
            }


            Chauffeur = Facture.Chauffeur;
            LoadChauffeur();


            RefreshGrid();

            UpdateControls();

        }

        private void LoadChauffeur()
        {
            txtNomChauffeur.Text = this.Chauffeur.Nom;
            //  txtNumeroPC.Text = this.Chauffeur.NumeroPC;
            //   txtDatePC.Text = this.Chauffeur.DatePC;
            txtMatricule.Text = this.Chauffeur.DefaultMatricule;
        }

        private void LoadDepartDepo()
        {
            tabCDepart.SelectedTab = tpDepartDepo;
            cbDepartDepo.SelectedItem = DepartDepo;

        }

        private void LoadDepartFournisseur()
        {
            tabCDepart.SelectedTab = tpDepartFournisseur;
            cbDepartFournisseur.SelectedItem = DepartFournisseur;
        }

        private void LoadDepartPartenaire()
        {
            tabCDepart.SelectedTab = tpDepartPartenaire;
            cbDepartPartenaire.SelectedItem = DepartPartenaire;
        }

        private void LoadDestinationDepo()
        {

            tabCDestination.SelectedTab = tpDestDepo;
            cbDestinationDepo.SelectedItem = DestinationDepo;
        }

        private void InitialiseControls()
        {


            //---------------------------------------------------------------
            cbDepartFournisseur.Items.Add(new Fournisseur { ID = 0, Nom = "" });
            cbDepartFournisseur.Items.AddRange(db.Fournisseurs.Where(p => p.ID > 0 && p.Active == true).ToArray());
            cbDepartFournisseur.SelectedIndex = cbDepartFournisseur.Items.Count == 2 ? 1 : 0;
            cbDepartFournisseur.ValueMember = "ID";
            cbDepartFournisseur.DisplayMember = "Nom";
            cbDepartFournisseur.DropDownStyle = ComboBoxStyle.DropDownList;
            //---------------------------------------------------------------

            //---------------------------------------------------------------
            cbDepartPartenaire.Items.Add(new Client { ID = 0, Nom = "" });
            cbDepartPartenaire.Items.AddRange(db.Clients.Where(p => p.ID > 0 && p.Active == true && p.isPartenaire == true).ToArray());
            cbDepartPartenaire.SelectedIndex = 0;
            cbDepartPartenaire.ValueMember = "ID";
            cbDepartPartenaire.DisplayMember = "Nom";
            cbDepartPartenaire.DropDownStyle = ComboBoxStyle.DropDownList;
            //-----------------------------------------------------------------

            //---------------------------------------------------------------

            cbDepartDepo.Items.Add(new Depo { ID = 0, Nom = "" });
            cbDepartDepo.Items.AddRange(db.Depoes.Where(p => p.ID > 0 && p.publish == true).ToArray());
            cbDepartDepo.SelectedIndex = 0;
            cbDepartDepo.ValueMember = "ID";
            cbDepartDepo.DisplayMember = "Nom";
            cbDepartDepo.DropDownStyle = ComboBoxStyle.DropDownList;

            //----------------------------------------------------------------
            cbDestinationDepo.Items.Add(new Depo { ID = 0, Nom = "" });
            cbDestinationDepo.Items.AddRange(db.Depoes.Where(p => p.ID > 0 && p.publish == true).ToArray());
            cbDestinationDepo.SelectedIndex = 0;
            cbDestinationDepo.ValueMember = "ID";
            cbDestinationDepo.DisplayMember = "Nom";
            cbDestinationDepo.DropDownStyle = ComboBoxStyle.DropDownList;
            //----------------------------------------------------------------



            cbMode.SelectedIndex = 0;


            var GoutID = db.PrixProduits.Select(p => p.GoutProduitID).ToList();
            var FormatID = db.PrixProduits.Select(p => p.FormatID).ToList();
            //--------------------------- Gout Produit
            cbGoutProduit.Items.Add(new GoutProduit { ID = 0, Nom = "" });
            cbGoutProduit.Items.AddRange(db.GoutProduits.Where(p => p.ID > 0 && GoutID.Contains(p.ID)).ToArray());
            cbGoutProduit.SelectedIndex = 0;
            cbGoutProduit.ValueMember = "ID";
            cbGoutProduit.DisplayMember = "Nom";
            cbGoutProduit.DropDownStyle = ComboBoxStyle.DropDownList;

            //---------------------- Format (Bouteilles)
            cbFormat.Items.Add(new Format { ID = 0, Volume = "" });
            cbFormat.Items.AddRange(db.Formats.Where(p => p.ID > 0 && FormatID.Contains(p.ID)).ToArray());
            cbFormat.DisplayMember = "Volume";
            cbFormat.ValueMember = "ID";
            cbFormat.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFormat.SelectedIndex = 0;
            //--------------------------------------------------------------

            cbMode.SelectedIndex = 4; // force a terme for all


            //--------------------------------------------------------------

        }

        private void btEnregistrer_Click(object sender, EventArgs e)
        {
            if (Facture == null || Facture.Ventes.Count == 0)
            {
                this.ShowWarning("La liste est vide, ajouter d'abord des éléments");
                return;
            }

            if (Chauffeur == null)
            {
                this.ShowWarning("\nSelectionner Transport !");
                return;
            }
            else if (DestinationClientPartenaire != null && Destination == null)
            {
                Tools.ShowWarning("Selectionner une déstination + coordonées");
                return;
            }
            else if (!Tools.ConfirmMessage("Continuer les ces informations?\n\nDEPART       : " +
               ((DepartPartenaire != null) ?
                  " Partenaire " + DepartPartenaire.Nom :
                   ((DepartFournisseur != null) ? "Fournisseur " + DepartFournisseur.Nom : "Depos " + DepartDepo.Nom))
                + "\nDESTINATION: " +
                ((DestinationClientPartenaire != null) ?
                  " Client " + DestinationClientPartenaire.Nom :
                    "Depos " + DestinationDepo.Nom)

                 + ("\n            WILAYA " + txtDestination.Text)))
            {

                return;
            }

            Facture.Chauffeur = this.Chauffeur;

            Facture.Client = this.DepartPartenaire;
            Facture.Client1 = this.DestinationClientPartenaire;

            Facture.Fournisseur = this.DepartFournisseur;
            Facture.Depo = this.DepartDepo;
            Facture.Depo1 = this.DestinationDepo;


            Facture.InscriptionTime = Tools.GetServerDateTime();
            Facture.InscriptionUID = Tools.CurrentUserID;

            if (Facture.Client1 != null)
            {
                Facture.Destination = Destination.Wilaya.Nom;
                Facture.DestinationWilayaID = Destination.WilayaID;
                Facture.Destination1 = Destination;

            }
            else if (Facture.Depo1 != null)
            {
                Facture.Destination = Facture.Depo1.Wilaya.Nom;
                Facture.DestinationWilayaID = Facture.Depo1.WilayaID;
            }

            db.SaveChangeTry();
            if (!Edit)
            {
                Facture.MontantTotalFacture = Facture.Ventes.Sum(p => p.PrixUnitaire * p.TotalBouteilles);
                Facture.MontantTotalAchat = Facture.Ventes.Sum(p => p.PrixAchat * p.TotalBouteilles);

                Facture.MontantTVA = Facture.MontantTotalFacture / 117;
                Facture.MontantHT = Facture.MontantTotalFacture - Facture.MontantTVA;
                Facture.Chauffeur = this.Chauffeur;
                Facture.ModePayement = cbMode.SelectedIndex;
                btEnregistrer.Enabled = false;


                db.SaveChangeTry();


                if (DepartPartenaire != null) FrMain.GenerateSoldeClient(db, DateTime.Now, DepartPartenaire);
                if (DestinationClientPartenaire != null) FrMain.GenerateSoldeClient(db, DateTime.Now, DestinationClientPartenaire);
                if (DepartFournisseur != null) FrMain.GenerateSoldeFourni(db, DateTime.Now, DepartFournisseur);


                if (DepartDepo != null) FrMain.UpdateStockDepos(db, Facture, DepartDepo);
                if (DestinationDepo != null) FrMain.UpdateStockDepos(db, Facture, DestinationDepo);

                db.SaveChanges();
            }
            db.Refresh(System.Data.Objects.RefreshMode.StoreWins, Destination);
            try
            {
                var txt = CtoL.convertMontant(Facture.MontantTotalFacture.Value);
                if (Facture.DestinationClientID != null)
                {
                    NsTRBsys.NsRapports.FrViewer.PrintFacture(Facture.ID, txt, Destination);
                    NsTRBsys.NsRapports.FrViewer.PrintLivraison(Facture.ID, Facture.Destination1.NomRaisonSocial);
                }
                else
                {
                    NsTRBsys.NsRapports.FrViewer.PrintLivraison(Facture.ID, Facture.Depo1.Nom);
                }

                Dispose();
            }
            catch (Exception z)
            {
                this.ShowError(z.AllMessages("Impossible de créer le rapport"));
            }



        }

        private void FrBonAttribution_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        private void btAnnuler_Click(object sender, EventArgs e)
        {
            if (this.ConfirmWarning("\nEtes vous sure de vouloir fermer sans enregistrer ?"))
            {
                Dispose();
            }
        }



        public Facture Facture { get; set; }
        private void txtClient_TextChanged(object sender, EventArgs e)
        {

        }

        private void LoadDestinationClient()
        {
            DestinationClientPartenaire = NsTRBsys.NsComercial.NsClient.FrList.UpdateDestination(DestinationClientPartenaire.ID, db);
            tabCDestination.SelectedTab = tpDestClientPartenaire;
            txtNomClient.Text = this.DestinationClientPartenaire.Nom;
            db.Refresh(System.Data.Objects.RefreshMode.StoreWins, db.Destinations);
            if (Facture.Destination1 != null)
            {
                Destination = Facture.Destination1;
            }
            else
            {
                Destination = DestinationClientPartenaire.Destinations.OrderBy(p=>p.ID).FirstOrDefault(p => p.IsDefault == true );
            }

            

            txtDestination.Text = Destination.Wilaya.Nom;

            //cbWilaya.SelectedItem = this.DestinationClientPartenaire.Wilaya;

            labSoldeClient.Text = this.DestinationClientPartenaire.Solde.ToAffInt() + " DA";
            if (this.DestinationClientPartenaire.Solde < 0)
            {
                labSoldeClient.ForeColor = Color.Red;
            }
            else if (this.DestinationClientPartenaire.Solde > 0)
            {
                labSoldeClient.ForeColor = Color.Green;
            }
            else
            {
                labSoldeClient.ForeColor = Color.Black;
            }

        }

        //    public List<Vente> ListVente { get; set; }

        public Client DepartPartenaire { get; set; }
        public Fournisseur DepartFournisseur { get; set; }
        public Depo DepartDepo { get; set; }

        public Depo DestinationDepo { get; set; }
        public Client DestinationClientPartenaire { get; set; }




        private void btParcourrirClient_Click(object sender, EventArgs e)
        {
            this.DestinationClientPartenaire = NsTRBsys.NsComercial.NsClient.FrList.SelectClientDialog(db);

            LoadDestinationClient();
        }

        private void btAjouter_Click(object sender, EventArgs e)
        {
            if (!isValidAll())
            {
                return;
            }
            if (Facture == null)
            {
                CreateFacture(); // 
            }

            var newVente = new Vente();
            //  (int)EnUniteProduit.Fardeaux;
            //newVente.Fa = Facture;
            newVente.Format = (Format)cbFormat.SelectedItem;
            newVente.GoutProduit = (GoutProduit)cbGoutProduit.SelectedItem;
            newVente.Facture = this.Facture;

            var prix = db.PrixProduits.Single(
                p => p.FormatID == newVente.FormatID &&
                p.GoutProduitID == newVente.GoutProduitID &&
                p.TypeClient == 0);

            newVente.Unite = cbUniteVente.SelectedIndex;//(int)EnUniteProduit.Palettes;
            if (newVente.Unite == (int)EnUniteProduit.Palettes)
            {
                newVente.NombrePalettes = txtNombre.Text.ParseToInt();
                newVente.TotalFardeaux = prix.FardeauxParPalette * newVente.NombrePalettes;
                newVente.TotalBouteilles = prix.BouteilleParFardeaux * newVente.TotalFardeaux;

            }
            else
            {
                newVente.NombreFardeaux = txtNombre.Text.ParseToInt();
                newVente.TotalFardeaux = newVente.NombreFardeaux;
                newVente.TotalBouteilles = prix.BouteilleParFardeaux * newVente.TotalFardeaux;


            }
          
            if (Facture.DestinationDepoID == null) newVente.PrixUnitaire = txtPrixVente.Text.Trim().ParseToDec();
            else newVente.PrixUnitaire = null;

            if (Facture.DepartDepoID == null) newVente.PrixAchat = txtPrixAchat.Text.ParseToDec();
            else newVente.PrixAchat = null;



            Facture.Ventes.Add(newVente);

            RefreshGrid();
            UpdateTotals();
            UpdateControls();

        }

        private void UpdateControls()
        {

            tabCDepart.Enabled = Facture == null || Facture.Ventes.Count == 0;
            tabCDestination.Enabled = Facture == null || Facture.Ventes.Count == 0;
            txtNombre.Text = "";
            cbGoutProduit.SelectedIndex = 0;

        }

        private void RefreshGrid()
        {
            if (Facture == null)
            {
                dgv.DataSource = null;

            }
            else
            {
                dgv.DataSource = Facture.Ventes.Select(p => new
                {
                    ID = p.ID,
                    Produit = p.GoutProduit.Nom,
                    Format = p.Format.Volume,
                    //REF = ,
                    Quantite = ((p.NombrePalettes != null) ? p.NombrePalettes : p.NombreFardeaux) + " " + p.Unite.ToUniteProduitTxt(),
                    Total = p.TotalFardeaux + " Fardeaux " + p.TotalBouteilles + " Bouteilles",
                    Prix_Achat = (p.PrixAchat != null) ? p.PrixAchat.ToAffInt() : "/",
                    Prix_Vente = (p.PrixUnitaire != null) ? p.PrixUnitaire.ToAffInt() : "/",

                }).ToList();
                // throw new NotImplementedException();

                dgv.HideIDColumn();
            }


        }

        private void UpdateTotals()
        {
            labTotal.Text = "";
            labMontantAchat.Text = "";
            labMontantVente.Text = "";

            if (Facture == null)
            {
                return;
            }

            var pal = Facture.Ventes.Sum(p => p.NombrePalettes);
            var fard = Facture.Ventes.Sum(p => p.NombreFardeaux);
            var bout = Facture.Ventes.Sum(p => p.TotalBouteilles);
            var sfard = Facture.Ventes.Sum(p => p.TotalFardeaux);
            var daAchat = Facture.Ventes.Sum(p => p.TotalBouteilles * p.PrixAchat);
            var daVente = Facture.Ventes.Sum(p => p.TotalBouteilles * p.PrixUnitaire);

            if (pal != 0)
            {
                labTotal.Text = pal.ToString() + " palettes";
            }

            if (fard != 0)
            {
                labTotal.Text += "   " + fard.ToString() + " Fardeaux    ";
            }


            labTotal.Text += " = " + sfard + " Fardeaux = " + bout + " bouteilles ";
            labMontantAchat.Text = daAchat.ToAffInt() + " DA";
            labMontantVente.Text = daVente.ToAffInt() + " DA";

            if (cbMode.SelectedIndex == 1)
            {
                txtVersement.Text = daAchat.ToAffInt();
            }

        }

        private void CreateFacture()
        {
            var date = dtDate.Value.Date;
            // var no = db.BonAttributions.Count(p => p.Date == date) + 1;

            Facture = new Facture()
            {
                Client = this.DepartPartenaire,
                Client1 = this.DestinationClientPartenaire,
                Fournisseur = this.DepartFournisseur,
                Depo = this.DepartDepo,
                Depo1 = this.DestinationDepo,
                Date = date,
                //Etat = (int)EnEtatBonAttribution.EnInstance,
                //Chauffeur = this.Chauffeur,


            };



        }

        private bool isValidAll()
        {
            var message = "";

            if (
                DepartPartenaire == null &&
                DepartDepo == null &&
                DepartFournisseur == null)
            {
                message += "Selectionnez un Départ!";
            }
            else if


            (this.DestinationClientPartenaire == null &&
               this.DestinationDepo == null)
            {
                message += "Selectionnez Déstination!";
            }
            else

                if (this.DepartDepo == this.DestinationDepo &&
                    this.DestinationDepo != null)
                {
                    message += "Départ est le meme que distination, veillez changer";
                }
                else if (this.DepartPartenaire == this.DestinationClientPartenaire &&
                    this.DestinationClientPartenaire != null)
                {
                    message += "Départ est le meme que distination, veillez changer";
                }


            if (DestinationClientPartenaire != null && Destination == null)
            {
                message += "\nEntrez une destination";
            }

            if (cbGoutProduit.SelectedIndex == 0)
            {
                message += "\nSelectionnez un produit";
            }

            if (cbFormat.SelectedIndex == 0)
            {
                message += "\nSelectionner un Format";
            }
            if (cbUniteVente.SelectedIndex <1)
            {
                  message += "\nSelectionner un palette ou Fardeaux";
            }

            if (txtNombre.Text.ParseToInt() == null)
            {
                message += "\nNombre Invalide !";
            }

            if (panPrixVente.Visible && txtPrixVente.Text.ParseToDec() == null)
            {
                message += "\nPrix vente Invalide !";
            }

            if (panPrixAchat.Visible && txtPrixAchat.Text.ParseToDec() == null)
            {
                message += "\nPrix achat Invalide !";
            }



            var IDFomrat = ((Format)cbFormat.SelectedItem).ID;
            var IDProduit = ((GoutProduit)cbGoutProduit.SelectedItem).ID;
            var prix = db.PrixProduits.SingleOrDefault(p => p.FormatID == IDFomrat && p.GoutProduitID == IDProduit);

            if (prix == null)
            {

                Tools.ShowError("Produit Introuvable vous devez ajouter dans la liste produits");
                var fr = new FrNEProduit();
                fr.loadInfos(IDFomrat, IDProduit);
                fr.ShowDialog();
                return false;
            }

            if (message != "")
            {
                this.ShowWarning(message);
                return false;
            }

            return true;
        }

        private void dtDate_ValueChanged(object sender, EventArgs e)
        {
            if (Facture != null)
            {
                Facture.Date = dtDate.Value.Date;
            }
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != 8 && !char.IsNumber(e.KeyChar) && e.KeyChar != '.' && e.KeyChar != ',')
            {
                e.Handled = true;
            }
            if (e.KeyChar == '.')
            {
                e.KeyChar = ',';
            }
        }

        private void btEnlever_Click(object sender, EventArgs e)
        {
            var index = dgv.GetSelectedIndex();

            var selected = Facture.Ventes.ElementAt(index.Value);
            Facture.Ventes.Remove(selected);

            RefreshGrid();
            UpdateTotals();
            UpdateControls();
        }

        private void txtClient_Leave(object sender, EventArgs e)
        {
            //   var clientNom = txtClient.Text.Trim().ToUpper();
            //   this.Client = db.Clients.SingleOrDefault(c => c.Nom == clientNom);
        }

        private void txtClient_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLower(e.KeyChar))
            {
                e.KeyChar = char.ToUpper(e.KeyChar);
            }
        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalBouteillesEtMontant();
        }

        private void UpdateTotalBouteillesEtMontant()
        {
            var Nombre = txtNombre.Text.ParseToInt();
            var Format = (Format)cbFormat.SelectedItem;
            var GoutProduit = (GoutProduit)cbGoutProduit.SelectedItem;

            var Prix = db.PrixProduits.SingleOrDefault(p => p.GoutProduitID == GoutProduit.ID && p.FormatID == Format.ID && p.TypeClient == 0);

            if (cbUniteVente.SelectedIndex >0 && Prix != null && Nombre != null && cbFormat.SelectedIndex != 0)
            {
                if (cbUniteVente.SelectedIndex == (int)EnUniteProduit.Palettes)
                txtTotalBouteilles.Text =  (Nombre * Prix.FardeauxParPalette * Prix.BouteilleParFardeaux).ToString();
                if (cbUniteVente.SelectedIndex == (int)EnUniteProduit.Fardeaux)
                    txtTotalBouteilles.Text = (Nombre  * Prix.BouteilleParFardeaux).ToString();
                


                decimal? prix = null;

                if (panPrixVente.Visible)

                    prix = txtPrixVente.Text.Trim().ParseToDec();

                else if (panPrixAchat.Visible)

                    prix = txtPrixAchat.Text.Trim().ParseToDec();

                if (prix != null)
                {
                    txtTotalDA.Text = (prix * txtTotalBouteilles.Text.Trim().ParseToInt()).ToAffInt();
                }
                else
                {
                    txtTotalDA.Text = "";
                }
            }
            else
            {
                txtTotalBouteilles.Text = "";
                txtTotalDA.Text = "";
            }






        }

        private void txtNombre_Enter(object sender, EventArgs e)
        {
            this.AcceptButton = btAjouter;
        }

        private void cbGoutProduit_SelectedIndexChanged(object sender, EventArgs e)
        {
            updatePrixDefault();
            if (cbGoutProduit.SelectedIndex > 0)
            {
                if (cbFormat.SelectedIndex > 0)
                {
                    txtNombre.Select();
                }
                else
                {
                    cbFormat.Select();
                }
            }


        }

        private void updatePrixDefault()
        {
            txtPrixVente.Text = "";
            txtPrixAchat.Text = "";

            if (cbFormat.SelectedIndex > 0 && cbGoutProduit.SelectedIndex > 0)
            {
                var IDFomrat = ((Format)cbFormat.SelectedItem).ID;
                var IDProduit = ((GoutProduit)cbGoutProduit.SelectedItem).ID;
                var prix = db.PrixProduits.SingleOrDefault(p => p.FormatID == IDFomrat && p.GoutProduitID == IDProduit);

                if (prix != null)
                {
                    txtPrixVente.Text = prix.PrixUnitaire.ToAffInt();
                    txtPrixAchat.Text = prix.PrixAchat.ToAffInt();
                }

            }

        }

        private void btSelectChauffeur_Click(object sender, EventArgs e)
        {
            if (Facture == null)
            {
                CreateFacture();
            }
            this.Chauffeur = NsTRBsys.NsComercial.NsChauffeur.FrList.SelectChaffeurDialog(Facture.Client, db);

            if (this.Chauffeur != null)
            {
                LoadChauffeur();
            }
        }

        public Chauffeur Chauffeur { get; set; }

        private void txtClient_TextChanged_1(object sender, EventArgs e)
        {
            //  var nom = txtClient.Text.Trim();
            //  this.Client = db.Clients.SingleOrDefault(p => p.Nom == nom);
        }

        private void btParcouClient_Click(object sender, EventArgs e)
        {
            if (Facture == null)
            {
                CreateFacture();
            }
            this.DestinationClientPartenaire = NsTRBsys.NsComercial.NsClient.FrList.SelectClientDialog(db, true); // active Only

            if (this.DestinationClientPartenaire != null)
            {
                LoadDestinationClient();

            }
            else
            {
                this.DestinationClientPartenaire = null;
                txtNomClient.Text = ""; // this.DestinationClient.Nom;
                // cbWilaya.SelectedIndex = 0;// this.DestinationClient.Wilaya.Nom;
                Destination = null;
                txtDestination.Text = "";
                labSoldeClient.Text = "";// this.DestinationClient.Solde.ToAffInt() + " DA";
            }
        }

        private void cbFormat_SelectedIndexChanged(object sender, EventArgs e)
        {
            updatePrixDefault();
        }

        private void cbMode_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public bool False { get; set; }

        public bool Edit { get; set; }

        private void cbFournisseur_SelectedIndexChanged(object sender, EventArgs e)
        {
            labSoldeFournisseur.Text = "";
            if (cbDepartFournisseur.SelectedIndex > 0)
            {

                DepartFournisseur = (Fournisseur)cbDepartFournisseur.SelectedItem;
                labSoldeFournisseur.Text = DepartFournisseur.Solde.ToAffInt() + " DA";

            }
            else
            {
                DepartFournisseur = null;
            }

        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            panPrixAchat.Visible = labMontantAchat.Visible = tabCDepart.SelectedTab != tpDepartDepo;

            cbDepartFournisseur.SelectedIndex = 0;
            cbDepartDepo.SelectedIndex = 0;
            cbDepartPartenaire.SelectedIndex = 0;

            if (tabCDepart.SelectedTab == tpDepartFournisseur) //fournisseur
            {
                cbDepartFournisseur.SelectedIndex = (cbDepartFournisseur.Items.Count != 2) ? 0 : 1;
            }
            else if (tabCDepart.SelectedTab == tpDepartDepo)
            {
                cbDepartDepo.SelectedIndex = (cbDepartDepo.Items.Count != 2) ? 0 : 1;

            }
            else if (tabCDepart.SelectedTab == tpDepartPartenaire)
            {
                cbDepartPartenaire.SelectedIndex = (cbDepartPartenaire.Items.Count != 2) ? 0 : 1;
            }
        }

        private void tabControl2_SelectedIndexChanged(object sender, EventArgs e)
        {

            panPrixVente.Visible = labMontantVente.Visible = tabCDestination.SelectedTab == tpDestClientPartenaire;

            cbDestinationDepo.SelectedIndex = 0;
            DestinationClientPartenaire = null;
            Destination = null;

            txtNomClient.Text = "";

            if (tabCDestination.SelectedTab == tpDestDepo)
            {

                cbDestinationDepo.SelectedIndex = (cbDestinationDepo.Items.Count != 2) ? 0 : 1;
            }

        }

        private void cbDepartDepo_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cbDepartDepo.SelectedIndex > 0)
            {
                DepartDepo = (Depo)cbDepartDepo.SelectedItem;

            }
            else
            {
                DepartDepo = null;
            }
        }

        private void cbDestDepo_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cbDestinationDepo.SelectedIndex > 0)
            {
                DestinationDepo = (Depo)cbDestinationDepo.SelectedItem;

            }
            else
            {
                DestinationDepo = null;

            }

            /*   if (cbDestinationDepo.SelectedIndex == 0)
               {
                   radNoFake.Checked = true;
                   radFake.Visible = false;
               }
               else
               {
                   radFake.Checked = true;
                   radFake.Visible = true;
               }

               radFake.Visible = radNoFake.Visible = cbDestinationDepo.SelectedIndex == 0;*/
        }

        private void cbPartenaire_SelectedIndexChanged(object sender, EventArgs e)
        {
            labSoldePartenaireDepart.Text = "";
            if (cbDepartPartenaire.SelectedIndex > 0)
            {

                this.DepartPartenaire = (Client)cbDepartPartenaire.SelectedItem;
                labSoldePartenaireDepart.Text = DepartPartenaire.Solde.ToAffInt() + " DA";

            }
            else
            {
                this.DepartPartenaire = null;
            }

        }

        private void panPrixVente_VisibleChanged(object sender, EventArgs e)
        {
            panMontantVente.Visible = panPrixVente.Visible;
            UpdateTotalBouteillesEtMontant();

            if (panPrixVente.Visible) labVA.Text = "DA (Vente)";
            else if (panPrixAchat.Visible) labVA.Text = "DA (Achat)";

            panMontantBouteille.Visible = panPrixAchat.Visible || panPrixVente.Visible;
        }

        private void panPrixAchat_VisibleChanged(object sender, EventArgs e)
        {
            panMontantAchat.Visible = panPrixAchat.Visible;
            UpdateTotalBouteillesEtMontant();

            if (panPrixVente.Visible) labVA.Text = "DA (Vente)";
            else if (panPrixAchat.Visible) labVA.Text = "DA (Achat)";

            panMontantBouteille.Visible = panPrixAchat.Visible || panPrixVente.Visible;
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void tpDestClientPartenaire_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (DestinationClientPartenaire != null)
            {
                var dest = FrListDestination.SelectionDialog(DestinationClientPartenaire.ID, this.Destination.ID);
                if (dest != null)
                {
                    db.Refresh(System.Data.Objects.RefreshMode.StoreWins, DestinationClientPartenaire);
                    db.Refresh(System.Data.Objects.RefreshMode.StoreWins, DestinationClientPartenaire.Destinations);
                    this.Destination = db.Destinations.Single(p => p.ID == dest.ID);
                    txtDestination.Text = this.Destination.Wilaya.Nom;
                }
            }
        }

        public Destination Destination { get; set; }

        private void cbUniteVente_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateTotalBouteillesEtMontant();
        }
    }
}
