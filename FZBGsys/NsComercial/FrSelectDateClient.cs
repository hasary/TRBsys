﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NsTRBsys.NsComercial
{
    public partial class FrSelectDateClient : Form
    {
        ModelEntities db = new ModelEntities();
        public FrSelectDateClient()
        {
            InitializeComponent();
            InitialiseDate();
            SelectionDone = false;

            //   DateTimePicker dateTimePicker1 = new DateTimePicker();


        }

        private void InitialiseDate()
        {
            string[] monthText = { "Ja", "" };
            Dictionary<string, string> months = new Dictionary<string, string>();

            DateTime fin = DateTime.Now;
            DateTime debut = Tools.DateDebutOperations;
            dateTimePickerDu.Value = debut;
            dateTimePickerDu.MinDate = Tools.DateDebutOperations;
            for (DateTime i = debut; i < fin; i = i.AddMonths(1))
            {
                // months.Add(i.Month,
            }

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            dateTimePickerMonth.Visible = radioButtonMon.Checked;
        }

        private void radioButtonPeriode_CheckedChanged(object sender, EventArgs e)
        {
            panelPeriode.Visible = radioButtonPeriode.Checked;
        }

        private void radioButtonDate_CheckedChanged(object sender, EventArgs e)
        {
            dateTimePickerJourn.Visible = radioButtonDate.Checked;
        }

        public static DateTime From { get; set; }
        public static DateTime To { get; set; }
        public static bool SelectionDone = false;
        private void button1_Click(object sender, EventArgs e)
        {

            if (Client == null)
            {
                this.ShowWarning("Selectionnez un client!");
                return;
            }
            SelectionDone = true;
            button1.Enabled = false;
            DateTime du = DateTime.Now;
            DateTime au = DateTime.Now;

            if (radioButtonDate.Checked)
            {
                du = au = dateTimePickerJourn.Value;

            }

            if (radioButtonPeriode.Checked)
            {
                du = dateTimePickerDu.Value;
                au = dateTimePickerAu.Value;
            }
            if (radioButtonMon.Checked)
            {
                var dt = dateTimePickerMonth.Value;
                au = new DateTime(dt.Year, dt.Month, DateTime.DaysInMonth(dt.Year, dt.Month));
                du = new DateTime(dt.Year, dt.Month, 1);
            }

            //   Actions.Rapports.FrViewer.PrintRecapShowroom(du, au);
            From = du;
            if (From < Tools.DateDebutOperations)
            {
                From = Tools.DateDebutOperations;
            }
            To = au;
            //    progressBar1.Visible = false;

            decimal? soldeBugin = 0;// Client.SoldeHistory;
            var dateHi =  Tools.DateDebutOperations;//  Client.DateHistory;
            if (dateHi != null)
            {
                soldeBugin += Client.Factures.Where(p => p.Etat != 2 && p.Date >= dateHi && p.Date < From).Sum(p => p.MontantTotalAchat);
                soldeBugin -= Client.Factures1.Where(p => p.Etat != 2 && p.Date >= dateHi && p.Date < From).Sum(p => p.MontantTotalFacture);
                
                
                soldeBugin -= Client.Operations.Where(p => p.Sen == "D" && p.Date >= dateHi && p.Date < From  ).Sum(p => p.Montant);
                soldeBugin += Client.Operations.Where(p => p.Sen == "C" && p.Date >= dateHi && p.Date < From ).Sum(p => p.Montant);
            }

            if (Client.isPartenaire == true)
            NsRapports.FrViewer.PrintReleverPartnaire(Client, soldeBugin, From, To);
            else
            NsRapports.FrViewer.PrintReleverClient(Client, soldeBugin, From, To);
            
            button1.Enabled = true;

        }




        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void btParcourrirClient_Click(object sender, EventArgs e)
        {

            this.Client = NsTRBsys.NsComercial.NsClient.FrList.SelectClientDialog(db, true);
            if (Client != null)
            {
                txtClient.Text = Client.Nom;
            }

        }

        public Client Client { get; set; }

        private void FrSelectDateClient_FormClosed(object sender, FormClosedEventArgs e)
        {
            db.Dispose();
        }
    }
}
