﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsComercial
{
    public partial class FrOperation : Form
    {
        ModelEntities db;
        DateTime CurrentDate;
        public FrOperation()
        {

            InitializeComponent();

            InitialiseData();
        }

        private void InitialiseData()
        {
            labMontantRecuOperation.Text = "";
            labMontantRecupayement.Text = "";
            cbDocument.SelectedIndex = 0;
            ListToSave = new List<Operation>();
            CurrentDate = Tools.GetServerDateTime();
            db = new ModelEntities();
        }

        private void cbTypeOperation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcOperation.SelectedTab == tpOperation)
            {
                txtMontantReçuOperation.Text = "";
                if (cbTypeOperation.SelectedIndex == 3) // operation payment espece
                {
                    tcOperation.SelectedTab = tpPayement;
                    //   cbTypeOperation.SelectedIndex = 0;

                }
            }

        }

        private void btAfficher_Click(object sender, EventArgs e)
        {
            panPayment.Enabled = false;
            LoadFacture();


        }

        private void LoadFacture()
        {
            var id = txtNoBon.Text.Trim().ParseToInt();
            //Facture facture = null;
            BonAttribution bonAttribution = null;

            if (cbDocument.SelectedIndex == 0) // facture
            {
                this.facture = db.Factures.SingleOrDefault(p => p.ID == id);
                if (facture == null)
                {
                    this.ShowWarning("N° de Facture introuvable");
                    return;

                }
                bonAttribution = facture.BonAttribution;
            }
            else
            {
                bonAttribution = db.BonAttributions.SingleOrDefault(p => p.ID == id);
                if (bonAttribution == null)
                {
                    this.ShowWarning("N° de Bon introuvable");
                    return;

                }
                else if (bonAttribution.Factures.Count == 0)
                {
                    this.ShowWarning("Ce Bon n'est pas encore Facturé");
                    return;
                }
                else
                {
                    facture = bonAttribution.Factures.Single();
                }

            }


            if (bonAttribution.Client != SelectedClient)
            {
                if (SelectedClient == null || this.ConfirmWarning("Cette Bon ne correspond pas le client choisi, Modifier le client?"))
                {
                    SelectedClient = bonAttribution.Client;
                    LoadClient();

                }
                else
                {

                    return;
                }

            }

            txtMontantFacture.Text = facture.MontantTotalFacture.ToAffInt();
            txtMontantReçuOperation.Text = facture.MontantTotalFacture.ToAffInt();
            panPayment.Enabled = true;

        }

        private void LoadClient()
        {
            txtClient.Text = SelectedClient.Nom;
            txtSolde.Text = SelectedClient.Solde.ToAffInt();
            txtParent.Text = (SelectedClient.ClientParent == null) ? "" : SelectedClient.ClientParent.Nom;


        }

        public Client SelectedClient { get; set; }

        private void txtMontantReçuOperation_TextChanged(object sender, EventArgs e)
        {
            var montant = txtMontantReçuOperation.Text.Trim().ParseToDec();
            if (montant != null)
            {
                if (SelectedClient.Solde != null)
                {
                    txtNouveauSolde.Text = (SelectedClient.Solde + montant).ToAffInt();
                }
                else
                {
                    txtNouveauSolde.Text = (montant).ToAffInt();
                }
                labMontantRecuOperation.Text = CtoL.convertMontant(montant.Value);
            }
            else
            {
                txtNouveauSolde.Text = "";
                labMontantRecuOperation.Text = "";
            }
        }

        private void btParcourrirClient_Click(object sender, EventArgs e)
        {
            SelectedClient = FZBGsys.NsComercial.NsClient.FrList.SelectClientDialog(db);
            ClearControls();

            if (SelectedClient != null)
            {
                LoadClient();
            }
        }

        private void ClearControls()
        {
            txtNoBon.Text = "";
            txtMontantReçuOperation.Text = "";
            txtMontantFacture.Text = "";
            txtNouveauSolde.Text = "";

            cbTypeOperation.SelectedIndex = 0;
            panOperation.Enabled = SelectedClient != null;
        }

        private void txtMontantReçuOperation_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != 8 && !char.IsNumber(e.KeyChar) && e.KeyChar != '.' && e.KeyChar != ',')
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.KeyChar = ',';
            }


        }

        private void txtMontantRecuPayement_TextChanged(object sender, EventArgs e)
        {
            var montant = txtMontantRecuPayement.Text.Trim().ParseToDec();

            if (montant != null)
            {
                labMontantRecupayement.Text = CtoL.convertMontant(montant.Value);

            }
        }

        private void btAjouter_Click(object sender, EventArgs e)
        {
            if (!IsValidAll())
            {
                return;
            }
            decimal montant = 0;
            if (tcOperation.SelectedTab == tpOperation)
            {
                montant = txtMontantReçuOperation.Text.Trim().ParseToDec().Value;

            }
            else
            {
                montant = txtMontantRecuPayement.Text.Trim().ParseToDec().Value;
            }
            var newOperation = new Operation();
            newOperation.Client = SelectedClient;
            newOperation.Montant = montant;

            newOperation.Sen = "C";
            newOperation.InscriptionUID = Tools.CurrentUserID;
            newOperation.InscriptionDate = CurrentDate;
            newOperation.Date = dtDate.Value.Date;

            newOperation.TypeOperaiton = (tcOperation.SelectedTab == tpOperation) ? cbTypeOperation.SelectedIndex : (int)EnTypeOpration.Payement_Espèces;
            newOperation.Lib = newOperation.TypeOperaiton.ToTypeOperationTxt();
            if (newOperation.TypeOperaiton == (int)EnTypeOpration.Payement_Espèces)
                newOperation.FactureID = facture.ID;
            ListToSave.Add(newOperation);
            RefreshGrid();
            SelectedClient = null;
            txtClient.Text = "";
            txtParent.Text = "";
            txtSolde.Text = "";
            txtMontantRecuPayement.Text = "";
            ClearControls();


        }

        private void RefreshGrid()
        {
            dgv.DataSource = ListToSave.Select(p => new
            {
                Date = p.Date,
                Client = p.Client.NomClientAndParent(),
                Libele = p.Lib,
                Facture = (p.FactureID != null) ? "N° " + p.FactureID.ToString() : "/",
                Montant = p.Montant.ToAffInt(),
            }).ToList();
        }

        public List<Operation> ListToSave { get; set; }
        private bool IsValidAll()
        {
            var message = "";

            if (SelectedClient == null)
            {
                message = "Selectionner un client!";

            }

            if (tcOperation.SelectedTab == tpOperation && cbTypeOperation.SelectedIndex < 1)
            {
                message += "\nSelectionner une Opération dans la liste!";
            }
            if (tcOperation.SelectedTab == tpOperation)
            {
                if (txtMontantReçuOperation.Text.Trim().ParseToDec() == null)
                {
                    message += "\nMontant Invalide !!!!!";
                }
            }

            if (tcOperation.SelectedTab == tpPayement)
            {
                if (facture == null)
                {
                    message += "\nEntrez numéro facture ou attribution!";
                }

                if (txtMontantRecuPayement.Text.Trim().ParseToDec() == null)
                {
                    message += "\nMontant Invalide !!!!!";
                }
            }


            if (message == "")
            {
                return true;
            }
            else
            {
                this.ShowWarning(message);
                return false;
            }
        }

        private void btEnlever_Click(object sender, EventArgs e)
        {
            var index = dgv.GetSelectedIndex();

            if (index != null)
            {
                var toDel = ListToSave.ElementAt(index.Value);
                SelectedClient = toDel.Client;
                LoadClient();
                cbTypeOperation.SelectedIndex = toDel.TypeOperaiton.Value;
                if (toDel.TypeOperaiton != (int)EnTypeOpration.Payement_Espèces)
                {

                    txtMontantReçuOperation.Text = toDel.Montant.ToString();

                }
                else
                {
                    cbDocument.SelectedIndex = 1;
                    txtNoBon.Text = toDel.FactureID.ToString();
                    LoadFacture();
                    txtMontantRecuPayement.Text = toDel.Montant.ToString();
                }

                db.DeleteObject(toDel);
                ListToSave.RemoveAt(index.Value);
                RefreshGrid();
            }


        }

        private void buttonAnnuler_Click(object sender, EventArgs e)
        {

            if (this.ConfirmWarning("Confirmer annulation?\nATTENTION: rien de sera enregistré!"))
            {
                this.Dispose();
            }
        }

        private void FrOperation_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        private void buttonEnregistrer_Click(object sender, EventArgs e)
        {
            if (this.ConfirmInformation("Confirmer enregistrement des données de la liste et mise à jour des client?"))
            {
                foreach (var operation in ListToSave)
                {
                    switch ((EnTypeOpration)operation.TypeOperaiton)
                    {
                        case EnTypeOpration.Reglement_Créance:
                            operation.Client.Solde += operation.Montant;
                            break;
                        case EnTypeOpration.Avance:
                            if (operation.Client.Solde != null)
                            {
                                operation.Client.Solde += operation.Montant;
                            }
                            else
                            {
                                operation.Client.Solde = operation.Montant;
                            }
                            break;
                        case EnTypeOpration.Payement_Espèces:
                            db.Factures.Single(p => p.ID == operation.FactureID).Etat = (int)EnEtatFacture.Payée;
                            break;
                        case EnTypeOpration.Depense:
                            break;
                        default:
                            break;
                    }
                }

                db.SaveChangeTry();
                
                
                
                Dispose();
            }
        }

        private void txtMontantRecuPayement_KeyUp(object sender, KeyEventArgs e)
        {

        }

        public Facture facture { get; set; }

        private void txtMontantReçuOperation_Enter(object sender, EventArgs e)
        {
            this.AcceptButton = btAjouter;
        }

        private void txtMontantRecuPayement_Enter(object sender, EventArgs e)
        {
            this.AcceptButton = btAjouter;
        }

        private void txtNoBon_Enter(object sender, EventArgs e)
        {
            this.AcceptButton = btAfficher;
        }

        private void tcOperation_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
