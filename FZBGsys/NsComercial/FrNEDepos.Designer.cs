﻿namespace NsTRBsys.NsComercial
{
    partial class FrNEDepos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btAnnuler = new System.Windows.Forms.Button();
            this.btEnregistre = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNom = new System.Windows.Forms.TextBox();
            this.txtAdress = new System.Windows.Forms.TextBox();
            this.txtResponsable = new System.Windows.Forms.TextBox();
            this.txtTelephone = new System.Windows.Forms.TextBox();
            this.cbWilaya = new System.Windows.Forms.ComboBox();
            this.chActif = new System.Windows.Forms.CheckBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtQttPal = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btEnlever = new System.Windows.Forms.Button();
            this.cbGoutProduit = new System.Windows.Forms.ComboBox();
            this.cbFormat = new System.Windows.Forms.ComboBox();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.btAjouter = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // btAnnuler
            // 
            this.btAnnuler.Image = global::NsTRBsys.Properties.Resources.icon_16_deny;
            this.btAnnuler.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btAnnuler.Location = new System.Drawing.Point(483, 390);
            this.btAnnuler.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(103, 30);
            this.btAnnuler.TabIndex = 8;
            this.btAnnuler.Text = "Annuler";
            this.btAnnuler.UseVisualStyleBackColor = true;
            this.btAnnuler.Click += new System.EventHandler(this.btAnnuler_Click);
            // 
            // btEnregistre
            // 
            this.btEnregistre.Image = global::NsTRBsys.Properties.Resources.icon_16_allow;
            this.btEnregistre.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btEnregistre.Location = new System.Drawing.Point(592, 390);
            this.btEnregistre.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btEnregistre.Name = "btEnregistre";
            this.btEnregistre.Size = new System.Drawing.Size(109, 30);
            this.btEnregistre.TabIndex = 7;
            this.btEnregistre.Text = "Enregistrer";
            this.btEnregistre.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btEnregistre.UseVisualStyleBackColor = true;
            this.btEnregistre.Click += new System.EventHandler(this.btEnregistre_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(117, 165);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 17);
            this.label1.TabIndex = 9;
            this.label1.Text = "Wilaya:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(114, 83);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 17);
            this.label2.TabIndex = 10;
            this.label2.Text = "Adresse:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(112, 37);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "Nom:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(117, 248);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "Telephone:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(117, 208);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "Reponsable:";
            this.label5.Click += new System.EventHandler(this.label2_Click);
            // 
            // txtNom
            // 
            this.txtNom.Location = new System.Drawing.Point(208, 33);
            this.txtNom.Margin = new System.Windows.Forms.Padding(4);
            this.txtNom.Name = "txtNom";
            this.txtNom.Size = new System.Drawing.Size(244, 22);
            this.txtNom.TabIndex = 12;
            // 
            // txtAdress
            // 
            this.txtAdress.Location = new System.Drawing.Point(212, 80);
            this.txtAdress.Margin = new System.Windows.Forms.Padding(4);
            this.txtAdress.Multiline = true;
            this.txtAdress.Name = "txtAdress";
            this.txtAdress.Size = new System.Drawing.Size(240, 57);
            this.txtAdress.TabIndex = 12;
            // 
            // txtResponsable
            // 
            this.txtResponsable.Location = new System.Drawing.Point(253, 205);
            this.txtResponsable.Margin = new System.Windows.Forms.Padding(4);
            this.txtResponsable.Name = "txtResponsable";
            this.txtResponsable.Size = new System.Drawing.Size(199, 22);
            this.txtResponsable.TabIndex = 12;
            // 
            // txtTelephone
            // 
            this.txtTelephone.Location = new System.Drawing.Point(253, 244);
            this.txtTelephone.Margin = new System.Windows.Forms.Padding(4);
            this.txtTelephone.Name = "txtTelephone";
            this.txtTelephone.Size = new System.Drawing.Size(199, 22);
            this.txtTelephone.TabIndex = 12;
            // 
            // cbWilaya
            // 
            this.cbWilaya.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWilaya.FormattingEnabled = true;
            this.cbWilaya.Location = new System.Drawing.Point(253, 162);
            this.cbWilaya.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbWilaya.Name = "cbWilaya";
            this.cbWilaya.Size = new System.Drawing.Size(199, 24);
            this.cbWilaya.TabIndex = 13;
            // 
            // chActif
            // 
            this.chActif.AutoSize = true;
            this.chActif.Checked = true;
            this.chActif.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chActif.Location = new System.Drawing.Point(121, 296);
            this.chActif.Margin = new System.Windows.Forms.Padding(4);
            this.chActif.Name = "chActif";
            this.chActif.Size = new System.Drawing.Size(57, 21);
            this.chActif.TabIndex = 14;
            this.chActif.Text = "Actif";
            this.chActif.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(693, 373);
            this.tabControl1.TabIndex = 15;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.txtAdress);
            this.tabPage1.Controls.Add(this.chActif);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.cbWilaya);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.txtTelephone);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.txtResponsable);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.txtNom);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(685, 344);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Informations dépo";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtQttPal);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.btEnlever);
            this.tabPage2.Controls.Add(this.cbGoutProduit);
            this.tabPage2.Controls.Add(this.cbFormat);
            this.tabPage2.Controls.Add(this.dgv);
            this.tabPage2.Controls.Add(this.btAjouter);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(685, 344);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Stock Dépo";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txtQttPal
            // 
            this.txtQttPal.Location = new System.Drawing.Point(36, 213);
            this.txtQttPal.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtQttPal.Name = "txtQttPal";
            this.txtQttPal.Size = new System.Drawing.Size(84, 22);
            this.txtQttPal.TabIndex = 52;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(131, 214);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 17);
            this.label9.TabIndex = 51;
            this.label9.Text = "Palettes";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(35, 187);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 17);
            this.label8.TabIndex = 51;
            this.label8.Text = "Quantite:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(33, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 17);
            this.label6.TabIndex = 33;
            this.label6.Text = "Gôut:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(33, 96);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 17);
            this.label7.TabIndex = 32;
            this.label7.Text = "Format:";
            // 
            // btEnlever
            // 
            this.btEnlever.Image = global::NsTRBsys.Properties.Resources.icon_16_denyinactive;
            this.btEnlever.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btEnlever.Location = new System.Drawing.Point(247, 287);
            this.btEnlever.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btEnlever.Name = "btEnlever";
            this.btEnlever.Size = new System.Drawing.Size(143, 31);
            this.btEnlever.TabIndex = 31;
            this.btEnlever.Text = "<< Enlever";
            this.btEnlever.UseVisualStyleBackColor = true;
            this.btEnlever.Click += new System.EventHandler(this.btEnlever_Click);
            // 
            // cbGoutProduit
            // 
            this.cbGoutProduit.FormattingEnabled = true;
            this.cbGoutProduit.Location = new System.Drawing.Point(33, 52);
            this.cbGoutProduit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbGoutProduit.Name = "cbGoutProduit";
            this.cbGoutProduit.Size = new System.Drawing.Size(200, 24);
            this.cbGoutProduit.TabIndex = 28;
            // 
            // cbFormat
            // 
            this.cbFormat.FormattingEnabled = true;
            this.cbFormat.Location = new System.Drawing.Point(33, 122);
            this.cbFormat.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbFormat.Name = "cbFormat";
            this.cbFormat.Size = new System.Drawing.Size(92, 24);
            this.cbFormat.TabIndex = 27;
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(247, 28);
            this.dgv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 16;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(420, 243);
            this.dgv.TabIndex = 29;
            // 
            // btAjouter
            // 
            this.btAjouter.Image = global::NsTRBsys.Properties.Resources.icon_16_new1;
            this.btAjouter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btAjouter.Location = new System.Drawing.Point(36, 287);
            this.btAjouter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAjouter.Name = "btAjouter";
            this.btAjouter.Size = new System.Drawing.Size(197, 31);
            this.btAjouter.TabIndex = 30;
            this.btAjouter.Text = "Ajouter / Corriger >>";
            this.btAjouter.UseVisualStyleBackColor = true;
            this.btAjouter.Click += new System.EventHandler(this.btAjouter_Click);
            // 
            // FrNEDepos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 438);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btAnnuler);
            this.Controls.Add(this.btEnregistre);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrNEDepos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Dépo";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrNEDepos_FormClosing);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btAnnuler;
        private System.Windows.Forms.Button btEnregistre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNom;
        private System.Windows.Forms.TextBox txtAdress;
        private System.Windows.Forms.TextBox txtResponsable;
        private System.Windows.Forms.TextBox txtTelephone;
        private System.Windows.Forms.ComboBox cbWilaya;
        private System.Windows.Forms.CheckBox chActif;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ComboBox cbGoutProduit;
        private System.Windows.Forms.ComboBox cbFormat;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Button btAjouter;
        private System.Windows.Forms.Button btEnlever;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtQttPal;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
    }
}