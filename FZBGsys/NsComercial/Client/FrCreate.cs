﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NsTRBsys;

namespace NsTRBsys.NsComercial.NsClient
{
    public partial class FrCreate : Form
    {
        public static Client ReturnClient { get; set; }
        public bool IsSelectionDialogReturn { get; set; }
        private ModelEntities db;
        public FrCreate(ModelEntities db = null, bool IsSelectionDialogReturn = false)
        {
            this.db = db;
            if (db == null) this.db = new ModelEntities();
            InitializeComponent();
            this.IsSelectionDialogReturn = IsSelectionDialogReturn;
            InitializeData();
            //this.IsSelectionDialogReturn = IsSelectionDialogReturn;
        }
        public static Client CreateClientDialog(ModelEntities db)
        {
            FrCreate e =
            new FrCreate(db, true);
            //          e.IsSelectionDialogReturn = true;
            e.ShowDialog();

            return ReturnClient;

        }
        private void InitializeData()
        {/*
            cbTypeClient.Items.Clear();
            //   cbTypeClient.Items.Add(new TypeClient { ID = 0, Nom = "" });
            //   cbTypeClient.Items.AddRange(db.TypeClients.Where(p => p.ID > 0).ToArray());
            cbTypeClient.DropDownStyle = ComboBoxStyle.DropDownList;
            // cbTypeClient.SelectedIndex = 0;
            cbTypeClient.ValueMember = "ID";
            cbTypeClient.DisplayMember = "Nom";
            */
            /* chAdd.Visible = true;
             if (IsSelectionDialogReturn)
             {
                 chAdd.Enabled = true;
                 chAdd.Checked = false;

             }
             else {
                 chAdd.Enabled = false;
                 chAdd.Checked = true;
            
             }*/

            cbWilaya.Items.Add(new Wilaya { ID = 0, Nom = "" });
            cbWilaya.Items.AddRange(db.Wilayas.Where(p => p.ID > 0).ToArray());
            cbWilaya.SelectedIndex = 0;
            cbWilaya.ValueMember = "ID";
            cbWilaya.DisplayMember = "Nom";
            chPartenaire.Checked = false;

            /* cbParentClient.Items.Add(new ClientParent { ID = 0, Nom = "" });
             cbParentClient.Items.AddRange(db.ClientParents.Where(p => p.ID > 0).ToArray());
             cbParentClient.SelectedIndex = 0;
             cbParentClient.ValueMember = "ID";
             cbParentClient.DisplayMember = "Nom";
             // *    cbParentClient.DropDownStyle = ComboBoxStyle.DropDownList;
             */

        }
        private void buttonEnregistrerEmployer_Click(object sender, EventArgs e)
        {
            if (isValidAll())
            {

                ReturnClient = new Client()
                 {

                     //   EquipeID = ((MarbreBLIDA.Equipe)cbEquipe.SelectedItem).ID,
                     Nom = txtNomraison.Text.Trim().ToUpper(),
                     Adresse = txtAdresse.Text.Trim().ToUpper(),
                     Wilaya = (Wilaya)cbWilaya.SelectedItem,
                     Telephone = txtTelephone.Text.Trim().ToUpper(),
                     Solde = 0,
                     // SoldeHistory = 0,
                     // DateHistory = DateTime.Now,
                     Active = true,
                     isPartenaire = chPartenaire.Checked
                 };
                //
                //         ReturnClient.TypeClient = (TypeClient)cbTypeClient.SelectedItem;

                var Destination = new Destination();
                Destination.WilayaID = ReturnClient.WilayaID;
                Destination.Adresse = ReturnClient.Adresse;
                Destination.Client = ReturnClient;
                Destination.IsDefault = false;

                Destination.NoArticle = ReturnClient.NoArticle;
                Destination.NoRegistre = Destination.NoRegistre;
                Destination.NoIdentifiant = Destination.NoIdentifiant;
                Destination.IsReal = true;
                ReturnClient.NoRegistre = txtRegistre.Text.Trim().ToUpper();
                ReturnClient.NoArticle = txtArticle.Text.Trim().ToUpper();
                ReturnClient.NoFiscale = txtFiscal.Text.Trim();

                if (!IsSelectionDialogReturn)
                {
                    db.AddToClients(ReturnClient);

                    db.SaveChanges();
                }

                Dispose();

            }
        }
        private bool isValidAll()
        {
            string ErrorMessage = String.Empty;


            if (txtNomraison.Text.Trim().Length < 1 || txtNomraison.Text.Length > 100)
            {

                ErrorMessage += "\nCe Nom est Invalide (doit faire entre 2 et 100 caracteres).";
            }
            else
            {
                var matricule = txtNomraison.Text.Trim();
                var Uti = db.Clients.FirstOrDefault(em => em.Nom == matricule);

                if (Uti != null)
                {
                    ErrorMessage += "\nCe Nom existe déja dans la liste des clients";

                }
            }

            if (cbWilaya.SelectedIndex == 0)
            {
                  ErrorMessage += "\nVeillez selectionner wilaya!";
            }
            /*    if (cbTypeClient.SelectedIndex < 1)
                {
                    ErrorMessage += "\nVeillez selectionner un type Client";
                }
                */
            if (panReg.Visible)
            {
                //  informations registre commerce
            }

            if (chIsParent.Checked && cbParentClient.SelectedIndex == 0)
            {
                ErrorMessage += "\nVeillez spécifier ou selectionné le nom du parent";
            }

            if (ErrorMessage != String.Empty)
            {
                Tools.ShowError(ErrorMessage);
                // MessageBox.Show(ErrorMessage, "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }
        private void AdminEmployerNew_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!IsSelectionDialogReturn)
            {
                db.Dispose();
            }
        }
        private void buttonAnnulerEmployer_Click(object sender, EventArgs e)
        {
            ReturnClient = null;
            Dispose();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void chIsParent_CheckedChanged(object sender, EventArgs e)
        {
            cbParentClient.Enabled = chIsParent.Checked;
        }

        private void txtNomraison_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLower(e.KeyChar))
            {
                e.KeyChar = char.ToUpper(e.KeyChar);
            }
        }

        private void cbTypeClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            /* if (cbTypeClient.SelectedIndex > 0)
             {
                 //     panReg.Visible = ((TypeClient)cbTypeClient.SelectedItem).HasRegistre.Value;
             }*/
        }


    }
}
