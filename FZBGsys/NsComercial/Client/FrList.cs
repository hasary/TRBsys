﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Objects;

namespace NsTRBsys.NsComercial.NsClient
{


    public partial class FrList : Form
    {
        ModelEntities db;
        public bool AddAucuneSelection { get; set; }

        public FrList()
        {
            db = new ModelEntities();
            this.InitType(false); // normal list
        }
        public bool ActiveOnly = false;
        public FrList(bool IsSelectionDialog, ModelEntities db, bool ActiveOnly = false)
        {
            this.db = db;
            this.ActiveOnly = ActiveOnly;
            this.InitType(IsSelectionDialog);

            txtNom.Select();


        }

        private void InitType(bool IsSelectionDialog)
        {
            this.IsSelectionDialog = IsSelectionDialog;


            InitializeComponent();
            panelEdit.Visible = !IsSelectionDialog;
            panelSelect.Visible = IsSelectionDialog;
            if (IsSelectionDialog)
            {
                this.AcceptButton = btSelectOK;
                this.CancelButton = btAnnuler;
            }
            else
            {
                this.AcceptButton = buttonModifier;
            }
            InitializeData();

        }


        private void InitializeData()
        {
            cbWilaya.Items.Add(new Wilaya { ID = 0, Nom = "(tout) " });
            cbWilaya.Items.AddRange(db.Wilayas.Where(p => p.ID > 0).ToArray());
            cbWilaya.SelectedIndex = 0;
            cbWilaya.ValueMember = "ID";
            cbWilaya.DisplayMember = "Nom";

            AutoCompleteStringCollection acsc = new AutoCompleteStringCollection();
            cbWilaya.AutoCompleteCustomSource = acsc;
            cbWilaya.AutoCompleteMode = AutoCompleteMode.Suggest;
            cbWilaya.AutoCompleteSource = AutoCompleteSource.ListItems;




            RefreshGrid(String.Empty); // 0 means all sections



        }

        private void RefreshGrid(string NomFilter)
        {
            /*if (IsSelectionDialog && NomFilter != String.Empty)
            {
                return;
            }

           */

            var Clients = db.Clients.Where(e => e.ID != 0);
            if (ActiveOnly)
            {
                Clients = Clients.Where(p => p.Active == true);
            }

            if (cbWilaya.SelectedIndex != 0)
            {
                var index = cbWilaya.SelectedIndex;
                Clients = Clients.Where(p => p.WilayaID == index);
            }
            Clients = Clients.Where(emp => emp.Nom.Contains(NomFilter));
            /*  if (parentFilter != "")
              {
                  Clients = Clients.Where(emp => emp.ClientParent.Nom.Contains(parentFilter));
              }*/

            //  List<MarbreBLIDA.Client> ListClientsList = null;

            if (!IsSelectionDialog)
            {
                dgv1.DataSource = Clients.OrderByDescending(p => p.isPartenaire).Where(p => p.Active.Value).OrderBy(data => data.Nom).Take(100).ToList().Select(
                         data => new
                         {

                             Numero = data.ID,
                             Partenaire = data.isPartenaire == true ? "Partenaire" : "/",
                             Nom = data.Nom,
                             Telephone = data.Telephone,
                             Wilaya = data.Wilaya.Nom,
                             Solde = data.Solde.ToAffInt(),
                             Adresse = data.Adresse,
                             //    Registre = (data.NoRegistre == null) ? "/" : data.NoRegistre,
                         }).ToList();
            }
            else
            {
                dgv1.DataSource = Clients.OrderByDescending(p => p.isPartenaire).OrderBy(data => data.Nom).Take(100).ToList().Select(
                        data => new
                        {
                            // Type = data.TypeClient.Nom,
                            Numero = data.ID,
                            Partenaire = data.isPartenaire == true ? "Partenaire" : "/",
                            Nom = data.NomClientAndParent(),
                            Telephone = data.Telephone,
                            Wilaya = data.Wilaya.Nom,
                            Solde = data.Solde.ToAffInt(),
                            Adresse = data.Adresse,
                            Etat = data.Active.Value ? "Actif" : "Désactivé",

                        }).ToList();
            }

            // = ListClientsList;
            dgv1.HideIDColumn("Numero");

            for (int i = 0; i < dgv1.RowCount; i++)
            {
                var sld = dgv1.Rows[i].Cells["Solde"].Value.ToString().ParseToDec();

                if (sld < 0)
                {
                    //   dgv1.Rows[i].Cells["Solde"].Style.BackColor = Color.Red;
                }
                else if (sld > 0)
                {
                    //   dgv1.Rows[i].Cells["Solde"].Style.BackColor = Color.Green;
                }
            }

        }
        public static Client SelectClientDialog(ModelEntities db, bool activeOnly = false)
        {


            FrList e =
            new FrList(true, db, activeOnly);

            //   e.buttonNouveau.Visible = false;
            e.ShowDialog();

            return ReturnClient;

        }
        private void AdminClient_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }
        private void buttonOKClient_Click(object sender, EventArgs e)
        {

            if (!AddAucuneSelection || dgv1.SelectedRows[0].Index != 0)
            {
                var matricule = dgv1.SelectedRows[0].Cells[0].Value.ToString();
                ReturnClient = db.Clients.Single(em => em.Nom == matricule);
            }
            else
                ReturnClient = null; // en cas de selection aucun
            Dispose();
        }
        private static Client ReturnClient { get; set; }
        public bool IsSelectionDialog { get; set; } // if control is called for selection only (not for editing)
        private void buttonAnnuler_Click(object sender, EventArgs e)
        {
            ReturnClient = null;
            Dispose();
        }
        private void buttonNewClient_Click(object sender, EventArgs e)
        {
            if (IsSelectionDialog)
            {
                ReturnClient = FrCreate.CreateClientDialog(db);

                Dispose(); // so return to Utilisateur Liste Selection,

            }
            else
            {
                new FrCreate(null, false).ShowDialog();

                InitializeData();
            }

        }
        private void buttonModifier_Click(object sender, EventArgs e)
        {
            var id = dgv1.GetSelectedID("Numero");
            var selectedClient = db.Clients.Single(em => em.ID == id);

            new FrEdit(selectedClient.ID).ShowDialog();
            db.Refresh(RefreshMode.StoreWins, selectedClient);


            InitializeData();
        }
        private void buttonSupprimer_Click(object sender, EventArgs e)
        {
            var id = dgv1.GetSelectedID("Numero");
            var selectedClient = db.Clients.Single(em => em.ID == id);

            if (this.ConfirmWarning("Supprimer le Client " + selectedClient.Nom + "?"))
            {
                if (selectedClient.Operations.Count + selectedClient.Factures.Count + selectedClient.Factures1.Count != 0)
                {
                    this.ShowError("Impossible de suprimer car il y a des factures ou des paiements enregistrés pour ce client\nsi vous voulez le supprimer, supprimer d'abord tout ses factures et paiements.");
                    return;
                }
                var destionation = selectedClient.Destinations.ToList();
                foreach (var item in destionation)
                {
                    db.Destinations.DeleteObject(item);
                }
                db.Clients.DeleteObject(selectedClient);
                db.SaveChanges();
                InitializeData();
            }
        }







        private void txtNom_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLower(e.KeyChar))
            {
                e.KeyChar = char.ToUpper(e.KeyChar);
            }
        }

        private void buttonSelectOK_Click(object sender, EventArgs e)
        {
            if (!IsSelectionDialog) return;
            if (dgv1.SelectedRows.Count == 1)
            {
                var id = dgv1.GetSelectedID("Numero");
                var selectedClient = UpdateDestination(id,db);


                // db.Refresh(System.Data.Objects.RefreshMode.StoreWins, db.Destinations);
                //  Destination = DestinationClientPartenaire.Destinations.SingleOrDefault(p => p.IsDefault == true);




                ReturnClient = selectedClient;
            }
            else
            {
                ReturnClient = null;
            }
            Dispose();
        }

        public static Client UpdateDestination(int? id, ModelEntities db)
        {
            var selectedClient = db.Clients.Single(em => em.ID == id);


            var Destination = selectedClient.Destinations.OrderBy(p=>p.ID).FirstOrDefault(p => p.IsDefault == true);
           

            if (Destination == null)
            {
                using (var dg = new ModelEntities())
                {
                    var dg_client = dg.Clients.Single(p => p.ID == id);

                    dg_client.Destinations.Add(new Destination
                    {
                        Adresse = dg_client.Adresse,
                        NomRaisonSocial = Tools.FakeNom,
                        Wilaya = dg_client.Wilaya,
                        NoRegistre = Tools.FakeRegistre.Replace("%WW", dg_client.WilayaID.ToString()),
                        NoArticle = Tools.FakeArticle.Replace("%WW", dg_client.WilayaID.ToString()),
                        NoIdentifiant = Tools.FakeFiscal,
                        IsDefault = true,
                        IsReal = false,


                    });


                    if (selectedClient.Destinations.SingleOrDefault(p => p.IsReal == true) == null)
                    {
                        dg_client.Destinations.Add(new Destination
                        {
                            Adresse = dg_client.Adresse,
                            NomRaisonSocial = dg_client.Nom,
                            Wilaya = dg_client.Wilaya,
                            NoRegistre = dg_client.NoRegistre,
                            NoArticle = dg_client.NoArticle,
                            NoIdentifiant = dg_client.NoFiscale,
                            IsDefault = false,
                            IsReal = true,
                            Client = dg_client,

                        });
                    }

                    dg.SaveChanges();
                    var factures = dg_client.Factures1.ToList();
                    foreach (var fact in factures)
                    {
                        fact.Destination1 = dg_client.Destinations.SingleOrDefault(p => p.WilayaID == fact.DestinationWilayaID && p.IsReal == false);
                        if (fact.Destination1 == null)
                        {
                            fact.Destination1 =
                                        new Destination
                                        {
                                            Adresse = fact.Wilaya.Nom,
                                            NomRaisonSocial = Tools.FakeNom,
                                            Wilaya = fact.Wilaya,
                                            NoRegistre = Tools.FakeRegistre.Replace("%WW", fact.DestinationWilayaID.ToString()),
                                            NoArticle = Tools.FakeArticle.Replace("%WW", fact.DestinationWilayaID.ToString()),
                                            NoIdentifiant = Tools.FakeFiscal,
                                            IsDefault = false,
                                            IsReal = false,
                                            Client = dg_client,

                                        };
                            dg.SaveChanges();

                        }
                    }

                    dg.SaveChanges();
                }

            }
            return selectedClient;
        }

        private void FrList_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!IsSelectionDialog) db.Dispose();
        }

        private void txtNom_TextChanged(object sender, EventArgs e)
        {
            RefreshGrid(txtNom.Text.ToUpper());
        }

        private void txtParent_TextChanged(object sender, EventArgs e)
        {
            //   txtNom.Text = "";
            //     InitialiseGridView(txtNom.Text.ToUpper(), txtParent.Text.ToUpper());
        }

        private void dgv1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((Keys)e.KeyChar == Keys.Enter)
            {
                //buttonOKClient_Click(sender, e);

            }
        }
    }
}
