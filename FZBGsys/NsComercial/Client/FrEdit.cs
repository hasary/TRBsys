﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NsTRBsys;

namespace NsTRBsys.NsComercial.NsClient
{
    public partial class FrEdit : Form
    {
        public static Client ReturnClient { get; set; }
        public bool IsSelectionDialogReturn { get; set; }
        private ModelEntities db = new ModelEntities();
        public FrEdit(int IDClient, bool isCreationTemp = false, string TempNom = "", string TempAdresse = "", bool forceCordone = false, bool editForCor = false)
        {
            this.forceCordone = forceCordone;
            this.isCreationTemp = isCreationTemp;
            this.IDClient = IDClient;
            InitializeComponent();
            /*
                        cbTypeClient.Items.Clear();
                        //    cbTypeClient.Items.Add(new TypeClient { ID = 0, Nom = "" });
                        //    cbTypeClient.Items.AddRange(db.TypeClients.Where(p => p.ID > 0).ToArray());
                        cbTypeClient.DropDownStyle = ComboBoxStyle.DropDownList;
                        cbTypeClient.SelectedIndex = 0;
                        cbTypeClient.ValueMember = "ID";
                        cbTypeClient.DisplayMember = "Nom";

            */

            InitializeData(TempNom, TempAdresse);
            if (forceCordone || editForCor)
            {
                tb.SelectedTab = tpDossier;
            }
            IsSelectionDialogReturn = false;
        }

        private void InitializeData(string TempNom = "", string TempAdresse = "")
        {
            Canceled = null;
            if (!isCreationTemp)
            {
                ReturnClient = db.Clients.Single(c => c.ID == this.IDClient);
            }
            else
            {
                ReturnClient = new Client() { Nom = TempNom, Adresse = TempAdresse, NoFiscale = "", NoArticle = "", NoRegistre = "", Telephone = "" };
            }
            txtAdresse.Text = ReturnClient.Adresse;
            txtNomraison.Text = ReturnClient.Nom;
            txtTelephone.Text = ReturnClient.Telephone;
            txtRegistre.Text = ReturnClient.NoRegistre;
            txtArticle.Text = ReturnClient.NoArticle;
            txtFiscal.Text = ReturnClient.NoFiscale;
            chActif.Checked = ReturnClient.Active.Value;
            chPartenaire.Checked = ReturnClient.isPartenaire == true;

            //  cbTypeClient.SelectedItem = ReturnClient.TypeClient;

            cbWilaya.Items.Add(new Wilaya { ID = 0, Nom = "" });
            cbWilaya.Items.AddRange(db.Wilayas.Where(p => p.ID > 0).ToArray());
            cbWilaya.SelectedItem = ReturnClient.Wilaya;
            cbWilaya.ValueMember = "ID";
            cbWilaya.DisplayMember = "Nom";


            /*  cbParentClient.Items.Add(new ClientParent { ID = 0, Nom = "" });
              cbParentClient.Items.AddRange(db.ClientParents.Where(p => p.ID > 0).ToArray());
              cbParentClient.SelectedIndex = 0;
              cbParentClient.ValueMember = "ID";
              cbParentClient.DisplayMember = "Nom";
              */

            /*    if (ReturnClient.ClientParentID != 0)
                {
                    chIsParent.Checked = true;
                    cbParentClient.SelectedItem = ReturnClient.ClientParent;
                }
                else
                {
                    chIsParent.Checked = false;
                }*/
        }
        private void buttonEnregistrerEmployer_Click(object sender, EventArgs e)
        {
            if (isValidAll())
            {

                ReturnClient.Nom = txtNomraison.Text.Trim();
                ReturnClient.Adresse = txtAdresse.Text.Trim();
                ReturnClient.Telephone = txtTelephone.Text.Trim();
                ReturnClient.Wilaya = (Wilaya)cbWilaya.SelectedItem;
                ReturnClient.Active = chActif.Checked;
                ReturnClient.isPartenaire = chPartenaire.Checked;





                ReturnClient.NoRegistre = txtRegistre.Text.Trim().ToUpper();
                ReturnClient.NoArticle = txtArticle.Text.Trim().ToUpper();
                ReturnClient.NoFiscale = txtFiscal.Text.Trim();



                if (chIsParent.Checked)
                {
                    var selected = cbParentClient.SelectedItem;
                    if (selected != null)
                    {
                        //    var parent = (ClientParent)cbParentClient.SelectedItem;
                        //    ReturnClient.ClientParentID = parent.ID;


                    }
                    else // new parent
                    {
                        //    ReturnClient.ClientParent = new ClientParent() { Nom = cbParentClient.Text.ToString() };
                    }
                }
                else
                {
                    //  ReturnClient.ClientParentID = 0;
                }


                Destination Destination = ReturnClient.Destinations.SingleOrDefault(p => p.IsReal == true);

                if (Destination != null)
                {

                    Destination.WilayaID = ReturnClient.WilayaID;
                    Destination.Adresse = ReturnClient.Adresse;
                    Destination.Client = ReturnClient;
                    Destination.IsDefault = false;
                    Destination.IsReal = true;
                    Destination.NoArticle = ReturnClient.NoArticle;
                    Destination.NoRegistre = ReturnClient.NoRegistre;
                    Destination.NoIdentifiant = ReturnClient.NoFiscale;

                }

                

                db.SaveChanges();
                Canceled = false;
                Dispose();

            }
        }

        public static Client createClientTemp(string tempNom, string tempAdre)
        {

            new FrEdit(0, true, tempNom, tempAdre).ShowDialog();
            return ReturnClient;


        }

        private bool isValidAll()
        {
            string ErrorMessage = String.Empty;


            if (txtNomraison.Text.Trim().Length < 1 || txtNomraison.Text.Length > 70)
            {

                ErrorMessage += "\nCe Nom est Invalide (doit faire entre 2 et 70 caracteres).";
            }
            else
            {
                var matricule = txtNomraison.Text.Trim();
                var Uti = db.Clients.FirstOrDefault(em => em.Nom == matricule && em.ID != IDClient);

                if (Uti != null)
                {
                    ErrorMessage += "\nCe Nom existe déja dans la liste des client";

                }
            }

            if (cbWilaya.SelectedIndex == 0)
            {
                ErrorMessage += "\nVeillez selectionner wilaya!";
            }
            /*    if (cbTypeClient.SelectedIndex < 1)
                {
                    ErrorMessage += "\nVeillez selectionner un type Client";
                }
    */
            if (panReg.Visible)
            {
                //  informations registre commerce
            }

            if (chIsParent.Checked && cbParentClient.SelectedIndex == 0)
            {
                ErrorMessage += "\nVeillez spécifier ou selectionné le nom du parent";
            }
            if (forceCordone)
            {
                if (txtFiscal.Text.Length < 6 || txtRegistre.Text.Length < 8 || txtArticle.Text.Length < 8)
                {
                    ErrorMessage += "\nDossier commercial incomplet ou invalide";
                }
                else
                {
                    var idWilaya = ((Wilaya)cbWilaya.SelectedItem).ID.ToString("00");
                    if (!txtRegistre.Text.StartsWith(idWilaya) || !txtArticle.Text.StartsWith(idWilaya))
                    {
                        ErrorMessage += "\nN° Registre et Article doivent commecer par Code Wilaya " + idWilaya;
                    }
                }
            }

            if (ErrorMessage != String.Empty)
            {
                Tools.ShowError(ErrorMessage);
                // MessageBox.Show(ErrorMessage, "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }
        private void AdminEmployerNew_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Canceled == null)
            {
                Canceled = true;
            }
            db.Dispose();
        }
        private void buttonAnnulerEmployer_Click(object sender, EventArgs e)
        {
            ReturnClient = null;

            Dispose();
        }

        public int IDClient { get; set; }

        public static bool? Canceled { get; set; }

        public bool isCreationTemp { get; set; }



        private void chIsParent_CheckedChanged(object sender, EventArgs e)
        {
            cbParentClient.Visible = chIsParent.Checked;
        }

        private void cbTypeClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            /* if (cbTypeClient.SelectedIndex > 0)
             {
                 //      panReg.Visible = ((TypeClient)cbTypeClient.SelectedItem).HasRegistre.Value;
             }
             */
        }

        private void cbParentClient_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLower(e.KeyChar))
            {
                e.KeyChar = char.ToUpper(e.KeyChar);
            }
        }

        public bool forceCordone { get; set; }
    }
}
