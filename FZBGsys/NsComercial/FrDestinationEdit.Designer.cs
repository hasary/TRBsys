﻿namespace NsTRBsys.NsComercial
{
    partial class FrDestinationEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbWilaya = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNomraison = new NsTRBsys.TextBoxx();
            this.txtAdresse = new NsTRBsys.TextBoxx();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtRegistre = new NsTRBsys.TextBoxx();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtFiscal = new NsTRBsys.TextBoxx();
            this.txtArticle = new NsTRBsys.TextBoxx();
            this.buttonAnnuler = new System.Windows.Forms.Button();
            this.buttonEnregistre = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cbWilaya
            // 
            this.cbWilaya.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWilaya.FormattingEnabled = true;
            this.cbWilaya.Location = new System.Drawing.Point(191, 123);
            this.cbWilaya.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbWilaya.Name = "cbWilaya";
            this.cbWilaya.Size = new System.Drawing.Size(239, 24);
            this.cbWilaya.TabIndex = 15;
            this.cbWilaya.SelectedIndexChanged += new System.EventHandler(this.cbWilaya_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 17);
            this.label1.TabIndex = 12;
            this.label1.Text = "Nom / Raison Sociale:";
            // 
            // txtNomraison
            // 
            this.txtNomraison.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNomraison.Location = new System.Drawing.Point(191, 27);
            this.txtNomraison.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNomraison.Name = "txtNomraison";
            this.txtNomraison.Size = new System.Drawing.Size(239, 22);
            this.txtNomraison.TabIndex = 13;
            // 
            // txtAdresse
            // 
            this.txtAdresse.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAdresse.Location = new System.Drawing.Point(191, 63);
            this.txtAdresse.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtAdresse.Multiline = true;
            this.txtAdresse.Name = "txtAdresse";
            this.txtAdresse.Size = new System.Drawing.Size(239, 45);
            this.txtAdresse.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(116, 126);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 17);
            this.label7.TabIndex = 17;
            this.label7.Text = "Wilaya:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(106, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 17);
            this.label3.TabIndex = 16;
            this.label3.Text = "Adresse:";
            // 
            // txtRegistre
            // 
            this.txtRegistre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRegistre.Location = new System.Drawing.Point(191, 177);
            this.txtRegistre.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtRegistre.Name = "txtRegistre";
            this.txtRegistre.Size = new System.Drawing.Size(238, 22);
            this.txtRegistre.TabIndex = 21;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 180);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 17);
            this.label2.TabIndex = 18;
            this.label2.Text = "N° Registre Comerce:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 207);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 17);
            this.label4.TabIndex = 19;
            this.label4.Text = "N° Fiscale:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 236);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 17);
            this.label5.TabIndex = 20;
            this.label5.Text = "N° Article:";
            // 
            // txtFiscal
            // 
            this.txtFiscal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFiscal.Location = new System.Drawing.Point(190, 205);
            this.txtFiscal.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtFiscal.Name = "txtFiscal";
            this.txtFiscal.Size = new System.Drawing.Size(239, 22);
            this.txtFiscal.TabIndex = 22;
            // 
            // txtArticle
            // 
            this.txtArticle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtArticle.Location = new System.Drawing.Point(190, 233);
            this.txtArticle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtArticle.Name = "txtArticle";
            this.txtArticle.Size = new System.Drawing.Size(239, 22);
            this.txtArticle.TabIndex = 23;
            // 
            // buttonAnnuler
            // 
            this.buttonAnnuler.Image = global::NsTRBsys.Properties.Resources.icon_16_deny;
            this.buttonAnnuler.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAnnuler.Location = new System.Drawing.Point(194, 277);
            this.buttonAnnuler.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonAnnuler.Name = "buttonAnnuler";
            this.buttonAnnuler.Size = new System.Drawing.Size(112, 33);
            this.buttonAnnuler.TabIndex = 25;
            this.buttonAnnuler.Text = "Annuler";
            this.buttonAnnuler.UseVisualStyleBackColor = true;
            this.buttonAnnuler.Click += new System.EventHandler(this.buttonAnnuler_Click);
            // 
            // buttonEnregistre
            // 
            this.buttonEnregistre.Image = global::NsTRBsys.Properties.Resources.icon_16_allow;
            this.buttonEnregistre.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonEnregistre.Location = new System.Drawing.Point(312, 277);
            this.buttonEnregistre.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonEnregistre.Name = "buttonEnregistre";
            this.buttonEnregistre.Size = new System.Drawing.Size(117, 33);
            this.buttonEnregistre.TabIndex = 24;
            this.buttonEnregistre.Text = "Enregistrer";
            this.buttonEnregistre.UseVisualStyleBackColor = true;
            this.buttonEnregistre.Click += new System.EventHandler(this.buttonEnregistre_Click);
            // 
            // FrDestinationEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(442, 330);
            this.Controls.Add(this.buttonAnnuler);
            this.Controls.Add(this.buttonEnregistre);
            this.Controls.Add(this.txtRegistre);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtFiscal);
            this.Controls.Add(this.txtArticle);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cbWilaya);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtNomraison);
            this.Controls.Add(this.txtAdresse);
            this.Name = "FrDestinationEdit";
            this.Text = "Destination / Coordonées";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrDestinationEdit_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbWilaya;
        private System.Windows.Forms.Label label1;
        private TextBoxx txtNomraison;
        private TextBoxx txtAdresse;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private TextBoxx txtRegistre;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private TextBoxx txtFiscal;
        private TextBoxx txtArticle;
        private System.Windows.Forms.Button buttonAnnuler;
        private System.Windows.Forms.Button buttonEnregistre;
    }
}