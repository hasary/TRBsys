﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NsTRBsys.NsComercial
{
    public partial class FrDestinationEdit : Form
    {
        ModelEntities db = new ModelEntities();
        public FrDestinationEdit(int ClientID, int? EditID = null)
        {
            InitializeComponent();
            InitializeControls();
            this.Client = db.Clients.Single(p => p.ID == ClientID);

            if (EditID == null) InputDefaultValues();
            else InputDestination(EditID.Value);
        }

        //public bool IsSelectionDialog { get; set; }


        private void InputDestination(int id)
        {
            this.Destination = db.Destinations.Single(p => p.ID == id);
            cbWilaya.SelectedItem = Destination.Wilaya;
            txtNomraison.Text = Destination.NomRaisonSocial;
            txtAdresse.Text = Destination.Adresse;
            txtArticle.Text = Destination.NoArticle;
            txtRegistre.Text = Destination.NoRegistre;

        }

        private void InputDefaultValues()
        {
            txtAdresse.Text = "";
            txtNomraison.Text = Tools.FakeNom;
            cbWilaya.SelectedItem = Client.Wilaya;
        }

        private void InitializeControls()
        {

            cbWilaya.Items.Add(new Wilaya { ID = 0, Nom = "" });
            cbWilaya.Items.AddRange(db.Wilayas.Where(p => p.ID > 0).ToArray());
            cbWilaya.SelectedIndex = 0;
            cbWilaya.ValueMember = "ID";
            cbWilaya.DisplayMember = "Nom";
            cbWilaya.DropDownStyle = ComboBoxStyle.DropDown;

            AutoCompleteStringCollection acsc = new AutoCompleteStringCollection();
            cbWilaya.AutoCompleteCustomSource = acsc;
            cbWilaya.AutoCompleteMode = AutoCompleteMode.Suggest;
            cbWilaya.AutoCompleteSource = AutoCompleteSource.CustomSource;
            var results = db.Wilayas.ToList();

            if (results.Count > 0)
                foreach (var wi in results)
                {
                    acsc.Add(wi.Nom);
                }
        }

        private void FrDestinationEdit_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        public Destination Destination { get; set; }

        public Client Client { get; set; }

        private void cbWilaya_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbWilaya.SelectedIndex > 0)
            {

                txtRegistre.Enabled = true;
                txtArticle.Enabled = true;
                txtFiscal.Enabled = true;

                var WilayaID = ((Wilaya)cbWilaya.SelectedItem).ID;

                txtRegistre.Text = Tools.FakeRegistre.Replace("%WW", WilayaID.ToString().PadLeft(2, '0'));
                txtArticle.Text = Tools.FakeArticle.Replace("%WW", WilayaID.ToString().PadLeft(2, '0'));
                txtFiscal.Text = Tools.FakeFiscal;

            }
            else
            {
                txtRegistre.Text = "";
                txtArticle.Text = "";
                txtFiscal.Text = "";

                txtRegistre.Enabled = false;
                txtArticle.Enabled = false;
                txtFiscal.Enabled = false;
            }
        }
        private bool ISvalidAll()
        {
            string ErrorMessage = "";
            if (txtFiscal.Text.Length < 6 || txtRegistre.Text.Length < 8 || txtArticle.Text.Length < 8)
            {
                ErrorMessage += "\nDossier commercial incomplet ou invalide";
            }
            else
            {
                var idWilaya = ((Wilaya)cbWilaya.SelectedItem).ID.ToString("00");
                if (!txtRegistre.Text.StartsWith(idWilaya) || !txtArticle.Text.StartsWith(idWilaya))
                {
                    ErrorMessage += "\nN° Registre et Article doivent commecer par Code Wilaya " + idWilaya;
                }
            }

            if (ErrorMessage != "")
            {
                Tools.ShowError(ErrorMessage);
                return false;
            }
            return true;
        }
        private void buttonEnregistre_Click(object sender, EventArgs e)
        {
            if (!ISvalidAll())
            {
                return;
            }

            if (Destination == null)
            {
                Destination = new Destination();
                db.AddToDestinations(Destination);
            }

            Destination.NomRaisonSocial = txtNomraison.Text;
            Destination.Adresse = txtAdresse.Text;
            Destination.NoArticle = txtArticle.Text;
            Destination.NoIdentifiant = txtFiscal.Text;
            Destination.NoRegistre = txtRegistre.Text;
            Destination.IsDefault = false;
            Destination.IsReal = false;
            Destination.Wilaya = (Wilaya)cbWilaya.SelectedItem;
            Destination.Client = Client;
            db.SaveChanges();
            Dispose();
        }

        private void buttonAnnuler_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
