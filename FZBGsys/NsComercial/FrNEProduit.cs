﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NsTRBsys.NsComercial
{
    public partial class FrNEProduit : Form
    {
        ModelEntities db = new ModelEntities();
        public FrNEProduit(int? IDGout = null, int? IDFormat = null)
        {
            InitializeComponent();
            InitializeControls();
            this.IDGout = IDGout;
            if (IDGout != null)
            {
                this.IDGout = IDGout;
                this.IDFormat = IDFormat;
                LoadProduit();

            }


        }

        public void loadInfos(int formatID, int goutID)
        {

            var Format = db.Formats.Single(p => p.ID == formatID);
            cbFormat.SelectedItem = Format;

            SelectedProduit = db.GoutProduits.Single(p => p.ID == goutID);

            txtNomProduit.Text = SelectedProduit.Nom;



        }



        private void InitializeControls()
        {

            cbFormat.Items.Add(new Format { ID = 0, Volume = "" });
            cbFormat.Items.AddRange(db.Formats.Where(p => p.ID > 0).ToArray());
            cbFormat.DisplayMember = "Volume";
            cbFormat.ValueMember = "ID";

            cbFormat.SelectedIndex = 0;

            AutoCompleteStringCollection acsc = new AutoCompleteStringCollection();
            txtNomProduit.AutoCompleteCustomSource = acsc;
            txtNomProduit.AutoCompleteMode = AutoCompleteMode.Suggest;
            txtNomProduit.AutoCompleteSource = AutoCompleteSource.CustomSource;
            var results = db.GoutProduits.ToList();

            if (results.Count > 0)
                foreach (var gout in results)
                {
                    acsc.Add(gout.Nom);
                }

        }

        private void LoadProduit()
        {

            var produit = db.GoutProduits.Single(p => p.ID == IDGout);
            var Format = db.Formats.Single(p => p.ID == IDFormat);
            var prix = db.PrixProduits.Single(p => p.FormatID == IDFormat && p.GoutProduitID == IDGout && p.TypeClient == 0);

            cbFormat.SelectedItem = Format;
            txtNomProduit.Text = produit.Nom;
            txtFardeaux.Text = prix.BouteilleParFardeaux.ToString();
            txtPalette.Text = prix.FardeauxParPalette.ToString();
            txtPrixVente.Text = prix.PrixUnitaire.ToAffInt();
            txtPrixAchat.Text = prix.PrixAchat.ToAffInt();

            cbFormat.Enabled = this.IDFormat == null;
        }

        private void buttonAnnuler_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void buttonEnregistre_Click(object sender, EventArgs e)
        {
            if (!IsValidAll())
            {
                return;
            }

            if (IDGout != null) //Edit
            {
                var produit = db.GoutProduits.Single(p => p.ID == IDGout);
                var Format = db.Formats.Single(p => p.ID == IDFormat);
                var prix = db.PrixProduits.Single(p => p.FormatID == IDFormat && p.GoutProduitID == IDGout && p.TypeClient == 0);

                produit.Nom =
                    System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(txtNomProduit.Text.Trim().ToLower());

                prix.BouteilleParFardeaux = txtFardeaux.Text.ParseToInt();
                prix.FardeauxParPalette = txtPalette.Text.ParseToInt();
                prix.PrixUnitaire = txtPrixVente.Text.ParseToDec();
                prix.PrixAchat = txtPrixAchat.Text.ParseToDec();

            }
            else // new
            {
                if (SelectedProduit == null)
                {
                    SelectedProduit = new GoutProduit()
                           {
                               Nom = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(txtNomProduit.Text.Trim().ToLower())

                           };
                    db.AddToGoutProduits(SelectedProduit);
                }
                var prix = new PrixProduit()
                {
                    GoutProduitID = SelectedProduit.ID,
                    FormatID = ((Format)cbFormat.SelectedItem).ID,
                    PrixUnitaire = txtPrixVente.Text.ParseToDec(),
                    PrixAchat = txtPrixAchat.Text.ParseToDec(),
                    TypeClient = 0,
                };
                prix.FardeauxParPalette = txtPalette.Text.ParseToInt();
                prix.BouteilleParFardeaux = txtFardeaux.Text.ParseToInt();
                prix.PrixUnitaire = txtPrixVente.Text.ParseToDec();
                db.AddToPrixProduits(prix);

            }

            db.SaveChanges();
            Dispose();

        }

        private bool IsValidAll()
        {
            if (txtNomProduit.Text.Trim().Length < 2)
            {
                Tools.ShowError("Nom invalide");
                return false;
            }
            else if (SelectedProduit != null && (Format)cbFormat.SelectedItem != null)
            {
                var format = (Format)cbFormat.SelectedItem;
                var exist = db.PrixProduits.Where(p => p.FormatID == format.ID && p.GoutProduitID == SelectedProduit.ID);
                if (exist.Count() != 0 && IDGout == null)
                {
                    Tools.ShowError("Ce produit Exist déja dans la liste");
                    return false;
                }
            }

            if (txtFardeaux.Text.ParseToInt() == null || txtFardeaux.Text.ParseToInt() < 1)
            {
                Tools.ShowError("Nombre bouteille dans fardeaux invalide");
                return false;
            }
            if (txtPalette.Text.ParseToInt() == null || txtPalette.Text.ParseToInt() < 1)
            {
                Tools.ShowError("Nombre fardeaux dans palette invalide");
                return false;
            }
            if (txtPrixVente.Text.Trim() != "")
            {
                if (txtPrixVente.Text.ParseToDec() == null)
                {
                    Tools.ShowError("prix vente invalide");
                    return false;
                }
            }

            if (txtPrixAchat.Text.Trim() != "")
            {
                if (txtPrixAchat.Text.ParseToDec() == null)
                {
                    Tools.ShowError("prix achat invalide");
                    return false;
                }
                
            }

            return true;
        }

        public int? IDGout { get; set; }

        private void FrNEProduit_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        public int? IDFormat { get; set; }

        private void txtFardeaux_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalBouteille();
        }

        private void UpdateTotalBouteille()
        {
            txtBinP.Text = "";
            if (txtPalette.Text.Trim().ParseToInt() != null &&
                txtFardeaux.Text.Trim().ParseToInt() != null)
            {
                txtBinP.Text = (txtPalette.Text.Trim().ParseToInt() * txtFardeaux.Text.Trim().ParseToInt()).ToString();
            }
        }

        private void txtPalette_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalBouteille();
        }

        private void txtNomProduit_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtNomProduit.Text = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(txtNomProduit.Text);
        }

        private void txtNomProduit_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLower(e.KeyChar))
            {
                e.KeyChar = char.ToUpper(e.KeyChar);
            }
        }

        private void txtPrix_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '.')
            {
                e.KeyChar = ',';
            }
        }

        public int IDGoutNew { get; set; }

        public int IDFormatNew { get; set; }

        private void txtNomProduit_TextChanged(object sender, EventArgs e)
        {
            var nom = txtNomProduit.Text.Trim();
            this.SelectedProduit = db.GoutProduits.SingleOrDefault(p => p.Nom == nom);
        }

        public GoutProduit SelectedProduit { get; set; }
    }
}
