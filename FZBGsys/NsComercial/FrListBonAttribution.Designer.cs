﻿namespace FZBGsys.NsComercial
{
    partial class FrListBonAttribution
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.cMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.afficherLeContenueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifierLeBonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.annulerLeBonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmBonSortie = new System.Windows.Forms.ToolStripMenuItem();
            this.imprimerBonDeSortieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifierBonDeSortieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.annulerBonDeSortieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmBonLivraison = new System.Windows.Forms.ToolStripMenuItem();
            this.imprimerBonLivraisonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifierLivraisonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.annulerLivraisonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFacture = new System.Windows.Forms.ToolStripMenuItem();
            this.imprimerLaFactureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifierLaFactureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.annulerLaFactureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.btFermer = new System.Windows.Forms.Button();
            this.chRechAvancee = new System.Windows.Forms.CheckBox();
            this.btRecherche = new System.Windows.Forms.Button();
            this.btImprimeBon = new System.Windows.Forms.Button();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.panDate = new System.Windows.Forms.Panel();
            this.dtDateAu = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.dtDateDu = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.panRechAvance = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chAnnulé = new System.Windows.Forms.CheckBox();
            this.chLivré = new System.Windows.Forms.CheckBox();
            this.chEnInstance = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtClient = new System.Windows.Forms.TextBox();
            this.txtNoBon = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.labFilter = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.cMenu.SuspendLayout();
            this.panDate.SuspendLayout();
            this.panRechAvance.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.ContextMenuStrip = this.cMenu;
            this.dgv.Location = new System.Drawing.Point(194, 37);
            this.dgv.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 16;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(575, 323);
            this.dgv.TabIndex = 10;
            this.dgv.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellClick);
            this.dgv.SelectionChanged += new System.EventHandler(this.dgv_SelectionChanged);
            // 
            // cMenu
            // 
            this.cMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.afficherLeContenueToolStripMenuItem,
            this.modifierLeBonToolStripMenuItem,
            this.annulerLeBonToolStripMenuItem,
            this.toolStripSeparator1,
            this.tsmBonSortie,
            this.tsmBonLivraison,
            this.tsmFacture});
            this.cMenu.Name = "cMenu";
            this.cMenu.Size = new System.Drawing.Size(215, 176);
            // 
            // afficherLeContenueToolStripMenuItem
            // 
            this.afficherLeContenueToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.afficherLeContenueToolStripMenuItem.Name = "afficherLeContenueToolStripMenuItem";
            this.afficherLeContenueToolStripMenuItem.Size = new System.Drawing.Size(214, 24);
            this.afficherLeContenueToolStripMenuItem.Text = "Afficher le Contenu";
            // 
            // modifierLeBonToolStripMenuItem
            // 
            this.modifierLeBonToolStripMenuItem.Name = "modifierLeBonToolStripMenuItem";
            this.modifierLeBonToolStripMenuItem.Size = new System.Drawing.Size(214, 24);
            this.modifierLeBonToolStripMenuItem.Text = "Modifier le Bon";
            this.modifierLeBonToolStripMenuItem.Click += new System.EventHandler(this.modifierLeBonToolStripMenuItem_Click);
            // 
            // annulerLeBonToolStripMenuItem
            // 
            this.annulerLeBonToolStripMenuItem.Name = "annulerLeBonToolStripMenuItem";
            this.annulerLeBonToolStripMenuItem.Size = new System.Drawing.Size(214, 24);
            this.annulerLeBonToolStripMenuItem.Text = "Annuler le Bon";
            this.annulerLeBonToolStripMenuItem.Click += new System.EventHandler(this.annulerLeBonToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(211, 6);
            // 
            // tsmBonSortie
            // 
            this.tsmBonSortie.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.imprimerBonDeSortieToolStripMenuItem,
            this.modifierBonDeSortieToolStripMenuItem,
            this.annulerBonDeSortieToolStripMenuItem});
            this.tsmBonSortie.Name = "tsmBonSortie";
            this.tsmBonSortie.Size = new System.Drawing.Size(214, 24);
            this.tsmBonSortie.Text = "Bon de Sortie";
            // 
            // imprimerBonDeSortieToolStripMenuItem
            // 
            this.imprimerBonDeSortieToolStripMenuItem.Name = "imprimerBonDeSortieToolStripMenuItem";
            this.imprimerBonDeSortieToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.imprimerBonDeSortieToolStripMenuItem.Text = "Imprimer Bon de Sortie";
            this.imprimerBonDeSortieToolStripMenuItem.Click += new System.EventHandler(this.imprimerBonDeSortieToolStripMenuItem_Click);
            // 
            // modifierBonDeSortieToolStripMenuItem
            // 
            this.modifierBonDeSortieToolStripMenuItem.Name = "modifierBonDeSortieToolStripMenuItem";
            this.modifierBonDeSortieToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.modifierBonDeSortieToolStripMenuItem.Text = "Modifier Bon de Sortie";
            this.modifierBonDeSortieToolStripMenuItem.Click += new System.EventHandler(this.modifierBonDeSortieToolStripMenuItem_Click);
            // 
            // annulerBonDeSortieToolStripMenuItem
            // 
            this.annulerBonDeSortieToolStripMenuItem.Name = "annulerBonDeSortieToolStripMenuItem";
            this.annulerBonDeSortieToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.annulerBonDeSortieToolStripMenuItem.Text = "Annuler Bon de Sortie";
            this.annulerBonDeSortieToolStripMenuItem.Click += new System.EventHandler(this.annulerBonDeSortieToolStripMenuItem_Click);
            // 
            // tsmBonLivraison
            // 
            this.tsmBonLivraison.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.imprimerBonLivraisonToolStripMenuItem,
            this.modifierLivraisonToolStripMenuItem,
            this.annulerLivraisonToolStripMenuItem});
            this.tsmBonLivraison.Name = "tsmBonLivraison";
            this.tsmBonLivraison.Size = new System.Drawing.Size(214, 24);
            this.tsmBonLivraison.Text = "Bon de Livraison";
            // 
            // imprimerBonLivraisonToolStripMenuItem
            // 
            this.imprimerBonLivraisonToolStripMenuItem.Name = "imprimerBonLivraisonToolStripMenuItem";
            this.imprimerBonLivraisonToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.imprimerBonLivraisonToolStripMenuItem.Text = "Imprimer Bon Livraison";
            this.imprimerBonLivraisonToolStripMenuItem.Click += new System.EventHandler(this.imprimerBonLivraisonToolStripMenuItem_Click);
            // 
            // modifierLivraisonToolStripMenuItem
            // 
            this.modifierLivraisonToolStripMenuItem.Name = "modifierLivraisonToolStripMenuItem";
            this.modifierLivraisonToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.modifierLivraisonToolStripMenuItem.Text = "Modifier Livraison";
            this.modifierLivraisonToolStripMenuItem.Click += new System.EventHandler(this.modifierLivraisonToolStripMenuItem_Click);
            // 
            // annulerLivraisonToolStripMenuItem
            // 
            this.annulerLivraisonToolStripMenuItem.Name = "annulerLivraisonToolStripMenuItem";
            this.annulerLivraisonToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.annulerLivraisonToolStripMenuItem.Text = "Annuler Livraison";
            this.annulerLivraisonToolStripMenuItem.Click += new System.EventHandler(this.annulerLivraisonToolStripMenuItem_Click);
            // 
            // tsmFacture
            // 
            this.tsmFacture.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.imprimerLaFactureToolStripMenuItem,
            this.modifierLaFactureToolStripMenuItem,
            this.annulerLaFactureToolStripMenuItem});
            this.tsmFacture.Name = "tsmFacture";
            this.tsmFacture.Size = new System.Drawing.Size(214, 24);
            this.tsmFacture.Text = "Facture";
            // 
            // imprimerLaFactureToolStripMenuItem
            // 
            this.imprimerLaFactureToolStripMenuItem.Name = "imprimerLaFactureToolStripMenuItem";
            this.imprimerLaFactureToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.imprimerLaFactureToolStripMenuItem.Text = "Imprimer la Facture";
            this.imprimerLaFactureToolStripMenuItem.Click += new System.EventHandler(this.imprimerLaFactureToolStripMenuItem_Click);
            // 
            // modifierLaFactureToolStripMenuItem
            // 
            this.modifierLaFactureToolStripMenuItem.Name = "modifierLaFactureToolStripMenuItem";
            this.modifierLaFactureToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.modifierLaFactureToolStripMenuItem.Text = "Modifier la Facture";
            this.modifierLaFactureToolStripMenuItem.Click += new System.EventHandler(this.modifierLaFactureToolStripMenuItem_Click);
            // 
            // annulerLaFactureToolStripMenuItem
            // 
            this.annulerLaFactureToolStripMenuItem.Name = "annulerLaFactureToolStripMenuItem";
            this.annulerLaFactureToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.annulerLaFactureToolStripMenuItem.Text = "Annuler la Facture";
            this.annulerLaFactureToolStripMenuItem.Click += new System.EventHandler(this.annulerLaFactureToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 16);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Date Création:";
            // 
            // btFermer
            // 
            this.btFermer.Location = new System.Drawing.Point(692, 362);
            this.btFermer.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btFermer.Name = "btFermer";
            this.btFermer.Size = new System.Drawing.Size(78, 28);
            this.btFermer.TabIndex = 57;
            this.btFermer.Text = "Fermer";
            this.btFermer.UseVisualStyleBackColor = true;
            this.btFermer.Click += new System.EventHandler(this.btFermer_Click);
            // 
            // chRechAvancee
            // 
            this.chRechAvancee.AutoSize = true;
            this.chRechAvancee.Location = new System.Drawing.Point(12, 55);
            this.chRechAvancee.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chRechAvancee.Name = "chRechAvancee";
            this.chRechAvancee.Size = new System.Drawing.Size(125, 17);
            this.chRechAvancee.TabIndex = 61;
            this.chRechAvancee.Text = "Recherche Avancée";
            this.chRechAvancee.UseVisualStyleBackColor = true;
            this.chRechAvancee.CheckedChanged += new System.EventHandler(this.chRechAvancee_CheckedChanged);
            // 
            // btRecherche
            // 
            this.btRecherche.Location = new System.Drawing.Point(12, 365);
            this.btRecherche.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btRecherche.Name = "btRecherche";
            this.btRecherche.Size = new System.Drawing.Size(110, 28);
            this.btRecherche.TabIndex = 63;
            this.btRecherche.Text = "Recherche";
            this.btRecherche.UseVisualStyleBackColor = true;
            this.btRecherche.Click += new System.EventHandler(this.btRecherche_Click);
            // 
            // btImprimeBon
            // 
            this.btImprimeBon.Location = new System.Drawing.Point(194, 362);
            this.btImprimeBon.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btImprimeBon.Name = "btImprimeBon";
            this.btImprimeBon.Size = new System.Drawing.Size(84, 28);
            this.btImprimeBon.TabIndex = 58;
            this.btImprimeBon.Text = "Détails...";
            this.btImprimeBon.UseVisualStyleBackColor = true;
            this.btImprimeBon.Click += new System.EventHandler(this.btImprimeBon_Click);
            // 
            // dtDate
            // 
            this.dtDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDate.Location = new System.Drawing.Point(86, 15);
            this.dtDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(96, 20);
            this.dtDate.TabIndex = 64;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 80);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Date Création:";
            // 
            // panDate
            // 
            this.panDate.Controls.Add(this.dtDateAu);
            this.panDate.Controls.Add(this.label5);
            this.panDate.Controls.Add(this.dtDateDu);
            this.panDate.Controls.Add(this.label4);
            this.panDate.Location = new System.Drawing.Point(1, 95);
            this.panDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panDate.Name = "panDate";
            this.panDate.Size = new System.Drawing.Size(160, 57);
            this.panDate.TabIndex = 24;
            this.panDate.Visible = false;
            // 
            // dtDateAu
            // 
            this.dtDateAu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateAu.Location = new System.Drawing.Point(47, 31);
            this.dtDateAu.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtDateAu.Name = "dtDateAu";
            this.dtDateAu.Size = new System.Drawing.Size(105, 20);
            this.dtDateAu.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 32);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Au:";
            // 
            // dtDateDu
            // 
            this.dtDateDu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateDu.Location = new System.Drawing.Point(47, 8);
            this.dtDateDu.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtDateDu.Name = "dtDateDu";
            this.dtDateDu.Size = new System.Drawing.Size(105, 20);
            this.dtDateDu.TabIndex = 1;
            this.dtDateDu.Value = new System.DateTime(2012, 6, 1, 0, 0, 0, 0);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 10);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Du:";
            // 
            // panRechAvance
            // 
            this.panRechAvance.Controls.Add(this.groupBox1);
            this.panRechAvance.Controls.Add(this.label6);
            this.panRechAvance.Controls.Add(this.txtClient);
            this.panRechAvance.Controls.Add(this.txtNoBon);
            this.panRechAvance.Controls.Add(this.label3);
            this.panRechAvance.Controls.Add(this.panDate);
            this.panRechAvance.Controls.Add(this.label2);
            this.panRechAvance.Enabled = false;
            this.panRechAvance.Location = new System.Drawing.Point(12, 77);
            this.panRechAvance.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panRechAvance.Name = "panRechAvance";
            this.panRechAvance.Size = new System.Drawing.Size(170, 274);
            this.panRechAvance.TabIndex = 62;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chAnnulé);
            this.groupBox1.Controls.Add(this.chLivré);
            this.groupBox1.Controls.Add(this.chEnInstance);
            this.groupBox1.Location = new System.Drawing.Point(14, 169);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(142, 85);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Etat";
            // 
            // chAnnulé
            // 
            this.chAnnulé.AutoSize = true;
            this.chAnnulé.Location = new System.Drawing.Point(44, 62);
            this.chAnnulé.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chAnnulé.Name = "chAnnulé";
            this.chAnnulé.Size = new System.Drawing.Size(44, 14);
            this.chAnnulé.TabIndex = 0;
            this.chAnnulé.Text = "Annulé";
            this.chAnnulé.UseVisualStyleBackColor = true;
            // 
            // chLivré
            // 
            this.chLivré.AutoSize = true;
            this.chLivré.Checked = true;
            this.chLivré.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chLivré.Location = new System.Drawing.Point(44, 40);
            this.chLivré.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chLivré.Name = "chLivré";
            this.chLivré.Size = new System.Drawing.Size(37, 14);
            this.chLivré.TabIndex = 0;
            this.chLivré.Text = "Livré";
            this.chLivré.UseVisualStyleBackColor = true;
            // 
            // chEnInstance
            // 
            this.chEnInstance.AutoSize = true;
            this.chEnInstance.Checked = true;
            this.chEnInstance.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chEnInstance.Location = new System.Drawing.Point(44, 18);
            this.chEnInstance.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chEnInstance.Name = "chEnInstance";
            this.chEnInstance.Size = new System.Drawing.Size(62, 14);
            this.chEnInstance.TabIndex = 0;
            this.chEnInstance.Text = "En Instance";
            this.chEnInstance.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 47);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 27;
            this.label6.Text = "Client:";
            // 
            // txtClient
            // 
            this.txtClient.Location = new System.Drawing.Point(48, 46);
            this.txtClient.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtClient.Name = "txtClient";
            this.txtClient.Size = new System.Drawing.Size(105, 20);
            this.txtClient.TabIndex = 26;
            // 
            // txtNoBon
            // 
            this.txtNoBon.Location = new System.Drawing.Point(66, 19);
            this.txtNoBon.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtNoBon.Name = "txtNoBon";
            this.txtNoBon.Size = new System.Drawing.Size(87, 20);
            this.txtNoBon.TabIndex = 26;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 21);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "N° Bon:";
            // 
            // labFilter
            // 
            this.labFilter.AutoSize = true;
            this.labFilter.Location = new System.Drawing.Point(191, 15);
            this.labFilter.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labFilter.Name = "labFilter";
            this.labFilter.Size = new System.Drawing.Size(13, 13);
            this.labFilter.TabIndex = 27;
            this.labFilter.Text = "  ";
            // 
            // FrListBonAttribution
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(785, 401);
            this.Controls.Add(this.dtDate);
            this.Controls.Add(this.labFilter);
            this.Controls.Add(this.btRecherche);
            this.Controls.Add(this.panRechAvance);
            this.Controls.Add(this.chRechAvancee);
            this.Controls.Add(this.btImprimeBon);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btFermer);
            this.Controls.Add(this.dgv);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FrListBonAttribution";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Liste Bons d\'attributions";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrListBonAttribution_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.cMenu.ResumeLayout(false);
            this.panDate.ResumeLayout(false);
            this.panDate.PerformLayout();
            this.panRechAvance.ResumeLayout(false);
            this.panRechAvance.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btFermer;
        private System.Windows.Forms.CheckBox chRechAvancee;
        private System.Windows.Forms.Button btRecherche;
        private System.Windows.Forms.Button btImprimeBon;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panDate;
        private System.Windows.Forms.DateTimePicker dtDateAu;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtDateDu;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panRechAvance;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chAnnulé;
        private System.Windows.Forms.CheckBox chLivré;
        private System.Windows.Forms.CheckBox chEnInstance;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtClient;
        private System.Windows.Forms.TextBox txtNoBon;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labFilter;
        private System.Windows.Forms.ContextMenuStrip cMenu;
        private System.Windows.Forms.ToolStripMenuItem afficherLeContenueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modifierLeBonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem annulerLeBonToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem tsmBonSortie;
        private System.Windows.Forms.ToolStripMenuItem modifierBonDeSortieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem annulerBonDeSortieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmBonLivraison;
        private System.Windows.Forms.ToolStripMenuItem modifierLivraisonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem annulerLivraisonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmFacture;
        private System.Windows.Forms.ToolStripMenuItem modifierLaFactureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem annulerLaFactureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imprimerBonDeSortieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imprimerBonLivraisonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imprimerLaFactureToolStripMenuItem;
    }
}