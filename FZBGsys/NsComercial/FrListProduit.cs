﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NsTRBsys.NsComercial
{
    public partial class FrListProduit : Form
    {
        ModelEntities db = new ModelEntities();
        public FrListProduit()
        {
            InitializeComponent();
            cbFormat.Items.Add(new Format { ID = 0, Volume = "(tout)" });
            cbFormat.Items.AddRange(db.Formats.Where(p => p.ID > 0).ToArray());
            cbFormat.DisplayMember = "Volume";
            cbFormat.ValueMember = "ID";

            cbFormat.SelectedIndex = 0;
            Recherche();
        }

        private void modifierToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void btAnnuler_Click(object sender, EventArgs e)
        {
            var selectedIndex = dgv.GetSelectedIndex();
            int scrollPosition = dgv.FirstDisplayedScrollingRowIndex;
            var IDProduit = dgv.GetSelectedID("IDProduit");
            var IDFormat = dgv.GetSelectedID("IDFormat");

            var ventes = db.Ventes.FirstOrDefault(p => p.FormatID == IDFormat && p.GoutProduitID == IDProduit);
            if (ventes != null)
            {
                Tools.ShowError("impossible de supprimer ce produit car il est déja Facturé");

            }
            else
            {
                var toDel = db.GoutProduits.Where(p => p.ID == IDProduit);
                //  db.DeleteObject(toDel);
                var Prix = db.PrixProduits.SingleOrDefault(p => p.TypeClient == 0 && p.GoutProduitID == IDProduit && p.FormatID == IDFormat);
                if (Prix != null)
                {
                    db.DeleteObject(Prix);
                }

                db.SaveChanges();
                Recherche();
                dgv.ClearSelection();
                try
                {
                    dgv.Rows[selectedIndex.Value - 1].Selected = true;
                }
                catch (Exception)
                {
                    
                   dgv.Rows[selectedIndex.Value].Selected = true;
                }
                dgv.FirstDisplayedScrollingRowIndex = scrollPosition;
            }

           
        }

        private void btNouv_Click(object sender, EventArgs e)
        {
            new FrNEProduit().ShowDialog();
            Recherche();
        }

        private void Recherche()
        {
            var data = db.PrixProduits.Where(p => p.TypeClient == 0).ToList().Select(p => new
            {

                Produit = p.GoutProduit.Nom + ' ' + p.Format.Volume,
                IDProduit = p.GoutProduitID,
                IDFormat = p.FormatID,
                Fardeau = p.BouteilleParFardeaux.ToString() + " bouteilles",
                Palette = p.FardeauxParPalette.ToString() + " fardeaux",
                prix_Achat = (p.PrixAchat != null) ? p.PrixAchat.ToAffInt() + " DA" : "",
                prix_Vente = (p.PrixUnitaire != null) ? p.PrixUnitaire.ToAffInt() + " DA" : ""


            }).ToList();

            if (cbFormat.SelectedIndex != 0)
            {
                var formatID = ((Format)cbFormat.SelectedItem).ID;
                data = data.Where(p => p.IDFormat == formatID).ToList();
            }

            dgv.DataSource = data;
            dgv.HideIDColumn("IDProduit");
            dgv.HideIDColumn("IDFormat");

        }

        private void FrListProduit_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        private void cbFormat_SelectedIndexChanged(object sender, EventArgs e)
        {
            Recherche();
        }

        private void btmodif_Click(object sender, EventArgs e)
        {
            var selectedIndex = dgv.GetSelectedIndex();
            int scrollPosition = dgv.FirstDisplayedScrollingRowIndex;
            var IDFormat = dgv.GetSelectedID("IDFormat");
            var IDProduit = dgv.GetSelectedID("IDProduit");
            new FrNEProduit(IDProduit, IDFormat).ShowDialog();
            var prix = db.PrixProduits.Single(p => p.TypeClient == 0 && p.FormatID == IDFormat && p.GoutProduitID == IDProduit);
            db.Refresh(System.Data.Objects.RefreshMode.StoreWins, prix);
            db.Refresh(System.Data.Objects.RefreshMode.StoreWins, prix.GoutProduit);
            Recherche();

            dgv.ClearSelection();
            dgv.Rows[selectedIndex.Value].Selected = true;
            dgv.FirstDisplayedScrollingRowIndex = scrollPosition;



        }
    }
}
