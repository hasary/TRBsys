﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NsTRBsys.NsComercial
{
    public partial class FrListBon : Form
    {
        ModelEntities db = new ModelEntities();
        //   public List<BonAttribution> ListFoundAttribution { get; set; }
        //   public List<BonSortie> ListFoundSortie { get; set; }
        //   public List<BonLivraison> ListFoundLivraison { get; set; }
        public List<Facture> ListFoundFacture { get; set; }

        public bool ActiveAttribution { get; set; }
        public bool ActiveSortie { get; set; }
        public bool ActiveLivraison { get; set; }
        public bool ActiveFacture { get; set; }

        private void UpdateTotal()
        {

            if (tcDocument.SelectedTab == tpFacture)
            {
                labTotalBon.Text = ListFoundFacture.Count + " factures";
                labTotalBon.Text += "  " + ListFoundFacture.Sum(p => p.MontantTotalFacture).ToAffInt() + " DA";
            }
        }

        public FrListBon()
        {
            InitializeComponent();
            ActiveAttribution = true;
            ActiveFacture = true;
            ActiveLivraison = true;
            ActiveSortie = true;

            InitiliseControls();

            AutoCompleteStringCollection acsc = new AutoCompleteStringCollection();
            txtClient.AutoCompleteCustomSource = acsc;
            txtClient.AutoCompleteMode = AutoCompleteMode.Suggest;
            txtClient.AutoCompleteSource = AutoCompleteSource.CustomSource;


            cbWilaya.Items.Add(new Wilaya { ID = 0, Nom = "(tout) " });
            cbWilaya.Items.AddRange(db.Wilayas.Where(p => p.ID > 0).ToArray());
            cbWilaya.SelectedIndex = 0;
            cbWilaya.ValueMember = "ID";
            cbWilaya.DisplayMember = "Nom";

            AutoCompleteStringCollection acsc2 = new AutoCompleteStringCollection();
            cbWilaya.AutoCompleteCustomSource = acsc2;
            cbWilaya.AutoCompleteMode = AutoCompleteMode.Suggest;
            cbWilaya.AutoCompleteSource = AutoCompleteSource.ListItems;


            var results = db.Clients.ToList();

            if (results.Count > 0)
                foreach (var client in results)
                {
                    acsc.Add(client.Nom);
                }
            Recherche();
        }

        private void InitiliseControls()
        {
            //   cbDocument.Items.Clear();
            /*
                             [Tout]
                Attribution
                Sortie
                Livraison
                Facture
             
             
             */
            //   cbDocument.Items.Add("[Tout]");
            //   if (ActiveAttribution) cbDocument.Items.Add("Attribution");
            //   if (ActiveFacture) cbDocument.Items.Add("Facture");
            //   if (ActiveSortie) cbDocument.Items.Add("Sortie");
            //  if (ActiveLivraison) cbDocument.Items.Add("Livraison");

            //   cbDocument.SelectedIndex = 0;


        }
        private void Recherche()
        {
            //using (var db = new ModelEntities())
            {

                IQueryable<Facture> resultFacture = db.Factures;



                if (!chRechAvancee.Checked)
                {
                    var date = dtDate.Value.Date;

                    if (ActiveFacture) resultFacture = resultFacture.Where(p => p.Date == date);
                    else resultFacture = resultFacture.Where(p => false);

                }
                else
                {

                    var dateDu = dtDateDu.Value.Date;
                    var dateAu = dtDateAu.Value.Date;

                    resultFacture = resultFacture.Where(p => p.Date >= dateDu && p.Date <= dateAu);

                    var ID = txtNoBon.Text.ParseToInt();
                    if (ID != null)
                    {

                        resultFacture = resultFacture.Where(p => p.ID == ID);
                    }

                   

                    if (Client != null)
                    {

                        resultFacture = resultFacture.Where(p => p.DestinationClientID == Client.ID);
                    }

                    if (cbWilaya.SelectedIndex > 0)
                    {
                        var wialaya = db.Wilayas.FirstOrDefault(p => p.Nom.Contains(cbWilaya.Text.Trim()));

                        resultFacture = resultFacture.Where(p => p.Destination == wialaya.Nom);
                    }


                }


                ListFoundFacture = resultFacture.ToList();
            }

            RefreshGrid();
        }
        private void RefreshGrid()
        {

            dgvFacture.DataSource = ListFoundFacture.Where(p => p.Ventes.Count != 0).ToList().Select(p => new
            {
                ID = p.ID,
                Date = p.Date.Value.ToShortDateString(),
                No = p.ID.ToString().PadLeft(4, '0'),
                // BonAttrib = p.BonAttributionID.ToString().PadLeft(4, '0'),
                Depart = (p.Fournisseur != null) ? p.Fournisseur.Nom :
                         ((p.Client != null) ? p.Client.Nom + " " + p.Client.Wilaya.Nom : p.Depo.Nom + " " + p.Depo.Wilaya.Nom),

                Destination = (p.Client1 != null) ? p.Client1.Nom + " " + p.Destination : p.Depo1.Nom + " " + p.Depo1.Wilaya.Nom,
                //Client = 
                Produit = (p.Ventes.First().GoutProduit.Nom + " " + p.Ventes.First().Format.Volume),
                Quantite = ((p.Ventes.Sum(s => s.NombrePalettes)!=0)? p.Ventes.Sum(s => s.NombrePalettes) + " Palettes":"") + " " +
                ((p.Ventes.Sum(s => s.NombreFardeaux) != 0) ? p.Ventes.Sum(s => s.NombreFardeaux) + " Fardeaux" : ""),
                //  Quantite = p.BonAttribution.Ventes.Sum(s => s.TotalFardeaux) + " Fardeaux  " + p.BonAttribution.Ventes.Sum(s => s.TotalBouteilles) + " Bouteilles",
                Montant_Achat = p.MontantTotalAchat.ToAffInt(true),
                Montant_Vente = p.MontantTotalFacture.ToAffInt(true),
                //   Payement = p.ModePayement.ToModePayementTxt(),
                //   Inscription = "par " + p.ApplicationUtilisateur.Employer.Nom + " le " + p.InscriptionTime.Value.ToString("dd/MM/yyyy à hh:mm "),
                UID = p.InscriptionUID,
            }).ToList();

            dgvFacture.HideIDColumn();
            dgvFacture.HideIDColumn("UID");

            UpdateControls();

        }
        private void UpdateControls()
        {
            tcDocument.TabPages.Clear();

            //    if (ListFoundAttribution.Count != 0) tcDocument.TabPages.Add(tpAttribution);
            //    if (ListFoundSortie.Count != 0) tcDocument.TabPages.Add(tpSortie);
            //    if (ListFoundLivraison.Count != 0) tcDocument.TabPages.Add(tpLivraison);
            if (ListFoundFacture.Count != 0) tcDocument.TabPages.Add(tpFacture);

        }



        private void ImprimerFacture()
        {
            var id = dgvFacture.GetSelectedID();
            if (id != null)
            {
                var facture = ListFoundFacture.Single(p => p.ID == id);

                var totalTxt = CtoL.convertMontant(facture.MontantTotalFacture.Value);
                // var remiseTxt = (facture.MontantTotalRemise == 0) ? "" : "(remise comprise)";
                NsTRBsys.NsRapports.FrViewer.PrintFacture(facture.ID, totalTxt, facture.Destination1);

            }
        }


        private void ModifieFacture()
        {


            var id = dgvFacture.GetSelectedID();
            if (id != null)
            {
                new NsTRBsys.NsComercial.FrFactureMO(id).ShowDialog();
            }
        }


        private void AnnulerFacture()
        {
            var id = dgvFacture.GetSelectedID();
            if (id == null)
            { return; }

            var facture = ListFoundFacture.Single(p => p.ID == id);

            if (this.ConfirmWarning("Confirmer suppression Facture N° " + facture.ID + "?"))
            {

                if (facture.Depo != null) FrMain.UpdateStockDepos(db, facture, facture.Depo, true);
                if (facture.Depo1 != null) FrMain.UpdateStockDepos(db, facture, facture.Depo1, true);

                var client1 = facture.Client1;
                var client = facture.Client;
                var fourni = facture.Fournisseur;

                var ventes = facture.Ventes.ToList();

                foreach (var vente in ventes)
                {
                    db.DeleteObject(vente);
                }

                db.DeleteObject(facture);

                db.SaveChanges();


                if (client1 != null) FrMain.GenerateSoldeClient(db, DateTime.Now, client1);
                if (client != null) FrMain.GenerateSoldeClient(db, DateTime.Now, client);
                if (fourni != null) FrMain.GenerateSoldeFourni(db, DateTime.Now, fourni);

                db.SaveChanges();


            }

        }



        private void imprimerBonLivraisonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //      ImprimerLivraison();


        }
        private void imprimerLaFactureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImprimerFacture();


        }
        private void modifierLaFactureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ModifieFacture();
        }
        private void annulerLaFactureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AnnulerFacture();
        }
        private void modifierLivraisonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //   ModifeLivraison();
        }
        private void annulerLivraisonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //      AnnulerLivraison();
            //
        }
        private void modifierBonDeSortieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //     ModifieSortie();
        }
        private void annulerBonDeSortieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //      AnnulerSortie();
        }
        private void cbDocument_SelectedIndexChanged(object sender, EventArgs e)
        {
            /* if (cbDocument.SelectedIndex != 0)
             {
                 txtNoBon.Enabled = true;

             }
             else
             {
                 txtNoBon.Enabled = false;
                 txtNoBon.Text = "";
             }

             gbEtat.Visible = cbDocument.SelectedIndex == 1;
             */
        }
        private void btAnnuler_Click(object sender, EventArgs e)
        {

            if (tcDocument.SelectedTab == tpFacture) AnnulerFacture();

            Recherche();
        }
        private void btImprimer_Click(object sender, EventArgs e)
        {
            btImprimerFact.Enabled = false;


            if (tcDocument.SelectedTab == tpFacture) ImprimerFacture();

            btImprimerFact.Enabled = true;
        }
        private void btModifieBon_Click(object sender, EventArgs e)
        {

            if (tcDocument.SelectedTab == tpFacture) ModifieFacture();

            Recherche();
        }
        private void modifierLeBonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //     ModifieAttribution();


        }
        private void annulerLeBonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //    AnnulerAttribution();


        }
        private void btFermer_Click(object sender, EventArgs e)
        {
            Dispose();
        }
        private void chRechAvancee_CheckedChanged(object sender, EventArgs e)
        {
            panRechAvance.Enabled = chRechAvancee.Checked;
        }
        private void FrListBonAttribution_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }
        private void btRecherche_Click(object sender, EventArgs e)
        {
            Recherche();
            UpdateTotal();

        }
        private void imprimerBonDeSortieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //


        }

        private void FrListBon_Shown(object sender, EventArgs e)
        {
            //Recherche();
        }

        private void tcDocument_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateTotal();
        }

        private void txtClient_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLower(e.KeyChar))
            {
                e.KeyChar = char.ToUpper(e.KeyChar);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            btImprimerFact.Enabled = false;

            //   if (tcDocument.SelectedTab == tpAttribution) ImprimerAttribution();
            //   if (tcDocument.SelectedTab == tpSortie) ImprimerSortie();
            if (tcDocument.SelectedTab == tpFacture) ImprimerLivraison();
            //   if (tcDocument.SelectedTab == tpLivraison) ImprimerLivraison();
            btImprimerFact.Enabled = true;
        }

        private void ImprimerLivraison()
        {
            var id = dgvFacture.GetSelectedID();
            if (id != null)
            {
                var facture = ListFoundFacture.Single(p => p.ID == id);

                //  var totalTxt = CtoL.convertMontant(facture.MontantTotalFacture.Value);
                // var remiseTxt = (facture.MontantTotalRemise == 0) ? "" : "(remise comprise)";
                NsTRBsys.NsRapports.FrViewer.PrintLivraison(facture.ID, (facture.Client1 == null) ? facture.Depo1.Nom : facture.Destination1.NomRaisonSocial);

            }
        }

        private void btCli_Click(object sender, EventArgs e)
        {
            this.Client = NsTRBsys.NsComercial.NsClient.FrList.SelectClientDialog(db);

            if (Client != null)
            {
                txtClient.Text = Client.Nom;
              //  cbWilaya.SelectedItem = Client.Wilaya;
            }
        }

        private void dgvFacture_SelectionChanged(object sender, EventArgs e)
        {
            var id = dgvFacture.GetSelectedID();
            if (id != null)
            {
                var facture = db.Factures.Single(p => p.ID == id);
                btImprimerFact.Visible = facture.DestinationClientID != null;
                if (facture.DestinationClientID == null)
                {
                  //  radNoFake.Checked = true;
                   // radFake.Visible = false;
                }
                else
                {
                  //  radFake.Checked = true;
                  //  radFake.Visible = true;
                }
            }

        }

        public Client Client { get; set; }
    }
}
