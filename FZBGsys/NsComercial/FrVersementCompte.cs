﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NsTRBsys.NsComercial
{
    public partial class FrVersementCompte : Form
    {
        ModelEntities db = new ModelEntities();
        public FrVersementCompte()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Client = NsTRBsys.NsComercial.NsClient.FrList.SelectClientDialog(db, true);
            if (Client != null)
            {
                txtNumCompte.Text = Client.ID.ToString().PadLeft(4, '0');
                txtNomCli.Text = Client.Nom + "   (" + Client.Wilaya.Nom + ")";
                labSolde.Text = (Client.Solde == null) ? "" : Client.Solde.Value.ToAffInt() + " DA";
                if (Client.Solde.Value < 0)
                {
                    labSolde.ForeColor = Color.Red;

                }
                else if (Client.Solde.Value > 0)
                {
                    labSolde.ForeColor = Color.Green;
                }
                else
                {
                    labSolde.ForeColor = Color.Black;
                }

                txtmontantVers.Text = "";
                txtmontantVers.Focus();
            }

        }

        private void txtmontantVers_TextChanged(object sender, EventArgs e)
        {
            if (Client == null)
            {
                return;
            }



            decimal montant = 0;

            if (decimal.TryParse(txtmontantVers.Text.Trim().Replace(" ", "").Trim(), out montant))
            {
                if (Client != null) labNewSolde.Text = (Client.Solde.Value + montant).ToAffInt() + " DA";
                LabelLettreDA.Text = CtoL.convertMontant(Math.Abs(montant));
            }

            else
            {
                labNewSolde.Text = "";
                LabelLettreDA.Text = "";
            }

            if (Client.Solde.Value + montant < 0)
            {
                labNewSolde.ForeColor = Color.Red;
            }
            else if (Client.Solde.Value + montant > 0)
            {
                labNewSolde.ForeColor = Color.Green;
            }
            else
            {
                labNewSolde.ForeColor = Color.Black;
            }

        }

        public Client Client { get; set; }

        private void button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        public bool IsValidAll()
        {
            string ErrorMessage = "";

            if (Client == null)
            {
                ErrorMessage += "Veuillez séléctionner un Client!";
            }

            decimal solde = 0;
            if (txtmontantVers.Text.Trim().Replace(" ", "") != "" && !decimal.TryParse(txtmontantVers.Text.Trim().Replace(" ", ""), out solde))
            {
                ErrorMessage += "\nCe montant est Invalide!!";
            }

            if (ErrorMessage != "")
            {
                Tools.ShowError(ErrorMessage);
                return false;
            }

            return true;

        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (!IsValidAll())
            {
                return;
            }

            var montant = int.Parse(txtmontantVers.Text.Trim().Replace(" ", "").Trim());
            if (this.ConfirmInformation("Confirmer le Reçu de versement du Compte ?\n\nClient: " + Client.Nom + "\n\nVersement: " + montant.ToString("N2")))
            {
                var CurrentDateTime = Tools.GetServerDateTime();
                Operation op = new Operation()
                {
                    Client = Client,
                    Date = dtDate.Value,
                    Montant = montant,
                    TypeOperaiton = (int)EnTypeOperation.Recu_de_Versement,
                    InscriptionDate = CurrentDateTime,
                    InscriptionUID = Tools.CurrentUserID,
                    Sen = "C",
                    Lib = txtObs.Text


                };

                db.AddToOperations(op);
                //   Client.Solde += montant;


              /*  if (Client.SoldeHistory == null)
                {
                    Client.SoldeHistory = 0;
                    Client.DateHistory = Tools.DateDebutOperations;
                }*/

                db.SaveChanges();
                FrMain.GenerateSoldeClient(db, DateTime.Now, Client);
                db.SaveChanges();

                //  db.AddToOperations(op);
                db.Refresh(System.Data.Objects.RefreshMode.StoreWins, Client);


                this.ShowInformation("Versement " + Client.Nom + " éffectué avec succés!!\n\nNouv Solde: " + Client.Solde);
                txtmontantVers.Text = "";
                this.Client = null;
                txtNomCli.Text = "";
                txtNumCompte.Text = "";
                labSolde.Text = "";
                labNewSolde.Text = "";

                //   Dispose();
            }

        }

        private void txtmontantVers_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!Char.IsDigit(ch) && ch != 8 && ch != '-')
            {
                e.Handled = true;
            }
        }

        private void txtNumCompte_Leave(object sender, EventArgs e)
        {
            if (txtNumCompte.Text.Trim() == "")
            {
                return;
            }
            var id = int.Parse(txtNumCompte.Text.Trim());
            this.Client = db.Clients.SingleOrDefault(p => p.ID == id);
            if (Client != null)
            {
                txtNumCompte.Text = Client.ID.ToString().PadLeft(4, '0');
                txtNomCli.Text = Client.Nom;
                labSolde.Text = Client.Solde.Value.ToString("N2");
                txtmontantVers.Text = "";
            }
            else
            {
                Tools.ShowError("Numéro de compte introuvable!");
                txtNumCompte.Focus();
            }
        }
    }
}
