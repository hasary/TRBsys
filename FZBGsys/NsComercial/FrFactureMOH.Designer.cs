﻿namespace NsTRBsys.NsComercial
{
    partial class FrFactureMO
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.btParcouClient = new System.Windows.Forms.Button();
            this.txtNomClient = new System.Windows.Forms.TextBox();
            this.txtMatricule = new System.Windows.Forms.TextBox();
            this.txtNomChauffeur = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbUniteVente = new System.Windows.Forms.ComboBox();
            this.panMontantBouteille = new System.Windows.Forms.Panel();
            this.txtTotalDA = new System.Windows.Forms.TextBox();
            this.labVA = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.panMontantVente = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.labMontantVente = new System.Windows.Forms.Label();
            this.panMontantAchat = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.labMontantAchat = new System.Windows.Forms.Label();
            this.panPrixVente = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPrixVente = new System.Windows.Forms.TextBox();
            this.panPrixAchat = new System.Windows.Forms.Panel();
            this.txtPrixAchat = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labTotal = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbGoutProduit = new System.Windows.Forms.ComboBox();
            this.txtTotalBouteilles = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbFormat = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btAjouter = new System.Windows.Forms.Button();
            this.btEnlever = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btSelectChauffeur = new System.Windows.Forms.Button();
            this.txtVersement = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cbMode = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.labSoldeClient = new System.Windows.Forms.Label();
            this.labSoldeFournisseur = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.btAnnuler = new System.Windows.Forms.Button();
            this.btEnregistrer = new System.Windows.Forms.Button();
            this.cbDepartFournisseur = new System.Windows.Forms.ComboBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.tabCDepart = new System.Windows.Forms.TabControl();
            this.tpDepartFournisseur = new System.Windows.Forms.TabPage();
            this.tpDepartPartenaire = new System.Windows.Forms.TabPage();
            this.cbDepartPartenaire = new System.Windows.Forms.ComboBox();
            this.labSoldePartenaireDepart = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.tpDepartDepo = new System.Windows.Forms.TabPage();
            this.cbDepartDepo = new System.Windows.Forms.ComboBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.tabCDestination = new System.Windows.Forms.TabControl();
            this.tpDestClientPartenaire = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.txtDestination = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tpDestDepo = new System.Windows.Forms.TabPage();
            this.cbDestinationDepo = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.panMontantBouteille.SuspendLayout();
            this.panMontantVente.SuspendLayout();
            this.panMontantAchat.SuspendLayout();
            this.panPrixVente.SuspendLayout();
            this.panPrixAchat.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.tabCDepart.SuspendLayout();
            this.tpDepartFournisseur.SuspendLayout();
            this.tpDepartPartenaire.SuspendLayout();
            this.tpDepartDepo.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tabCDestination.SuspendLayout();
            this.tpDestClientPartenaire.SuspendLayout();
            this.tpDestDepo.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(241, 16);
            this.dgv.Margin = new System.Windows.Forms.Padding(2);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 16;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(505, 183);
            this.dgv.TabIndex = 9;
            // 
            // btParcouClient
            // 
            this.btParcouClient.Image = global::NsTRBsys.Properties.Resources.icon_16_module;
            this.btParcouClient.Location = new System.Drawing.Point(7, 10);
            this.btParcouClient.Name = "btParcouClient";
            this.btParcouClient.Size = new System.Drawing.Size(34, 20);
            this.btParcouClient.TabIndex = 91;
            this.btParcouClient.UseVisualStyleBackColor = true;
            this.btParcouClient.Click += new System.EventHandler(this.btParcouClient_Click);
            // 
            // txtNomClient
            // 
            this.txtNomClient.Location = new System.Drawing.Point(45, 10);
            this.txtNomClient.Margin = new System.Windows.Forms.Padding(2);
            this.txtNomClient.Name = "txtNomClient";
            this.txtNomClient.ReadOnly = true;
            this.txtNomClient.Size = new System.Drawing.Size(210, 20);
            this.txtNomClient.TabIndex = 90;
            this.txtNomClient.TextChanged += new System.EventHandler(this.txtClient_TextChanged_1);
            // 
            // txtMatricule
            // 
            this.txtMatricule.Location = new System.Drawing.Point(103, 47);
            this.txtMatricule.Name = "txtMatricule";
            this.txtMatricule.ReadOnly = true;
            this.txtMatricule.Size = new System.Drawing.Size(116, 20);
            this.txtMatricule.TabIndex = 87;
            // 
            // txtNomChauffeur
            // 
            this.txtNomChauffeur.Location = new System.Drawing.Point(49, 23);
            this.txtNomChauffeur.Name = "txtNomChauffeur";
            this.txtNomChauffeur.ReadOnly = true;
            this.txtNomChauffeur.Size = new System.Drawing.Size(170, 20);
            this.txtNomChauffeur.TabIndex = 88;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(46, 50);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 13);
            this.label9.TabIndex = 83;
            this.label9.Text = "Matricule:";
            // 
            // dtDate
            // 
            this.dtDate.CalendarForeColor = System.Drawing.SystemColors.HotTrack;
            this.dtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtDate.Location = new System.Drawing.Point(577, 11);
            this.dtDate.Margin = new System.Windows.Forms.Padding(2);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(194, 19);
            this.dtDate.TabIndex = 24;
            this.dtDate.ValueChanged += new System.EventHandler(this.dtDate_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(541, 13);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 25;
            this.label2.Text = "Date:";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBox2.Controls.Add(this.cbUniteVente);
            this.groupBox2.Controls.Add(this.panMontantBouteille);
            this.groupBox2.Controls.Add(this.panMontantVente);
            this.groupBox2.Controls.Add(this.panMontantAchat);
            this.groupBox2.Controls.Add(this.panPrixVente);
            this.groupBox2.Controls.Add(this.panPrixAchat);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.labTotal);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.cbGoutProduit);
            this.groupBox2.Controls.Add(this.txtTotalBouteilles);
            this.groupBox2.Controls.Add(this.txtNombre);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.cbFormat);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.dgv);
            this.groupBox2.Controls.Add(this.btAjouter);
            this.groupBox2.Controls.Add(this.btEnlever);
            this.groupBox2.Location = new System.Drawing.Point(11, 168);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(759, 246);
            this.groupBox2.TabIndex = 26;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Produit";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // cbUniteVente
            // 
            this.cbUniteVente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbUniteVente.FormattingEnabled = true;
            this.cbUniteVente.Items.AddRange(new object[] {
            "",
            "Palettes",
            "Fardeaux"});
            this.cbUniteVente.Location = new System.Drawing.Point(144, 105);
            this.cbUniteVente.Name = "cbUniteVente";
            this.cbUniteVente.Size = new System.Drawing.Size(87, 21);
            this.cbUniteVente.TabIndex = 102;
            this.cbUniteVente.SelectedIndexChanged += new System.EventHandler(this.cbUniteVente_SelectedIndexChanged);
            // 
            // panMontantBouteille
            // 
            this.panMontantBouteille.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panMontantBouteille.Controls.Add(this.txtTotalDA);
            this.panMontantBouteille.Controls.Add(this.labVA);
            this.panMontantBouteille.Controls.Add(this.label15);
            this.panMontantBouteille.Location = new System.Drawing.Point(10, 187);
            this.panMontantBouteille.Margin = new System.Windows.Forms.Padding(2);
            this.panMontantBouteille.Name = "panMontantBouteille";
            this.panMontantBouteille.Size = new System.Drawing.Size(200, 24);
            this.panMontantBouteille.TabIndex = 101;
            // 
            // txtTotalDA
            // 
            this.txtTotalDA.Location = new System.Drawing.Point(66, 3);
            this.txtTotalDA.Margin = new System.Windows.Forms.Padding(2);
            this.txtTotalDA.Name = "txtTotalDA";
            this.txtTotalDA.ReadOnly = true;
            this.txtTotalDA.Size = new System.Drawing.Size(64, 20);
            this.txtTotalDA.TabIndex = 51;
            this.txtTotalDA.TextChanged += new System.EventHandler(this.txtNombre_TextChanged);
            this.txtTotalDA.Enter += new System.EventHandler(this.txtNombre_Enter);
            this.txtTotalDA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNombre_KeyPress);
            // 
            // labVA
            // 
            this.labVA.AutoSize = true;
            this.labVA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labVA.Location = new System.Drawing.Point(141, 6);
            this.labVA.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labVA.Name = "labVA";
            this.labVA.Size = new System.Drawing.Size(24, 13);
            this.labVA.TabIndex = 2;
            this.labVA.Text = "DA";
            this.labVA.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(10, 5);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 13);
            this.label15.TabIndex = 100;
            this.label15.Text = "Montant";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panMontantVente
            // 
            this.panMontantVente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panMontantVente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panMontantVente.Controls.Add(this.label22);
            this.panMontantVente.Controls.Add(this.labMontantVente);
            this.panMontantVente.Location = new System.Drawing.Point(575, 217);
            this.panMontantVente.Margin = new System.Windows.Forms.Padding(2);
            this.panMontantVente.Name = "panMontantVente";
            this.panMontantVente.Size = new System.Drawing.Size(170, 25);
            this.panMontantVente.TabIndex = 101;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(4, 5);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(94, 13);
            this.label22.TabIndex = 99;
            this.label22.Text = "Montant Vente:";
            // 
            // labMontantVente
            // 
            this.labMontantVente.AutoSize = true;
            this.labMontantVente.Location = new System.Drawing.Point(96, 5);
            this.labMontantVente.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labMontantVente.Name = "labMontantVente";
            this.labMontantVente.Size = new System.Drawing.Size(34, 13);
            this.labMontantVente.TabIndex = 96;
            this.labMontantVente.Text = "---------";
            this.labMontantVente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panMontantAchat
            // 
            this.panMontantAchat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panMontantAchat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panMontantAchat.Controls.Add(this.label19);
            this.panMontantAchat.Controls.Add(this.labMontantAchat);
            this.panMontantAchat.Location = new System.Drawing.Point(395, 219);
            this.panMontantAchat.Margin = new System.Windows.Forms.Padding(2);
            this.panMontantAchat.Name = "panMontantAchat";
            this.panMontantAchat.Size = new System.Drawing.Size(175, 24);
            this.panMontantAchat.TabIndex = 101;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(9, 5);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(94, 13);
            this.label19.TabIndex = 95;
            this.label19.Text = "Montant Achat:";
            // 
            // labMontantAchat
            // 
            this.labMontantAchat.AutoSize = true;
            this.labMontantAchat.Location = new System.Drawing.Point(100, 6);
            this.labMontantAchat.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labMontantAchat.Name = "labMontantAchat";
            this.labMontantAchat.Size = new System.Drawing.Size(34, 13);
            this.labMontantAchat.TabIndex = 96;
            this.labMontantAchat.Text = "---------";
            this.labMontantAchat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panPrixVente
            // 
            this.panPrixVente.Controls.Add(this.label14);
            this.panPrixVente.Controls.Add(this.label7);
            this.panPrixVente.Controls.Add(this.txtPrixVente);
            this.panPrixVente.Location = new System.Drawing.Point(2, 129);
            this.panPrixVente.Margin = new System.Windows.Forms.Padding(2);
            this.panPrixVente.Name = "panPrixVente";
            this.panPrixVente.Size = new System.Drawing.Size(198, 28);
            this.panPrixVente.TabIndex = 98;
            this.panPrixVente.VisibleChanged += new System.EventHandler(this.panPrixVente_VisibleChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(142, 6);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(58, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "DA / Unite";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 6);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Prix vente:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtPrixVente
            // 
            this.txtPrixVente.Location = new System.Drawing.Point(72, 4);
            this.txtPrixVente.Margin = new System.Windows.Forms.Padding(2);
            this.txtPrixVente.Name = "txtPrixVente";
            this.txtPrixVente.Size = new System.Drawing.Size(64, 20);
            this.txtPrixVente.TabIndex = 51;
            this.txtPrixVente.TextChanged += new System.EventHandler(this.txtNombre_TextChanged);
            this.txtPrixVente.Enter += new System.EventHandler(this.txtNombre_Enter);
            this.txtPrixVente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNombre_KeyPress);
            // 
            // panPrixAchat
            // 
            this.panPrixAchat.Controls.Add(this.txtPrixAchat);
            this.panPrixAchat.Controls.Add(this.label6);
            this.panPrixAchat.Controls.Add(this.label10);
            this.panPrixAchat.Location = new System.Drawing.Point(5, 71);
            this.panPrixAchat.Margin = new System.Windows.Forms.Padding(2);
            this.panPrixAchat.Name = "panPrixAchat";
            this.panPrixAchat.Size = new System.Drawing.Size(202, 28);
            this.panPrixAchat.TabIndex = 97;
            this.panPrixAchat.VisibleChanged += new System.EventHandler(this.panPrixAchat_VisibleChanged);
            // 
            // txtPrixAchat
            // 
            this.txtPrixAchat.Location = new System.Drawing.Point(69, 4);
            this.txtPrixAchat.Margin = new System.Windows.Forms.Padding(2);
            this.txtPrixAchat.Name = "txtPrixAchat";
            this.txtPrixAchat.Size = new System.Drawing.Size(64, 20);
            this.txtPrixAchat.TabIndex = 51;
            this.txtPrixAchat.TextChanged += new System.EventHandler(this.txtNombre_TextChanged);
            this.txtPrixAchat.Enter += new System.EventHandler(this.txtNombre_Enter);
            this.txtPrixAchat.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNombre_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 6);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Prix Achat:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(136, 6);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(58, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "DA / Unite";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(402, 204);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 28;
            this.label1.Text = "Total:";
            // 
            // labTotal
            // 
            this.labTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labTotal.AutoSize = true;
            this.labTotal.Location = new System.Drawing.Point(449, 204);
            this.labTotal.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labTotal.Name = "labTotal";
            this.labTotal.Size = new System.Drawing.Size(34, 13);
            this.labTotal.TabIndex = 29;
            this.labTotal.Text = "---------";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 21);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Gôut:";
            // 
            // cbGoutProduit
            // 
            this.cbGoutProduit.FormattingEnabled = true;
            this.cbGoutProduit.Location = new System.Drawing.Point(74, 17);
            this.cbGoutProduit.Margin = new System.Windows.Forms.Padding(2);
            this.cbGoutProduit.Name = "cbGoutProduit";
            this.cbGoutProduit.Size = new System.Drawing.Size(157, 21);
            this.cbGoutProduit.TabIndex = 1;
            this.cbGoutProduit.SelectedIndexChanged += new System.EventHandler(this.cbGoutProduit_SelectedIndexChanged);
            // 
            // txtTotalBouteilles
            // 
            this.txtTotalBouteilles.Location = new System.Drawing.Point(76, 163);
            this.txtTotalBouteilles.Margin = new System.Windows.Forms.Padding(2);
            this.txtTotalBouteilles.Name = "txtTotalBouteilles";
            this.txtTotalBouteilles.ReadOnly = true;
            this.txtTotalBouteilles.Size = new System.Drawing.Size(64, 20);
            this.txtTotalBouteilles.TabIndex = 51;
            this.txtTotalBouteilles.TextChanged += new System.EventHandler(this.txtNombre_TextChanged);
            this.txtTotalBouteilles.Enter += new System.EventHandler(this.txtNombre_Enter);
            this.txtTotalBouteilles.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNombre_KeyPress);
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(74, 107);
            this.txtNombre.Margin = new System.Windows.Forms.Padding(2);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(64, 20);
            this.txtNombre.TabIndex = 50;
            this.txtNombre.TextChanged += new System.EventHandler(this.txtNombre_TextChanged);
            this.txtNombre.Enter += new System.EventHandler(this.txtNombre_Enter);
            this.txtNombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNombre_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(143, 111);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(45, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Palettes";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(151, 167);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "Bouteilles";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(30, 163);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(36, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Total";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 111);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Quantite:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbFormat
            // 
            this.cbFormat.FormattingEnabled = true;
            this.cbFormat.Location = new System.Drawing.Point(74, 45);
            this.cbFormat.Margin = new System.Windows.Forms.Padding(2);
            this.cbFormat.Name = "cbFormat";
            this.cbFormat.Size = new System.Drawing.Size(76, 21);
            this.cbFormat.TabIndex = 1;
            this.cbFormat.SelectedIndexChanged += new System.EventHandler(this.cbFormat_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 47);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Format:";
            // 
            // btAjouter
            // 
            this.btAjouter.Image = global::NsTRBsys.Properties.Resources.icon_16_new1;
            this.btAjouter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btAjouter.Location = new System.Drawing.Point(135, 217);
            this.btAjouter.Margin = new System.Windows.Forms.Padding(2);
            this.btAjouter.Name = "btAjouter";
            this.btAjouter.Size = new System.Drawing.Size(100, 25);
            this.btAjouter.TabIndex = 20;
            this.btAjouter.Text = "Ajouter >>";
            this.btAjouter.UseVisualStyleBackColor = true;
            this.btAjouter.Click += new System.EventHandler(this.btAjouter_Click);
            // 
            // btEnlever
            // 
            this.btEnlever.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btEnlever.Image = global::NsTRBsys.Properties.Resources.icon_16_denyinactive;
            this.btEnlever.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btEnlever.Location = new System.Drawing.Point(241, 217);
            this.btEnlever.Margin = new System.Windows.Forms.Padding(2);
            this.btEnlever.Name = "btEnlever";
            this.btEnlever.Size = new System.Drawing.Size(107, 25);
            this.btEnlever.TabIndex = 21;
            this.btEnlever.Text = "<< Enlever";
            this.btEnlever.UseVisualStyleBackColor = true;
            this.btEnlever.Click += new System.EventHandler(this.btEnlever_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtNomChauffeur);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.txtMatricule);
            this.groupBox3.Controls.Add(this.btSelectChauffeur);
            this.groupBox3.Location = new System.Drawing.Point(539, 39);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox3.Size = new System.Drawing.Size(231, 124);
            this.groupBox3.TabIndex = 89;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Transport";
            // 
            // btSelectChauffeur
            // 
            this.btSelectChauffeur.Image = global::NsTRBsys.Properties.Resources.icon_16_module1;
            this.btSelectChauffeur.Location = new System.Drawing.Point(9, 21);
            this.btSelectChauffeur.Name = "btSelectChauffeur";
            this.btSelectChauffeur.Size = new System.Drawing.Size(34, 23);
            this.btSelectChauffeur.TabIndex = 86;
            this.btSelectChauffeur.UseVisualStyleBackColor = true;
            this.btSelectChauffeur.Click += new System.EventHandler(this.btSelectChauffeur_Click);
            // 
            // txtVersement
            // 
            this.txtVersement.Location = new System.Drawing.Point(559, 504);
            this.txtVersement.Margin = new System.Windows.Forms.Padding(2);
            this.txtVersement.Name = "txtVersement";
            this.txtVersement.Size = new System.Drawing.Size(108, 20);
            this.txtVersement.TabIndex = 91;
            this.txtVersement.Visible = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(394, 506);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(37, 13);
            this.label16.TabIndex = 92;
            this.label16.Text = "Mode:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label16.Visible = false;
            // 
            // cbMode
            // 
            this.cbMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMode.FormattingEnabled = true;
            this.cbMode.Items.AddRange(new object[] {
            "",
            "Cache",
            "Versement",
            "sur Avance",
            "à Terme"});
            this.cbMode.Location = new System.Drawing.Point(442, 504);
            this.cbMode.Margin = new System.Windows.Forms.Padding(2);
            this.cbMode.Name = "cbMode";
            this.cbMode.Size = new System.Drawing.Size(109, 21);
            this.cbMode.TabIndex = 93;
            this.cbMode.Visible = false;
            this.cbMode.SelectedIndexChanged += new System.EventHandler(this.cbMode_SelectedIndexChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(673, 505);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(22, 13);
            this.label17.TabIndex = 92;
            this.label17.Text = "DA";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label17.Visible = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(43, 65);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(37, 13);
            this.label18.TabIndex = 94;
            this.label18.Text = "Solde:";
            // 
            // labSoldeClient
            // 
            this.labSoldeClient.AutoSize = true;
            this.labSoldeClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labSoldeClient.ForeColor = System.Drawing.Color.Black;
            this.labSoldeClient.Location = new System.Drawing.Point(86, 65);
            this.labSoldeClient.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labSoldeClient.Name = "labSoldeClient";
            this.labSoldeClient.Size = new System.Drawing.Size(14, 13);
            this.labSoldeClient.TabIndex = 94;
            this.labSoldeClient.Text = "0";
            this.labSoldeClient.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labSoldeFournisseur
            // 
            this.labSoldeFournisseur.AutoSize = true;
            this.labSoldeFournisseur.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labSoldeFournisseur.Location = new System.Drawing.Point(105, 50);
            this.labSoldeFournisseur.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labSoldeFournisseur.Name = "labSoldeFournisseur";
            this.labSoldeFournisseur.Size = new System.Drawing.Size(14, 13);
            this.labSoldeFournisseur.TabIndex = 41;
            this.labSoldeFournisseur.Text = "0";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(7, 50);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(94, 13);
            this.label20.TabIndex = 9;
            this.label20.Text = "Solde Fournisseur:";
            // 
            // btAnnuler
            // 
            this.btAnnuler.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btAnnuler.Image = global::NsTRBsys.Properties.Resources.icon_16_deny;
            this.btAnnuler.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btAnnuler.Location = new System.Drawing.Point(548, 442);
            this.btAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(111, 27);
            this.btAnnuler.TabIndex = 4;
            this.btAnnuler.Text = "Annuler";
            this.btAnnuler.UseVisualStyleBackColor = true;
            this.btAnnuler.Click += new System.EventHandler(this.btAnnuler_Click);
            // 
            // btEnregistrer
            // 
            this.btEnregistrer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btEnregistrer.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEnregistrer.Image = global::NsTRBsys.Properties.Resources.icon_16_allow;
            this.btEnregistrer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btEnregistrer.Location = new System.Drawing.Point(664, 442);
            this.btEnregistrer.Margin = new System.Windows.Forms.Padding(2);
            this.btEnregistrer.Name = "btEnregistrer";
            this.btEnregistrer.Size = new System.Drawing.Size(106, 27);
            this.btEnregistrer.TabIndex = 3;
            this.btEnregistrer.Text = "Enregistrer";
            this.btEnregistrer.UseVisualStyleBackColor = true;
            this.btEnregistrer.Click += new System.EventHandler(this.btEnregistrer_Click);
            // 
            // cbDepartFournisseur
            // 
            this.cbDepartFournisseur.FormattingEnabled = true;
            this.cbDepartFournisseur.Location = new System.Drawing.Point(5, 15);
            this.cbDepartFournisseur.Margin = new System.Windows.Forms.Padding(2);
            this.cbDepartFournisseur.Name = "cbDepartFournisseur";
            this.cbDepartFournisseur.Size = new System.Drawing.Size(200, 21);
            this.cbDepartFournisseur.TabIndex = 42;
            this.cbDepartFournisseur.SelectedIndexChanged += new System.EventHandler(this.cbFournisseur_SelectedIndexChanged);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.tabCDepart);
            this.groupBox7.Location = new System.Drawing.Point(7, 11);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(237, 152);
            this.groupBox7.TabIndex = 99;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Départ";
            // 
            // tabCDepart
            // 
            this.tabCDepart.Controls.Add(this.tpDepartFournisseur);
            this.tabCDepart.Controls.Add(this.tpDepartPartenaire);
            this.tabCDepart.Controls.Add(this.tpDepartDepo);
            this.tabCDepart.Location = new System.Drawing.Point(6, 20);
            this.tabCDepart.Name = "tabCDepart";
            this.tabCDepart.SelectedIndex = 0;
            this.tabCDepart.Size = new System.Drawing.Size(225, 118);
            this.tabCDepart.TabIndex = 0;
            this.tabCDepart.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tpDepartFournisseur
            // 
            this.tpDepartFournisseur.Controls.Add(this.cbDepartFournisseur);
            this.tpDepartFournisseur.Controls.Add(this.labSoldeFournisseur);
            this.tpDepartFournisseur.Controls.Add(this.label20);
            this.tpDepartFournisseur.Location = new System.Drawing.Point(4, 22);
            this.tpDepartFournisseur.Name = "tpDepartFournisseur";
            this.tpDepartFournisseur.Padding = new System.Windows.Forms.Padding(3);
            this.tpDepartFournisseur.Size = new System.Drawing.Size(217, 92);
            this.tpDepartFournisseur.TabIndex = 0;
            this.tpDepartFournisseur.Text = "Fournisseur";
            this.tpDepartFournisseur.UseVisualStyleBackColor = true;
            // 
            // tpDepartPartenaire
            // 
            this.tpDepartPartenaire.Controls.Add(this.cbDepartPartenaire);
            this.tpDepartPartenaire.Controls.Add(this.labSoldePartenaireDepart);
            this.tpDepartPartenaire.Controls.Add(this.label21);
            this.tpDepartPartenaire.Location = new System.Drawing.Point(4, 22);
            this.tpDepartPartenaire.Margin = new System.Windows.Forms.Padding(2);
            this.tpDepartPartenaire.Name = "tpDepartPartenaire";
            this.tpDepartPartenaire.Padding = new System.Windows.Forms.Padding(2);
            this.tpDepartPartenaire.Size = new System.Drawing.Size(217, 92);
            this.tpDepartPartenaire.TabIndex = 2;
            this.tpDepartPartenaire.Text = "Partenaire";
            this.tpDepartPartenaire.UseVisualStyleBackColor = true;
            // 
            // cbDepartPartenaire
            // 
            this.cbDepartPartenaire.FormattingEnabled = true;
            this.cbDepartPartenaire.Location = new System.Drawing.Point(9, 11);
            this.cbDepartPartenaire.Margin = new System.Windows.Forms.Padding(2);
            this.cbDepartPartenaire.Name = "cbDepartPartenaire";
            this.cbDepartPartenaire.Size = new System.Drawing.Size(198, 21);
            this.cbDepartPartenaire.TabIndex = 97;
            this.cbDepartPartenaire.SelectedIndexChanged += new System.EventHandler(this.cbPartenaire_SelectedIndexChanged);
            // 
            // labSoldePartenaireDepart
            // 
            this.labSoldePartenaireDepart.AutoSize = true;
            this.labSoldePartenaireDepart.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labSoldePartenaireDepart.ForeColor = System.Drawing.Color.Black;
            this.labSoldePartenaireDepart.Location = new System.Drawing.Point(111, 48);
            this.labSoldePartenaireDepart.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labSoldePartenaireDepart.Name = "labSoldePartenaireDepart";
            this.labSoldePartenaireDepart.Size = new System.Drawing.Size(14, 13);
            this.labSoldePartenaireDepart.TabIndex = 95;
            this.labSoldePartenaireDepart.Text = "0";
            this.labSoldePartenaireDepart.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(8, 48);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(88, 13);
            this.label21.TabIndex = 96;
            this.label21.Text = "Solde Partenaire:";
            // 
            // tpDepartDepo
            // 
            this.tpDepartDepo.Controls.Add(this.cbDepartDepo);
            this.tpDepartDepo.Location = new System.Drawing.Point(4, 22);
            this.tpDepartDepo.Name = "tpDepartDepo";
            this.tpDepartDepo.Padding = new System.Windows.Forms.Padding(3);
            this.tpDepartDepo.Size = new System.Drawing.Size(217, 92);
            this.tpDepartDepo.TabIndex = 1;
            this.tpDepartDepo.Text = "Mes Dépos";
            this.tpDepartDepo.UseVisualStyleBackColor = true;
            // 
            // cbDepartDepo
            // 
            this.cbDepartDepo.FormattingEnabled = true;
            this.cbDepartDepo.Location = new System.Drawing.Point(5, 13);
            this.cbDepartDepo.Margin = new System.Windows.Forms.Padding(2);
            this.cbDepartDepo.Name = "cbDepartDepo";
            this.cbDepartDepo.Size = new System.Drawing.Size(200, 21);
            this.cbDepartDepo.TabIndex = 43;
            this.cbDepartDepo.SelectedIndexChanged += new System.EventHandler(this.cbDepartDepo_SelectedIndexChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.tabCDestination);
            this.groupBox6.Location = new System.Drawing.Point(252, 11);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(282, 152);
            this.groupBox6.TabIndex = 100;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Déstination";
            // 
            // tabCDestination
            // 
            this.tabCDestination.Controls.Add(this.tpDestClientPartenaire);
            this.tabCDestination.Controls.Add(this.tpDestDepo);
            this.tabCDestination.Location = new System.Drawing.Point(6, 20);
            this.tabCDestination.Name = "tabCDestination";
            this.tabCDestination.SelectedIndex = 0;
            this.tabCDestination.Size = new System.Drawing.Size(270, 118);
            this.tabCDestination.TabIndex = 0;
            this.tabCDestination.SelectedIndexChanged += new System.EventHandler(this.tabControl2_SelectedIndexChanged);
            // 
            // tpDestClientPartenaire
            // 
            this.tpDestClientPartenaire.Controls.Add(this.button1);
            this.tpDestClientPartenaire.Controls.Add(this.txtDestination);
            this.tpDestClientPartenaire.Controls.Add(this.btParcouClient);
            this.tpDestClientPartenaire.Controls.Add(this.txtNomClient);
            this.tpDestClientPartenaire.Controls.Add(this.labSoldeClient);
            this.tpDestClientPartenaire.Controls.Add(this.label8);
            this.tpDestClientPartenaire.Controls.Add(this.label18);
            this.tpDestClientPartenaire.Location = new System.Drawing.Point(4, 22);
            this.tpDestClientPartenaire.Name = "tpDestClientPartenaire";
            this.tpDestClientPartenaire.Padding = new System.Windows.Forms.Padding(3);
            this.tpDestClientPartenaire.Size = new System.Drawing.Size(262, 92);
            this.tpDestClientPartenaire.TabIndex = 0;
            this.tpDestClientPartenaire.Text = "Client / Partenaire";
            this.tpDestClientPartenaire.UseVisualStyleBackColor = true;
            this.tpDestClientPartenaire.Click += new System.EventHandler(this.tpDestClientPartenaire_Click);
            // 
            // button1
            // 
            this.button1.Image = global::NsTRBsys.Properties.Resources.icon_16_module;
            this.button1.Location = new System.Drawing.Point(220, 37);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(34, 19);
            this.button1.TabIndex = 96;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtDestination
            // 
            this.txtDestination.Location = new System.Drawing.Point(128, 38);
            this.txtDestination.Margin = new System.Windows.Forms.Padding(2);
            this.txtDestination.Name = "txtDestination";
            this.txtDestination.ReadOnly = true;
            this.txtDestination.Size = new System.Drawing.Size(89, 20);
            this.txtDestination.TabIndex = 95;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(5, 39);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(120, 13);
            this.label8.TabIndex = 94;
            this.label8.Text = "Coordoné / Destination:";
            // 
            // tpDestDepo
            // 
            this.tpDestDepo.Controls.Add(this.cbDestinationDepo);
            this.tpDestDepo.Location = new System.Drawing.Point(4, 22);
            this.tpDestDepo.Name = "tpDestDepo";
            this.tpDestDepo.Padding = new System.Windows.Forms.Padding(3);
            this.tpDestDepo.Size = new System.Drawing.Size(262, 92);
            this.tpDestDepo.TabIndex = 1;
            this.tpDestDepo.Text = "Mes Dépos";
            this.tpDestDepo.UseVisualStyleBackColor = true;
            // 
            // cbDestinationDepo
            // 
            this.cbDestinationDepo.FormattingEnabled = true;
            this.cbDestinationDepo.Location = new System.Drawing.Point(11, 15);
            this.cbDestinationDepo.Margin = new System.Windows.Forms.Padding(2);
            this.cbDestinationDepo.Name = "cbDestinationDepo";
            this.cbDestinationDepo.Size = new System.Drawing.Size(224, 21);
            this.cbDestinationDepo.TabIndex = 43;
            this.cbDestinationDepo.SelectedIndexChanged += new System.EventHandler(this.cbDestDepo_SelectedIndexChanged);
            // 
            // FrFactureMO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 488);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.btAnnuler);
            this.Controls.Add(this.cbMode);
            this.Controls.Add(this.btEnregistrer);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtVersement);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtDate);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrFactureMO";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Livraison ou Récéption";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrBonAttribution_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panMontantBouteille.ResumeLayout(false);
            this.panMontantBouteille.PerformLayout();
            this.panMontantVente.ResumeLayout(false);
            this.panMontantVente.PerformLayout();
            this.panMontantAchat.ResumeLayout(false);
            this.panMontantAchat.PerformLayout();
            this.panPrixVente.ResumeLayout(false);
            this.panPrixVente.PerformLayout();
            this.panPrixAchat.ResumeLayout(false);
            this.panPrixAchat.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.tabCDepart.ResumeLayout(false);
            this.tpDepartFournisseur.ResumeLayout(false);
            this.tpDepartFournisseur.PerformLayout();
            this.tpDepartPartenaire.ResumeLayout(false);
            this.tpDepartPartenaire.PerformLayout();
            this.tpDepartDepo.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.tabCDestination.ResumeLayout(false);
            this.tpDestClientPartenaire.ResumeLayout(false);
            this.tpDestClientPartenaire.PerformLayout();
            this.tpDestDepo.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Button btAnnuler;
        private System.Windows.Forms.Button btEnregistrer;
        private System.Windows.Forms.Button btAjouter;
        private System.Windows.Forms.Button btEnlever;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbGoutProduit;
        private System.Windows.Forms.ComboBox cbFormat;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labTotal;
        private System.Windows.Forms.TextBox txtPrixVente;
        private System.Windows.Forms.Label labVA;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtMatricule;
        private System.Windows.Forms.TextBox txtNomChauffeur;
        private System.Windows.Forms.Button btSelectChauffeur;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtNomClient;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btParcouClient;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtVersement;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cbMode;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label labSoldeClient;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label labMontantAchat;
        private System.Windows.Forms.Label labSoldeFournisseur;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox cbDepartFournisseur;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox txtPrixAchat;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabControl tabCDepart;
        private System.Windows.Forms.TabPage tpDepartFournisseur;
        private System.Windows.Forms.TabPage tpDepartDepo;
        private System.Windows.Forms.ComboBox cbDepartDepo;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TabControl tabCDestination;
        private System.Windows.Forms.TabPage tpDestClientPartenaire;
        private System.Windows.Forms.TabPage tpDestDepo;
        private System.Windows.Forms.ComboBox cbDestinationDepo;
        private System.Windows.Forms.TextBox txtTotalDA;
        private System.Windows.Forms.TextBox txtTotalBouteilles;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TabPage tpDepartPartenaire;
        private System.Windows.Forms.ComboBox cbDepartPartenaire;
        private System.Windows.Forms.Label labSoldePartenaireDepart;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panPrixAchat;
        private System.Windows.Forms.Panel panPrixVente;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label labMontantVente;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panMontantVente;
        private System.Windows.Forms.Panel panMontantAchat;
        private System.Windows.Forms.Panel panMontantBouteille;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtDestination;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox cbUniteVente;
    }
}