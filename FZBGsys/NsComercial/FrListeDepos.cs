﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NsTRBsys.NsComercial
{
    public partial class FrListeDepos : Form
    {
        public FrListeDepos()
        {
            InitializeComponent();
            RefreshGrid();
        }

        private void RefreshGrid()
        {
            using (var db = new ModelEntities())
            {
                dgv.DataSource = db.Depoes.Select(p => new
                {
                    ID = p.ID,
                    Nom = p.Nom,
                    Adresse = p.Adresse,
                    Wilaya = p.Wilaya.Nom,
                    Responsable = p.AgentRespensable,
                    Tel = p.NoTelephone,
                    Etat = p.publish.Value ? "Actif" : "Désactivé",


                }).ToList();
            }
        }

        private void btNouv_Click(object sender, EventArgs e)
        {
            new FrNEDepos().ShowDialog();
            RefreshGrid();
        }

        private void btmodif_Click(object sender, EventArgs e)
        {
            var id = dgv.GetSelectedID();
            if (id != null)
            {
                new FrNEDepos(id).ShowDialog();
                RefreshGrid();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var id = dgv.GetSelectedID();
            if (id != null)
            {
                using (var db =  new ModelEntities())
                {
                    var toDel = db.Depoes.Single(p => p.ID == id);

                    if (toDel.Factures.Count != 0 || toDel.Factures1.Count != 0)
                    {
                        Tools.ShowError("impossible de supprimer dépos, veillez le désactiver si vous voulez le supprimer");
                    }
                    else 
                    {
                        var Stock = toDel.StockProduits.ToList();
                        foreach (var item in Stock)
                        {
                            db.DeleteObject(item);
                        }

                        db.DeleteObject(toDel);

                        db.SaveChangeTry();
                        RefreshGrid();
                    }
                }
            }
        }
    }
}
