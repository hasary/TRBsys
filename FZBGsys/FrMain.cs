﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using System.Diagnostics;
using System.Net.Mail;
using System.Net;
using System.Deployment.Application;
using System.Threading;

namespace NsTRBsys
{
    public partial class FrMain : Form
    {
        public ApplicationUtilisateur CurrentUser { get; set; }
        private List<string> ActionsAutorisees = null;

        public int UserID
        {
            set
            {

                InitializeUser(value);
                Tools.CurrentUserID = value;
                UpdateSoleStat();
            }

        }

        private void InitializeUser(int userID)
        {

            using (ModelEntities db = new ModelEntities())
            {
                this.CurrentUser = db.ApplicationUtilisateurs.SingleOrDefault(u => u.ID == userID);

                if (CurrentUser == null) // user annonyme no connexion
                {
                    groupBoxInfos.Visible = false;
                    return;
                }

                Tools.isBoss = (bool?)CurrentUser.ApplicationGroupe.IsBoss;

                labelUserName.Text = CurrentUser.UserName;
                labeluserRole.Text = "";
                labelUserSection.Text = "";//;;CurrentUser.Employer.Section.Description;
                groupBoxInfos.Visible = true;


                /*
                                if (CurrentUser.GroupeID == 1)
                                {  // Admin Got All Actions
                                    EnableAllActions();
                                    return;

                                }
                */

                this.ActionsAutorisees =
            CurrentUser.ApplicationGroupe.ApplicationAutorisations.Select(a => a.ApplicationAction.Nom).ToList();

                this.ActionsAutorisees =
                 CurrentUser.ApplicationGroupe.ApplicationAutorisations.Where(a => a.Autorisation).Select(a => a.ApplicationAction.Nom).ToList();

                // Autoriser le Groupe 0 (annonyme)
                /*  this.ActionsAutorisees.AddRange(
                     db.ApplicationGroupes.Single(g => g.ID == 0).ApplicationAutorisations.Where(a => a.Autorisation == true).Select(a => a.ApplicationAction.Nom).ToList()
                     );

  */


                ActionsAutorisees = this.ActionsAutorisees.Distinct().ToList();
                #region Refuse
                /*
                                this.ActionsRefusees =
                                 CurrentUser.ApplicationGroupe.ApplicationAutorisations.Where(a => a.Autorisation == false).Select(a => a.ApplicationAction.Nom).ToList();

                                // Refuser le Groupe 0 (annonyme)
                                this.ActionsRefusees.AddRange(
                                    db.ApplicationGroupes.Single(g => g.ID == 0).ApplicationAutorisations.Where(a => a.Autorisation == false).Select(a => a.ApplicationAction.Nom).ToList()
                                   );
                                */

                #endregion

                if (userID != null && userID > 0)
                {
                    flw.Visible = true;

                }
            }



            foreach (ToolStripMenuItem item in menuMain.Items)
            {
                processMenuItems(item);
            }

            menuMain.Enabled = true;

        }

        private void processMenuItems(ToolStripMenuItem item)
        {
            if (item.Name == "Fichier" || item.Name == "Aide")
            {

                item.Visible = true;
                item.Enabled = true;
                return;
            }

            if (ActionsAutorisees == null)
            {

                return;
            }

            if (ActionsAutorisees.Contains(item.Name))
            {
                item.Visible = true;
                item.Enabled = true;
                foreach (ToolStripItem subitem in item.DropDownItems)
                {
                    if (subitem is ToolStripMenuItem)
                    {
                        //processMenuItems((ToolStripMenuItem)subitem);
                        if (ActionsAutorisees.Contains(subitem.Name))
                        {
                            subitem.Visible = true;
                            subitem.Enabled = true;
                        }
                        else
                        {
                            subitem.Visible = false;
                            subitem.Enabled = false;

                        }
                    }
                }

            }
            else
            {
                item.Visible = false;
                item.Enabled = false;

            }

        }

        private static void TestConnexion()
        {
            try
            {
                var db2 = new ModelEntities();
                var test = db2.ApplicationUtilisateurs.Take(1).ToList();
                test = null;
                db2 = null;



            }
            catch (Exception e)
            {
                Tools.ShowError("Erreur de connexion:\n" + e.Message);
                Application.Exit();
            }
        }

        private void TestExecution()
        {
            //******************
            //En début de code
            //******************


            //******************************************************
            //Au chargement (Main() ou dans Form_Load() pour applic. Windows
            //******************************************************
            //Obtient le processus en cours de l'application
            Process Proc_EnCours = Process.GetCurrentProcess();
            //Collection des processus actuellement lancés
            Process[] Les_Proc = Process.GetProcesses();
            //Pour chaque processus lancé
            foreach (Process Processus in Les_Proc)
            {
                /*Il ne faut pas comparer par rapport à cette instance
                    du programme mais une autre (grâce à l'ID)*/
                if (Proc_EnCours.Id != Processus.Id)
                {
                    //Si les ID sont différents mais de même nom ==> 2 fois le même programme
                    if (Proc_EnCours.ProcessName == Processus.ProcessName)
                    {
                        MessageBox.Show("Le programme est déja en cours d'éxecution.");
                        Application.Exit();
                        this.Dispose();
                    }
                }
            }
        }

        private void SendRecapsEmail(string[] args)
        {
            #region mail
            /*
            //  MessageBox.Show("Email");
            //  return;
            var typeperiode = "";

            var dateFrom = DateTime.Now.Date;
            var dateTo = DateTime.Now.Date;
            var filename = dateTo.ToString("dd_MM_yyyy");

            if (args.Contains("day"))
            {
                typeperiode = "day";
                dateFrom = DateTime.Now.Date;
                dateTo = DateTime.Now.Date;
                filename = dateTo.ToString("dd_MM_yyyy");
            }

            if (args.Contains("day-1"))
            {
                typeperiode = "day";
                dateFrom = dateFrom.AddDays(-1).Date;
                dateTo = dateTo.AddDays(-1).Date;
                filename = dateTo.ToString("dd_MM_yyyy");
            }


            if (args.Contains("week"))
            {
                typeperiode = "week";
                var now = DateTime.Now;
                while (now.DayOfWeek != DayOfWeek.Wednesday)
                {
                    now = now.AddDays(-1);
                }

                dateFrom = now.AddDays(-5).Date;
                dateTo = now.Date;
                filename = dateFrom.ToString("dd_MM_yyyy") + "_au_" + dateTo.ToString("dd_MM_yyyy");
            }


            if (args.Contains("ten"))
            {
                typeperiode = "ten";
                var now = DateTime.Now;

                dateFrom = now.AddDays(-10).Date;
                dateTo = now.Date;
                filename = dateFrom.ToString("dd_MM_yyyy") + "_au_" + dateTo.ToString("dd_MM_yyyy");
            }


            if (args.Contains("month-1"))
            {


                typeperiode = "month";
                var now = DateTime.Now;
                var month = now.AddMonths(-1);
                var days = DateTime.DaysInMonth(month.Year, month.Month);

                dateFrom = new DateTime(month.Year, month.Month, 1);
                dateTo = new DateTime(month.Year, month.Month, days);
                filename = dateFrom.ToString("MMMM_yyyy");
            }


            if (args.Contains("month"))
            {


                typeperiode = "month";
                var now = DateTime.Now;
                var month = now;//.AddMonths(1);
                var days = DateTime.DaysInMonth(month.Year, month.Month);

                dateFrom = new DateTime(month.Year, month.Month, 1);
                dateTo = new DateTime(month.Year, month.Month, days);
                filename = dateFrom.ToString("MMMM_yyyy");
            }


            var listFiles = new List<string>();
            Actions.Rapports.FrViewer.ExportPDF = true;
            // true;
            // Actions.Rapports.FrViewer.sufix = dateFrom.ToString("dd_MM_yyyy") + " " + dateTo.ToString("dd_MM_yyyy");
            foreach (var item in args)
            {

                if (item == "all")
                {
                    if ((dateFrom.DayOfWeek == DayOfWeek.Friday) && dateTo == dateFrom)
                    { // jour ferier week

                    }
                    else
                    {
                        //Actions.Rapports.FrViewer.PrintSuiviUPlaques(AffDate, date);
                        Actions.Rapports.FrViewer.ExportFileName = "reap_generale_" + filename;
                        Actions.Rapports.FrViewer.PrintRecapAll(dateFrom, dateTo);
                        if (Actions.Rapports.FrViewer.ExportFileName != null)
                        {
                            listFiles.Add(Actions.Rapports.FrViewer.ExportFileName);
                        }
                        else
                        {
                            MessageBox.Show("ErrorExporting");
                        }

                    }
                }

                if (item == "plaque")
                {
                    if ((dateFrom.DayOfWeek == DayOfWeek.Friday || dateFrom.DayOfWeek == DayOfWeek.Saturday) && dateTo == dateFrom)
                    { // jour ferier week

                    }
                    else
                    {
                        //Actions.Rapports.FrViewer.PrintSuiviUPlaques(AffDate, date);
                        Actions.Rapports.FrViewer.ExportFileName = "U_plaques_" + filename;
                        Actions.Rapports.FrViewer.PrintRecapUplaques(dateFrom, dateTo);
                        if (Actions.Rapports.FrViewer.ExportFileName != null)
                        {
                            listFiles.Add(Actions.Rapports.FrViewer.ExportFileName);
                        }
                        else
                        {
                            MessageBox.Show("ErrorExporting");
                        }

                    }
                }


                if (item == "inventaireBloc")
                {
                    Actions.Rapports.FrViewer.ExportFileName = "inventaire_blocs_" + filename;
                    InventaireBlocs();
                    if (Actions.Rapports.FrViewer.ExportFileName != null)
                    {
                        listFiles.Add(Actions.Rapports.FrViewer.ExportFileName);
                    }
                    else
                    {
                        MessageBox.Show("ErrorExporting");
                    }
                }

                if (item.StartsWith("suiviArrivage"))
                {
                    string arrivage = item.Replace("suiviArrivage", "");
                    Actions.Rapports.FrViewer.ExportFileName = "SUIVI_BlOC_" + arrivage + "--production_transfert_ventes_et_stock_au_" + filename;
                    Actions.Rapports.FrViewer.PrintRecapArrivageBloc(arrivage);



                    if (Actions.Rapports.FrViewer.ExportFileName != null)
                    {
                        listFiles.Add(Actions.Rapports.FrViewer.ExportFileName);
                    }
                    else
                    {
                        MessageBox.Show("ErrorExporting");
                    }
                }



                if (item == "inventaireSW")
                {
                    Actions.Rapports.FrViewer.ExportFileName = "inventaire_showroom_" + filename;
                    Actions.Rapports.FrViewer.PrintInventaireSW();
                    if (Actions.Rapports.FrViewer.ExportFileName != null)
                    {
                        listFiles.Add(Actions.Rapports.FrViewer.ExportFileName);
                    }
                    else
                    {
                        MessageBox.Show("ErrorExporting");
                    }


                    Actions.Rapports.FrViewer.ExportFileName = "resumé_inventaire_showroom_" + filename;
                    Actions.Rapports.FrViewer.PrintRecapInventaireSW();
                    if (Actions.Rapports.FrViewer.ExportFileName != null)
                    {
                        listFiles.Add(Actions.Rapports.FrViewer.ExportFileName);
                    }
                    else
                    {
                        MessageBox.Show("ErrorExporting " + Actions.Rapports.FrViewer.ExportFileName);
                    }

                }

                if (item == "inventaireMGZ")
                {
                    Actions.Rapports.FrViewer.ExportFileName = "inventaire_magazin_" + filename;
                    Actions.Rapports.FrViewer.PrintInventaireMGZ();
                    if (Actions.Rapports.FrViewer.ExportFileName != null)
                    {
                        listFiles.Add(Actions.Rapports.FrViewer.ExportFileName);
                    }
                    else
                    {
                        MessageBox.Show("ErrorExporting");
                    }
                }


                if (item == "arrivage")
                {
                    Actions.Rapports.FrViewer.ExportFileName = "suivi_Blocs_" + filename;
                    SuivisArrivagePeriode(dateFrom, dateTo);
                    if (Actions.Rapports.FrViewer.ExportFileName != null)
                    {
                        listFiles.Add(Actions.Rapports.FrViewer.ExportFileName);
                    }
                    else
                    {
                        MessageBox.Show("ErrorExporting");
                    }
                }


                if (item == "analyse")
                {
                    Actions.Rapports.FrViewer.ExportFileName = "ANALYSE_DE_PRODUCTION_ET_TRANSFERT_BLOCS_FINIS_" + filename;
                    //SuivisArrivagePeriode(dateFrom, dateTo);
                    var list = GenerateAnalyse();

                    if (Actions.Rapports.FrViewer.ExportFileName != null)
                    {
                        listFiles.Add(Actions.Rapports.FrViewer.ExportFileName);
                        using (var db = new ModelEntities())
                        {
                            //var blocs = db.Blocs.Where(p => list.Contains(p.Reference)).ToList();
                            foreach (var reference in list)
                            {
                                var bloc = db.Blocs.Single(p => p.Reference == reference);
                                bloc.isDone = true;
                            }
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        MessageBox.Show("ErrorExporting");
                    }
                }





                if (item == "bloc")
                {
                    if ((dateFrom.DayOfWeek == DayOfWeek.Friday || dateFrom.DayOfWeek == DayOfWeek.Saturday) && dateTo == dateFrom)
                    { // jour ferier week

                    }
                    else
                    {
                        //Actions.Rapports.FrViewer.PrintSuiviUPlaques(AffDate, date);
                        Actions.Rapports.FrViewer.ExportFileName = "recap_blocs_" + filename;
                        Actions.Rapports.FrViewer.PrintRecapBloc(dateFrom, dateTo);
                        if (Actions.Rapports.FrViewer.ExportFileName != null)
                        {
                            listFiles.Add(Actions.Rapports.FrViewer.ExportFileName);
                        }
                        else
                        {
                            MessageBox.Show("ErrorExporting");
                        }

                    }
                }




                if (item == "carreaux")
                {
                    if ((dateFrom.DayOfWeek == DayOfWeek.Friday || dateFrom.DayOfWeek == DayOfWeek.Saturday) && dateTo == dateFrom)
                    { // jour ferier week

                    }
                    else
                    {
                        //Actions.Rapports.FrViewer.PrintSuiviUPlaques(AffDate, date);
                        Actions.Rapports.FrViewer.ExportFileName = "U_carreaux_" + filename;
                        Actions.Rapports.FrViewer.PrintRecapUCarreaux(dateFrom, dateTo);
                        if (Actions.Rapports.FrViewer.ExportFileName != null)
                        {
                            listFiles.Add(Actions.Rapports.FrViewer.ExportFileName);
                        }
                        else
                        {
                            MessageBox.Show("ErrorExporting");
                        }

                    }
                }

                if (item == "magazin")
                {
                    if ((dateFrom.DayOfWeek == DayOfWeek.Friday || dateFrom.DayOfWeek == DayOfWeek.Saturday) && dateTo == dateFrom)
                    { // jour ferier week

                    }
                    else
                    {
                        //Actions.Rapports.FrViewer.PrintSuiviUPlaques(AffDate, date);
                        Actions.Rapports.FrViewer.ExportFileName = "magazin_" + filename;
                        Actions.Rapports.FrViewer.PrintRecapMagazin(dateFrom, dateTo);
                        if (Actions.Rapports.FrViewer.ExportFileName != null)
                        {
                            listFiles.Add(Actions.Rapports.FrViewer.ExportFileName);
                        }
                        else
                        {
                            MessageBox.Show("ErrorExporting");
                        }

                    }
                }


                if (item == "showroom")
                {
                    if ((dateFrom.DayOfWeek == DayOfWeek.Friday) &&
                                     dateTo == dateFrom)
                    { // jour ferier week

                    }
                    else
                    {

                        Actions.Rapports.FrViewer.ExportFileName = "showroom_" + filename;
                        Actions.Rapports.FrViewer.PrintRecapShowroom(dateFrom, dateTo);
                        if (Actions.Rapports.FrViewer.ExportFileName != null)
                        {
                            listFiles.Add(Actions.Rapports.FrViewer.ExportFileName);
                        }
                        else
                        {
                            MessageBox.Show("ErrorExporting");

                        }
                    }
                }

                if (item == "vente")
                {
                    Actions.Rapports.FrViewer.ExportFileName = "ventes_" + filename;
                    Actions.Rapports.FrViewer.PrintListVenteClient(dateFrom, dateTo);
                    if (Actions.Rapports.FrViewer.ExportFileName != null)
                    {
                        listFiles.Add(Actions.Rapports.FrViewer.ExportFileName);
                    }
                    else
                    {
                        MessageBox.Show("ErrorExporting");

                    }

                }

            }

            Actions.Rapports.FrViewer.ExportPDF = false;

            sendmail(listFiles, args);
            */
            #endregion
        }

        private void sendmail(List<string> fileNames, string[] args)
        {
            if (fileNames.Count == 0)
            {
                return;
            }
            string subjectNC = "";
            if (args.Contains("day"))
            {
                subjectNC = "Situations journalières automatiques du " + DateTime.Now.ToShortDateString();
            }


            if (args.Contains("analyse"))
            {
                subjectNC = "Analyse de Production et transferts au " + DateTime.Now.ToShortDateString();
            }

            if (args.Contains("day-1"))
            {
                subjectNC = "Situations journalières automatiques du " + DateTime.Now.AddDays(-1).ToShortDateString();
            }

            if (args.Contains("week"))
            {
                subjectNC = "Situations Hébdomadaires automatiques ";
            }

            if (args.Contains("month"))
            {
                subjectNC = "Situations Mensuelles automatiques ";
            }

            if (args.Contains("month-1"))
            {
                subjectNC = "Situations Mensuelles automatiques ";
            }

            if (args.Contains("ten") && args.Contains("arrivage"))
            {
                subjectNC = "Production et suivi des Blocs (10 jours) ";
            }

            if (args.Contains("suiviArrivageD"))
            {
                subjectNC = "Résumé du suivi des Blocs --D-- ";
            }

            if (args.Contains("inventaireSW") || args.Contains("inventaireMGZ") || args.Contains("inventaireBloc"))
            {
                subjectNC = "Etats d'inventaires au " + DateTime.Now.ToShortDateString();
            }
            var fromAddress = new MailAddress("newmarbreblida@gmail.com", "New Marbre Continental");


            const string fromPassword = "nmc190584";
            string subject = subjectNC;
            const string body = "Envoyé automatiquement par le serveur NMC";

            var smtp = new SmtpClient
            {

                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };


            using (var message = new MailMessage()
            {
                Subject = subject,
                Body = body,
                From = fromAddress

            })
            {

                message.Bcc.Add(new MailAddress("med.hassairi@gmail.com"));
                message.To.Add(new MailAddress("nmc_ahcene@hotmail.fr"));
                message.To.Add(new MailAddress("zaahafmohamed@hotmail.com"));
                message.To.Add(new MailAddress("noureddinezahaf@yahoo.fr"));
                message.Bcc.Add(new MailAddress("hasaryll@hotmail.com"));



                foreach (var att in fileNames)
                {
                    message.Attachments.Add(new Attachment(att));
                }
                smtp.Send(message);
            }
        }


        public FrMain()
        {
            Tools.InitiliseDb();
            Tools.loadParams();

            string[] args = Environment.GetCommandLineArgs();

            #region send Mail if args
            if (args.Length != 0 & args.Contains("mail"))
            {
                using (ModelEntities db = new ModelEntities())
                {
                    var usr = db.ApplicationUtilisateurs.SingleOrDefault(u => u.ID == 1);

                }


                Tools.CurrentUserID = 1;
                //  CurrentUser = 1;
                SendRecapsEmail(args);

                Application.Exit();
                this.Dispose();
                return;
            }
            #endregion

            TestExecution();
            TestConnexion();
            InitializeComponent();
            flw.Visible = false;
            string sVersion = "";
            if (ApplicationDeployment.IsNetworkDeployed) //Car ne fonctionner pas en mode debug
            {
                ApplicationDeployment ad = ApplicationDeployment.CurrentDeployment;
                sVersion = ad.CurrentVersion.ToString();
            }
            this.Text += " " + sVersion;



            groupBoxInfos.BringToFront();

            InitialiseAllActions(); // disable All

            this.UserID = 0; //turn 0

            var userName = Environment.UserName;
            if (true)
            {
                using (ModelEntities db = new ModelEntities())
                {
                    var usr = db.ApplicationUtilisateurs.SingleOrDefault(u => u.WindowsUserName == userName);
                    if (usr != null)
                    {
                        UserID = usr.ID;
                    }

                }
            }

            //  new FZBGsys.NsApplication.FrLoginForm(this).ShowDialog();

        }


        #region Enable / Disable All

        private void InitialiseAllActions()
        {
            using (ModelEntities db = new ModelEntities())
            {
                List<ApplicationAction> itemsTexts = null; ;
                try
                {
                    itemsTexts = db.ApplicationActions.ToList();
                }
                catch (Exception e)
                {
                    string ErrorMessage = "";
                    while (e != null)
                    {
                        ErrorMessage += e.Message + "\n";
                        e = e.InnerException;
                    }
                    Tools.ShowError("Erreur de connexion:\n " + ErrorMessage);
                    Application.Exit();

                }

                foreach (var item in menuMain.Items)
                {
                    if (item is ToolStripMenuItem)
                        InitActions((ToolStripMenuItem)item, itemsTexts);
                }
            }
        }

        private void EnableAllActions()
        {
            foreach (var item in menuMain.Items)
            {
                if (item is ToolStripMenuItem)
                    EnableActions((ToolStripMenuItem)item);
            }
        }

        private void InitActions(ToolStripMenuItem item, List<ApplicationAction> list)
        {
            if (item.Name == "Fichier" || item.Name == "Aide")
                return;
            item.Enabled = false;
            item.Visible = false;

            var itemcorr = list.FirstOrDefault(l => l.Nom == item.Name);

            if (itemcorr != null)
            {
                item.Text = itemcorr.Text;
                if (itemcorr.ParentActionID != 0)
                {
                    item.Text += "";
                }

            }
            else
            {

                //Tools.ShowError("Erreur de chargement de l'interface,\nElement de menu Manquant: " + item.Name);
            }
            foreach (ToolStripItem subitem in item.DropDownItems)
            {
                if (subitem is ToolStripMenuItem)
                {
                    subitem.Enabled = false;
                    subitem.Visible = false;
                    //  InitActions((ToolStripMenuItem)subitem, list);
                }
            }

        }

        private void EnableActions(ToolStripMenuItem item)
        {
            if (item.Name == "Fichier")
                return;
            item.Enabled = true;
            item.Visible = true;
            foreach (ToolStripItem subitem in item.DropDownItems)
            {
                if (subitem is ToolStripMenuItem)
                {
                    item.Enabled = true;
                    item.Visible = true;
                    EnableActions((ToolStripMenuItem)subitem);
                }
            }




        }
        #endregion




        #region menu click
        private void matierePremiereToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //        new NsTRBsys.NsStock.FrArrivage().ShowDialog();
        }
        private void nouveauToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //     new NsTRBsys.NsStock.FrArrivage().ShowDialog();
        }
        private void utilisateursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //      new NsTRBsys.NsAdministration.NsUtilisateur.FrList().ShowDialog();
        }
        private void groupesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //    new NsTRBsys.NsAdministration.NsGroupe.FrList().ShowDialog();
        }
        #endregion
        private void FichierConnecter_Click(object sender, EventArgs e)
        {
            new NsTRBsys.NsApplication.FrLoginForm(this).ShowDialog();

        }
        private void FichierDeconnecter_Click(object sender, EventArgs e)
        {
            InitialiseAllActions();
            this.UserID = 0;
            flw.Visible = false;
        }
        private void FichierQuitter_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void ArrivageList_Click(object sender, EventArgs e)
        {
            //new NsTRBsys.NsStock.FrListArrivage().ShowDialog();
        }

        public void UpdateSoleStat()
        {
            labSoldeFournisseurD.Text = "";
            //  if (soldeFournD != 0 && soldeFournD != null)
            labSoldeFournisseurC.Text = "";
            // if (soldeClientsD != 0 && soldeClientsD != null)
            LabSoldeClientD.Text = "";
            // if (soldeClientsC != 0 && soldeClientsC != null)
            LabSoldeClientC.Text = "";



            using (var db = new ModelEntities())
            {
                var soldeFournD = db.Fournisseurs.Where(p => p.Solde < 0).Sum(p => p.Solde);
                var soldeFournC = db.Fournisseurs.Where(p => p.Solde > 0).Sum(p => p.Solde);

                var soldeClientsD = db.Clients.Where(p => p.Solde < 0).Sum(p => p.Solde);
                var soldeClientsC = db.Clients.Where(p => p.Solde > 0).Sum(p => p.Solde);

                if (soldeFournD != 0 && soldeFournD != null)
                    labSoldeFournisseurD.Text = soldeFournD.Value.ToString("N2");// +" créance";

                if (soldeFournC != 0 && soldeFournC != null)
                    labSoldeFournisseurC.Text = soldeFournC.Value.ToString("N2");// +" avance";

                if (soldeClientsD != 0 && soldeClientsD != null)
                    LabSoldeClientD.Text = soldeClientsD.Value.ToString("N2");// " créance";

                if (soldeClientsC != 0 && soldeClientsC != null)
                    LabSoldeClientC.Text = soldeClientsC.Value.ToString("N2");// " avance";

                labSoldeFournisseurD.ForeColor = Color.Red;
                labSoldeFournisseurC.ForeColor = Color.Green;
                LabSoldeClientD.ForeColor = Color.Red;
                LabSoldeClientC.ForeColor = Color.Green;

            }

        }



        private void menuMain_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void ProductionPET_Click(object sender, EventArgs e)
        {
            //     new FZBGsys.NsProduction.FrProductionPET().ShowDialog();

        }

        private void ProductionListPET_Click(object sender, EventArgs e)
        {

            //  new FZBGsys.NsProduction.FrListProduction().ShowDialog();
        }

        private void versUnitéPETToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //     new NsTRBsys.NsStock.FrTransfertMatiere(EnSection.Unité_PET).ShowDialog();

        }

        private void versUnitéCanetteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //        new NsTRBsys.NsStock.FrTransfertMatiere(EnSection.Unité_Canette).ShowDialog();
        }

        private void versAutresDéstinationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //      new NsTRBsys.NsStock.FrTransfertMatiere(EnSection.Autre).ShowDialog();
        }

        private void TransfertListMP_Click(object sender, EventArgs e)
        {
            //       new NsTRBsys.NsStock.FrListTransfertMatiere().ShowDialog();
        }

        private void TransfertPF_Click(object sender, EventArgs e)
        {
            //    new FZBGsys.NsStock.FrTransfertProduitFini().ShowDialog();
        }

        private void listTransfertsPF_Click(object sender, EventArgs e)
        {
            //  new FZBGsys.NsStock.FrListTransfertProduitFini().ShowDialog();
        }

        private void StockMP_Click(object sender, EventArgs e)
        {
            //         new NsTRBsys.NsStock.FrStockMatiere().ShowDialog();
        }

        private void StockPF_Click(object sender, EventArgs e)
        {
            //        new NsTRBsys.NsStock.FrStockProduitFini().ShowDialog();
        }

        private void AdministrationMaintenance_Click(object sender, EventArgs e)
        {
            //   new FZBGsys.NsAdministration.FrMaintenance().ShowDialog();
        }

        private void AideApropos_Click(object sender, EventArgs e)
        {
            this.ShowInformation("Réalisée par Mohamed HASSAIRI\nTel: 0666 66 68 06\nEmail: med.hassairi@gmail.com");
        }



        private void matièrePremièreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var db = new ModelEntities())
            {
                //            var list = db.StockMatieres.Where(p => p.EnStock == true).OrderBy(p => p.Ressource.TypeRessourceID).ToList();
                //        FZBGsys.NsRapports.FrViewer.PrintCustumStockMp(list, "Produits en Stock", db);

            }
        }


        private void listeDesClientsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new NsTRBsys.NsComercial.NsClient.FrList().ShowDialog();
        }

        private void nouveauClientToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //new FZBGsys.NsComercial.NsClient.FrCreate().ShowDialog();
            new NsTRBsys.NsComercial.FrListProduit().ShowDialog();
        }

        private void listeDesChauffeurToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new NsTRBsys.NsComercial.NsChauffeur.FrList().ShowDialog();
        }


        private void ComercialBonLiv_Click(object sender, EventArgs e)
        {
            //   new FZBGsys.NsComercial.FrBonLivraison().ShowDialog();
        }

        private void ComercialFacture_Click(object sender, EventArgs e)
        {
            new NsTRBsys.NsComercial.FrFactureMO().ShowDialog();
        }

        private void ComercialBonSort_Click(object sender, EventArgs e)
        {
            // new FZBGsys.NsComercial.FrBonSortie().ShowDialog();
        }

        private void ComercialBonAttrib_Click(object sender, EventArgs e)
        {
            // new FZBGsys.NsComercial.FrBonAttribution().ShowDialog();
        }

        private void CommercialListeBon_Click(object sender, EventArgs e)
        {
            new NsTRBsys.NsComercial.FrListBon().ShowDialog();
        }

        private void ComercialListVentes_Click(object sender, EventArgs e)
        {
            //       new FZBGsys.NsComercial.FrListVentes().ShowDialog();

            var dates = NsTRBsys.NsRapport.FrSelectDatePeriode.SelectPeriode();

            if (dates != null && dates[0] != null)
            {
                var from = dates[0];
                var to = dates[1];

                NsTRBsys.NsRapports.FrViewer.PrintVentes(from.Value, to.Value);
            }
        }


        private void RapportPF_Click(object sender, EventArgs e)
        {

        }

        private void RapportProduction_Click(object sender, EventArgs e)
        {

        }

        private void etatDesStocksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // FZBGsys.NsRapports.FrViewer.PrintStockProduitFini();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            //        new NsTRBsys.NsRessources.NsFournisseur.FrCreateEdit().ShowDialog();
        }

        private void ArrivagelistFourni_Click(object sender, EventArgs e)
        {
            //       new NsTRBsys.NsRessources.NsFournisseur.FrList().ShowDialog();
        }

        private void FrMain_Load(object sender, EventArgs e)
        {

        }
        private void AutoBackup()
        {

        }
        private void FrMain_Shown(object sender, EventArgs e)
        {
            bk1.RunWorkerAsync();
            if (Tools.AutoBackup && Tools.AutoBackupMethod == "Startup")
            {
                bkAutoBackup.RunWorkerAsync();
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void ComercialCommand_Click(object sender, EventArgs e)
        {
            new NsComercial.FrVersementCompte().ShowDialog();
        }

        private void releveClientToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new NsComercial.FrSelectDateClient().ShowDialog();
        }

        private void listeDesVersementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new NsComercial.FrListOperation().ShowDialog();
        }

        private void flw_VisibleChanged(object sender, EventArgs e)
        {
            pictureBox1.Visible = !flw.Visible;
        }

        private void button2_Click_2(object sender, EventArgs e)
        {
            new NsTRBsys.NsComercial.FrListProduit().ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var dates = NsTRBsys.NsRapport.FrSelectDatePeriode.SelectPeriode();

            if (dates != null && dates[0] != null)
            {
                var from = dates[0];
                var to = dates[1];

                NsTRBsys.NsRapports.FrViewer.PrintVentes(from.Value, to.Value);
            }

        }

        private void button10_Click(object sender, EventArgs e)
        {
            new NsTRBsys.NsComercial.NsChauffeur.FrList().ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            new NsTRBsys.NsComercial.FrFactureMO().ShowDialog();
            UpdateSoleStat();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            new NsTRBsys.NsComercial.FrVersementCompte().ShowDialog();
            UpdateSoleStat();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            new NsComercial.FrListeDepos().ShowDialog();


        }

        private void button8_Click(object sender, EventArgs e)
        {
            new NsComercial.FrSelectDateClient().ShowDialog();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            new NsTRBsys.NsComercial.FrListBon().ShowDialog();
            UpdateSoleStat();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            new NsComercial.FrListOperation().ShowDialog();
            UpdateSoleStat();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            NsTRBsys.NsRapports.FrViewer.PrintSituationComerciale(DateTime.Now);
            UpdateSoleStat();
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            new NsTRBsys.FrParametres().ShowDialog();
        }

        private void flw_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (var db = new ModelEntities())
            {
                var collection = db.Clients.Select(p => p.ID).ToList();
                foreach (var id in collection)
                {
                    var selectedClient = db.Clients.Single(em => em.ID == id);


                    var Destination = selectedClient.Destinations.SingleOrDefault(p => p.IsDefault == true);


                    if (Destination == null)
                    {
                        using (var dg = new ModelEntities())
                        {
                            var dg_client = dg.Clients.Single(p => p.ID == id);

                            dg_client.Destinations.Add(new Destination
                            {
                                Adresse = dg_client.Adresse,
                                NomRaisonSocial = Tools.FakeNom,
                                Wilaya = dg_client.Wilaya,
                                NoRegistre = Tools.FakeRegistre.Replace("%WW", dg_client.WilayaID.ToString()),
                                NoArticle = Tools.FakeArticle.Replace("%WW", dg_client.WilayaID.ToString()),
                                NoIdentifiant = Tools.FakeFiscal,
                                IsDefault = true,
                                IsReal = false,


                            });


                            if (selectedClient.Destinations.SingleOrDefault(p => p.IsReal == true) == null)
                            {
                                dg_client.Destinations.Add(new Destination
                                {
                                    Adresse = dg_client.Adresse,
                                    NomRaisonSocial = dg_client.Nom,
                                    Wilaya = dg_client.Wilaya,
                                    NoRegistre = dg_client.NoRegistre,
                                    NoArticle = dg_client.NoArticle,
                                    NoIdentifiant = dg_client.NoFiscale,
                                    IsDefault = false,
                                    IsReal = true,
                                    Client = dg_client,

                                });
                            }

                            dg.SaveChanges();
                            var factures = dg_client.Factures1.ToList();
                            foreach (var fact in factures)
                            {
                                fact.Destination1 = dg_client.Destinations.SingleOrDefault(p => p.WilayaID == fact.DestinationWilayaID && p.IsReal == false);
                                if (fact.Destination1 == null)
                                {
                                    fact.Destination1 =
                                                new Destination
                                                {
                                                    Adresse = fact.Wilaya.Nom,
                                                    NomRaisonSocial = Tools.FakeNom,
                                                    Wilaya = fact.Wilaya,
                                                    NoRegistre = Tools.FakeRegistre.Replace("%WW", fact.DestinationWilayaID.ToString()),
                                                    NoArticle = Tools.FakeArticle.Replace("%WW", fact.DestinationWilayaID.ToString()),
                                                    NoIdentifiant = Tools.FakeFiscal,
                                                    IsDefault = false,
                                                    IsReal = false,
                                                    Client = dg_client,

                                                };
                                    dg.SaveChanges();

                                }
                            }

                            dg.SaveChanges();
                        }

                    } 
                }
                // return selectedClient;
            }

            Tools.ShowMessage("done");





            Tools.CurrentUserID = 0;
            InitialiseAllActions();
            flw.Visible = false;
            groupBoxInfos.Visible = false;
        }

        private void button16_Click(object sender, EventArgs e)
        {

        }

        private void button13_Click(object sender, EventArgs e)
        {
            new NsComercial.FrVersementFournisseur().ShowDialog();
            UpdateSoleStat();
        }

        public static void GenerateSoldeClient(ModelEntities db, DateTime? to, Client client)
        {
            if (client == null)
            {
                return;
            }
            client.XSolde = null;

            if (client.ID == 3228)
            {

            }

            if (to == null)
            {
                to = DateTime.Now;
            }

            DateTime from = Tools.DateDebutOperations;

            var facturesDepart = client.Factures.Where(p => p.Date >= from && p.Date <= to);
            var facturesDestination = client.Factures1.Where(p => p.Date >= from && p.Date <= to);

            int dd = facturesDepart.Count();

            int cc = facturesDestination.Count();
            var operationD = client.Operations.Where(p => p.Date >= from && p.Sen == "D" && p.Date <= to);
            var operationC = client.Operations.Where(p => p.Date >= from && p.Sen == "C" && p.Date <= to);


            //   client.DateHistory = Tools.DateDebutOperations;
            db.Refresh(System.Data.Objects.RefreshMode.StoreWins, db.Operations);

            //  var OpHistory = client.Operations.Where(p => p.Date == from).SingleOrDefault();
            /*   if (OpHistory == null)
               {
                   client.SoldeHistory = 0;
               }
               else
               {
                   client.SoldeHistory = OpHistory.Montant;
               }
               */
            //client.SoldeHistory = soldeHistory;
            client.XSolde =
                    operationC.Sum(p => p.Montant) -
                    operationD.Sum(p => p.Montant) -
                    facturesDestination.Sum(p => p.MontantTotalFacture) +
                    facturesDepart.Sum(p => p.MontantTotalAchat);

            if (to.Value.Date.CompareTo(DateTime.Now.Date) == 0)
            {
                client.Solde = client.XSolde;
                //  client.DateMajSoldeHistory = Tools.CurrentDateTime.Value.Date;
            }

        }


        public static void GenerateSoldeFourni(ModelEntities db, DateTime? to, Fournisseur fourni)
        {
            if (fourni == null)
            {
                return;
            }
            fourni.XSolde = null;


            if (to == null)
            {
                to = DateTime.Now;
            }

            DateTime from = Tools.DateDebutOperations;

            var factures = fourni.Factures.Where(p => p.Date >= from && p.Date <= to);
            int dd = factures.Count();

            var operationD = fourni.Operations.Where(p => p.Date >= from && p.Sen == "D" && p.Date <= to);


            var operationC = fourni.Operations.Where(p => p.Date >= from && p.Sen == "C" && p.Date <= to);


            //  fourni.DateHistory = Tools.DateDebutOperations;
            db.Refresh(System.Data.Objects.RefreshMode.StoreWins, db.Operations);
            //var OpHistory = fourni.Operations.Where(p => p.Date == from).SingleOrDefault();
            /*  if (OpHistory == null)
              {
                  fourni.SoldeHistory = 0;
              }
              else
              {
                  fourni.SoldeHistory = OpHistory.Montant;
              }
              */

            //client.SoldeHistory = soldeHistory;
            fourni.XSolde =
                    operationC.Sum(p => p.Montant) -
                    factures.Sum(p => p.MontantTotalAchat);

            if (to.Value.Date.CompareTo(DateTime.Now.Date) == 0)
            {
                fourni.Solde = fourni.XSolde;
                //  client.DateMajSoldeHistory = Tools.CurrentDateTime.Value.Date;
            }

        }

        private void button14_Click(object sender, EventArgs e)
        {
            new NsComercial.FrSelectDateFourni().ShowDialog();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }



        internal static void UpdateStockDepos(ModelEntities db, Facture facture, Depo depo, bool annulation = false)
        {
            //Depo depo = facture.Depo1 != null ? facture.Depo1 : facture.Depo;
            if (depo == null) return;

            StockProduit stock = null;
            foreach (var item in facture.Ventes)
            {
                stock = db.StockProduits.SingleOrDefault(p => p.FormatID == item.FormatID && p.GoutProduitID == item.GoutProduitID && p.DepoID == depo.ID);
                if (stock == null)
                {
                    stock = new StockProduit
                    {
                        DepoID = depo.ID,
                        FormatID = item.FormatID.Value,
                        GoutProduitID = item.GoutProduitID.Value,
                        QuantiteFardeaux = 0,
                        QuantitePalette = 0,

                    };

                    db.StockProduits.AddObject(stock);

                }


                if (facture.Depo1 == depo && !annulation ||
                    facture.Depo == depo && annulation)
                {
                    stock.QuantiteFardeaux += item.NombreFardeaux;
                    stock.QuantitePalette += item.NombrePalettes;
                }
                else if (facture.Depo1 == depo && annulation ||
                         facture.Depo == depo && !annulation)
                {
                    stock.QuantiteFardeaux -= item.NombreFardeaux;
                    stock.QuantitePalette -= item.NombrePalettes;

                }



            }


            if (stock.QuantitePalette < 0)
            {
                Tools.ShowWarning("stock dépo négtif, veillez le corriger");
            }

        }

        private void listeDesDéposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new NsComercial.FrListeDepos().ShowDialog();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            new NsTRBsys.NsComercial.NsClient.FrList().ShowDialog();
        }

        private void button10_Click_1(object sender, EventArgs e)
        {
            new NsComercial.NsFournisseur.FrList().ShowDialog();

        }

        private void sauveguarderLaBaseDesDonnéesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tools.BackupDataBase();
        }

        private void retorerLaBaseDesDonnéesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tools.RestoreDataBase();
        }

        private void groupBoxInfos_Enter(object sender, EventArgs e)
        {

        }

        private void bkAutoBackup_DoWork(object sender, DoWorkEventArgs e)
        {
            Tools.BackupDataBase(Tools.AutoBackupPath, true);
        }

        private void FrMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Tools.AutoBackup && Tools.AutoBackupMethod == "Close")
            {
                Tools.BackupDataBase(Tools.AutoBackupPath, true);
            }
        }

        private void bk1_DoWork(object sender, DoWorkEventArgs e)
        {
            new NsTRBsys.NsRapports.CR_SituationComercial().Load();
        }
    }
}
