﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsRessources.NsFournisseur
{
    public partial class FrCreateEdit : Form
    {
        ModelEntities db = new ModelEntities();
        public FrCreateEdit(int? FourniseurID = null)
        {
            InitializeComponent();
            InitialiseControls();

            if (FourniseurID != null)
            {
                this.Fournisseur = db.Fournisseurs.Single(p => p.ID == FourniseurID);
                InputFournisseur();
            }


        }

        public FrCreateEdit(TypeRessource typeRessource)
        {
            InitializeComponent();
            InitialiseControls();
            for (int i = 0; i < chl.Items.Count; i++)
            {
                bool check = ((TypeRessource)chl.Items[i]).ID == typeRessource.ID;
                chl.SetItemChecked(i, check);
                ((ListBox)chl).Enabled = false;
            }

        }

        public static int? CreateFournisseurDialog(TypeRessource typeRessource)
        {
            FrCreateEdit fr = new FrCreateEdit(typeRessource);
            fr.ShowDialog();
            return (fr.Fournisseur == null) ? null : (int?)fr.Fournisseur.ID;

        }


        //   private static Fournisseur ReturnFournisseur { get; set; }
        private void InputFournisseur()
        {
            txtAdresse.Text = Fournisseur.Adresse;
            txtCodeSage.Text = Fournisseur.CodeSage;
            txtNom.Text = Fournisseur.Nom;

            for (int i = 0; i < chl.Items.Count; i++)
            {
                chl.SetItemChecked(i, Fournisseur.TypeRessources.Contains((TypeRessource)chl.Items[i]));
                // ((CheckBox)chl.Items[i]).Font.Bold = Fournisseur.TypeRessources.Contains((TypeRessource)chl.Items[i];
            }



        }

        private void InitialiseControls()
        {
            ((ListBox)this.chl).DataSource = db.TypeRessources.ToList();
            ((ListBox)this.chl).DisplayMember = "Nom";
            ((ListBox)this.chl).ValueMember = "ID";

        }

        private void FrCreateEdit_FormClosed(object sender, FormClosedEventArgs e)
        {
            db.Dispose();
        }

        private void btEnregistrer_Click(object sender, EventArgs e)
        {
            if (!isValidAll())
            {
                return;
            }

            if (Fournisseur == null)
            {
                Fournisseur = new Fournisseur();
            }

            var newFournisseur = this.Fournisseur;
            newFournisseur.Adresse = txtAdresse.Text.Trim();
            newFournisseur.Nom = txtNom.Text.Trim();
            newFournisseur.CodeSage = txtCodeSage.Text.Trim();

            newFournisseur.TypeRessources.Clear();

            foreach (var selected in chl.CheckedItems)
            {
                newFournisseur.TypeRessources.Add((TypeRessource)selected);
            }



            db.SaveChanges();
            //  ReturnFournisseur = Fournisseur;
            Dispose();

        }

        private bool isValidAll()
        {
            var message = "";
            var name = txtNom.Text.Trim();

            if (name.Length < 2)
            {
                message += "Nom Invalide!!";
            }
            var existingName = db.Fournisseurs.Where(p => p.Nom == name);
            if (this.Fournisseur != null) // Edit
            {
                existingName = existingName.Where(p => p.ID != Fournisseur.ID);
            }
            if (existingName.Count() != 0)
            {
                message += "\nNom de fournisseur existe déja!!";
            }

            if (chl.CheckedItems.Count == 0)
            {
                message += "\nSelectionner une ou plusieurs matière fourni par ce fournisseur!";
            }

            if (message != "")
            {
                this.ShowWarning(message);
                return false;
            }

            return true;
        }

        private void txtNom_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLower(e.KeyChar))
            {
                e.KeyChar = char.ToUpper(e.KeyChar);
            }
        }

        public Fournisseur Fournisseur { get; set; }

        private void btAnnuler_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
