﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsRessources.NsFournisseur
{
    public partial class FrList : Form
    {
        ModelEntities db = new ModelEntities();
        public FrList()
        {
            InitializeComponent();
            InitialiseControls();
            RefreshGrid();
        }

        private void RefreshGrid()
        {
            List<Fournisseur> fourni = db.Fournisseurs.Where(p => p.ID > 0).ToList();
            if (txtNom.Text.Trim() != "")
            {
                var nom = txtNom.Text.Trim();
                fourni = fourni.Where(p => p.Nom.Contains(nom)).ToList();
            }
            if (cbTypeRessource.SelectedIndex != 0)
            {
                var ress = (TypeRessource)cbTypeRessource.SelectedItem;
                fourni = fourni.ToList().Where(p => p.TypeRessources.Contains(ress)).ToList();
            }

            dgv.DataSource = fourni.Select(p =>
                new
                {
                    ID = p.ID,
                    Code = p.CodeSage,
                    Nom = p.Nom,
                    Adresse = p.Adresse,
                    Matiere = (p.TypeRessources.Count == 1) ? p.TypeRessources.First().Nom : "(multiples)"
                }).ToList();
            dgv.HideIDColumn();
        }

        private void InitialiseControls()
        {
            //---------------------- Categorie (type Ressource) ---------------------------------------------------------------
            cbTypeRessource.Items.Add(new TypeRessource { ID = 0, Nom = "[Tout]" });
            cbTypeRessource.Items.AddRange(db.TypeRessources.Where(p => p.ID > 0).ToArray());
            cbTypeRessource.DisplayMember = "Nom";
            cbTypeRessource.ValueMember = "ID";
            cbTypeRessource.DropDownStyle = ComboBoxStyle.DropDownList;
            cbTypeRessource.SelectedIndex = 0;
        }

        private void FrList_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        private void btNouveau_Click(object sender, EventArgs e)
        {
            new FrCreateEdit().ShowDialog();
        }

        private void btModifier_Click(object sender, EventArgs e)
        {
            var id = dgv.GetSelectedID();
            new FrCreateEdit(id).ShowDialog();
        }

        private void btSupprimer_Click(object sender, EventArgs e)
        {
            var id = dgv.GetSelectedID();
            var fourni = db.Fournisseurs.Single(p => p.ID == id);

            if (
                fourni.ProductionBouchons.Count != 0 ||
                fourni.ProductionEtiquettes.Count != 0 ||
                fourni.ProductionFilmThermoes.Count != 0 ||
                fourni.ProductionSiroperies.Count != 0 ||
                fourni.ProductionSoufflages.Count != 0 ||
                //fourni.Ressources.Count != 0 ||
                fourni.TransfertMatieres.Count != 0
                )
            {

                this.ShowWarning("Impossible de suprimmer ce fournisseur car il est lié à d'autres éléments. Si vous voulez supprimer ce fournisseur, supprimez d'abord ces éléments.");
                return;
            }

            if (this.ConfirmWarning("Supprimer le Fournisseur " + fourni.Nom + " ?"))
            {
                db.DeleteObject(fourni);
                db.SaveChanges();
                RefreshGrid();
            }
        }

        private void btFermer_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void txtNom_TextChanged(object sender, EventArgs e)
        {
            RefreshGrid();
        }

        private void cbTypeRessource_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshGrid();
        }

        private void txtNom_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLower(e.KeyChar))
            {
                e.KeyChar = char.ToUpper(e.KeyChar);
            }
        }
    }
}
