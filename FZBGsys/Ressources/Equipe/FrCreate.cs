﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FZBGsys.NsRessources.NsEmployer;

namespace FZBGsys.NsRessources.NsEquipe
{
    public partial class FrCreate : Form
    {
        ModelEntities db = new ModelEntities();



        public FrCreate()
        {
            InitializeComponent();
            InitializeData();
        }

        private List<Employer> _ListeMembres;
        public List<Employer> ListeMembres
        {
            get { return _ListeMembres; }
            set { _ListeMembres = value; }
        }

        private void InitializeData()
        {
            cbSection.Items.Clear();
            cbSection.Items.Add(new Section { ID = 0, Nom = "[Choisir une Section]" });
            cbSection.Items.AddRange(db.Sections.Where(u => u.ID > 0).OrderBy(u => u.Nom).ToArray());
            cbSection.SelectedIndex = 0;
            cbSection.DropDownStyle = ComboBoxStyle.DropDownList;
            cbSection.ValueMember = "ID";
            cbSection.DisplayMember = "Nom";


            ListeMembres = new List<Employer>();



        }

        private void Create_FormClosing(object sender, FormClosingEventArgs e)
        {
            Dispose();
        }

        private void buttonParcourir_Click(object sender, EventArgs e)
        {
            var selectedEmployer = FZBGsys.NsRessources.NsEmployer.FrList.SelectEmployerOrNoneDialog(); // add Aucun
            if (selectedEmployer != null)
            {
                txtResponsable.Text = selectedEmployer.Nom;
                this.ResponsableID = selectedEmployer.Matricule;

            }

        }

        public string ResponsableID = null;

        private void buttonEnregistrer_Click(object sender, EventArgs e)
        {
            if (IsValidAll())
            {
                Equipe NewEquipe = new Equipe()
                {
                    Nom = txtNom.Text,
                    SectionID = ((Section)cbSection.SelectedItem).ID,
                    ResponsableID = this.ResponsableID,
                //    DateCreation = DateTime.Now

                };
                
                db.AddToEquipes(NewEquipe);
                db.SaveChanges();


                
                foreach (var item in this.ListeMembres)
                {
                    db.Employers.Single(em => em.Matricule==item.Matricule ).Equipe = NewEquipe;
                    
                }
                db.SaveChanges();
                Dispose();


            }
        }

        private bool IsValidAll()
        {
            string ErrorMessage = String.Empty;


            if (txtNom.Text.Length < 2 || txtNom.Text.Length > 20)
            {

                ErrorMessage += "\nCe Nom est Invalide (doit faire entre 2 et 20 caracteres";
            }
            else
            {
                var nom = txtNom.Text;
                var Equi = db.Equipes.FirstOrDefault(em => em.Nom == nom);

                if (Equi != null)
                {
                    ErrorMessage += "\nCe Nom existe déja !";

                }
            }


            if (cbSection.SelectedIndex == 0)
            {

                ErrorMessage += "\nAucune Section n'est séléctionée.";
            }
            /*
                        if (ResponsableID == null)
                        {
                            ErrorMessage += "\nResponsable non attribué!";
                        }
            */
            if (ErrorMessage != String.Empty)
            {
                Tools.ShowError(ErrorMessage);
                // MessageBox.Show(ErrorMessage, "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }

        private void buttonAnnuler_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var selectedEmployer = FZBGsys.NsRessources.NsEmployer.FrList.SelectEmployerDialog();
            if (selectedEmployer != null)
            {
                this.ListeMembres.Add(selectedEmployer);
                // dgv1.Rows.Clear();
                //dgv1.DataSource = dgv1.DataSource;
                dgv1.DataSource = ListeMembres.Select(val => new { Matricule = val.Matricule, Nom = val.Nom, Section = (val.Section!=null)?val.Section.Nom:null }).ToList();
            }
        }

    }
}
