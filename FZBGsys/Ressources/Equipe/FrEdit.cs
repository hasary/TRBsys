﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FZBGsys.NsRessources.NsEmployer;

namespace FZBGsys.NsRessources.NsEquipe
{
    public partial class FrEdit : Form
    {
        ModelEntities db = new ModelEntities();
        Equipe Model = new Equipe();
        public FrEdit(Equipe Model)
        {
            this.Model = Model;
            InitializeComponent();
            InitializeData();
        }

        private List<Employer> _ListeMembres;
        public List<Employer> ListeMembres
        {
            get { return _ListeMembres; }
            set { _ListeMembres = value;  }
        }

        private void InitializeData()
        {
            var listSection = db.Sections.OrderBy(u => u.Nom).ToList();
            cbSection.DataSource = listSection;
            cbSection.SelectedItem = listSection.Single(l => l.ID == Model.SectionID);
            cbSection.DisplayMember = "Nom";
            cbSection.ValueMember = "ID";
            cbSection.DropDownStyle = ComboBoxStyle.DropDownList;
            txtNom.Text = Model.Nom;
            txtResponsable.Text = "(Aucun).";

            if (Model.Employer != null)
            {
                txtResponsable.Text = Model.Employer.Nom; // Responsable
                this.ResponsableID = Model.Employer.Matricule; // ID Responsable

            }


            this.ListeMembres = db.Employers.Where(em => em.EquipeID == Model.ID).ToList();
            dgv1.DataSource = dgv1.DataSource = ListeMembres.Select(val => new { Matricule = val.Matricule, Nom = val.Nom, Section = (val.Section != null) ? val.Section.Nom : null }).ToList();
        }

        private void Create_FormClosing(object sender, FormClosingEventArgs e)
        {
            Dispose();
        }

        private void buttonParcourir_Click(object sender, EventArgs e)
        {
            var selectedEmployer = NsRessources.NsEmployer.FrList.SelectEmployerOrNoneDialog(); // possible selection aucun
            if (selectedEmployer != null)
            {
                txtResponsable.Text = selectedEmployer.Nom;
                this.ResponsableID = selectedEmployer.Matricule;

            }

        }

        public string ResponsableID = null;

        private void buttonEnregistrer_Click(object sender, EventArgs e)
        {
            if (this.IsValidAll())
            {
                var Model = db.Equipes.Single(eq => eq.ID == this.Model.ID); // peut pas Utiliser direcement Model.
                Model.ResponsableID = this.ResponsableID;
                Model.Nom = txtNom.Text;
                Model.SectionID = ((Section)cbSection.SelectedItem).ID;

                foreach (var item in this.ListeMembres)
                {
                     db.Employers.Single(em => em.Matricule==item.Matricule ).Equipe = Model;
                }


                db.SaveChanges();
                Dispose();


            }
        }

        private bool IsValidAll()
        {
            string ErrorMessage = String.Empty;


            if (txtNom.Text.Length < 1 || txtNom.Text.Length > 20)
            {

                ErrorMessage += "\nCe Nom est Invalide (doit faire entre 2 et 20 caracteres";
            }
            else
            {
                var nom = txtNom.Text;
                var Equi = db.Equipes.FirstOrDefault(em => em.Nom == nom && em.ID != this.Model.ID);

                if (Equi != null)
                {
                    ErrorMessage += "\nCe Nom existe déja !";

                }
            }
            /*
                        if (ResponsableID == null)
                        {
                            ErrorMessage += "\nResponsable n'est pas spécifié!";
                        }
            */
            if (ErrorMessage != String.Empty)
            {
                Tools.ShowError(ErrorMessage);
                // MessageBox.Show(ErrorMessage, "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }

        private void buttonAnnuler_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var selectedEmployer = NsRessources.NsEmployer.FrList.SelectEmployerDialog(); 
            if (selectedEmployer != null)
            {
                this.ListeMembres.Add(selectedEmployer);
                dgv1.DataSource = dgv1.DataSource = ListeMembres.Select(val => new { Matricule = val.Matricule, Nom = val.Nom, Section = (val.Section != null) ? val.Section.Nom : null }).ToList();
            }
        }



    }
}
