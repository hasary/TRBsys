﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//using MarbreBLIDA;

namespace FZBGsys.NsRessources.NsEmployer
{
    public partial class FrCreate : Form
    {
        public static Employer ReturnEmployer { get; set; }
        public bool IsSelectionDialogReturn { get; set; }
        private ModelEntities db = new ModelEntities();
        public FrCreate()
        {
            InitializeComponent();
            InitializeData();
            IsSelectionDialogReturn = false;
        }
        public static Employer CreateEmployerDialog()
        {
            FrCreate e =
            new FrCreate();
            e.IsSelectionDialogReturn = true;
            e.ShowDialog();

            return ReturnEmployer;

        }
        private void InitializeData()
        {

            cbSection.Items.Clear();
            cbSection.Items.Add(new Section { ID = 0, Nom = "[Choisir une Section]" });
            cbSection.Items.AddRange(db.Sections.Where(u => u.ID > 0).OrderBy(u=>u.Nom).ToArray());
            cbSection.SelectedIndex = 0;
            cbSection.DropDownStyle = ComboBoxStyle.DropDownList;
            cbSection.ValueMember = "ID";
            cbSection.DisplayMember = "Nom";

            /*
            cbEquipe.Items.Clear();
            cbEquipe.Items.Add(new Equipe { ID = 0, Nom = "[Choisir une Equipe]" });
            cbEquipe.Items.AddRange(db.Equipes.Where(u => u.ID > 0).OrderBy(u => u.Nom).ToArray());
            cbEquipe.SelectedIndex = 0;
            cbEquipe.DropDownStyle = ComboBoxStyle.DropDownList;
            cbEquipe.ValueMember = "ID";
            cbEquipe.DisplayMember = "Nom";
            */

            cbActivite.Items.Clear();
            cbActivite.Items.Add(new Activite { Code = 0, Description = "[Choisir une Activite]" });
            cbActivite.Items.AddRange(db.Activites.Where(u => u.Code > 0).OrderBy(u => u.Description).ToArray());
            cbActivite.SelectedIndex = 0;
            cbActivite.DropDownStyle = ComboBoxStyle.DropDownList;
            cbActivite.ValueMember = "Code";
            cbActivite.DisplayMember = "Description";

        }
        private void buttonEnregistrerEmployer_Click(object sender, EventArgs e)
        {
            if (isValidAll())
            {

               ReturnEmployer = new Employer()
                {
                    ActiviteID = ((Activite)cbActivite.SelectedItem).Code,
                    SectionID = ((Section)cbSection.SelectedItem).ID,
                 //   EquipeID = ((Equipe)cbEquipe.SelectedItem).ID,
                    Matricule = txtMatricule.Text,
                    Nom = txtNom.Text

                };

               db.AddToEmployers(ReturnEmployer);

                db.SaveChanges();
                
                Dispose();

            }
        }
        private bool isValidAll()
        {
            string ErrorMessage = String.Empty;


            if (txtMatricule.Text.Length < 1 || txtMatricule.Text.Length > 10)
            {

                ErrorMessage += "\nCe Matricule est Invalide (doit faire entre 2 et 10 caracteres).";
            }
            else
            {
                var matricule = txtMatricule.Text;
                var Uti = db.ApplicationUtilisateurs.FirstOrDefault(em => em.EmployerMatricule == matricule);

                if (Uti != null)
                {
                    ErrorMessage += "\nCe Matricule existe déja";

                }
            }


            if (txtNom.Text.Length > 69 || txtNom.Text.Length < 1)
            {
                ErrorMessage += "\n Ce Nom est Invalide (doit faire entre 2 et 70 caractères)";


            }

            if (cbSection.SelectedIndex == 0)
            {

                ErrorMessage += "\nAucune Section n'est séléctionée.";
            }
/*
            if (cbEquipe.SelectedIndex == 0)
            {

                ErrorMessage += "\nAucune Equipe n'est séléctionée.";
            }
*/
            if (cbActivite.SelectedIndex == 0)
            {

                ErrorMessage += "\nAucune Activite n'est séléctionée.";
            }


            if (ErrorMessage != String.Empty)
            {
                this.ShowError(ErrorMessage);
               // MessageBox.Show(ErrorMessage, "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }
        private void AdminEmployerNew_FormClosing(object sender, FormClosingEventArgs e)
        { if(!IsSelectionDialogReturn)
            db.Dispose();
        }
        private void buttonAnnulerEmployer_Click(object sender, EventArgs e)
        {
            ReturnEmployer = null;
            Dispose();
        }
    }
}
