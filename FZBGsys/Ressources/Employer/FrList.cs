﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Objects;

namespace FZBGsys.NsRessources.NsEmployer
{
    

    public partial class FrList : Form
    {
        ModelEntities db = new ModelEntities();
        public bool AddAucuneSelection { get; set; }

        public FrList()
        {
            this.InitType(false, false, false); // normal list
        }

        public FrList(bool IsSelectionDialog, bool AddAucuneSelection, bool IsOnlyNotUtilisateur)
        {
            this.InitType(IsSelectionDialog, AddAucuneSelection, IsOnlyNotUtilisateur);
        }

        private void InitType(bool IsSelectionDialog, bool AddAucuneSelection, bool IsOnlyNotUtilisateur)
        {
            this.IsSelectionDialog = IsSelectionDialog;
            this.AddAucuneSelection = AddAucuneSelection;
            this.IsOnlyNotUtilisateur = IsOnlyNotUtilisateur;
            InitializeComponent();
            panelEdit.Visible = !IsSelectionDialog;
            panelSelect.Visible = IsSelectionDialog;

            InitializeData();

        }


        private void InitializeData()
        {
            InitialiseGridView(0,String.Empty); // 0 means all sections

            cbSection.Items.Clear();
            cbSection.Items.Add(new Section { ID = 0, Nom = "[Tout]" });
            cbSection.Items.AddRange(db.Sections.Where(u => u.ID > 0).OrderBy(u => u.Nom).ToArray());
            cbSection.SelectedIndex = 0;
            cbSection.DropDownStyle = ComboBoxStyle.DropDownList;
            cbSection.ValueMember = "ID";
            cbSection.DisplayMember = "Nom";

        }

        private void InitialiseGridView(int SectionFilter, string NomFilter)
        {
            var Employers = db.Employers.Where(e => e.Matricule != "0").OrderBy(u => u.Matricule).ToList();

             if (SectionFilter != 0) {

                 Employers = Employers.Where(emp => emp.SectionID == SectionFilter).ToList();
            
            }

             if (NomFilter != String.Empty)
             {

                 Employers = Employers.Where(emp => emp.Nom.Contains(NomFilter)).ToList();

             }


            if (IsOnlyNotUtilisateur)
            {
                Employers = Employers.Where(e => db.ApplicationUtilisateurs.FirstOrDefault(u => u.EmployerMatricule == e.Matricule) == null).ToList();

            }
            var ListEmployers = Employers.OrderBy(data => int.Parse(data.Matricule)).Select(data => new { Matricule = data.Matricule, Nom = data.Nom, Section = (data.Section != null) ? data.Section.Nom : null, Equipe = (data.Equipe != null) ? data.Equipe.Nom : null, Fonction = data.Activite.Description }).ToList();
            if (AddAucuneSelection)
            {
                ListEmployers.Insert(0, new { Matricule = "(Aucun).", Nom = "", Section = "", Equipe = "", Fonction="" });

            }

            dgv1.DataSource = ListEmployers;
        }
        public static Employer SelectEmployerDialog()
        {

            FrList e =
            new FrList(true, false, false);
            // e.IsSelectionDialog = true;
            // e.AddAucuneSelection = false;


            e.ShowDialog();

            return ReturnEmployer;

        }
        private void AdminEmployer_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }
        private void buttonOKEmployer_Click(object sender, EventArgs e)
        {

            if (!AddAucuneSelection || dgv1.SelectedRows[0].Index != 0)
            {
                var matricule = dgv1.SelectedRows[0].Cells[0].Value.ToString();
                ReturnEmployer = db.Employers.Single(em => em.Matricule == matricule);
            }
            else
                ReturnEmployer = null; // en cas de selection aucun
            Dispose();
        }
        private static Employer ReturnEmployer { get; set; }
        public bool IsSelectionDialog { get; set; } // if control is called for selection only (not for editing)
        private void buttonAnnuler_Click(object sender, EventArgs e)
        {
            ReturnEmployer = null;
            Dispose();
        }
        private void buttonNewEmployer_Click(object sender, EventArgs e)
        {
            if (IsSelectionDialog)
            {
                ReturnEmployer = FrCreate.CreateEmployerDialog();

                Dispose(); // so return to Utilisateur Liste Selection,

            }
            else
            {

                new FrCreate().ShowDialog();

                InitializeData();
            }

        }
        private void buttonModifier_Click(object sender, EventArgs e)
        {
            var matricule = dgv1.SelectedRows[0].Cells[0].Value.ToString();
            var selectedEmployer = db.Employers.Single(em => em.Matricule == matricule);
            db.Refresh(RefreshMode.StoreWins, selectedEmployer);
            new Edit(selectedEmployer).ShowDialog();



            InitializeData();
        }
        private void buttonSupprimer_Click(object sender, EventArgs e)
        {
            var matricule = dgv1.SelectedRows[0].Cells[0].Value.ToString();
            var selectedEmployer = db.Employers.Single(em => em.Matricule == matricule);
            if (this.ConfirmWarning("Supprimer l'Employer " + matricule + "?"))
            {
                db.Employers.DeleteObject(selectedEmployer);
                db.SaveChanges();
                InitializeData();
            }
        }

        public static Employer SelectEmployerOrNoneDialog()
        {
            FrList e =
             new FrList(true, true, false);
            // e.IsSelectionDialog = true;
            // e.AddAucuneSelection = true;

            e.ShowDialog();

            return ReturnEmployer;

        }

        public bool IsOnlyNotUtilisateur { get; set; }

        public static Employer SelectEmployerNotUtilisateurDialog()
        {

            FrList e =
            new FrList(true, false, true);
            // e.IsSelectionDialog = true;
            // e.AddAucuneSelection = false;
            //   e.IsOnlyNotUtilisateur = true;

            e.ShowDialog();

            return ReturnEmployer;
        }

      

        private void cbSection_SelectedIndexChanged(object sender, EventArgs e)
        {
            var SelectedID =  ((Section)cbSection.SelectedItem).ID;
            txtNom.Text = String.Empty;
            InitialiseGridView(SelectedID, txtNom.Text);

        }

        private void txtNom_KeyPress(object sender, KeyPressEventArgs e)
        { var SelectedID =  ((Section)cbSection.SelectedItem).ID;
            InitialiseGridView(SelectedID,txtNom.Text);

          
        }
    }
}
