﻿namespace NsTRBsys
{
    partial class FrMain
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrMain));
            this.menuMain = new System.Windows.Forms.MenuStrip();
            this.Fichier = new System.Windows.Forms.ToolStripMenuItem();
            this.FichierConnecter = new System.Windows.Forms.ToolStripMenuItem();
            this.FichierDeconnecter = new System.Windows.Forms.ToolStripMenuItem();
            this.FichierQuitter = new System.Windows.Forms.ToolStripMenuItem();
            this.Arrivage = new System.Windows.Forms.ToolStripMenuItem();
            this.ArrivageMP = new System.Windows.Forms.ToolStripMenuItem();
            this.ArrivageListMP = new System.Windows.Forms.ToolStripMenuItem();
            this.ArrivagelistFourni = new System.Windows.Forms.ToolStripMenuItem();
            this.Production = new System.Windows.Forms.ToolStripMenuItem();
            this.ProductionPET = new System.Windows.Forms.ToolStripMenuItem();
            this.ProductionListPET = new System.Windows.Forms.ToolStripMenuItem();
            this.ProductionCan = new System.Windows.Forms.ToolStripMenuItem();
            this.ProductionListCan = new System.Windows.Forms.ToolStripMenuItem();
            this.Transfert = new System.Windows.Forms.ToolStripMenuItem();
            this.TransfertMT = new System.Windows.Forms.ToolStripMenuItem();
            this.versUnitéPETToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.versUnitéCanetteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.versAutresDéstinationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TransfertListMP = new System.Windows.Forms.ToolStripMenuItem();
            this.TransfertPF = new System.Windows.Forms.ToolStripMenuItem();
            this.listTransfertsPF = new System.Windows.Forms.ToolStripMenuItem();
            this.Stock = new System.Windows.Forms.ToolStripMenuItem();
            this.StockMP = new System.Windows.Forms.ToolStripMenuItem();
            this.StockPF = new System.Windows.Forms.ToolStripMenuItem();
            this.Comercial = new System.Windows.Forms.ToolStripMenuItem();
            this.ComercialBonAttrib = new System.Windows.Forms.ToolStripMenuItem();
            this.ComercialBonSort = new System.Windows.Forms.ToolStripMenuItem();
            this.ComercialFacture = new System.Windows.Forms.ToolStripMenuItem();
            this.ComercialBonLiv = new System.Windows.Forms.ToolStripMenuItem();
            this.ComercialCommand = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.CommercialListeBon = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesVersementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.ComercialListVentes = new System.Windows.Forms.ToolStripMenuItem();
            this.releveClientToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.ComercialClient = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesDéposToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nouveauClientToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesClientsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesChauffeurToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Rapports = new System.Windows.Forms.ToolStripMenuItem();
            this.RapportMP = new System.Windows.Forms.ToolStripMenuItem();
            this.matièrePremièreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.produitFiniToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RapportPF = new System.Windows.Forms.ToolStripMenuItem();
            this.etatDesStocksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.etatDesMouvementsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RapportProduction = new System.Windows.Forms.ToolStripMenuItem();
            this.transfertMatièrePremièreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RapportEtatVente = new System.Windows.Forms.ToolStripMenuItem();
            this.RapportActivite = new System.Windows.Forms.ToolStripMenuItem();
            this.Administration = new System.Windows.Forms.ToolStripMenuItem();
            this.AdministrationMaintenance = new System.Windows.Forms.ToolStripMenuItem();
            this.sauveguarderLaBaseDesDonnéesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.retorerLaBaseDesDonnéesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Aide = new System.Windows.Forms.ToolStripMenuItem();
            this.AideApropos = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBoxInfos = new System.Windows.Forms.GroupBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.LabSoldeClientC = new System.Windows.Forms.Label();
            this.LabSoldeClientD = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.labSoldeFournisseurC = new System.Windows.Forms.Label();
            this.labSoldeFournisseurD = new System.Windows.Forms.Label();
            this.labelUserSection = new System.Windows.Forms.Label();
            this.labeluserRole = new System.Windows.Forms.Label();
            this.labelUserName = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.flw = new System.Windows.Forms.FlowLayoutPanel();
            this.button9 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.bkAutoBackup = new System.ComponentModel.BackgroundWorker();
            this.bk1 = new System.ComponentModel.BackgroundWorker();
            this.menuMain.SuspendLayout();
            this.groupBoxInfos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.flw.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuMain
            // 
            this.menuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Fichier,
            this.Arrivage,
            this.Production,
            this.Transfert,
            this.Stock,
            this.Comercial,
            this.Rapports,
            this.Administration,
            this.Aide});
            this.menuMain.Location = new System.Drawing.Point(0, 0);
            this.menuMain.Name = "menuMain";
            this.menuMain.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuMain.Size = new System.Drawing.Size(1070, 28);
            this.menuMain.TabIndex = 0;
            this.menuMain.Text = "menuStrip1";
            this.menuMain.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuMain_ItemClicked);
            // 
            // Fichier
            // 
            this.Fichier.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FichierConnecter,
            this.FichierDeconnecter,
            this.FichierQuitter});
            this.Fichier.Name = "Fichier";
            this.Fichier.Size = new System.Drawing.Size(64, 24);
            this.Fichier.Text = "Fichier";
            // 
            // FichierConnecter
            // 
            this.FichierConnecter.Name = "FichierConnecter";
            this.FichierConnecter.Size = new System.Drawing.Size(162, 24);
            this.FichierConnecter.Text = "Connecter";
            this.FichierConnecter.Click += new System.EventHandler(this.FichierConnecter_Click);
            // 
            // FichierDeconnecter
            // 
            this.FichierDeconnecter.Name = "FichierDeconnecter";
            this.FichierDeconnecter.Size = new System.Drawing.Size(162, 24);
            this.FichierDeconnecter.Text = "Deconnecter";
            this.FichierDeconnecter.Click += new System.EventHandler(this.FichierDeconnecter_Click);
            // 
            // FichierQuitter
            // 
            this.FichierQuitter.Name = "FichierQuitter";
            this.FichierQuitter.Size = new System.Drawing.Size(162, 24);
            this.FichierQuitter.Text = "Quitter";
            this.FichierQuitter.Click += new System.EventHandler(this.FichierQuitter_Click);
            // 
            // Arrivage
            // 
            this.Arrivage.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ArrivageMP,
            this.ArrivageListMP,
            this.ArrivagelistFourni});
            this.Arrivage.Name = "Arrivage";
            this.Arrivage.Size = new System.Drawing.Size(77, 24);
            this.Arrivage.Text = "Arrivage";
            // 
            // ArrivageMP
            // 
            this.ArrivageMP.Name = "ArrivageMP";
            this.ArrivageMP.Size = new System.Drawing.Size(252, 24);
            this.ArrivageMP.Text = "Arrivage Matiere Première";
            this.ArrivageMP.Click += new System.EventHandler(this.matierePremiereToolStripMenuItem_Click);
            // 
            // ArrivageListMP
            // 
            this.ArrivageListMP.Name = "ArrivageListMP";
            this.ArrivageListMP.Size = new System.Drawing.Size(252, 24);
            this.ArrivageListMP.Text = "Liste Arrivage MP";
            this.ArrivageListMP.Click += new System.EventHandler(this.ArrivageList_Click);
            // 
            // ArrivagelistFourni
            // 
            this.ArrivagelistFourni.Name = "ArrivagelistFourni";
            this.ArrivagelistFourni.Size = new System.Drawing.Size(252, 24);
            this.ArrivagelistFourni.Text = "Liste des Fournisseurs";
            this.ArrivagelistFourni.Click += new System.EventHandler(this.ArrivagelistFourni_Click);
            // 
            // Production
            // 
            this.Production.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ProductionPET,
            this.ProductionListPET,
            this.ProductionCan,
            this.ProductionListCan});
            this.Production.Name = "Production";
            this.Production.Size = new System.Drawing.Size(93, 24);
            this.Production.Text = "Production";
            // 
            // ProductionPET
            // 
            this.ProductionPET.Name = "ProductionPET";
            this.ProductionPET.Size = new System.Drawing.Size(299, 24);
            this.ProductionPET.Text = "Production Unité PET";
            this.ProductionPET.Click += new System.EventHandler(this.ProductionPET_Click);
            // 
            // ProductionListPET
            // 
            this.ProductionListPET.Name = "ProductionListPET";
            this.ProductionListPET.Size = new System.Drawing.Size(299, 24);
            this.ProductionListPET.Text = "Liste de Productions PET";
            this.ProductionListPET.Click += new System.EventHandler(this.ProductionListPET_Click);
            // 
            // ProductionCan
            // 
            this.ProductionCan.Name = "ProductionCan";
            this.ProductionCan.Size = new System.Drawing.Size(299, 24);
            this.ProductionCan.Text = "Production Unité Canette";
            // 
            // ProductionListCan
            // 
            this.ProductionListCan.Name = "ProductionListCan";
            this.ProductionListCan.Size = new System.Drawing.Size(299, 24);
            this.ProductionListCan.Text = "Liste de Production Unité Canette";
            // 
            // Transfert
            // 
            this.Transfert.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TransfertMT,
            this.TransfertListMP,
            this.TransfertPF,
            this.listTransfertsPF});
            this.Transfert.Name = "Transfert";
            this.Transfert.Size = new System.Drawing.Size(79, 24);
            this.Transfert.Text = "Transfert";
            // 
            // TransfertMT
            // 
            this.TransfertMT.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.versUnitéPETToolStripMenuItem,
            this.versUnitéCanetteToolStripMenuItem,
            this.versAutresDéstinationsToolStripMenuItem});
            this.TransfertMT.Name = "TransfertMT";
            this.TransfertMT.Size = new System.Drawing.Size(254, 24);
            this.TransfertMT.Text = "Transfert Matiere Premiere";
            // 
            // versUnitéPETToolStripMenuItem
            // 
            this.versUnitéPETToolStripMenuItem.Name = "versUnitéPETToolStripMenuItem";
            this.versUnitéPETToolStripMenuItem.Size = new System.Drawing.Size(238, 24);
            this.versUnitéPETToolStripMenuItem.Text = "Vers Unité PET";
            this.versUnitéPETToolStripMenuItem.Click += new System.EventHandler(this.versUnitéPETToolStripMenuItem_Click);
            // 
            // versUnitéCanetteToolStripMenuItem
            // 
            this.versUnitéCanetteToolStripMenuItem.Name = "versUnitéCanetteToolStripMenuItem";
            this.versUnitéCanetteToolStripMenuItem.Size = new System.Drawing.Size(238, 24);
            this.versUnitéCanetteToolStripMenuItem.Text = "Vers Unité Canette";
            this.versUnitéCanetteToolStripMenuItem.Click += new System.EventHandler(this.versUnitéCanetteToolStripMenuItem_Click);
            // 
            // versAutresDéstinationsToolStripMenuItem
            // 
            this.versAutresDéstinationsToolStripMenuItem.Name = "versAutresDéstinationsToolStripMenuItem";
            this.versAutresDéstinationsToolStripMenuItem.Size = new System.Drawing.Size(238, 24);
            this.versAutresDéstinationsToolStripMenuItem.Text = "Vers Autres Déstinations";
            this.versAutresDéstinationsToolStripMenuItem.Click += new System.EventHandler(this.versAutresDéstinationsToolStripMenuItem_Click);
            // 
            // TransfertListMP
            // 
            this.TransfertListMP.Name = "TransfertListMP";
            this.TransfertListMP.Size = new System.Drawing.Size(254, 24);
            this.TransfertListMP.Text = "Liste des Transfets MP ";
            this.TransfertListMP.Click += new System.EventHandler(this.TransfertListMP_Click);
            // 
            // TransfertPF
            // 
            this.TransfertPF.Name = "TransfertPF";
            this.TransfertPF.Size = new System.Drawing.Size(254, 24);
            this.TransfertPF.Text = "Récéption Produit Fini";
            this.TransfertPF.Click += new System.EventHandler(this.TransfertPF_Click);
            // 
            // listTransfertsPF
            // 
            this.listTransfertsPF.Name = "listTransfertsPF";
            this.listTransfertsPF.Size = new System.Drawing.Size(254, 24);
            this.listTransfertsPF.Text = "Liste des Récéption PF";
            this.listTransfertsPF.Click += new System.EventHandler(this.listTransfertsPF_Click);
            // 
            // Stock
            // 
            this.Stock.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StockMP,
            this.StockPF});
            this.Stock.Name = "Stock";
            this.Stock.Size = new System.Drawing.Size(57, 24);
            this.Stock.Text = "Stock";
            // 
            // StockMP
            // 
            this.StockMP.Name = "StockMP";
            this.StockMP.Size = new System.Drawing.Size(232, 24);
            this.StockMP.Text = "Stock Matiere Première";
            this.StockMP.Click += new System.EventHandler(this.StockMP_Click);
            // 
            // StockPF
            // 
            this.StockPF.Name = "StockPF";
            this.StockPF.Size = new System.Drawing.Size(232, 24);
            this.StockPF.Text = "Stock Produit Fini";
            this.StockPF.Click += new System.EventHandler(this.StockPF_Click);
            // 
            // Comercial
            // 
            this.Comercial.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ComercialBonAttrib,
            this.ComercialBonSort,
            this.ComercialFacture,
            this.ComercialBonLiv,
            this.ComercialCommand,
            this.toolStripSeparator1,
            this.CommercialListeBon,
            this.listeDesVersementToolStripMenuItem,
            this.toolStripSeparator2,
            this.ComercialListVentes,
            this.releveClientToolStripMenuItem,
            this.toolStripSeparator3,
            this.ComercialClient});
            this.Comercial.Name = "Comercial";
            this.Comercial.Size = new System.Drawing.Size(88, 24);
            this.Comercial.Text = "Comercial";
            // 
            // ComercialBonAttrib
            // 
            this.ComercialBonAttrib.Name = "ComercialBonAttrib";
            this.ComercialBonAttrib.Size = new System.Drawing.Size(215, 24);
            this.ComercialBonAttrib.Text = "Bon d\'Atribution";
            this.ComercialBonAttrib.Click += new System.EventHandler(this.ComercialBonAttrib_Click);
            // 
            // ComercialBonSort
            // 
            this.ComercialBonSort.Name = "ComercialBonSort";
            this.ComercialBonSort.Size = new System.Drawing.Size(215, 24);
            this.ComercialBonSort.Text = "Bon de Sortie";
            this.ComercialBonSort.Click += new System.EventHandler(this.ComercialBonSort_Click);
            // 
            // ComercialFacture
            // 
            this.ComercialFacture.BackColor = System.Drawing.SystemColors.Control;
            this.ComercialFacture.Name = "ComercialFacture";
            this.ComercialFacture.Size = new System.Drawing.Size(215, 24);
            this.ComercialFacture.Text = "Facturation";
            this.ComercialFacture.Click += new System.EventHandler(this.ComercialFacture_Click);
            // 
            // ComercialBonLiv
            // 
            this.ComercialBonLiv.Name = "ComercialBonLiv";
            this.ComercialBonLiv.Size = new System.Drawing.Size(215, 24);
            this.ComercialBonLiv.Text = "Bon de Livraison";
            this.ComercialBonLiv.Click += new System.EventHandler(this.ComercialBonLiv_Click);
            // 
            // ComercialCommand
            // 
            this.ComercialCommand.BackColor = System.Drawing.Color.AliceBlue;
            this.ComercialCommand.Name = "ComercialCommand";
            this.ComercialCommand.Size = new System.Drawing.Size(215, 24);
            this.ComercialCommand.Text = "Versement Client";
            this.ComercialCommand.Click += new System.EventHandler(this.ComercialCommand_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(212, 6);
            // 
            // CommercialListeBon
            // 
            this.CommercialListeBon.BackColor = System.Drawing.SystemColors.Control;
            this.CommercialListeBon.Name = "CommercialListeBon";
            this.CommercialListeBon.Size = new System.Drawing.Size(215, 24);
            this.CommercialListeBon.Text = "Liste des Livraisons";
            this.CommercialListeBon.Click += new System.EventHandler(this.CommercialListeBon_Click);
            // 
            // listeDesVersementToolStripMenuItem
            // 
            this.listeDesVersementToolStripMenuItem.BackColor = System.Drawing.Color.AliceBlue;
            this.listeDesVersementToolStripMenuItem.Name = "listeDesVersementToolStripMenuItem";
            this.listeDesVersementToolStripMenuItem.Size = new System.Drawing.Size(215, 24);
            this.listeDesVersementToolStripMenuItem.Text = "Liste des Versements";
            this.listeDesVersementToolStripMenuItem.Click += new System.EventHandler(this.listeDesVersementToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(212, 6);
            // 
            // ComercialListVentes
            // 
            this.ComercialListVentes.Name = "ComercialListVentes";
            this.ComercialListVentes.Size = new System.Drawing.Size(215, 24);
            this.ComercialListVentes.Text = "Rapport des Ventes";
            this.ComercialListVentes.Click += new System.EventHandler(this.ComercialListVentes_Click);
            // 
            // releveClientToolStripMenuItem
            // 
            this.releveClientToolStripMenuItem.Name = "releveClientToolStripMenuItem";
            this.releveClientToolStripMenuItem.Size = new System.Drawing.Size(215, 24);
            this.releveClientToolStripMenuItem.Text = "Releve Client";
            this.releveClientToolStripMenuItem.Click += new System.EventHandler(this.releveClientToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(212, 6);
            // 
            // ComercialClient
            // 
            this.ComercialClient.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listeDesDéposToolStripMenuItem,
            this.nouveauClientToolStripMenuItem,
            this.listeDesClientsToolStripMenuItem,
            this.listeDesChauffeurToolStripMenuItem});
            this.ComercialClient.Name = "ComercialClient";
            this.ComercialClient.Size = new System.Drawing.Size(215, 24);
            this.ComercialClient.Text = "Gestion ";
            // 
            // listeDesDéposToolStripMenuItem
            // 
            this.listeDesDéposToolStripMenuItem.Name = "listeDesDéposToolStripMenuItem";
            this.listeDesDéposToolStripMenuItem.Size = new System.Drawing.Size(203, 24);
            this.listeDesDéposToolStripMenuItem.Text = "Liste des Dépos";
            this.listeDesDéposToolStripMenuItem.Click += new System.EventHandler(this.listeDesDéposToolStripMenuItem_Click);
            // 
            // nouveauClientToolStripMenuItem
            // 
            this.nouveauClientToolStripMenuItem.Name = "nouveauClientToolStripMenuItem";
            this.nouveauClientToolStripMenuItem.Size = new System.Drawing.Size(203, 24);
            this.nouveauClientToolStripMenuItem.Text = "Liste des Produits";
            this.nouveauClientToolStripMenuItem.Click += new System.EventHandler(this.nouveauClientToolStripMenuItem_Click);
            // 
            // listeDesClientsToolStripMenuItem
            // 
            this.listeDesClientsToolStripMenuItem.Name = "listeDesClientsToolStripMenuItem";
            this.listeDesClientsToolStripMenuItem.Size = new System.Drawing.Size(203, 24);
            this.listeDesClientsToolStripMenuItem.Text = "Liste des Clients";
            this.listeDesClientsToolStripMenuItem.Click += new System.EventHandler(this.listeDesClientsToolStripMenuItem_Click);
            // 
            // listeDesChauffeurToolStripMenuItem
            // 
            this.listeDesChauffeurToolStripMenuItem.Name = "listeDesChauffeurToolStripMenuItem";
            this.listeDesChauffeurToolStripMenuItem.Size = new System.Drawing.Size(203, 24);
            this.listeDesChauffeurToolStripMenuItem.Text = "Liste des Chauffeur";
            this.listeDesChauffeurToolStripMenuItem.Click += new System.EventHandler(this.listeDesChauffeurToolStripMenuItem_Click);
            // 
            // Rapports
            // 
            this.Rapports.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.RapportMP,
            this.RapportPF,
            this.RapportProduction,
            this.RapportEtatVente,
            this.RapportActivite});
            this.Rapports.Name = "Rapports";
            this.Rapports.Size = new System.Drawing.Size(75, 24);
            this.Rapports.Text = "Rapport";
            // 
            // RapportMP
            // 
            this.RapportMP.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.matièrePremièreToolStripMenuItem,
            this.produitFiniToolStripMenuItem});
            this.RapportMP.Name = "RapportMP";
            this.RapportMP.Size = new System.Drawing.Size(196, 24);
            this.RapportMP.Text = "Matière Première ";
            // 
            // matièrePremièreToolStripMenuItem
            // 
            this.matièrePremièreToolStripMenuItem.Name = "matièrePremièreToolStripMenuItem";
            this.matièrePremièreToolStripMenuItem.Size = new System.Drawing.Size(220, 24);
            this.matièrePremièreToolStripMenuItem.Text = "Etat du stock";
            this.matièrePremièreToolStripMenuItem.Click += new System.EventHandler(this.matièrePremièreToolStripMenuItem_Click);
            // 
            // produitFiniToolStripMenuItem
            // 
            this.produitFiniToolStripMenuItem.Name = "produitFiniToolStripMenuItem";
            this.produitFiniToolStripMenuItem.Size = new System.Drawing.Size(220, 24);
            this.produitFiniToolStripMenuItem.Text = "Etat des Mouvements";
            // 
            // RapportPF
            // 
            this.RapportPF.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.etatDesStocksToolStripMenuItem,
            this.etatDesMouvementsToolStripMenuItem});
            this.RapportPF.Name = "RapportPF";
            this.RapportPF.Size = new System.Drawing.Size(196, 24);
            this.RapportPF.Text = "Produit Fini";
            this.RapportPF.Click += new System.EventHandler(this.RapportPF_Click);
            // 
            // etatDesStocksToolStripMenuItem
            // 
            this.etatDesStocksToolStripMenuItem.Name = "etatDesStocksToolStripMenuItem";
            this.etatDesStocksToolStripMenuItem.Size = new System.Drawing.Size(220, 24);
            this.etatDesStocksToolStripMenuItem.Text = "Etat des Stocks";
            this.etatDesStocksToolStripMenuItem.Click += new System.EventHandler(this.etatDesStocksToolStripMenuItem_Click);
            // 
            // etatDesMouvementsToolStripMenuItem
            // 
            this.etatDesMouvementsToolStripMenuItem.Name = "etatDesMouvementsToolStripMenuItem";
            this.etatDesMouvementsToolStripMenuItem.Size = new System.Drawing.Size(220, 24);
            this.etatDesMouvementsToolStripMenuItem.Text = "Etat des Mouvements";
            // 
            // RapportProduction
            // 
            this.RapportProduction.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.transfertMatièrePremièreToolStripMenuItem,
            this.transToolStripMenuItem});
            this.RapportProduction.Name = "RapportProduction";
            this.RapportProduction.Size = new System.Drawing.Size(196, 24);
            this.RapportProduction.Text = "Production";
            this.RapportProduction.Click += new System.EventHandler(this.RapportProduction_Click);
            // 
            // transfertMatièrePremièreToolStripMenuItem
            // 
            this.transfertMatièrePremièreToolStripMenuItem.Name = "transfertMatièrePremièreToolStripMenuItem";
            this.transfertMatièrePremièreToolStripMenuItem.Size = new System.Drawing.Size(176, 24);
            this.transfertMatièrePremièreToolStripMenuItem.Text = "Unité PET";
            // 
            // transToolStripMenuItem
            // 
            this.transToolStripMenuItem.Name = "transToolStripMenuItem";
            this.transToolStripMenuItem.Size = new System.Drawing.Size(176, 24);
            this.transToolStripMenuItem.Text = "Unité Cannette";
            // 
            // RapportEtatVente
            // 
            this.RapportEtatVente.Name = "RapportEtatVente";
            this.RapportEtatVente.Size = new System.Drawing.Size(196, 24);
            this.RapportEtatVente.Text = "Etat des Ventes";
            // 
            // RapportActivite
            // 
            this.RapportActivite.Name = "RapportActivite";
            this.RapportActivite.Size = new System.Drawing.Size(196, 24);
            this.RapportActivite.Text = "Rapport d\'activité";
            // 
            // Administration
            // 
            this.Administration.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AdministrationMaintenance});
            this.Administration.ForeColor = System.Drawing.Color.Crimson;
            this.Administration.Name = "Administration";
            this.Administration.Size = new System.Drawing.Size(119, 24);
            this.Administration.Text = "Administration";
            // 
            // AdministrationMaintenance
            // 
            this.AdministrationMaintenance.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sauveguarderLaBaseDesDonnéesToolStripMenuItem,
            this.retorerLaBaseDesDonnéesToolStripMenuItem});
            this.AdministrationMaintenance.Name = "AdministrationMaintenance";
            this.AdministrationMaintenance.Size = new System.Drawing.Size(163, 24);
            this.AdministrationMaintenance.Text = "Maintenance";
            this.AdministrationMaintenance.Click += new System.EventHandler(this.AdministrationMaintenance_Click);
            // 
            // sauveguarderLaBaseDesDonnéesToolStripMenuItem
            // 
            this.sauveguarderLaBaseDesDonnéesToolStripMenuItem.Name = "sauveguarderLaBaseDesDonnéesToolStripMenuItem";
            this.sauveguarderLaBaseDesDonnéesToolStripMenuItem.Size = new System.Drawing.Size(307, 24);
            this.sauveguarderLaBaseDesDonnéesToolStripMenuItem.Text = "Sauveguarder la Base des données";
            this.sauveguarderLaBaseDesDonnéesToolStripMenuItem.Click += new System.EventHandler(this.sauveguarderLaBaseDesDonnéesToolStripMenuItem_Click);
            // 
            // retorerLaBaseDesDonnéesToolStripMenuItem
            // 
            this.retorerLaBaseDesDonnéesToolStripMenuItem.Name = "retorerLaBaseDesDonnéesToolStripMenuItem";
            this.retorerLaBaseDesDonnéesToolStripMenuItem.Size = new System.Drawing.Size(307, 24);
            this.retorerLaBaseDesDonnéesToolStripMenuItem.Text = "Restaurer la Base des données";
            this.retorerLaBaseDesDonnéesToolStripMenuItem.Click += new System.EventHandler(this.retorerLaBaseDesDonnéesToolStripMenuItem_Click);
            // 
            // Aide
            // 
            this.Aide.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AideApropos});
            this.Aide.Name = "Aide";
            this.Aide.Size = new System.Drawing.Size(52, 24);
            this.Aide.Text = "Aide";
            // 
            // AideApropos
            // 
            this.AideApropos.Name = "AideApropos";
            this.AideApropos.Size = new System.Drawing.Size(137, 24);
            this.AideApropos.Text = "à propos";
            this.AideApropos.Click += new System.EventHandler(this.AideApropos_Click);
            // 
            // groupBoxInfos
            // 
            this.groupBoxInfos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxInfos.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBoxInfos.BackColor = System.Drawing.Color.White;
            this.groupBoxInfos.Controls.Add(this.pictureBox3);
            this.groupBoxInfos.Controls.Add(this.LabSoldeClientC);
            this.groupBoxInfos.Controls.Add(this.LabSoldeClientD);
            this.groupBoxInfos.Controls.Add(this.pictureBox2);
            this.groupBoxInfos.Controls.Add(this.labSoldeFournisseurC);
            this.groupBoxInfos.Controls.Add(this.labSoldeFournisseurD);
            this.groupBoxInfos.Controls.Add(this.labelUserSection);
            this.groupBoxInfos.Controls.Add(this.labeluserRole);
            this.groupBoxInfos.Controls.Add(this.labelUserName);
            this.groupBoxInfos.Controls.Add(this.button1);
            this.groupBoxInfos.Location = new System.Drawing.Point(0, 418);
            this.groupBoxInfos.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBoxInfos.Name = "groupBoxInfos";
            this.groupBoxInfos.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBoxInfos.Size = new System.Drawing.Size(1070, 135);
            this.groupBoxInfos.TabIndex = 2;
            this.groupBoxInfos.TabStop = false;
            this.groupBoxInfos.Text = "Connecté";
            this.groupBoxInfos.Enter += new System.EventHandler(this.groupBoxInfos_Enter);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::NsTRBsys.Properties.Resources.Utulisateur1;
            this.pictureBox3.Location = new System.Drawing.Point(379, 68);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(39, 34);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 7;
            this.pictureBox3.TabStop = false;
            // 
            // LabSoldeClientC
            // 
            this.LabSoldeClientC.AutoSize = true;
            this.LabSoldeClientC.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabSoldeClientC.Location = new System.Drawing.Point(425, 90);
            this.LabSoldeClientC.Name = "LabSoldeClientC";
            this.LabSoldeClientC.Size = new System.Drawing.Size(62, 17);
            this.LabSoldeClientC.TabIndex = 6;
            this.LabSoldeClientC.Text = "label2";
            this.LabSoldeClientC.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LabSoldeClientD
            // 
            this.LabSoldeClientD.AutoSize = true;
            this.LabSoldeClientD.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabSoldeClientD.Location = new System.Drawing.Point(425, 71);
            this.LabSoldeClientD.Name = "LabSoldeClientD";
            this.LabSoldeClientD.Size = new System.Drawing.Size(62, 17);
            this.LabSoldeClientD.TabIndex = 6;
            this.LabSoldeClientD.Text = "label2";
            this.LabSoldeClientD.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::NsTRBsys.Properties.Resources.Fournisseur1;
            this.pictureBox2.Location = new System.Drawing.Point(379, 23);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(39, 34);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // labSoldeFournisseurC
            // 
            this.labSoldeFournisseurC.AutoSize = true;
            this.labSoldeFournisseurC.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labSoldeFournisseurC.Location = new System.Drawing.Point(425, 43);
            this.labSoldeFournisseurC.Name = "labSoldeFournisseurC";
            this.labSoldeFournisseurC.Size = new System.Drawing.Size(62, 17);
            this.labSoldeFournisseurC.TabIndex = 4;
            this.labSoldeFournisseurC.Text = "label1";
            this.labSoldeFournisseurC.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labSoldeFournisseurD
            // 
            this.labSoldeFournisseurD.AutoSize = true;
            this.labSoldeFournisseurD.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labSoldeFournisseurD.Location = new System.Drawing.Point(425, 23);
            this.labSoldeFournisseurD.Name = "labSoldeFournisseurD";
            this.labSoldeFournisseurD.Size = new System.Drawing.Size(62, 17);
            this.labSoldeFournisseurD.TabIndex = 4;
            this.labSoldeFournisseurD.Text = "label1";
            this.labSoldeFournisseurD.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelUserSection
            // 
            this.labelUserSection.AutoSize = true;
            this.labelUserSection.Location = new System.Drawing.Point(11, 84);
            this.labelUserSection.Name = "labelUserSection";
            this.labelUserSection.Size = new System.Drawing.Size(55, 17);
            this.labelUserSection.TabIndex = 2;
            this.labelUserSection.Text = "Section";
            // 
            // labeluserRole
            // 
            this.labeluserRole.AutoSize = true;
            this.labeluserRole.Location = new System.Drawing.Point(11, 66);
            this.labeluserRole.Name = "labeluserRole";
            this.labeluserRole.Size = new System.Drawing.Size(65, 17);
            this.labeluserRole.TabIndex = 1;
            this.labeluserRole.Text = "userRole";
            // 
            // labelUserName
            // 
            this.labelUserName.AutoSize = true;
            this.labelUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUserName.Location = new System.Drawing.Point(12, 38);
            this.labelUserName.Name = "labelUserName";
            this.labelUserName.Size = new System.Drawing.Size(110, 25);
            this.labelUserName.TabIndex = 0;
            this.labelUserName.Text = "userName";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.Location = new System.Drawing.Point(914, 94);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(144, 30);
            this.button1.TabIndex = 3;
            this.button1.Text = "Deconnexion";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // flw
            // 
            this.flw.BackColor = System.Drawing.Color.White;
            this.flw.Controls.Add(this.button9);
            this.flw.Controls.Add(this.button2);
            this.flw.Controls.Add(this.button3);
            this.flw.Controls.Add(this.button10);
            this.flw.Controls.Add(this.button4);
            this.flw.Controls.Add(this.button5);
            this.flw.Controls.Add(this.button13);
            this.flw.Controls.Add(this.button8);
            this.flw.Controls.Add(this.button14);
            this.flw.Controls.Add(this.button15);
            this.flw.Controls.Add(this.button12);
            this.flw.Controls.Add(this.button7);
            this.flw.Controls.Add(this.button11);
            this.flw.Controls.Add(this.button6);
            this.flw.Location = new System.Drawing.Point(0, 31);
            this.flw.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.flw.Name = "flw";
            this.flw.Size = new System.Drawing.Size(1070, 383);
            this.flw.TabIndex = 3;
            this.flw.VisibleChanged += new System.EventHandler(this.flw_VisibleChanged);
            this.flw.Paint += new System.Windows.Forms.PaintEventHandler(this.flw_Paint);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.White;
            this.button9.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.button9.Image = global::NsTRBsys.Properties.Resources.Vente;
            this.button9.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button9.Location = new System.Drawing.Point(3, 2);
            this.button9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(127, 133);
            this.button9.TabIndex = 8;
            this.button9.Text = "Dépos";
            this.button9.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.White;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.button2.Image = global::NsTRBsys.Properties.Resources.pofuna;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button2.Location = new System.Drawing.Point(136, 2);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(139, 150);
            this.button2.TabIndex = 0;
            this.button2.Text = "Produits et Articles";
            this.button2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_2);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.White;
            this.button3.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.button3.Image = global::NsTRBsys.Properties.Resources.Utulisateur1;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button3.Location = new System.Drawing.Point(281, 2);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(117, 150);
            this.button3.TabIndex = 1;
            this.button3.Text = "Clients  / Partenaires";
            this.button3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.White;
            this.button10.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.button10.Image = global::NsTRBsys.Properties.Resources.Fournisseur1;
            this.button10.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button10.Location = new System.Drawing.Point(404, 2);
            this.button10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(131, 150);
            this.button10.TabIndex = 1;
            this.button10.Text = "Fournisseurs (Usines)";
            this.button10.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button10_Click_1);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.White;
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.button4.Image = global::NsTRBsys.Properties.Resources.Rayon;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button4.Location = new System.Drawing.Point(541, 2);
            this.button4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(117, 150);
            this.button4.TabIndex = 2;
            this.button4.Text = "Livraison / récéption";
            this.button4.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.White;
            this.button5.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.button5.Image = global::NsTRBsys.Properties.Resources.Stock;
            this.button5.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button5.Location = new System.Drawing.Point(664, 2);
            this.button5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(175, 150);
            this.button5.TabIndex = 3;
            this.button5.Text = "Reçu Versement Client / Partenaire";
            this.button5.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.White;
            this.button13.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.button13.Image = global::NsTRBsys.Properties.Resources.cal2;
            this.button13.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button13.Location = new System.Drawing.Point(845, 2);
            this.button13.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(156, 170);
            this.button13.TabIndex = 12;
            this.button13.Text = "Versement Frounisseurs / Partenaire";
            this.button13.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.White;
            this.button8.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.button8.Image = global::NsTRBsys.Properties.Resources.relfou;
            this.button8.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button8.Location = new System.Drawing.Point(3, 176);
            this.button8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(127, 162);
            this.button8.TabIndex = 6;
            this.button8.Text = "Releve Client / Partenaire";
            this.button8.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.White;
            this.button14.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button14.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button14.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.button14.Image = global::NsTRBsys.Properties.Resources.cal6;
            this.button14.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button14.Location = new System.Drawing.Point(136, 176);
            this.button14.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(128, 162);
            this.button14.TabIndex = 13;
            this.button14.Text = "Releve Fournisseur";
            this.button14.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button14.UseVisualStyleBackColor = false;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.White;
            this.button15.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button15.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button15.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.button15.Image = global::NsTRBsys.Properties.Resources.fiko;
            this.button15.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button15.Location = new System.Drawing.Point(270, 176);
            this.button15.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(128, 162);
            this.button15.TabIndex = 1;
            this.button15.Text = "Statistiques des Ventes";
            this.button15.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button15.UseVisualStyleBackColor = false;
            this.button15.Click += new System.EventHandler(this.button3_Click);
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.White;
            this.button12.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.button12.Image = global::NsTRBsys.Properties.Resources.Correction;
            this.button12.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button12.Location = new System.Drawing.Point(404, 176);
            this.button12.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(131, 162);
            this.button12.TabIndex = 11;
            this.button12.Text = "Situation Comerciale";
            this.button12.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.White;
            this.button7.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.button7.Image = global::NsTRBsys.Properties.Resources.Arrivage;
            this.button7.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button7.Location = new System.Drawing.Point(541, 176);
            this.button7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(117, 162);
            this.button7.TabIndex = 5;
            this.button7.Text = "Liste Livraisons";
            this.button7.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.White;
            this.button11.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.button11.Image = global::NsTRBsys.Properties.Resources.fourn;
            this.button11.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button11.Location = new System.Drawing.Point(664, 176);
            this.button11.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(196, 150);
            this.button11.TabIndex = 10;
            this.button11.Text = "Liste Versement";
            this.button11.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.White;
            this.button6.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.button6.Image = global::NsTRBsys.Properties.Resources.Params1;
            this.button6.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button6.Location = new System.Drawing.Point(866, 176);
            this.button6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(119, 150);
            this.button6.TabIndex = 11;
            this.button6.Text = "Paramètres";
            this.button6.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click_1);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::NsTRBsys.Properties.Resources.wallpaper_1673198;
            this.pictureBox1.Location = new System.Drawing.Point(0, 28);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1070, 526);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // bkAutoBackup
            // 
            this.bkAutoBackup.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bkAutoBackup_DoWork);
            // 
            // bk1
            // 
            this.bk1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bk1_DoWork);
            // 
            // FrMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1070, 554);
            this.Controls.Add(this.flw);
            this.Controls.Add(this.groupBoxInfos);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuMain;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.Name = "FrMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gestion de distribution Boissons";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrMain_FormClosing);
            this.Load += new System.EventHandler(this.FrMain_Load);
            this.Shown += new System.EventHandler(this.FrMain_Shown);
            this.menuMain.ResumeLayout(false);
            this.menuMain.PerformLayout();
            this.groupBoxInfos.ResumeLayout(false);
            this.groupBoxInfos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.flw.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuMain;
        private System.Windows.Forms.ToolStripMenuItem Fichier;
        private System.Windows.Forms.ToolStripMenuItem Arrivage;
        private System.Windows.Forms.ToolStripMenuItem ArrivageMP;
        private System.Windows.Forms.ToolStripMenuItem Production;
        private System.Windows.Forms.ToolStripMenuItem Transfert;
        private System.Windows.Forms.ToolStripMenuItem Rapports;
        private System.Windows.Forms.ToolStripMenuItem Comercial;
        private System.Windows.Forms.ToolStripMenuItem Administration;
        private System.Windows.Forms.ToolStripMenuItem Aide;
        private System.Windows.Forms.ToolStripMenuItem FichierConnecter;
        private System.Windows.Forms.ToolStripMenuItem FichierDeconnecter;
        private System.Windows.Forms.ToolStripMenuItem FichierQuitter;
        private System.Windows.Forms.ToolStripMenuItem AideApropos;
        private System.Windows.Forms.ToolStripMenuItem ArrivageListMP;
        private System.Windows.Forms.GroupBox groupBoxInfos;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label labelUserSection;
        private System.Windows.Forms.Label labeluserRole;
        private System.Windows.Forms.Label labelUserName;
        private System.Windows.Forms.ToolStripMenuItem ProductionPET;
        private System.Windows.Forms.ToolStripMenuItem ProductionListPET;
        private System.Windows.Forms.ToolStripMenuItem ProductionCan;
        private System.Windows.Forms.ToolStripMenuItem ProductionListCan;
        private System.Windows.Forms.ToolStripMenuItem TransfertMT;
        private System.Windows.Forms.ToolStripMenuItem TransfertListMP;
        private System.Windows.Forms.ToolStripMenuItem Stock;
        private System.Windows.Forms.ToolStripMenuItem StockMP;
        private System.Windows.Forms.ToolStripMenuItem StockPF;
        private System.Windows.Forms.ToolStripMenuItem ComercialBonAttrib;
        private System.Windows.Forms.ToolStripMenuItem ComercialBonSort;
        private System.Windows.Forms.ToolStripMenuItem ComercialFacture;
        private System.Windows.Forms.ToolStripMenuItem ComercialBonLiv;
        private System.Windows.Forms.ToolStripMenuItem ComercialListVentes;
        private System.Windows.Forms.ToolStripMenuItem ComercialCommand;
        private System.Windows.Forms.ToolStripMenuItem TransfertPF;
        private System.Windows.Forms.ToolStripMenuItem listTransfertsPF;
        private System.Windows.Forms.ToolStripMenuItem versUnitéPETToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem versUnitéCanetteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem versAutresDéstinationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ComercialClient;
        private System.Windows.Forms.ToolStripMenuItem nouveauClientToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeDesClientsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeDesChauffeurToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RapportMP;
        private System.Windows.Forms.ToolStripMenuItem matièrePremièreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem produitFiniToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RapportProduction;
        private System.Windows.Forms.ToolStripMenuItem transfertMatièrePremièreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RapportEtatVente;
        private System.Windows.Forms.ToolStripMenuItem RapportActivite;
        private System.Windows.Forms.ToolStripMenuItem AdministrationMaintenance;
        private System.Windows.Forms.ToolStripMenuItem CommercialListeBon;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem RapportPF;
        private System.Windows.Forms.ToolStripMenuItem etatDesStocksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem etatDesMouvementsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ArrivagelistFourni;
        private System.Windows.Forms.ToolStripMenuItem releveClientToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeDesVersementToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.FlowLayoutPanel flw;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label labSoldeFournisseurD;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label LabSoldeClientD;
        private System.Windows.Forms.Label LabSoldeClientC;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.ToolStripMenuItem listeDesDéposToolStripMenuItem;
        private System.Windows.Forms.Label labSoldeFournisseurC;
        private System.Windows.Forms.ToolStripMenuItem sauveguarderLaBaseDesDonnéesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem retorerLaBaseDesDonnéesToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker bkAutoBackup;
        private System.ComponentModel.BackgroundWorker bk1;
    }
}

