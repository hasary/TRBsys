﻿namespace NsTRBsys.NsAdministration.NsUtilisateur
{
    partial class FrEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chActive = new System.Windows.Forms.CheckBox();
            this.txtPassRe = new NsTRBsys.TextBoxx();
            this.txtPass = new NsTRBsys.TextBoxx();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNom = new NsTRBsys.TextBoxx();
            this.buttonEnregistrer = new System.Windows.Forms.Button();
            this.ButtonAnnuler = new System.Windows.Forms.Button();
            this.cbGroupe = new System.Windows.Forms.ComboBox();
            this.labelGroupe = new System.Windows.Forms.Label();
            this.txtUserName = new NsTRBsys.TextBoxx();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonThis = new System.Windows.Forms.Button();
            this.txtWinUser = new NsTRBsys.TextBoxx();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // chActive
            // 
            this.chActive.AutoSize = true;
            this.chActive.Checked = true;
            this.chActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chActive.Location = new System.Drawing.Point(18, 221);
            this.chActive.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chActive.Name = "chActive";
            this.chActive.Size = new System.Drawing.Size(126, 17);
            this.chActive.TabIndex = 26;
            this.chActive.Text = "Le compte est Activé";
            this.chActive.UseVisualStyleBackColor = true;
            // 
            // txtPassRe
            // 
            this.txtPassRe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPassRe.Location = new System.Drawing.Point(141, 130);
            this.txtPassRe.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtPassRe.Name = "txtPassRe";
            this.txtPassRe.Size = new System.Drawing.Size(157, 20);
            this.txtPassRe.TabIndex = 18;
            this.txtPassRe.UseSystemPasswordChar = true;
            // 
            // txtPass
            // 
            this.txtPass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPass.Location = new System.Drawing.Point(141, 106);
            this.txtPass.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtPass.Name = "txtPass";
            this.txtPass.PasswordChar = '#';
            this.txtPass.Size = new System.Drawing.Size(157, 20);
            this.txtPass.TabIndex = 17;
            this.txtPass.UseSystemPasswordChar = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 28);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 25;
            this.label5.Text = "Matricule";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 55);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Nom / Prenom";
            // 
            // txtNom
            // 
            this.txtNom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNom.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNom.Location = new System.Drawing.Point(141, 53);
            this.txtNom.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtNom.Name = "txtNom";
            this.txtNom.ReadOnly = true;
            this.txtNom.Size = new System.Drawing.Size(157, 19);
            this.txtNom.TabIndex = 21;
            // 
            // buttonEnregistrer
            // 
            this.buttonEnregistrer.Location = new System.Drawing.Point(230, 250);
            this.buttonEnregistrer.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonEnregistrer.Name = "buttonEnregistrer";
            this.buttonEnregistrer.Size = new System.Drawing.Size(81, 26);
            this.buttonEnregistrer.TabIndex = 22;
            this.buttonEnregistrer.Text = "Enregistrer";
            this.buttonEnregistrer.UseVisualStyleBackColor = true;
            this.buttonEnregistrer.Click += new System.EventHandler(this.buttonEnregistrer_Click);
            // 
            // ButtonAnnuler
            // 
            this.ButtonAnnuler.Location = new System.Drawing.Point(141, 250);
            this.ButtonAnnuler.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ButtonAnnuler.Name = "ButtonAnnuler";
            this.ButtonAnnuler.Size = new System.Drawing.Size(84, 26);
            this.ButtonAnnuler.TabIndex = 23;
            this.ButtonAnnuler.Text = "Annuler";
            this.ButtonAnnuler.UseVisualStyleBackColor = true;
            this.ButtonAnnuler.Click += new System.EventHandler(this.ButtonAnnuler_Click);
            // 
            // cbGroupe
            // 
            this.cbGroupe.DisplayMember = "Text";
            this.cbGroupe.FormattingEnabled = true;
            this.cbGroupe.Location = new System.Drawing.Point(141, 165);
            this.cbGroupe.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbGroupe.Name = "cbGroupe";
            this.cbGroupe.Size = new System.Drawing.Size(158, 21);
            this.cbGroupe.TabIndex = 19;
            this.cbGroupe.ValueMember = "Value";
            // 
            // labelGroupe
            // 
            this.labelGroupe.AutoSize = true;
            this.labelGroupe.Location = new System.Drawing.Point(15, 168);
            this.labelGroupe.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelGroupe.Name = "labelGroupe";
            this.labelGroupe.Size = new System.Drawing.Size(67, 13);
            this.labelGroupe.TabIndex = 20;
            this.labelGroupe.Text = "Autorisations";
            // 
            // txtUserName
            // 
            this.txtUserName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserName.Location = new System.Drawing.Point(141, 28);
            this.txtUserName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.ReadOnly = true;
            this.txtUserName.Size = new System.Drawing.Size(157, 19);
            this.txtUserName.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 130);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Re-Mot de passe";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 106);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Nouveau Mot de passe";
            // 
            // buttonThis
            // 
            this.buttonThis.Location = new System.Drawing.Point(258, 75);
            this.buttonThis.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonThis.Name = "buttonThis";
            this.buttonThis.Size = new System.Drawing.Size(40, 19);
            this.buttonThis.TabIndex = 27;
            this.buttonThis.Text = "this";
            this.buttonThis.UseVisualStyleBackColor = true;
            this.buttonThis.Click += new System.EventHandler(this.buttonThis_Click);
            // 
            // txtWinUser
            // 
            this.txtWinUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWinUser.Location = new System.Drawing.Point(141, 75);
            this.txtWinUser.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtWinUser.Name = "txtWinUser";
            this.txtWinUser.Size = new System.Drawing.Size(115, 20);
            this.txtWinUser.TabIndex = 28;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 80);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 29;
            this.label1.Text = "Utilisateur Session:";
            // 
            // FrEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(323, 285);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtWinUser);
            this.Controls.Add(this.buttonThis);
            this.Controls.Add(this.chActive);
            this.Controls.Add(this.txtPassRe);
            this.Controls.Add(this.txtPass);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtNom);
            this.Controls.Add(this.buttonEnregistrer);
            this.Controls.Add(this.ButtonAnnuler);
            this.Controls.Add(this.cbGroupe);
            this.Controls.Add(this.labelGroupe);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FrEdit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Modifier un Utilisateur";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AdminUserEdit_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chActive;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonEnregistrer;
        private System.Windows.Forms.Button ButtonAnnuler;
        private System.Windows.Forms.ComboBox cbGroupe;
        private System.Windows.Forms.Label labelGroupe;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonThis;
        private System.Windows.Forms.Label label1;
        private TextBoxx txtPassRe;
        private TextBoxx txtPass;
        private TextBoxx txtNom;
        private TextBoxx txtUserName;
        private TextBoxx txtWinUser;
    }
}