﻿
namespace NsTRBsys.NsAdministration.NsUtilisateur
{
    partial class FrCreate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtUserName = new NsTRBsys.TextBoxx();
            this.labelGroupe = new System.Windows.Forms.Label();
            this.cbGroupe = new System.Windows.Forms.ComboBox();
            this.ButtonAnnuler = new System.Windows.Forms.Button();
            this.buttonEnregistrer = new System.Windows.Forms.Button();
            this.txtNom = new NsTRBsys.TextBoxx();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonNewMatricule = new System.Windows.Forms.Button();
            this.txtPass = new NsTRBsys.TextBoxx();
            this.txtPassRe = new NsTRBsys.TextBoxx();
            this.chActive = new System.Windows.Forms.CheckBox();
            this.txtWinUser = new NsTRBsys.TextBoxx();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonThis = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 104);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Mot de passe";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 128);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Re-Mot de passe";
            // 
            // txtUserName
            // 
            this.txtUserName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUserName.Location = new System.Drawing.Point(106, 20);
            this.txtUserName.Margin = new System.Windows.Forms.Padding(2);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(118, 20);
            this.txtUserName.TabIndex = 1;
            this.txtUserName.Leave += new System.EventHandler(this.txtUserName_Leave);
            // 
            // labelGroupe
            // 
            this.labelGroupe.AutoSize = true;
            this.labelGroupe.Location = new System.Drawing.Point(11, 166);
            this.labelGroupe.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelGroupe.Name = "labelGroupe";
            this.labelGroupe.Size = new System.Drawing.Size(67, 13);
            this.labelGroupe.TabIndex = 5;
            this.labelGroupe.Text = "Autorisations";
            // 
            // cbGroupe
            // 
            this.cbGroupe.DisplayMember = "Text";
            this.cbGroupe.FormattingEnabled = true;
            this.cbGroupe.Location = new System.Drawing.Point(106, 163);
            this.cbGroupe.Margin = new System.Windows.Forms.Padding(2);
            this.cbGroupe.Name = "cbGroupe";
            this.cbGroupe.Size = new System.Drawing.Size(163, 21);
            this.cbGroupe.TabIndex = 5;
            this.cbGroupe.ValueMember = "Value";
            // 
            // ButtonAnnuler
            // 
            this.ButtonAnnuler.Location = new System.Drawing.Point(99, 233);
            this.ButtonAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.ButtonAnnuler.Name = "ButtonAnnuler";
            this.ButtonAnnuler.Size = new System.Drawing.Size(77, 26);
            this.ButtonAnnuler.TabIndex = 7;
            this.ButtonAnnuler.Text = "Annuler";
            this.ButtonAnnuler.UseVisualStyleBackColor = true;
            this.ButtonAnnuler.Click += new System.EventHandler(this.ButtonAnnuler_Click);
            // 
            // buttonEnregistrer
            // 
            this.buttonEnregistrer.Location = new System.Drawing.Point(182, 233);
            this.buttonEnregistrer.Margin = new System.Windows.Forms.Padding(2);
            this.buttonEnregistrer.Name = "buttonEnregistrer";
            this.buttonEnregistrer.Size = new System.Drawing.Size(87, 26);
            this.buttonEnregistrer.TabIndex = 6;
            this.buttonEnregistrer.Text = "Enregistrer";
            this.buttonEnregistrer.UseVisualStyleBackColor = true;
            this.buttonEnregistrer.Click += new System.EventHandler(this.buttonEnregistrer_Click);
            // 
            // txtNom
            // 
            this.txtNom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNom.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNom.Location = new System.Drawing.Point(106, 76);
            this.txtNom.Margin = new System.Windows.Forms.Padding(2);
            this.txtNom.Name = "txtNom";
            this.txtNom.ReadOnly = true;
            this.txtNom.Size = new System.Drawing.Size(162, 19);
            this.txtNom.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 79);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Nom / Prenom";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 20);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Matricule";
            // 
            // buttonNewMatricule
            // 
            this.buttonNewMatricule.Location = new System.Drawing.Point(228, 20);
            this.buttonNewMatricule.Margin = new System.Windows.Forms.Padding(2);
            this.buttonNewMatricule.Name = "buttonNewMatricule";
            this.buttonNewMatricule.Size = new System.Drawing.Size(40, 20);
            this.buttonNewMatricule.TabIndex = 2;
            this.buttonNewMatricule.Text = "...";
            this.buttonNewMatricule.UseVisualStyleBackColor = true;
            this.buttonNewMatricule.Click += new System.EventHandler(this.buttonParcourirMatricule_Click);
            // 
            // txtPass
            // 
            this.txtPass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPass.Location = new System.Drawing.Point(106, 101);
            this.txtPass.Margin = new System.Windows.Forms.Padding(2);
            this.txtPass.Name = "txtPass";
            this.txtPass.PasswordChar = '#';
            this.txtPass.Size = new System.Drawing.Size(162, 20);
            this.txtPass.TabIndex = 3;
            this.txtPass.UseSystemPasswordChar = true;
            // 
            // txtPassRe
            // 
            this.txtPassRe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPassRe.Location = new System.Drawing.Point(106, 125);
            this.txtPassRe.Margin = new System.Windows.Forms.Padding(2);
            this.txtPassRe.Name = "txtPassRe";
            this.txtPassRe.Size = new System.Drawing.Size(162, 20);
            this.txtPassRe.TabIndex = 4;
            this.txtPassRe.UseSystemPasswordChar = true;
            // 
            // chActive
            // 
            this.chActive.AutoSize = true;
            this.chActive.Checked = true;
            this.chActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chActive.Location = new System.Drawing.Point(14, 209);
            this.chActive.Margin = new System.Windows.Forms.Padding(2);
            this.chActive.Name = "chActive";
            this.chActive.Size = new System.Drawing.Size(126, 17);
            this.chActive.TabIndex = 12;
            this.chActive.Text = "Le compte est Activé";
            this.chActive.UseVisualStyleBackColor = true;
            // 
            // txtWinUser
            // 
            this.txtWinUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWinUser.Location = new System.Drawing.Point(106, 46);
            this.txtWinUser.Margin = new System.Windows.Forms.Padding(2);
            this.txtWinUser.Name = "txtWinUser";
            this.txtWinUser.Size = new System.Drawing.Size(118, 20);
            this.txtWinUser.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 46);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Utilisateur Session:";
            // 
            // buttonThis
            // 
            this.buttonThis.Location = new System.Drawing.Point(228, 45);
            this.buttonThis.Margin = new System.Windows.Forms.Padding(2);
            this.buttonThis.Name = "buttonThis";
            this.buttonThis.Size = new System.Drawing.Size(40, 19);
            this.buttonThis.TabIndex = 15;
            this.buttonThis.Text = "this";
            this.buttonThis.UseVisualStyleBackColor = true;
            this.buttonThis.Click += new System.EventHandler(this.buttonThis_Click);
            // 
            // FrCreate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(278, 270);
            this.Controls.Add(this.buttonThis);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtWinUser);
            this.Controls.Add(this.chActive);
            this.Controls.Add(this.txtPassRe);
            this.Controls.Add(this.txtPass);
            this.Controls.Add(this.buttonNewMatricule);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtNom);
            this.Controls.Add(this.buttonEnregistrer);
            this.Controls.Add(this.ButtonAnnuler);
            this.Controls.Add(this.cbGroupe);
            this.Controls.Add(this.labelGroupe);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrCreate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nouvel Utilisateur";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AdminUserNew_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelGroupe;
        private System.Windows.Forms.ComboBox cbGroupe;
        private System.Windows.Forms.Button ButtonAnnuler;
        private System.Windows.Forms.Button buttonEnregistrer;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button buttonNewMatricule;
        private System.Windows.Forms.CheckBox chActive;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonThis;
        private TextBoxx txtUserName;
        private TextBoxx txtNom;
        private TextBoxx txtPass;
        private TextBoxx txtPassRe;
        private TextBoxx txtWinUser;
       // private Noogen.Validation.ValidationProvider validationProvider1;
    }
}