﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SIL.FieldWorks.Common.Controls;

namespace NsTRBsys.NsAdministration.NsGroupe
{
    public partial class FrCreate : Form
    {
        ModelEntities db = new ModelEntities();

        public FrCreate()
        {
            InitializeComponent();
            InitializeData();
        }

        private void InitializeData()
        {
            treeView1.Nodes.Clear();
            // treeView1.Nodes.Add("Modules");
            var Actions = db.ApplicationActions.OrderBy(u => u.Nom).ToList();
            foreach (var rootNode in Actions.Where(r =>
                r.ParentActionID == null && r.IsPublic != true).OrderBy(a => a.Position))
            {
                treeView1.Nodes.Add(rootNode.ID.ToString(), rootNode.Text).ToolTipText = rootNode.Description;

                foreach (var childNode in Actions.Where(r => r.ParentActionID == rootNode.ID).OrderBy(a => a.Position))
                {
                    treeView1.Nodes[rootNode.ID.ToString()].Nodes.Add(childNode.ID.ToString(), childNode.Text).ToolTipText = childNode.Description;

                }
            }
            //   treeView1.Nodes[0].Expand();

        }

        private void buttonAnnuler_Click(object sender, EventArgs e)
        {
            Dispose();
        }
        private void AdminGroupeCreate_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }
        private void buttonEnregistre_Click(object sender, EventArgs e)
        {
            if (IsvalidAll())
            {

                var Groupe = new ApplicationGroupe()
                {
                    Nom = txt1.Text,

                };

                db.AddToApplicationGroupes(Groupe);
                db.SaveChanges();

                /* autorisations for the new Groupe
                 * seulement les actions qui sont coché (ou GrayCoché) sont insrit dans la table avec autorisation = 1 
                 
                 */
                foreach (TreeNode rootNodes in treeView1.Nodes)
                {
                    // notde.Name is ID
                    var id = int.Parse(rootNodes.Name);
                    bool autorisation = (treeView1.GetChecked(rootNodes) != TriStateTreeView.CheckState.Unchecked); // GrayCheked is cheaked
                    db.AddToApplicationAutorisations(new ApplicationAutorisation() { GroupeID = Groupe.ID, ApplicationActionID = id, Autorisation = autorisation });
                    if (autorisation) // if node not cheaked refuse all
                    {
                        foreach (TreeNode ChildNode in rootNodes.Nodes)
                        {
                            id = int.Parse(ChildNode.Name);
                            autorisation = (treeView1.GetChecked(ChildNode) == TriStateTreeView.CheckState.Checked);
                            if (autorisation) // save only qui sont pas refusés. (les autres sont automatiquement autorisés)
                                db.AddToApplicationAutorisations(new ApplicationAutorisation() { GroupeID = Groupe.ID, ApplicationActionID = id, Autorisation = autorisation });
                        }
                    }


                }

                db.SaveChanges();
                Dispose();
            }
        }
        private bool IsvalidAll()
        {
            string nom = txt1.Text;
            string ErrorMessage = String.Empty;
            if (nom.Length < 2 || nom.Length > 30)
            {
                ErrorMessage += "\nCe Nom de Groupe est invalide! (doit faire entre 2 et 30 caractères).";

            }

            if (db.ApplicationGroupes.SingleOrDefault(g => g.Nom == nom) != null)
            {
                ErrorMessage += "\nCe Nom de Groupe existe déja.";
            }

            if (ErrorMessage != String.Empty)
            {
                Tools.ShowError(ErrorMessage);
                return false;
            }

            return true;
        }


    }
}
