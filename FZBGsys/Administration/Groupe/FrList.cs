﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Objects;

namespace NsTRBsys.NsAdministration.NsGroupe
{
    public partial class FrList : Form
    {
        ModelEntities db = new ModelEntities();

        public FrList()
        {
            InitializeComponent();
            InitialiseData();
           
        }

        private void InitialiseData()
        {
            dgv1.DataSource = db.ApplicationGroupes.Where(g=>g.ID >0).OrderBy(u => u.Nom).Select(g => new { ID = g.ID, Groupe = g.Nom, Utilisateurs = g.ApplicationUtilisateurs.Count }).ToList();
        }

        private void buttonNew_Click(object sender, EventArgs e)
        {
            new FrCreate().ShowDialog();
            InitialiseData();
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            var selectedID = int.Parse(dgv1.SelectedRows[0].Cells[0].Value.ToString());
            var selected = db.ApplicationGroupes.Single(g => g.ID == selectedID);

            db.Refresh(RefreshMode.StoreWins, selected);
            new FrEdit(selected).ShowDialog();
            InitialiseData();

        }

        private void buttonDel_Click(object sender, EventArgs e)
        {
            var selectedID = int.Parse(dgv1.SelectedRows[0].Cells[0].Value.ToString());
            var selected = db.ApplicationGroupes.Single(g => g.ID == selectedID);

            if (this.ConfirmWarning("Supprimer le Groupe '" + selected.Nom + "' ?"))
            {
                var autorisations  =  db.ApplicationAutorisations.Where(a=>a.ApplicationGroupe.ID == selected.ID).ToList();
                foreach (var item in autorisations)
                {
                    db.DeleteObject(item);
                }
               // db.DeleteObject();
                db.SaveChanges();
                db.DeleteObject(selected);
                db.SaveChanges();
                InitialiseData();
            }
        }

        private void AdminGroupe_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }
    }
}
