﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FZBGsys.Rapports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;


using System.IO;
using System.Diagnostics;


namespace FZBGsys.NsRapports
{
    public partial class FrViewer : Form
    {
        public FrViewer(bool tree = false)
        {
            InitializeComponent();
            if (tree)
            {
                crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.GroupTree;
            }
        }


        public static bool ExportPDF = false;
        public string extention { get; set; }
        public static string ExportFileName { get; set; }

        #region repport methods

        //--------------------------------------------------------Bon D'atribution
        internal static void PrintBonAttribution(int BonID)
        {
            CR_BonAttribution report = new CR_BonAttribution();
            report.SetParameterValue("@ID", BonID);
            PrintReport(report);

        }
        //---------------------------------------------------------- Stock Produit Fini
        internal static void PrintStockProduitFini()
        {
            CR_StockProduitFini rapport = new CR_StockProduitFini();
            PrintReport(rapport);

        }
        //---------------------------------------------------------- Liste arrivage
        internal static void PrintListArrivage(string FilterTxt)
        {
            CR_ListArrivage rapport = new CR_ListArrivage();
            rapport.SetParameterValue("FilterTxt", FilterTxt);
            rapport.SetParameterValue("@UtilisateurID", Tools.CurrentUserID);
            PrintReport(rapport);

        }
        //---------------------------------------------------------- List Stock MT
        internal static void PrintListStockMT(string FilterTxt)
        {
            CR_ListStockMT rapport = new CR_ListStockMT();
            rapport.SetParameterValue("FilterTxt", FilterTxt);
            rapport.SetParameterValue("@UtilisateurID", Tools.CurrentUserID);
            PrintReport(rapport);
        }

        internal static void PrintListTransfert(string FilterTxt, EnSection DestinationTxt)
        {
            CR_ListTransfert rapport = new CR_ListTransfert();
            rapport.SetParameterValue("FilterTxt", FilterTxt);
            rapport.SetParameterValue("DestinationTxt", DestinationTxt.ToSectionTxt());
            rapport.SetParameterValue("@UtilisateurID", Tools.CurrentUserID);
            PrintReport(rapport);
        }
        //---------------------------------------------------------- List Production 
        internal static void PrintListProduction(string FilterTxt)
        {
            CR_ListProduction rapport = new CR_ListProduction();
            rapport.SetParameterValue("FilterTxt", FilterTxt);
            rapport.SetParameterValue("@UtilisateurID", Tools.CurrentUserID);
            PrintReport(rapport);
        }


        //------------------------------------ Souflage

        internal static void PrintSoufflage(int productionID)
        {
            FZBGsys.Rapports.PET.CR_Soufflage rapport = new FZBGsys.Rapports.PET.CR_Soufflage();
            rapport.SetParameterValue("@ProductionID", productionID);
            PrintReport(rapport);
        }

        //------------------------------ bouchon
        internal static void PrintBouchon(int productionID)
        {
            FZBGsys.Rapports.PET.CR_Bouchon rapport = new FZBGsys.Rapports.PET.CR_Bouchon();
            rapport.SetParameterValue("@ProductionID", productionID);
            PrintReport(rapport);
        }


        //------------------------------ Etiquette
        internal static void PrintEtiquette(int productionID)
        {
            FZBGsys.Rapports.PET.CR_Etiquette rapport = new FZBGsys.Rapports.PET.CR_Etiquette();
            rapport.SetParameterValue("@ProductionID", productionID);
            PrintReport(rapport);
        }

        //------------------------------ Siroperie
        internal static void PrintSiropherie(int productionID)
        {
            FZBGsys.Rapports.PET.CR_Siroperie rapport = new FZBGsys.Rapports.PET.CR_Siroperie();
            rapport.SetParameterValue("@ProductionID", productionID);
            PrintReport(rapport);
        }

        //------------------------------ FilmThermo
        internal static void PrintFilmThermo(int productionID)
        {
            FZBGsys.Rapports.PET.CR_FilmThermo rapport = new FZBGsys.Rapports.PET.CR_FilmThermo();
            rapport.SetParameterValue("@ProductionID", productionID);
            PrintReport(rapport);
        }

        //---------------------------- Suivi jouranlier production
        internal static void PrintSuiviProd()
        {
            FZBGsys.Rapports.PET.CR_SuiviJournalierProduction rapport = new FZBGsys.Rapports.PET.CR_SuiviJournalierProduction();

            rapport.SetParameterValue("@UtilisateurID", Tools.CurrentUserID);
            PrintReport(rapport);
        }


        //--------------------------- Rapport production
        internal static void PrintRapportProd(int productionID)
        {
            FZBGsys.Rapports.PET.CR_RapoortJournalierProduction rapport = new FZBGsys.Rapports.PET.CR_RapoortJournalierProduction();
            rapport.SetParameterValue("@ProductionID", productionID);
            PrintReport(rapport);
        }






        #endregion
        #region output methods
        public static string ExportToPDF(ReportClass report)
        {

            try
            {
                // System.IO.FileInfo fn =  new System.IO.FileInfo(report.FileName);
                //string fileName = System.IO.Path.GetTempPath() + report.GetClassName().Replace("Actions.Rapports.", "") + ".pdf";
                string fileName = System.IO.Path.GetTempPath() + ExportFileName + ".pdf";
                ExportOptions CrExportOptions;
                DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();

                CrDiskFileDestinationOptions.DiskFileName = fileName;
                CrExportOptions = report.ExportOptions;

                {
                    CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;

                    CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                    CrExportOptions.FormatOptions = CrFormatTypeOptions;
                }
                report.Export();
                return CrDiskFileDestinationOptions.DiskFileName;
            }
            catch (Exception ex)
            {
                return null;
                //MessageBox.Show(ex.ToString());
            }

        }
        public static string ExportToPDF(ReportDocument report)
        {

            try
            {
                // System.IO.FileInfo fn =  new System.IO.FileInfo(report.FileName);
                //string fileName = System.IO.Path.GetTempPath() + report.GetClassName().Replace("Actions.Rapports.", "") + sufix + ".pdf";
                string fileName = System.IO.Path.GetTempPath() + ExportFileName + ".pdf";
                ExportOptions CrExportOptions;
                DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                CrDiskFileDestinationOptions.DiskFileName = fileName;
                CrExportOptions = report.ExportOptions;
                {
                    CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                    CrExportOptions.FormatOptions = CrFormatTypeOptions;
                }
                report.Export();
                return CrDiskFileDestinationOptions.DiskFileName;
            }
            catch (Exception ex)
            {
                return null;
                //MessageBox.Show(ex.ToString());
            }

        }
        private static void PrintReport(ReportClass report, bool tree = false)
        {
            //report.Refresh();
            TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
            TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
            ConnectionInfo crConnectionInfo = new ConnectionInfo();
            Tables CrTables;
            //  FrViewer.ExportFileName = null;
            string utilisateur = null;
            using (ModelEntities db = new ModelEntities())
            {
                var section =
                utilisateur = db.ApplicationUtilisateurs.Single(u => u.ID == Tools.CurrentUserID).Employer.Section.Employer.Nom;

                crConnectionInfo.ServerName = @"localhost\SQLHOME";
                crConnectionInfo.DatabaseName = "FZBGdb";
                crConnectionInfo.UserID = "sa";
                crConnectionInfo.Password = "neogeo=NEOGEO190584";
                //  crConnectionInfo.IntegratedSecurity = true;
            }



            CrTables = report.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
            {
                crtableLogoninfo = CrTable.LogOnInfo;
                crtableLogoninfo.ConnectionInfo = crConnectionInfo;
                CrTable.ApplyLogOnInfo(crtableLogoninfo);
            }

            try
            {
                report.SetParameterValue("UtilisateurNom", utilisateur);
            }
            catch (Exception)
            {


            }

            var form = new FrViewer(false);
            //ExportPDF = false;
            form.crystalReportViewer1.ReportSource = report;
            if (!ExportPDF)
            {
                form.ShowDialog();
            }
            else
            {
                FrViewer.ExportFileName = ExportToPDF(report);
            }

            report.Dispose();
        }
        private static void PrintReport(ReportDocument report)
        {
            //report.Refresh();
            TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
            TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
            ConnectionInfo crConnectionInfo = new ConnectionInfo();
            Tables CrTables;

            string utilisateur = null;
            using (ModelEntities db = new ModelEntities())
            {
                utilisateur = db.ApplicationUtilisateurs.Single(u => u.ID == Tools.CurrentUserID).Employer.Nom;

                crConnectionInfo.ServerName = @"localhost\SQLHOME";
                crConnectionInfo.DatabaseName = "MarBlida";
                crConnectionInfo.UserID = "sa";
                crConnectionInfo.Password = "neogeo=NEOGEO190584";
                //crConnectionInfo.IntegratedSecurity = true;
            }



            CrTables = report.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
            {
                crtableLogoninfo = CrTable.LogOnInfo;
                crtableLogoninfo.ConnectionInfo = crConnectionInfo;
                CrTable.ApplyLogOnInfo(crtableLogoninfo);
            }

            report.SetParameterValue("UtilisateurNom", utilisateur);
            var form = new FrViewer();
            //  report.VerifyDatabase();

            form.crystalReportViewer1.ReportSource = report;
            form.crystalReportViewer1.ReportSource = report;
            if (!ExportPDF)
            {
                form.ShowDialog();
            }
            else
            {
                FrViewer.ExportFileName = ExportToPDF(report);
            }
        }


        #endregion




    }
}
